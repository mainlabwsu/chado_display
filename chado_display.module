<?php
use Symfony\Component\HttpFoundation\RedirectResponse;

use Drupal\Core\Url;

use Drupal\chado_display\Render\Cache;

function chado_display_theme ($existing, $type, $theme, $path) {
  $host = \Drupal::request()->getHost();
  return [
    'chado_page' => [
      'variables' => [
        'category' => NULL,
        'table_of_content' => NULL,
        'alt_header' => NULL,
        'content' => NULL,
        'base_url' => $host,
        'class_name' => NULL
      ]
    ],
    'chado_page_right_sidebar' => [
      'variables' => [
        'category' => NULL,
        'table_of_content' => NULL,
        'alt_header' => NULL,
        'content' => NULL,
        'base_url' => $host,
        'class_name' => NULL
      ]
    ],    
    'chado_page_expandable_boxes' => [
      'variables' => [
        'category' => NULL,
        'table_of_content' => NULL,
        'alt_header' => NULL,
        'content' => NULL,
        'base_url' => $host,
        'class_name' => NULL
      ]
    ],
    'chado_page_nosidebar' => [
      'variables' => [
        'category' => NULL,
        'table_of_content' => NULL,
        'alt_header' => NULL,
        'content' => NULL,
        'base_url' => $host,
        'class_name' => NULL
      ]
    ]
  ];
}

function chado_display_get_path () {
  return '/display/record';
}

/**
 * Return all supporting Chado base_table
 * Any Chado table added here will be supported
 */
function chado_display_get_supported_data_tyep() {
  $data = [
    'analysis' => 'Analysis',
    'contact' => 'Contact',
    'eimage' => 'Eimage',
    'feature' => 'Feature',
    'library' => 'Library',
    'featuremap' => 'Map',
    'nd_geolocation' => 'ND Geolocation',
    'organism' => 'Organism',
    'project' => 'Project',
    'pub' => 'Publication',
    'stock' => 'Stock',
    'stockcollection' => 'Stock Collection',
  ];
  return $data;
}

function chado_display_get_extended_data_type() {
  $data = chado_display_get_supported_data_tyep();
  $extended = \Drupal::state()->get('chado_display_extended_data', '');
  if ($extended) {
    $tables = explode(',', $extended);
    foreach ($tables AS $table) {
      $table = trim($table);
      if ($table) {
        $data[$table] = $table;
      }
    }
  }
  return $data;
}

/**
 * Return all rendering methods of a Renderer class
 * The rendering methods must have name like 'renderXXX'
 */
function chado_display_render_info($class) {
  $methods = get_class_methods($class);
  $render_info = [];
  foreach ($methods AS $method) {
    $renderable = preg_match('/render(.+)/', $method, $matches);
    if ($renderable) {
      $render_info [$matches[1]] = $matches[1];
    }
  }
  return $render_info;
}

/**
 * Redirect if $table is a supported data type
 * @param unknown $table
 */
function chado_display_redirect($table, $id, $clear_cache = FALSE) {
  $supported = chado_display_get_extended_data_type();
  if (key_exists($table, $supported)) {
    $url = Url::fromRoute('chado_display.record', ['base_table' => $table, 'id' => $id]);
    if ($clear_cache) {
      Cache::delete($table, $id);
    }
    chado_display_goto($url->toString());
  }
}

function chado_display_goto($url) {
  $response = new RedirectResponse($url);
  $response->send();
}