/*
 Show only the first section
*/
jQuery('document').ready(function(){
  var anchor = location.hash;
  var hash = window.location.hash;
  jQuery('.chado_page-section').hide();
  jQuery('.chado_page-content').hide();
  if (hash) {
    var current_section =  hash.replace('content', 'toc');
     jQuery(current_section).css('font-weight', 'bold');
    jQuery(hash).prev().show();
    jQuery(hash).show();
  }
  else {
    jQuery('.chado_page-section').first().show();
    jQuery('.chado_page-content').first().show();
  }
});

/**
 * Show corresponding content when TOC is clicked.
 */
 jQuery('.chado_page-toc').click(function() {
  jQuery('.chado_page-toc').each(function() {
    jQuery(this).css('font-weight', 'normal');
  });
  jQuery(this).css('font-weight', 'bold');
  var target = '#' + this.title;
  if (jQuery(target).length) {
    jQuery('.chado_page-section').hide();
    jQuery('.chado_page-content').hide();
    var content = target.replace('section', 'content');
    jQuery(target).fadeIn(500);
    jQuery(content).fadeIn(500);
    jQuery('#chado_page-content-jbrowse iframe').attr('src', jQuery('#chado_page-content-jbrowse iframe').attr('src'));
  }
});

jQuery('ul.js-pager__items li a').each(function() {
  pager_link = jQuery(this);
  parent = pager_link.parents('div.chado_page-content');
  pager_link.attr('href', pager_link.attr('href') + '#' + parent.attr('id'));
});