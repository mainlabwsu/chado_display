/**
 * Scroll to top when Document is ready
 */
jQuery('document').ready(function(){
  var anchor = location.hash;
  if (anchor != '') {
     var current_section =  jQuery(anchor).parent().attr('id').replace('content', 'toc');
     jQuery('#' + current_section).css('font-style', 'italic');
     jQuery('#' + current_section.replace('toc', 'section')).css('font-style', 'italic');
  }
  jQuery('.chado_page-toc').css('opacity', '0.6');
  jQuery('.chado_page-section').css('opacity', '0.6');
  jQuery('.chado_page-toc:first').css('font-style', 'italic');
  jQuery('.chado_page-toc:first').css('opacity', '1');
  jQuery('.chado_page-section:first').css('font-style', 'italic');
  jQuery('.chado_page-section:first').css('opacity', '1');
});

/**
 *  Scrolling to corresponding content for TOC items
 */
jQuery('.chado_page-toc').click(function() {
  var adminbaroffset = typeof jQuery('#toolbar-administration').scrollTop() !== 'undefined' ? 100 : 20;
  // Scroll the page
  var target = '#' + this.title;
  if(jQuery(target).length) {
    jQuery('html, body').animate({
      scrollTop: (jQuery(target).offset().top - adminbaroffset)
    }, 500);
  }
  // Make sure clicked item has the highl
  jQuery('html, body').promise().done(function(){
    jQuery('.chado_page-toc').each(function() {
      jQuery(this).css('font-style', 'normal');
      jQuery(this).css('opacity', '0.6');
    });
    jQuery('.chado_page-section').each(function() {
      jQuery(this).css('font-style', 'normal');
      jQuery(this).css('opacity', '0.6');
    });
    jQuery(target).css('font-style', 'italic');
    jQuery(target).css('opacity', '1');
    jQuery(target.replace('section', 'toc')).css('font-style', 'italic');
    jQuery(target.replace('section', 'toc')).css('opacity', '1');
  });
});

jQuery('ul.js-pager__items li a').each(function() {
  pager_link = jQuery(this);
  parent = pager_link.parents('div.chado_display_table_wrapper');
  pager_link.attr('href', pager_link.attr('href') + '#' + parent.attr('id'));
});


jQuery(window).on('scroll', function () {
  //alert(jQuery('.chado_page-section:first').position().scrollTop);
  var currentPos = jQuery(document).scrollTop() - 100;
  jQuery('.chado_page-section').each(function () {
    var top = jQuery(this).position().top;
    if (top > currentPos) {
      // Update Section
      jQuery('.chado_page-section').css('font-style', 'normal');
      jQuery('.chado_page-section').css('opacity', '0.6');
      jQuery(this).css('font-style', 'italic');
      jQuery(this).css('opacity', '1');
      // Update TOC
      var id = '#' + jQuery(this).attr('id').replace('section', 'toc');
      jQuery('.chado_page-toc').css('font-style', 'normal');
      jQuery('.chado_page-toc').css('opacity', '0.6');
      jQuery(id).css('font-style', 'italic');
      jQuery(id).css('opacity', '1');
      return false;
    }
  });
});
