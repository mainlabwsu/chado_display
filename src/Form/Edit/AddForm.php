<?php 
namespace Drupal\chado_display\Form\Edit;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\chado_display\Database\Chado;

class AddForm extends FormBase {
  
  protected $chado;
  protected $base_table;
  protected $pkey;
  protected $columns;
  protected $redirect_url;
  
  public function getFormId() {
    return 'chado_display_add_form';
  }
  
  public function title($base_table = NULL, $id = NULL) {
    $c = new Chado();
    if ($c->tableExists($base_table)) {
        return 'Add Chado ' . $base_table;
    }
    else {
      return 'Page not found';
    }
  }
  
  public function buildForm(array $form, FormStateInterface $form_state, $base_table = NULL, $id = NULL, $redirect_url = NULL) {
    $form = array();
    
    $this->base_table = $base_table;
    $this->redirect_url = $redirect_url;
    
    $c = new Chado();
    $this->chado = $c;
    if ($c->tableExists($base_table)) {
      $pk = $c->getPkey($base_table);
      $this->pkey = $pk;
      $columns = $c->getTypedColumns($base_table);
      unset($columns[$pk]); // exclude Pkey from the columns
      $this->columns = $columns;
      $notnull = $c->getNotNullColumns($base_table);
      $colDefault = $c->getColumnDefault($base_table);
      $fkeys = $c->getFkeyColumns($base_table);
      
      foreach ($columns AS $key => $ctype) {
        $type = 'textfield';
        $format = '';
        if ($ctype == 'boolean') {
          $type = 'checkbox';
        }
        else if (preg_match('/^timestamp /', $ctype)) {
          $type = 'date';
        }
        else if ($key == 'residues') {
          $type = 'textarea';
        }
        else if ($ctype == 'text' && !in_array($key, ['uniquename', 'title', 'volumetitle', 'sourceuri'])) {
          $type = 'text_format';
        }        
        $form[$base_table][$key] = [
          '#title' => $key,
          '#type' => $type,
          '#format' => 'plain_text'
        ];
        if ($type == 'textfield') {
          $form[$base_table][$key]['#maxlength'] = 2048;
        }
        // Add required for NOT NULL fields that do not have a default value
        if (in_array($key, $notnull) && !isset($colDefault[$key])) {
          $form[$base_table][$key]['#required'] = TRUE;
        }
        // Add autocomplete for FKey fields
        if (key_exists($key, $fkeys)) {
          $fks = $fkeys[$key];
          $ftable = '';
          $fcol = '';
          foreach ($fks AS $ftable => $fcol) {// Get the last foreign key
          }
          if ($ftable && $fcol) {
            $form[$base_table][$key]['#autocomplete_route_name'] = 'chado_display.autocomplete';
            $form[$base_table][$key]['#autocomplete_route_parameters'] = ['table' => $ftable];
            if ($base_table == 'feature') {
              $form[$base_table][$key]['#autocomplete_route_parameters']['ontology'] = 'sequence';
            }
          }
        }
        // If this is a property table, use passed in $id as base_id
        if (is_numeric($id) && preg_match('/prop$/', $base_table)) {
          $table = str_replace('prop', '', $base_table);
          $exists = $c->getFieldById($table, $table . '_id', $id);
          if(is_numeric($exists) && $key == $table . '_id') {
            $form[$base_table][$key]['#default_value'] = $id;
            $form[$base_table][$key]['#disabled'] = TRUE;
          }
        }
      }
      
      // Allow setting URL Alias for supported display type
      $supported = chado_display_get_extended_data_type();
      if (key_exists($base_table, $supported)) {
        // Setting URL
        $form[$base_table]['url_alias'] = [
          '#type' => 'textfield',
          '#title' => 'URL Alias',
        ];
        
        // Uploading one associated image
        $form[$base_table]['image_container'] = [
          '#type' => 'details',
          '#title' => 'Overview Image',
          '#description' => 'Show an image in the Overview section',
          '#open' => TRUE
        ];
        $form[$base_table]['image_container']['image'] = [
          '#type' => 'file',
        ];
      }
      
      $form[$base_table]['submit'] = [
        '#type' => 'submit',
        '#value' => 'Save'
      ];

    }
    else {
      $form['no_data'] = [
        '#markup' => 'The requested page could not be found.'
      ];
    }

    return $form;
  }
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    // Check numeric for integer
    foreach ($this->columns AS $col => $type) {
      if (($type == 'integer' || $type == 'real')  && $values[$col] && !is_numeric($values[$col])) {
        $form_state->setErrorByName($col, 'Please input a number.');
      }
    }
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $returning = $this->pkey;
    $values = $form_state->getValues();
    $vals = [];
    foreach ($this->columns AS $col => $type) {
      // Ignore values that are not on the form
      if (!key_exists($col, $values)) {
        continue;
      }
      if (is_array($values[$col])) {
        $vals[$col] = $values[$col]['value'];
      }
      else {
        // Ignore empty numeric data        
        if (trim($values[$col]) == '' && in_array(explode(' ', $type)[0], ['bigint', 'bigserial', 'double precision', 'integer', 'numeric', 'real', 'smallint', 'smallserial', 'serial', 'time', 'timestamp', 'date', 'interval'])) {
          continue;
        }
        else {
          $vals[$col] = $values[$col];
        }
      }
    }
    try {
      $id = $this->chado->setField($this->base_table, $vals, $returning, TRUE);
      
      // Save URL Alias
      if (isset($values['url_alias']) && $values['url_alias']) {
        $path = chado_display_get_path() . '/' . $this->base_table . '/' . $id;
        $url_alias = preg_match('/^\//', $values['url_alias']) ? $values['url_alias'] : '/' . $values['url_alias'];
        // Remove redundant aliases
        \Drupal::database()->query('DELETE FROM path_alias WHERE path = :path', [':path' => $path]);
        $path_alias = \Drupal\path_alias\Entity\PathAlias::create([
          'path' => $path,
          'alias' => $url_alias,
        ]);
        $path_alias->save();
      }
      
      // Save image
      $all_files = $this->getRequest()->files->get('files', []);
      if (isset($all_files['image'])) {
        $path = chado_display_get_path() . '/' . $this->base_table . '/' . $id;
        $image = file_get_contents($all_files['image']->getRealPath());
        $data = base64_encode($image);
        $this->chado->getFirstField('eimage', 'eimage_id', ['image_uri' => $path, 'eimage_data' => $data, 'eimage_type' => $all_files['image']->getClientOriginalName()], [], TRUE);
      }
      // Show a message
      \Drupal::messenger()->addMessage($this->base_table . ' record added to Chado.');
      
      // Redirect
      // If redirect_url specified
      if ($this->redirect_url) {
        chado_display_goto(str_replace('::', '/', $this->redirect_url));
      }
      // If redirect_url not specified, try to redirect to the Chado Display page or stay on the form
      else {
        chado_display_redirect($this->base_table, $id);
      }
    } catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
      return;
    }
  }
}