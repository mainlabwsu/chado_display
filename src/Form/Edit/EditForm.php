<?php 
namespace Drupal\chado_display\Form\Edit;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;

use Drupal\chado_display\Database\Chado;
use Drupal\chado_display\Render\Cache;

class EditForm extends FormBase {
  
  protected $chado;
  protected $base_table;
  protected $id;
  protected $pkey;
  protected $columns;
  protected $redirect_url;
  
  public function getFormId() {
    return 'chado_display_edit_form';
  }
  
  public function title($base_table = NULL, $id = NULL) {
    $c = new Chado();
    if ($c->tableExists($base_table) && is_numeric($id)) {
      $obj = $c->getObjectById($base_table, ['*'], $id);
      if ($obj) {
        return 'Edit Chado ' . $base_table;
      }
    }
    return 'Page not found';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state, $base_table = NULL, $id = NULL, $redirect_url = NULL) {
    $form = array();
    
    $this->base_table = $base_table;
    $this->id = $id;
    $this->redirect_url = $redirect_url;
    
    $c = new Chado();
    $this->chado = $c;
    if ($c->tableExists($base_table) && is_numeric($id)) {
      $pk = $c->getPkey($base_table);
      $this->pkey = $pk;
      $columns = $c->getTypedColumns($base_table);
      unset($columns[$pk]); // exclude Pkey from the columns
      $this->columns = $columns;
      $notnull = $c->getNotNullColumns($base_table);
      $colDefault = $c->getColumnDefault($base_table);
      $fkeys = $c->getFkeyColumns($base_table);
      
      $obj = $c->getObjectById($base_table, ['*'], $id);
      if($pk && $obj) {
        foreach ($obj AS $key => $val) {
          if ($key != $pk) {
            // Skip the big text value
            if (!empty($val) && strlen($val) > 50000) {
              $form[$base_table][$key] = [
                '#markup' => $key . ' data is too big to edit'
              ];
              continue;
            }
            // Ignore Pkey, do not allow user to change it
            $type = 'textfield';
            $format = '';
            if ($columns[$key] == 'boolean') {
              $type = 'checkbox';
            }
            else if (preg_match('/^timestamp /', $columns[$key])) {
              $type = 'date';
              $datetime = explode(' ', $val);
              $val = $datetime[0];
            }
            else if ($key == 'residues') {
              $type = 'textarea';
            }
            else if ($columns[$key] == 'text' && !in_array($key, ['uniquename', 'title', 'volumetitle', 'sourceuri'])) {
              $type = 'text_format';
            }
            $form[$base_table][$key] = [
              '#title' => $key,
              '#type' => $type,
              '#default_value' => $val,
              '#format' => 'plain_text'
            ];
            if ($type == 'textfield') {
              $form[$base_table][$key]['#maxlength'] = 2048;
            }
            // Add autocomplete for FKey fields
            if (in_array($key, $notnull) && !isset($colDefault[$key])) {
              $form[$base_table][$key]['#required'] = TRUE;
            }
            if (key_exists($key, $fkeys)) {
              $fks = $fkeys[$key];
              $ftable = '';
              $fcol = '';
              foreach ($fks AS $ftable => $fcol) {// Get the last foreign key
              }
              if ($ftable && $fcol) {
                $form[$base_table][$key]['#autocomplete_route_name'] = 'chado_display.autocomplete';
                $form[$base_table][$key]['#autocomplete_route_parameters'] = ['table' => $ftable];
                if ($base_table == 'feature') {
                  $form[$base_table][$key]['#autocomplete_route_parameters']['ontology'] = 'sequence';
                }
              }
            }
          }        
        }
        // List properties if they're available
        $proptable = $base_table . 'prop';
        if ($c->tableExists($proptable)) {
          // Property fieldset
          $form[$base_table]['props'] = [
            '#type' => 'details',
            '#title' => 'Properties for this ' . $base_table,
            '#open' => TRUE
          ];
          $from_url = str_replace('/', '::', Url::fromRoute('chado_display.edit', ['base_table' => $base_table, 'id' => $id])->toString());
          // Add a property
          $add = Link::fromTextAndUrl(
            'Add a property', 
            Url::fromRoute(
              'chado_display.add', 
              ['base_table' => $proptable, 'id' => $id, 'redirect_url' => $from_url], 
              [
                'attributes' => [
                  'class' => 'use-ajax', 
                  'data-dialog-options' => Json::encode(['width' => 800]), 
                  'data-dialog-type' => 'modal'
                ]
              ]
            )
          )->toString();
          $form[$base_table]['props']['addprop'] = [
            '#markup' => $add, 
          ];
          // List of current properties
          $proppkey = $c->getPkey($proptable);
          $props = $c->getObjects($proptable, ['(SELECT name FROM chado.cvterm WHERE cvterm_id = type_id) AS type', '*'], [], [$pk => $id]);
          if(count($props) > 0) {
            $headers = [];
            $rows = [];
            foreach ($props AS $idx => $prop) {
              $cell = [];
              foreach ($prop AS $col => $v) {
                if ($col != $proppkey) { // Ignore Prop Pkey column
                  if ($idx == 0) {
                    $headers[]  = $col;
                  }
                  $cell [] = Markup::create($v);
                }
              }
              $prop_id = $prop->$proppkey;
              $edit = Link::fromTextAndUrl(
                  'Edit',
                  Url::fromRoute(
                      'chado_display.edit',
                      ['base_table' => $proptable, 'id' => $prop_id, 'redirect_url' => $from_url],
                      [
                        'attributes' => [
                          'class' => 'use-ajax',
                          'data-dialog-options' => Json::encode(['width' => 800]), 
                          'data-dialog-type' => 'modal'
                        ]
                      ]
                      )
                  )->toString();
                  $delete =Link::fromTextAndUrl(
                      'Delete',
                      Url::fromRoute(
                          'chado_display.delete',
                          ['base_table' => $proptable, 'id' => $prop_id, 'redirect_url' => $from_url],
                          [
                            'attributes' => [
                              'class' => 'use-ajax',
                              'data-dialog-options' => Json::encode(['width' => 800]), 
                              'data-dialog-type' => 'modal'
                            ]
                          ]
                          )
                      )->toString();
              $cell [] = $edit;
              $cell [] = $delete;
              $rows[] = $cell;
            }
            
            $headers [] = 'Edit';
            $headers [] = 'Delete';
            
            $form[$base_table]['props'][$proptable] = [
              '#type' => 'table',
              '#header' => $headers,
              '#rows' => $rows,
              '#attributes' => ['class' => ['chado_display_property_table']],
            ];
          }
        }
        
        // Allow setting URL Alias/Uploading an image for supported display type
        $supported = chado_display_get_extended_data_type();
        if (key_exists($base_table, $supported)) {
          // Setting URL
          $path = chado_display_get_path() . '/' . $base_table . '/' . $id;
          $current_url = \Drupal::database()->select('path_alias')->fields('path_alias', ['alias'])->condition('path', $path)->execute()->fetchField();
          $form[$base_table]['url_alias'] = [
            '#type' => 'textfield',
            '#title' => 'URL Alias',
            '#description' => 'Chado Display allows only one URL alias. Redundant aliases will be removed when the form is saved.',
            '#default_value' => $current_url
          ];
          // Uploading one associated image
          $form[$base_table]['image_container'] = [
            '#type' => 'details',
            '#title' => 'Overview Image',
            '#description' => 'Show an image in the Overview section',
            '#open' => TRUE
          ];
          $image = $this->chado->getFirstObject('eimage', ['*'], [], ['image_uri' => $path]);
          if ($image) {
            $delete_image =Link::fromTextAndUrl(
                '[ Delete Image ]',
                Url::fromRoute(
                    'chado_display.delete_image',
                    ['eimage_id' => $image->eimage_id, 'redirect_url' => $from_url],
                    [
                      'attributes' => [
                        'class' => 'use-ajax',
                        'data-dialog-options' => Json::encode(['width' => 800]),
                        'data-dialog-type' => 'modal'
                      ]
                    ]
                    )
                )->toString();
            $form[$base_table]['image_container']['image_remove'] = [
              '#markup' => $delete_image,
              '#id' => $image->eimage_id,
              '#prefix' => '<div>' . $image->eimage_type . ' ',
              '#suffix' => '</div>'
            ];
          }
          $form[$base_table]['image_container']['image'] = [
            '#type' => 'file',
          ];
        }
        
        // Save button
        $form[$base_table]['submit'] = [
          '#type' => 'submit',
          '#value' => 'Save'
        ];
        // Cancel button
        if (!$this->redirect_url) {
          $form[$base_table]['cancel'] = [
            '#type' => 'button',
            '#value' => 'Cancel'
          ];
        }
      }
      else {
        $form['no_data'] = [
          '#markup' => 'The requested record could not be found.'
        ];
      }
    }
    else {
      $form['no_data'] = [
        '#markup' => 'The requested page could not be found.'
      ];
    }
    $form['#attached'] = [
      'library' => ['core/drupal.dialog.ajax']
    ];
    return $form;
  }
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $triggering = $form_state->getTriggeringElement();
    if($triggering['#value'] == 'Cancel') {
      // Redirect if base_table is a supported display type when Cancel is clicked
      chado_display_redirect($this->base_table, $this->id);
    }
    
    $values = $form_state->getValues();
    // Check numeric for integer
    foreach ($this->columns AS $col => $type) {
      if (($type == 'integer' || $type == 'real') && $values[$col] && !is_numeric($values[$col])) {
        $form_state->setErrorByName($col, 'Please input a number.');
      }
    }
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering = $form_state->getTriggeringElement();
    if($triggering['#value'] != 'Cancel') {
      $condition = [$this->pkey => $this->id];
      $values = $form_state->getValues();
      $vals = [];
      foreach ($this->columns AS $col => $type) {
        // Do NOT update values that are not on the form
        if (!key_exists($col, $values)) {
          continue;
        }
        if (is_array($values[$col])) {
          $vals[$col] = $values[$col]['value'];
        }
        else {
          // Ignore empty numeric data
          if (trim($values[$col]) == '' && in_array(explode(' ', $type)[0], ['bigint', 'bigserial', 'double precision', 'integer', 'numeric', 'real', 'smallint', 'smallserial', 'serial', 'time', 'timestamp', 'date', 'interval'])) {
            continue;
          }
          else {
            $vals[$col] = $values[$col];
          }
        }
      }
      try {
        $this->chado->updateFields($this->base_table, $vals, $condition, FALSE, TRUE);
      } catch (\Exception $e) {
        \Drupal::messenger()->addError($e->getMessage());
        return;
      }
      
      // Save URL Alias
      if (isset($values['url_alias']) && $values['url_alias']) {
        $path = chado_display_get_path() . '/' . $this->base_table . '/' . $this->id;
        $url_alias = preg_match('/^\//', $values['url_alias']) ? $values['url_alias'] : '/' . $values['url_alias'];
        // Remove redundant aliases
        \Drupal::database()->query('DELETE FROM path_alias WHERE path = :path', [':path' => $path]);
        \Drupal::database()->query('DELETE FROM path_alias_revision WHERE path = :path', [':path' => $path]);
        $path_alias = \Drupal\path_alias\Entity\PathAlias::create([
          'path' => $path,
          'alias' => $url_alias,
        ]);
        $path_alias->save();
      }
      
      // Save image
      $all_files = $this->getRequest()->files->get('files', []);
      if (isset($all_files['image'])) {
        $path = chado_display_get_path() . '/' . $this->base_table . '/' . $this->id;
        $image = file_get_contents($all_files['image']->getRealPath());
        $data = base64_encode($image);
        $exists = $this->chado->getFirstField('eimage', 'eimage_id', ['image_uri' => $path], []);
        if ($exists) {
          \Drupal::database()->query('DELETE FROM chado.eimage WHERE eimage_id = :eimage_id', [':eimage_id' => $exists]);
        }
        $this->chado->getFirstField('eimage', 'eimage_id', ['image_uri' => $path, 'eimage_data' => $data, 'eimage_type' => $all_files['image']->getClientOriginalName()], [], TRUE);
      }
      Cache::delete($this->base_table, $this->id);
      // Show a message
      \Drupal::messenger()->addMessage($this->base_table . ' record updated.');
    }
    // Redirect
    // If redirect_url specified
    if ($this->redirect_url) {
      chado_display_goto(str_replace('::', '/', $this->redirect_url));
    }
    // If redirect_url not specified, try to redirect to the Chado Display page or stay on the form
    else {
      chado_display_redirect($this->base_table, $this->id, TRUE);
    }
  }
}
