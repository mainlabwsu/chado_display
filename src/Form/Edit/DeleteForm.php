<?php 
namespace Drupal\chado_display\Form\Edit;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\chado_display\Database\Chado;
use Drupal\chado_display\Render\Cache;

class DeleteForm extends FormBase {
  
  protected $chado;
  protected $base_table;
  protected $id;
  protected $redirect_url;
  
  public function getFormId() {
    return 'chado_display_delete_form';
  }
  
  public function title($base_table = NULL, $id = NULL) {
    $c = new Chado();
    if ($c->tableExists($base_table) && is_numeric($id)) {
      $obj = $c->getObjectById($base_table, ['*'], $id);
      if ($obj) {
        return 'Delete Chado ' . $base_table;
      }
    }
    return 'Page not found';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state, $base_table = NULL, $id = NULL, $redirect_url = NULL) {
    $form = array();
    
    $this->base_table = $base_table;
    $this->id = $id;
    $this->redirect_url = $redirect_url;
    
    $c = new Chado();
    $this->chado = $c;
    if ($c->tableExists($base_table) && is_numeric($id)) {      
      $obj = $c->getObjectById($base_table, ['*'], $id);
      if($obj) {
        $data = '<ul>';
        foreach ($obj AS $k => $v) {
          if (empty($v) || strlen($v) < 100) {
            $data .= '<li>'. $k . ': ' . $v . '</li>';
          }
          else {
            $data .= '<li>'. $k . ': ' . substr(strip_tags($v), 0, 100) . '... </li>';
          }
        }
        $data .= '</ul>';
        $form[$base_table]['confirm'] = [
          '#markup' => '<p>Are you sure you want to delete this record?</p>' . $data,
        ];
        $form[$base_table]['submit'] = [
          '#type' => 'submit',
          '#value' => 'Delete'
        ];
        // Cancel button
        if (!$this->redirect_url) {
          $form[$base_table]['cancel'] = [
            '#type' => 'button',
            '#value' => 'Cancel'
          ];
        }
      }
      else {
        $form['no_data'] = [
          '#markup' => 'The requested record could not be found.'
        ];
      }
    }
    else {
      $form['no_data'] = [
        '#markup' => 'The requested page could not be found.'
      ];
    }

    return $form;
  }
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $triggering = $form_state->getTriggeringElement();
    if($triggering['#value'] == 'Cancel') {
      // Redirect if base_table is a supported display type when Cancel is clicked
      chado_display_redirect($this->base_table, $this->id);
    }
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $pk = $this->chado->getPkey($this->base_table);
      $condition = [$pk => $this->id];
      $this->chado->delete($this->base_table, $condition);
    } catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
      return;
    }
    // Show a message
    \Drupal::messenger()->addMessage($this->base_table . ' record deleted from Chado.');
    
    // Redirect
    // If redirect_url specified
    if ($this->redirect_url) {
      chado_display_goto(str_replace('::', '/', $this->redirect_url));
    }
    // If redirect_url not specified, redirect to the frontpage
    else {
      Cache::delete($this->base_table, $this->id);
      $url = Url::fromRoute('<front>');
      $form_state->setRedirectUrl($url);
    }
  }
}