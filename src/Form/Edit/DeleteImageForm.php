<?php 
namespace Drupal\chado_display\Form\Edit;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\chado_display\Database\Chado;

class DeleteImageForm extends FormBase {
  
  protected $chado;
  protected $eimage_id;
  protected $redirect_url;
  
  public function getFormId() {
    return 'chado_display_delete_image_form';
  }
  
  public function title($eimage_id = NULL) {
    $c = new Chado();
    if ($c->tableExists('eimage') && is_numeric($eimage_id)) {
      $obj = $c->getObjectById('eimage', ['*'], $eimage_id);
      if ($obj) {
        return 'Delete Chado Image';
      }
    }
    return 'Page not found';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state, $eimage_id = NULL, $redirect_url = NULL) {
    $form = array();
    
    $base_table = 'eimage';
    $this->eimage_id = $eimage_id;
    $this->redirect_url = $redirect_url;
    
    $c = new Chado();
    $this->chado = $c;
    if ($c->tableExists($base_table) && is_numeric($eimage_id)) {      
      $obj = $c->getObjectById($base_table, ['eimage_id', 'eimage_type', 'image_uri'], $eimage_id);
      if($obj) {
        $data = '<ul>';
        foreach ($obj AS $k => $v) {
          if (empty($v) || strlen($v) < 100) {
            $data .= '<li>'. $k . ': ' . $v . '</li>';
          }
          else {
            $data .= '<li>'. $k . ': ' . substr(strip_tags($v), 0, 100) . '... </li>';
          }
        }
        $data .= '</ul>';
        $form[$base_table]['confirm'] = [
          '#markup' => '<p>Are you sure you want to delete this record? This cannot be undone.</p>' . $data,
        ];
        $form[$base_table]['submit'] = [
          '#type' => 'submit',
          '#value' => 'Delete'
        ];
        // Cancel button
        if (!$this->redirect_url) {
          $form[$base_table]['cancel'] = [
            '#type' => 'button',
            '#value' => 'Cancel'
          ];
        }
      }
      else {
        $form['no_data'] = [
          '#markup' => 'The requested record could not be found.'
        ];
      }
    }
    else {
      $form['no_data'] = [
        '#markup' => 'The requested page could not be found.'
      ];
    }

    return $form;
  }
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      \Drupal::database()->query('DELETE from chado.eimage WHERE eimage_id = :eimage_id', [':eimage_id' => $this->eimage_id]);
    } catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
      return;
    }
    // Show a message
    \Drupal::messenger()->addMessage(' Image deleted from Chado.');
    
    // Redirect
    // If redirect_url specified
    if ($this->redirect_url) {
      chado_display_goto(str_replace('::', '/', $this->redirect_url));
    }
    // If redirect_url not specified, redirect to the frontpage
    else {
      $url = Url::fromRoute('<front>');
      $form_state->setRedirectUrl($url);
    }
  }
}