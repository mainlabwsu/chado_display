<?php 
namespace Drupal\chado_display\Form\Data;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Render\Markup;

class TraitListing extends FormBase {
  
  public function getFormId() {
    return 'chado_display_trait_listing_form';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = array();
    $form['label'] = array(
      '#markup' => '
          <div style="margin:20px 0px;">To see all the traits, click to expand the trait category below.  Or narrow the list using the keyword search.</div>
        ',
    );
    $form['keyword'] =array(
      '#id' => 'mainlab_trait_listing-id',
      '#title' => 'Keyword',
      '#type' => 'textfield',
      '#suffix' => '</div>'
    );
    $form['submit'] = array(
      '#type' => 'button',
      '#value' => 'Search',
      '#ajax' => array(
        'callback' => '::ajaxResult',
        'wrapper' => 'mainlab_trait_listing-result-field',
        'effect' => 'fade'
      ),
    );
    $result = '';
    if (!$form_state->getValues()) {
      $result = $this->renderList();
    }
    else {
      $result = $this->renderList($form_state->getValue('keyword'));
      $result .= '<script>jQuery("#mainlab_trait_listing-id").focus();</script>';
    }
    $form['result'] = array(
      '#markup' => Markup::create($result),
      '#prefix' => '<div id="mainlab_trait_listing-result-field">',
      '#suffix' => '</div>'
    );
    return $form;
  }
  
  function getTitle() {
    return '';
  }
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }
  
  protected function renderList ($keyword = NULL) {
    $traits = $this->getTraits($keyword);
    $output = '';
    if (count($traits) > 0) {
      $trait_def = \Drupal::state()->set('mainlab_tripal_show_trait_definition', 0);
      $output = '<table>';
      $output .= '<tr>
                        <th style="border:0;">Category</th>
                        <th style="border:0;"><div  class="mainlab_tripal_hidden_header" style="display:none;">Abbreviation</th>
                        <th style="border:0;"><div  class="mainlab_tripal_hidden_header" style="display:none;">Trait Name</th>
                        <th style="border:0;"><div  class="mainlab_tripal_hidden_header" style="display:none;">Alias</th>';
      if ($trait_def) {
        $output .= '<th style="border:0;"><div  class="mainlab_tripal_hidden_header" style="display:none;">Definition</th>';
      }
      $output .=   '<th style="border:0;" nowrap>Count</th>
                      </tr>';
      $color = "#FFF'";
      $js = "
        var cat = jQuery(this).get(0).id;
        var cat_html = jQuery(this).html();
        var sign = cat_html.substring(0, 1);
        var text = cat_html.substring(2);
        if (sign == \"+\") {
          jQuery(this).html(\"- \" + text);
          jQuery(\".\" + cat).css(\"padding-bottom\", \"4px\");
          jQuery(\".\" + cat).show();
        }
        else {
          jQuery(this).html(\"+ \" + text);
          jQuery(\".\" + cat).css(\"padding-bottom\", \"0px\");
          jQuery(\".\" + cat).hide();
        }
        var all = document.getElementsByClassName(\"mainlab_tripal_hidden_element\");
        var i;
        var hide = 1;
        for (i = 0; i < all.length; i++) {
          var disp = all[i].style.display;
          if (disp != \"none\") {
            jQuery(\".mainlab_tripal_hidden_header\").show();
            hide = 0;
            break;
          }
        }
        if (hide == 1) {
          jQuery(\".mainlab_tripal_hidden_header\").hide();
        }
        return false;
      ";
      $index = 0;
      foreach ($traits AS $cat => $trait) {
        $num_traits = count($trait);
        if ($num_traits > 0) {
          $counter = 0;
          foreach ($trait AS $t) {
            $output .= "<tr style='border:0;background-color:$color'>";
            $padding = 'padding:0px 10px';
            if ($counter == 0) {
              $output .=
              "<td rowspan='$num_traits' style='vertical-align:top;padding:4px 10px 4px;min-width:300px;border:0'>
                       <a id='mainlab_tripal_hidden_element-$index' href='#' onClick='$js'>+ $cat</a>
                     </td>";
              $padding = 'padding:4px 10px 0px';
            }
            $output .=
            "<td style='$padding;border:0'>
                     <div class='mainlab_tripal_hidden_element mainlab_tripal_hidden_element-$index' style='display:none;'>" . $t['abbreviation'] . '</div>
                   </td>';
            $trait_name = $t['trait'];
            if ($keyword) {
              $position = strpos(strtolower($t['trait']), strtolower($keyword));
              if ($position !== FALSE) {
                $start = substr($t['trait'], 0, $position);
                $mid = substr($t['trait'], $position, strlen($keyword));
                $end = substr($t['trait'],$position + strlen($keyword));
                $trait_name = $start . "<b>$mid</b>" . $end;
              }
            }
            $output .=
            "<td style='$padding;border:0'>
                     <div class='mainlab_tripal_hidden_element mainlab_tripal_hidden_element-$index' style='display:none;min-width:260px !important'><a href='/display/trait/" . $t['cvterm_id'] . "'>" . $trait_name . '</a></div>
                   </td>';
            $output .=
            "<td style='$padding;border:0'>
                     <div class='mainlab_tripal_hidden_element mainlab_tripal_hidden_element-$index' style='display:none;min-width:260px !important'>" . $t['synonym'] . '</div>
                   </td>';
            if ($trait_def) {
              $output .=
              "<td style='$padding;border:0'>
                       <div class='mainlab_tripal_hidden_element mainlab_tripal_hidden_element-$index' style='display:none;'>" . $t['definition'] . '</div>
                     </td>';
            }
            if ($counter == 0) {
              $output .= "<td rowspan='$num_traits'  style='vertical-align:top;padding:4px 10px 0px;border:0;background-color:$color'>" . $num_traits . "</td>";
            }
            $output .= '</tr>';
            $counter ++;
          }
          $color = $color == '#EEE' ? '#FFF' : '#EEE';
        }
        $index ++;
      }
      $output .= '</table>';
    }
    else {
      $output = '<div style="clear:both">No trait matches the keyword.</div>';
    }
    return $output;
  }
  
  protected function getTraits($keyword = NULL) {
    $db = \Drupal::database();
    $sql = "
      SELECT
        C.cvterm_id,
        CAT.name AS category,
        C.name AS trait,
        Alias.synonym,
        C.definition,
        A.value AS abbreviation,
        C.definition
      FROM chado.cvterm C
      INNER JOIN chado.cv ON C.cv_id = cv.cv_id
      LEFT JOIN (
        SELECT cvterm_id, value
        FROM chado.cvtermprop
        WHERE type_id = (
          SELECT cvterm_id
          FROM chado.cvterm C
          INNER JOIN chado.cv on CV.cv_id = C.cv_id
          WHERE C.name = 'abbreviation' AND cv.name = 'MAIN')
      ) A on A.cvterm_id = C.cvterm_id
      LEFT JOIN (
        SELECT C1.name, C1.cvterm_id, CR.subject_id
        FROM chado.cvterm C1
        INNER JOIN chado.cvterm_relationship CR on CR.object_id = C1.cvterm_id
        INNER JOIN chado.cvterm C2 on C2.cvterm_id = CR.type_id
        INNER JOIN chado.cv CV2 on CV2.cv_id = C2.cv_id
        WHERE CV2.name = 'relationship' AND C2.name = 'is_a'
      ) CAT on CAT.subject_id = C.cvterm_id
      LEFT JOIN (
        SELECT cvterm_id, string_agg(DISTINCT synonym, ' / ') AS synonym FROM chado.cvtermsynonym WHERE trim(synonym) != '' GROUP BY cvterm_id
      ) ALIAS ON ALIAS.cvterm_id = C.cvterm_id
      WHERE cv.name LIKE '%_trait_ontology'
      AND CAT.name IS NOT NULL
      AND A.value IS NOT NULL  ";
    if ($keyword) {
      $sql .= "AND lower(C.name) LIKE lower(:keyword) ";
    }
    $sql .= "ORDER BY CAT.name, A.value";
    $result = NULL;
    if ($keyword) {
      $result = $db->query($sql, array(':keyword' => '%' . $keyword . '%'));
    }
    else {
      $result = $db->query($sql);
    }
    $traits = array();
    while ($t = $result->fetchObject()) {
      if (!key_exists($t->category, $traits)) {
        $traits[$t->category] = array();
      }
      $traits[$t->category][] =  array(
        'abbreviation' => $t->abbreviation,
        'trait' => $t->trait,
        'synonym' => $t->synonym,
        'cvterm_id' => $t->cvterm_id,
        'definition' => $t->definition,
      );
    }
    return $traits;
  }
  
  public function ajaxResult ($form, &$form_state) {
    return $form['result'];
  }
  
}