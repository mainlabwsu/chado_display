<?php
namespace Drupal\chado_display\Form\Admin;

class ImageAdminForm {

    protected $statement;

    public function __construct($statement) {
        $this->statement = $statement;
    }

    public function addForm($form = array()) {
        /**
         * Image Settings
         */
        $statement = $this->statement;
        $form['image'] = [
            '#type' => 'details',
            '#title' => 'Image',
            '#description' => 'Settings for Chado Image Page',
        ];
        $dispaly_opts = chado_display_render_info('Drupal\chado_display\Render\Based\Image');
        $default_display = \Drupal::state()->get('chado_display_image_render_info', $dispaly_opts);
        $form['image']['image_display'] = array(
          '#type' => 'checkboxes',
          '#title' => 'Display Information in Sidebar',
          '#description' => 'Display selected information about an Image.',
          '#options' => $dispaly_opts,
          '#default_value' => $default_display
        );
        $default_sidebar_order = \Drupal::state()->get('chado_display_image_sidebar_order', []);
        $form['image']['image_sidebar_order'] = [
          '#type' => 'textfield',
          '#title' => 'Sidebar Priority Order',
          '#description' => 'Items on the list will show up first in the Sidebar. Separate each item with a comma (,)',
          '#default_value' => $default_sidebar_order
        ];
        $default_overview_order = \Drupal::state()->get('chado_display_image_overview_order', '');
        $form['image']['image_overview_order'] = [
          '#type' => 'textfield',
          '#title' => 'Overview Priority Order',
          '#description' => 'Fields on the list will show up first in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_order
        ];
        $default_overview_hidden = \Drupal::state()->get('chado_display_image_overview_hidden', '');
        $form['image']['image_overview_hidden'] = [
          '#type' => 'textfield',
          '#title' => 'Hide Overview Fields',
          '#description' => 'Fields on the list will not be shown in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_hidden
        ];
        $default_props = \Drupal::state()->get('chado_display_eimageprop', []);
        $default_props_sidebar = \Drupal::state()->get('chado_display_eimageprop_sidebar', []);
        $cache_props = \Drupal::state()->get('chado_display_eimageprop_cache', []);
        $props = [];
        if (count($cache_props) == 0) {
            $ptypes = $statement->base->getPropertyTypes('eimage');
            foreach ($ptypes AS $type) {
                $props[$type->type_id] = $type->cv . ': ' . $type->name;
            }
            \Drupal::state()->set('chado_display_eimageprop_cache', $props);
        }
        else {
            $props = $cache_props;
        }
        $form['image']['refreshprop'] = array(
            '#type' => 'button',
            '#value' => 'Refresh Property List: eimage',
          '#suffix' => '<div id="chado_display_admin_prop_counter"> ' . count($props) . ' properties found</div>'
        );
        if (count($props) > 0) {
          $form['image']['allimageprops'] = array(
            '#type' => 'checkbox',
            '#title' => 'Select all properties',
            '#id' => 'image',
            '#ajax' => [
              'callback' => '::selectAllProps',
              'wrapper' => 'chado_display_admin_image_props',
              'effect' => 'fade'
            ]
          );
          $form['image']['eimageprop'] = array(
              '#type' => 'checkboxes',
              '#title' => 'Show Image Properties in Overview',
              '#description' => 'Display checked image properties in the Overview.',
              '#options' => $props,
              '#default_value' => $default_props,
            '#prefix' => '<div id="chado_display_admin_props"><div id="chado_display_admin_image_props">',
            '#suffix' => '</div>'
          );
          $form['image']['eimageprop_sidebar'] = array(
            '#type' => 'checkboxes',
            '#title' => 'Show Image Properties in Sidebar',
            '#description' => 'Display checked image properties as a sidebar item. The Chado Display sidebar has limited space, please select sparsingly.',
            '#options' => $props,
            '#default_value' => $default_props_sidebar,
            '#prefix' => '<div class="chado_display_admin_props_in_sidebar">',
            '#suffix' => '</div></div>'
          );
        }
        return $form;
    }
}