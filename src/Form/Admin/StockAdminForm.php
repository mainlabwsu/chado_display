<?php
namespace Drupal\chado_display\Form\Admin;

class StockAdminForm {

    protected $statement;

    public function __construct($statement) {
        $this->statement = $statement;
    }

    public function addForm($form = array()) {
        /**
         * Stock Settings
         */
        $statement = $this->statement;
        $form['stock'] = [
            '#type' => 'details',
            '#title' => 'Stock',
            '#description' => 'Settings for Chado Stock Page',
        ];
        $dispaly_opts = chado_display_render_info('Drupal\chado_display\Render\Based\Stock');
        $default_display = \Drupal::state()->get('chado_display_stock_render_info', $dispaly_opts);
        $form['stock']['stock_display'] = array(
          '#type' => 'checkboxes',
          '#title' => 'Display Information in Sidebar',
          '#description' => 'Display selected information about a Stock.',
          '#options' => $dispaly_opts,
          '#default_value' => $default_display
        );
        $default_sidebar_order = \Drupal::state()->get('chado_display_stock_sidebar_order', []);
        $form['stock']['stock_sidebar_order'] = [
          '#type' => 'textfield',
          '#title' => 'Sidebar Priority Order',
          '#description' => 'Items on the list will show up first in the Sidebar. Separate each item with a comma (,)',
          '#default_value' => $default_sidebar_order
        ];
        $default_overview_order = \Drupal::state()->get('chado_display_stock_overview_order', '');
        $form['stock']['stock_overview_order'] = [
          '#type' => 'textfield',
          '#title' => 'Overview Priority Order',
          '#description' => 'Fields on the list will show up first in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_order
        ];
        $default_overview_hidden = \Drupal::state()->get('chado_display_stock_overview_hidden', '');
        $form['stock']['stock_overview_hidden'] = [
          '#type' => 'textfield',
          '#title' => 'Hide Overview Fields',
          '#description' => 'Fields on the list will not be shown in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_hidden
        ];
        $default_props = \Drupal::state()->get('chado_display_stockprop', []);
        $default_props_sidebar = \Drupal::state()->get('chado_display_stockprop_sidebar', []);
        $cache_props = \Drupal::state()->get('chado_display_stockprop_cache', []);
        $props = [];
        if (count($cache_props) == 0) {
            $ptypes = $statement->base->getPropertyTypes('stock');
            foreach ($ptypes AS $type) {
                $props[$type->type_id] = $type->name;
            }
            \Drupal::state()->set('chado_display_stockprop_cache', $props);
        }
        else {
            $props = $cache_props;
        }
        $form['stock']['refreshprop'] = array(
            '#type' => 'button',
            '#value' => 'Refresh Property List: stock',
          '#suffix' => '<div id="chado_display_admin_prop_counter"> ' . count($props) . ' properties found</div>'
        );
        if (count($props) > 0) {
          $form['stock']['allstockprops'] = array(
            '#type' => 'checkbox',
            '#title' => 'Select all properties',
            '#id' => 'stock',
            '#ajax' => [
              'callback' => '::selectAllProps',
              'wrapper' => 'chado_display_admin_stock_props',
              'effect' => 'fade'
            ]
          );
          $form['stock']['stockprop'] = array(
              '#type' => 'checkboxes',
              '#title' => 'Show Stock Properties in Overview',
              '#description' => 'Display checked stock properties in the Overview.',
              '#options' => $props,
              '#default_value' => $default_props,
            '#prefix' => '<div id="chado_display_admin_props"><div id="chado_display_admin_stock_props">',
            '#suffix' => '</div>'
          );
          $form['stock']['stockprop_sidebar'] = array(
            '#type' => 'checkboxes',
            '#title' => 'Show Stock Properties in Sidebar',
            '#description' => 'Display checked stock properties as a sidebar item. The Chado Display sidebar has limited space, please select sparsingly.',
            '#options' => $props,
            '#default_value' => $default_props_sidebar,
            '#prefix' => '<div class="chado_display_admin_props_in_sidebar">',
            '#suffix' => '</div></div>'
          );
        }
        return $form;
    }
}