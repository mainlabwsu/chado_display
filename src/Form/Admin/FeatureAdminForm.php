<?php
namespace Drupal\chado_display\Form\Admin;

class FeatureAdminForm {

    protected $statement;

    public function __construct($statement) {
        $this->statement = $statement;
    }

    public function addForm($form = array()) {
        /**
         * Feature Settings
         */
        $statement = $this->statement;
        $form['feature'] = [
            '#type' => 'details',
            '#title' => 'Feature',
            '#description' => 'Settings for Chado Feature Page',
        ];
        $dispaly_opts = chado_display_render_info('Drupal\chado_display\Render\Based\Feature');
        $default_display = \Drupal::state()->get('chado_display_feature_render_info', $dispaly_opts);
        $form['feature']['feature_display'] = array(
            '#type' => 'checkboxes',
            '#title' => 'Display Information in Sidebar',
            '#description' => 'Display selected information about a Feature.',
            '#options' => $dispaly_opts,
            '#default_value' => $default_display
        );
        $default_residues = \Drupal::state()->get('chado_display_residues', 100000);
        $form['feature']['residues'] = array(
            '#type' => 'textfield',
            '#title' => 'Feature Residues Display Limit',
            '#description' => 'Do not show sequence if a feature has residues with size greater than this cutoff',
            '#default_value' => $default_residues,
            '#size' => 10,
            '#maxlength' => 10,
        );
        $default_seqwrap = \Drupal::state()->get('chado_display_sequence_wrap', 80);
        $form['feature']['seqwrap'] = array(
            '#type' => 'textfield',
            '#title' => 'Sequence Wrap',
            '#description' => 'Wrap the sequence at this character length',
            '#default_value' => $default_seqwrap,
            '#size' => 3,
            '#maxlength' => 3,
        );
        $default_jbrowse_width = \Drupal::state()->get('chado_display_jbrowse_width', 800);
        $form['feature']['jbrowse_width'] = array(
            '#type' => 'textfield',
            '#title' => 'Genome Viewer (JBrowse) Window Width',
            '#description' => 'The display width for the JBrowse window for Genomic Location. Set to 0 to disable the genome viewer',
            '#default_value' => $default_jbrowse_width,
            '#size' => 4,
            '#maxlength' => 4,
        );
        $default_jbrowse_height = \Drupal::state()->get('chado_display_jbrowse_height', 300);
        $form['feature']['jbrowse_height'] = array(
            '#type' => 'textfield',
            '#title' => 'Genome Viewer (JBrowse) Window Height',
            '#description' => 'The display height for the JBrowse window for Genomic Location',
            '#default_value' => $default_jbrowse_height,
            '#size' => 4,
            '#maxlength' => 4,
        );
        $default_sidebar_order = \Drupal::state()->get('chado_display_feature_sidebar_order', []);
        $form['feature']['feature_sidebar_order'] = [
          '#type' => 'textfield',
          '#title' => 'Sidebar Priority Order',
          '#description' => 'Items on the list will show up first in the Sidebar. Separate each item with a comma (,)',
          '#default_value' => $default_sidebar_order
        ];
        $default_overview_order = \Drupal::state()->get('chado_display_feature_overview_order', '');
        $form['feature']['feature_overview_order'] = [
          '#type' => 'textfield',
          '#title' => 'Overview Priority Order',
          '#description' => 'Fields on the list will show up first in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_order
        ];
        $default_overview_hidden = \Drupal::state()->get('chado_display_feature_overview_hidden', '');
        $form['feature']['feature_overview_hidden'] = [
          '#type' => 'textfield',
          '#title' => 'Hide Overview Fields',
          '#description' => 'Fields on the list will not be shown in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_hidden
        ];
        $default_props = \Drupal::state()->get('chado_display_featureprop', []);
        $default_props_sidebar = \Drupal::state()->get('chado_display_featureprop_sidebar', []);
        $cache_props = \Drupal::state()->get('chado_display_featureprop_cache', []);
        $props = [];
        if (count($cache_props) == 0) {
            $ptypes = $statement->base->getPropertyTypes('feature');
            foreach ($ptypes AS $type) {
              $props[$type->type_id] = $type->cv . ': ' . $type->name;
            }
            \Drupal::state()->set('chado_display_featureprop_cache', $props);
        }
        else {
            $props = $cache_props;
        }
        $form['feature']['refreshprop'] = array(
            '#type' => 'button',
            '#value' => 'Refresh Property List: feature',
          '#suffix' => '<div id="chado_display_admin_prop_counter"> ' . count($props) . ' properties found</div>'
        );
        if (count($props) > 0) {
          $form['feature']['allfeatureprops'] = array(
            '#type' => 'checkbox',
            '#title' => 'Select all properties',
            '#id' => 'feature',
            '#ajax' => [
              'callback' => '::selectAllProps',
              'wrapper' => 'chado_display_admin_feature_props',
              'effect' => 'fade'
            ]
          );
          $form['feature']['featureprop'] = array(
              '#type' => 'checkboxes',
              '#title' => 'Show Feature Properties in Overview',
              '#description' => 'Display checked feature properties in the Overview.',
              '#options' => $props,
              '#default_value' => $default_props,
            '#prefix' => '<div id="chado_display_admin_props"><div id="chado_display_admin_feature_props">',
            '#suffix' => '</div>'
          );
          $form['feature']['featureprop_sidebar'] = array(
            '#type' => 'checkboxes',
            '#title' => 'Show Feature Properties in Sidebar',
            '#description' => 'Display checked feature properties as a sidebar item. The Chado Display sidebar has limited space, please select sparsingly.',
            '#options' => $props,
            '#default_value' => $default_props_sidebar,
            '#prefix' => '<div class="chado_display_admin_props_in_sidebar">',
            '#suffix' => '</div></div>'
          );
        }
        return $form;
    }
}