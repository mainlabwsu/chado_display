<?php 
namespace Drupal\chado_display\Form\Admin;

class SourceAdminForm {
    
    public function addForm($form = array()) {
        /**
         * Data Source
         */
        $form['data'] = [
            '#type' => 'details',
            '#title' => 'Data Source',
            '#description' => 'Create pages for the following Chado data type',
            '#open' => TRUE
        ];
        $data_opt = chado_display_get_supported_data_tyep();
        $default_data_opt = \Drupal::state()->get('chado_display_supported_data', array_keys($data_opt));
        $form['data']['supported'] = [
            '#type' => 'checkboxes',
            '#title' => 'Base Table',
            '#options' => $data_opt,
            '#default_value' => $default_data_opt
        ];
        $default_extended = \Drupal::state()->get('chado_display_extended_data', '');
        $form['data']['extended'] = [
          '#type' => 'textfield',
          '#title' => 'Create pages for other Chado tables',
          '#description' => 'Chado Display is able to support any Chado table, separated table names by a comma (,). The new pages will be accessible through /display/record/{table}/{id}. See README if customization of these pages is desired.',
          '#default_value' => $default_extended
        ];
        $default_ignore_type = \Drupal::state()->get('chado_display_ignored_data_type', '');
        $form['data']['ignored'] = [
          '#type' => 'textfield',
          '#title' => 'Do NOT create pages for the following types',
          '#description' => 'If a Chado table has a type_id column, ignore specified types when creating pages. Separate \'table_name:type\' by a comma (,). (e.g. \'feature:chromosome, stock:sample\')',
          '#default_value' => $default_ignore_type
        ];
        return $form;       
    }
}