<?php
namespace Drupal\chado_display\Form\Admin;

class AnalysisAdminForm {

    protected $statement;

    public function __construct($statement) {
        $this->statement = $statement;
    }

    public function addForm($form = array()) {
        /**
         * Analysis Settings
         */
        $statement = $this->statement;
        $form['analysis'] = [
            '#type' => 'details',
            '#title' => 'Analysis',
            '#description' => 'Settings for Chado Analysis Page',
        ];
        $dispaly_opts = chado_display_render_info('Drupal\chado_display\Render\Based\Analysis');
        $default_display = \Drupal::state()->get('chado_display_analysis_render_info', $dispaly_opts);
        $form['analysis']['analysis_display'] = array(
          '#type' => 'checkboxes',
          '#title' => 'Display Information in Sidebar',
          '#description' => 'Display selected information about an Analysis.',
          '#options' => $dispaly_opts,
          '#default_value' => $default_display
        );
        $default_sidebar_order = \Drupal::state()->get('chado_display_analysis_sidebar_order', []);
        $form['analysis']['analysis_sidebar_order'] = [
          '#type' => 'textfield',
          '#title' => 'Sidebar Priority Order',
          '#description' => 'Items on the list will show up first in the Sidebar. Separate each item with a comma (,)',
          '#default_value' => $default_sidebar_order
        ];
        $default_overview_order = \Drupal::state()->get('chado_display_analysis_overview_order', '');
        $form['analysis']['analysis_overview_order'] = [
          '#type' => 'textfield',
          '#title' => 'Overview Priority Order',
          '#description' => 'Fields on the list will show up first in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_order
        ];
        $default_overview_hidden = \Drupal::state()->get('chado_display_analysis_overview_hidden', '');
        $form['analysis']['analysis_overview_hidden'] = [
          '#type' => 'textfield',
          '#title' => 'Hide Overview Fields',
          '#description' => 'Fields on the list will not be shown in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_hidden
        ];
        $default_props = \Drupal::state()->get('chado_display_analysisprop', []);
        $default_props_sidebar = \Drupal::state()->get('chado_display_analysisprop_sidebar', []);
        $cache_props = \Drupal::state()->get('chado_display_analysisprop_cache', []);
        $props = [];
        if (count($cache_props) == 0) {
            $ptypes = $statement->base->getPropertyTypes('analysis');
            foreach ($ptypes AS $type) {
                $props[$type->type_id] = $type->cv . ': ' . $type->name;
            }
            \Drupal::state()->set('chado_display_analysisprop_cache', $props);
        }
        else {
            $props = $cache_props;
        }
        $form['analysis']['refreshprop'] = array(
            '#type' => 'button',
            '#value' => 'Refresh Property List: analysis',
          '#suffix' => '<div id="chado_display_admin_prop_counter"> ' . count($props) . ' properties found</div>'
        );
        if (count($props) > 0) {
          $form['analysis']['allanalysisprops'] = array(
            '#type' => 'checkbox',
            '#title' => 'Select all properties',
            '#id' => 'analysis',
            '#ajax' => [
              'callback' => '::selectAllProps',
              'wrapper' => 'chado_display_admin_analysis_props',
              'effect' => 'fade'
            ]
          );
          $form['analysis']['analysisprop'] = array(
              '#type' => 'checkboxes',
              '#title' => 'Show Analysis Properties in Overview',
              '#description' => 'Display checked analysis properties in the Overview.',
              '#options' => $props,
              '#default_value' => $default_props,
            '#prefix' => '<div id="chado_display_admin_props"><div id="chado_display_admin_analysis_props">',
            '#suffix' => '</div>'
          );
          $form['analysis']['analysisprop_sidebar'] = array(
            '#type' => 'checkboxes',
            '#title' => 'Show Analysis Properties in Sidebar',
            '#description' => 'Display checked analysis properties as a sidebar item. The Chado Display sidebar has limited space, please select sparsingly.',
            '#options' => $props,
            '#default_value' => $default_props_sidebar,
            '#prefix' => '<div class="chado_display_admin_props_in_sidebar">',
            '#suffix' => '</div></div>'
          );
        }
        return $form;
    }
}