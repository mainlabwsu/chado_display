<?php 
namespace Drupal\chado_display\Form\Admin;

class PageAdminForm {
    
    public function addForm($form = array()) {
        /**
         * Page Settings
         */
        $form['page'] = [
            '#type' => 'details',
            '#title' => 'Page Layout',
            '#description' => 'The global page settings',
            '#open' => TRUE
        ];
        $options = [
            'scroll' => 'Scroll Style. Show all content on one page with a Table of Content (TOC) sidebar floating on the left. Clicking on a TOC item scrolls to the corresponding section on the page.',
            'scroll_right' => 'Scroll (Right sidebar). Scroll style with floating TOC sidebar on the right.',
            'fadein' => 'Fade In Style. Show a section of the content at a time with a Table of Content (TOC) sidebar fixed on the left. Clicking on a TOC item brings up the corresponding content.',
            'fadein_right' => 'Fade in (Right sidebar). Fade In style with floating TOC sidebar on the right.',
            'expandable' => 'Expandable Boxes. Put each section of content in an expandable box that can be opened or closed. No sidebar.',
            'nosidebar' => 'One page. Everything on one page without a TOC sidbar.'
        ];
        $default_style = \Drupal::state()->get('chado_display_page_style', 'scroll');
        $form['page']['style'] = array(
            '#type' => 'radios',
            '#title' => 'Page Style',
            '#description' => 'The style of a Chado data page.',
            '#options' => $options,
            '#default_value' => $default_style
        );
        
        $toptions = [
            'none' => 'None (styled by theme)',
            'grey' => 'Grey',
            'blue' => 'Blue',
            'green' => 'Green',
            'dark_green' => 'Dark Green',
            'light_green' => 'Light Green',
            'red' => 'Red'
        ];
        $default_table = \Drupal::state()->get('chado_display_table_style', 'none');
        $form['page']['table'] = array(
            '#type' => 'radios',
            '#title' => 'Table Style',
            '#description' => 'The table style.',
            '#options' => $toptions,
            '#default_value' => $default_table
        );
        
        $default_table_rows = \Drupal::state()->get('chado_display_table_rows', 15);
        $form['page']['table_rows'] = array(
            '#type' => 'textfield',
            '#title' => 'Table Rows',
            '#description' => 'The number of rows per page for a table.',
            '#default_value' => $default_table_rows,
            '#size' => 4,
            '#maxlength' => 4,
        );
        $default_biodata_reroute = \Drupal::state()->get('chado_display_biodata_reroute', FALSE);
        $form['page']['biodata_reroute'] = array(
          '#type' => 'checkbox',
          '#title' => 'Reroute Tripal Biodata',
          '#description' => 'Reroute Tripal bio_data URL to the corresponding Chado Display page',
          '#default_value' => $default_biodata_reroute,
        );
        $default_cache = \Drupal::state()->get('chado_display_page_cache', FALSE);
        $form['page']['cache'] = array(
          '#type' => 'checkbox',
          '#title' => 'Cache Chado Display pages',
          '#description' => 'Cache will improve page load performance. However, you might want to turn this off when developing custom Chado Display pages to show your changes immediately',
          '#default_value' => $default_cache,
        );
        return $form;
    }
}