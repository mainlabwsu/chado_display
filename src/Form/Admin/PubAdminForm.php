<?php
namespace Drupal\chado_display\Form\Admin;

class PubAdminForm {

    protected $statement;

    public function __construct($statement) {
        $this->statement = $statement;
    }

    public function addForm($form = array()) {
        /**
         * Pub Settings
         */
        $statement = $this->statement;
        $form['pub'] = [
            '#type' => 'details',
            '#title' => 'Pub',
            '#description' => 'Settings for Chado Pub Page',
        ];
        $dispaly_opts = chado_display_render_info('Drupal\chado_display\Render\Based\Pub');
        $default_display = \Drupal::state()->get('chado_display_pub_render_info', $dispaly_opts);
        $form['pub']['pub_display'] = array(
          '#type' => 'checkboxes',
          '#title' => 'Display Information in Sidebar',
          '#description' => 'Display selected information about a Pub.',
          '#options' => $dispaly_opts,
          '#default_value' => $default_display
        );
        $default_sidebar_order = \Drupal::state()->get('chado_display_pub_sidebar_order', []);
        $form['pub']['pub_sidebar_order'] = [
          '#type' => 'textfield',
          '#title' => 'Sidebar Priority Order',
          '#description' => 'Items on the list will show up first in the Sidebar. Separate each item with a comma (,)',
          '#default_value' => $default_sidebar_order
        ];
        $default_overview_order = \Drupal::state()->get('chado_display_pub_overview_order', '');
        $form['pub']['pub_overview_order'] = [
          '#type' => 'textfield',
          '#title' => 'Overview Priority Order',
          '#description' => 'Fields on the list will show up first in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_order
        ];
        $default_overview_hidden = \Drupal::state()->get('chado_display_pub_overview_hidden', '');
        $form['pub']['pub_overview_hidden'] = [
          '#type' => 'textfield',
          '#title' => 'Hide Overview Fields',
          '#description' => 'Fields on the list will not be shown in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_hidden
        ];
        $default_props = \Drupal::state()->get('chado_display_pubprop', []);
        $default_props_sidebar = \Drupal::state()->get('chado_display_pubprop_sidebar', []);
        $cache_props = \Drupal::state()->get('chado_display_pubprop_cache', []);
        $props = [];
        if (count($cache_props) == 0) {
            $ptypes = $statement->base->getPropertyTypes('pub');
            foreach ($ptypes AS $type) {
                $props[$type->type_id] = $type->cv . ': ' . $type->name;
            }
            \Drupal::state()->set('chado_display_pubprop_cache', $props);
        }
        else {
            $props = $cache_props;
        }
        $form['pub']['refreshprop'] = array(
            '#type' => 'button',
            '#value' => 'Refresh Property List: pub',
          '#suffix' => '<div id="chado_display_admin_prop_counter"> ' . count($props) . ' properties found</div>'
        );
        if (count($props) > 0) {
          $form['pub']['allpubprops'] = array(
            '#type' => 'checkbox',
            '#title' => 'Select all properties',
            '#id' => 'pub',
            '#ajax' => [
              'callback' => '::selectAllProps',
              'wrapper' => 'chado_display_admin_pub_props',
              'effect' => 'fade'
            ]
          );
          $form['pub']['pubprop'] = array(
              '#type' => 'checkboxes',
              '#title' => 'Show Pub Properties in Overview',
              '#description' => 'Display checked pub properties in the Overview.',
              '#options' => $props,
              '#default_value' => $default_props,
            '#prefix' => '<div id="chado_display_admin_props"><div id="chado_display_admin_pub_props">',
            '#suffix' => '</div>'
          );
          $form['pub']['pubprop_sidebar'] = array(
            '#type' => 'checkboxes',
            '#title' => 'Show Pub Properties in Sidebar',
            '#description' => 'Display checked pub properties as a sidebar item. The Chado Display sidebar has limited space, please select sparsingly.',
            '#options' => $props,
            '#default_value' => $default_props_sidebar,
            '#prefix' => '<div class="chado_display_admin_props_in_sidebar">',
            '#suffix' => '</div></div>'
          );
        }
        return $form;
    }
}