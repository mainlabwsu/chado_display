<?php
namespace Drupal\chado_display\Form\Admin;

class ContactAdminForm {

    protected $statement;

    public function __construct($statement) {
        $this->statement = $statement;
    }

    public function addForm($form = array()) {
        /**
         * Contact Settings
         */
        $statement = $this->statement;
        $form['contact'] = [
            '#type' => 'details',
            '#title' => 'Contact',
            '#description' => 'Settings for Chado Contact Page',
        ];
        $dispaly_opts = chado_display_render_info('Drupal\chado_display\Render\Based\Contact');
        $default_display = \Drupal::state()->get('chado_display_contact_render_info', $dispaly_opts);
        $form['contact']['contact_display'] = array(
          '#type' => 'checkboxes',
          '#title' => 'Display Information in Sidebar',
          '#description' => 'Display selected information about a Contact.',
          '#options' => $dispaly_opts,
          '#default_value' => $default_display
        );
        $default_sidebar_order = \Drupal::state()->get('chado_display_contact_sidebar_order', []);
        $form['contact']['contact_sidebar_order'] = [
          '#type' => 'textfield',
          '#title' => 'Sidebar Priority Order',
          '#description' => 'Items on the list will show up first in the Sidebar. Separate each item with a comma (,)',
          '#default_value' => $default_sidebar_order
        ];
        $default_overview_order = \Drupal::state()->get('chado_display_contact_overview_order', '');
        $form['contact']['contact_overview_order'] = [
          '#type' => 'textfield',
          '#title' => 'Overview Priority Order',
          '#description' => 'Fields on the list will show up first in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_order
        ];
        $default_overview_hidden = \Drupal::state()->get('chado_display_contact_overview_hidden', '');
        $form['contact']['contact_overview_hidden'] = [
          '#type' => 'textfield',
          '#title' => 'Hide Overview Fields',
          '#description' => 'Fields on the list will not be shown in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_hidden
        ];
        $default_props = \Drupal::state()->get('chado_display_contactprop', []);
        $default_props_sidebar = \Drupal::state()->get('chado_display_contactprop_sidebar', []);
        $cache_props = \Drupal::state()->get('chado_display_contactprop_cache', []);
        $props = [];
        if (count($cache_props) == 0) {
            $ptypes = $statement->base->getPropertyTypes('contact');
            foreach ($ptypes AS $type) {
              $props[$type->type_id] = $type->cv . ': ' . $type->name;
            }
            \Drupal::state()->set('chado_display_contactprop_cache', $props);
        }
        else {
            $props = $cache_props;
        }
        $form['contact']['refreshprop'] = array(
            '#type' => 'button',
            '#value' => 'Refresh Property List: contact',
          '#suffix' => '<div id="chado_display_admin_prop_counter"> ' . count($props) . ' properties found</div>'
        );
        if (count($props) > 0) {
          $form['contact']['allcontactprops'] = array(
            '#type' => 'checkbox',
            '#title' => 'Select all properties',
            '#id' => 'contact',
            '#ajax' => [
              'callback' => '::selectAllProps',
              'wrapper' => 'chado_display_admin_contact_props',
              'effect' => 'fade'
            ]
          );
          $form['contact']['contactprop'] = array(
              '#type' => 'checkboxes',
              '#title' => 'Show Contact Properties in Overview',
              '#description' => 'Display checked contact properties in the Overview.',
              '#options' => $props,
              '#default_value' => $default_props,
            '#prefix' => '<div id="chado_display_admin_props"><div id="chado_display_admin_contact_props">',
            '#suffix' => '</div>'
          );
          $form['contact']['contactprop_sidebar'] = array(
            '#type' => 'checkboxes',
            '#title' => 'Show Contact Properties in Sidebar',
            '#description' => 'Display checked contact properties as a sidebar item. The Chado Display sidebar has limited space, please select sparsingly.',
            '#options' => $props,
            '#default_value' => $default_props_sidebar,
            '#prefix' => '<div class="chado_display_admin_props_in_sidebar">',
            '#suffix' => '</div></div>'
          );
        }
        return $form;
    }
}