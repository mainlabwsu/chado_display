<?php
namespace Drupal\chado_display\Form\Admin;

class ExtraAdminForm {

    protected $statement;

    public function __construct($statement) {
        $this->statement = $statement;
    }

    public function addForm($form = array()) {
        /**
         * Extra Page Settings
         */
        $statement = $this->statement;
        $form['extra'] = [
            '#type' => 'details',
            '#title' => 'Extra Pages',
            '#description' => 'Settings for Extra Pages',
        ];
        $extra_opts = [
          'json' => 'Json data page',
          'allele' => 'Allele page', 
          'gene_class' => 'Gene Class page',
          'gene_class_listing' => 'Gene Class Listing page',
          'polymorphism' => 'Polymorphism page', 
          'trait' => 'Trait page', 
          'trait_descriptor' => 'Trait Descriptor page',
          'trait_listing' => 'Triat Listing page',          
        ];
        $default_extra = \Drupal::state()->get('chado_display_extra_render_info', $extra_opts);
        $form['extra']['extra_display'] = array(
          '#type' => 'checkboxes',
          '#title' => 'Chado Display Data/Listing Pages',
          '#description' => 'Chado Display can also create these pages when data are available',
          '#options' => $extra_opts,
          '#default_value' => $default_extra
        );
        return $form;
    }
}