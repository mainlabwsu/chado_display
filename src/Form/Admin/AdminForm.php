<?php
namespace Drupal\chado_display\Form\Admin;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\chado_display\Database\Chado;
use Drupal\chado_display\Database\Statement;
use Drupal\chado_display\Render\Cache;

class AdminForm extends FormBase {

    /**
     * {@inheritdoc}
     */
     public function getFormId() {
        return 'chado_display_admin_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state) {
        $statement = new Statement(new Chado());

        // Data source
        $sourceform = new SourceAdminForm();
        $form = $sourceform->addForm($form);

        // Access control
        $accessform = new AccessAdminForm();
        $form = $accessform->addForm($form);

        // Page style settings
        $pageform = new PageAdminForm();
        $form = $pageform->addForm($form);

        // Analysis settings
        $analysisform = new AnalysisAdminForm($statement);
        $form = $analysisform->addForm($form);
        
        // Contact settings
        $contactform = new ContactAdminForm($statement);
        $form = $contactform->addForm($form);

        // Feature settings
        $featureform = new FeatureAdminForm($statement);
        $form = $featureform->addForm($form);

        // Feature settings
        $imageform = new ImageAdminForm($statement);
        $form = $imageform->addForm($form);
        
        // Library settings
        $libraryform = new LibraryAdminForm($statement);
        $form = $libraryform->addForm($form);
        
        // Map settings
        $mapform = new MapAdminForm($statement);
        $form = $mapform->addForm($form);
        
        // NdGeolocation settings
        $ndgform = new NdGeolocationAdminForm($statement);
        $form = $ndgform->addForm($form);
        
        // Organism settings
        $organismform = new OrganismAdminForm($statement);
        $form = $organismform->addForm($form);
        
        // Project settings
        $projectform = new ProjectAdminForm($statement);
        $form = $projectform->addForm($form);

        // Pub settings
        $pubform = new PubAdminForm($statement);
        $form = $pubform->addForm($form);
        
        // Stock settings
        $stockform = new StockAdminForm($statement);
        $form = $stockform->addForm($form);
        
        // Stock Collection settings
        $stockcollectionform = new StockCollectionAdminForm($statement);
        $form = $stockcollectionform->addForm($form);
        
        // Extra Page settings
        $extraform = new ExtraAdminForm($statement);
        $form = $extraform->addForm($form);
        
        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => 'Save'
        );
        $form['#attached']['library'] = [
            'chado_display/chado_display_admin_form'
        ];
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();
        $operation = $form_state->getValue('op');
        if (preg_match('/^Refresh Property List:/', $operation)) {
            $tokens = explode (': ', $operation);
            $base = $tokens[1];
            $statement = new Statement(new Chado());
            $ptypes = $statement->base->getPropertyTypes($base);
            foreach ($ptypes AS $type) {
              $props[$type->type_id] = $type->cv . ': ' . $type->name;
            }
            \Drupal::state()->set('chado_display_' . $base . 'prop_cache', $props);
            \Drupal::messenger()->addMessage($base . ' property list updated.');
        }
    }

    /**
    * {@inheritdoc}
    */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        $values = $form_state->getValues();

        // Data
        \Drupal::state()->set('chado_display_supported_data', $values['supported']);
        \Drupal::state()->set('chado_display_extended_data', $values['extended']);
        \Drupal::state()->set('chado_display_ignored_data_type', $values['ignored']);
        
        // Access
        \Drupal::state()->set('chado_display_permission', $values['permission']);
        \Drupal::state()->set('chado_display_private_permission', $values['private']);
        
        // Page
        \Drupal::state()->set('chado_display_page_style', $values['style']);
        \Drupal::state()->set('chado_display_table_style', $values['table']);
        \Drupal::state()->set('chado_display_table_rows', $values['table_rows']);
        \Drupal::state()->set('chado_display_biodata_reroute', $values['biodata_reroute']);
        \Drupal::state()->set('chado_display_page_cache', $values['cache']);
        
        // Feature options
        \Drupal::state()->set('chado_display_jbrowse_width', $values['jbrowse_width']);
        \Drupal::state()->set('chado_display_jbrowse_height', $values['jbrowse_height']);
        \Drupal::state()->set('chado_display_residues', $values['residues']);
        \Drupal::state()->set('chado_display_sequence_wrap', $values['seqwrap']);
        
        // Render info
        \Drupal::state()->set('chado_display_analysis_render_info', $values['analysis_display']);
        \Drupal::state()->set('chado_display_contact_render_info', $values['contact_display']);
        \Drupal::state()->set('chado_display_feature_render_info', $values['feature_display']);
        \Drupal::state()->set('chado_display_image_render_info', $values['image_display']);
        \Drupal::state()->set('chado_display_library_render_info', $values['library_display']);
        \Drupal::state()->set('chado_display_map_render_info', $values['map_display']);
        \Drupal::state()->set('chado_display_nd_geolocation_render_info', $values['nd_geolocation_display']);
        \Drupal::state()->set('chado_display_organism_render_info', $values['organism_display']);
        \Drupal::state()->set('chado_display_project_render_info', $values['project_display']);
        \Drupal::state()->set('chado_display_pub_render_info', $values['pub_display']);
        \Drupal::state()->set('chado_display_stock_render_info', $values['stock_display']);
        \Drupal::state()->set('chado_display_stockcollection_render_info', $values['stockcollection_display']);
        \Drupal::state()->set('chado_display_extra_render_info', $values['extra_display']);
        
        // Display Orders
        \Drupal::state()->set('chado_display_analysis_sidebar_order', $values['analysis_sidebar_order']);
        \Drupal::state()->set('chado_display_analysis_overview_order', $values['analysis_overview_order']);
        \Drupal::state()->set('chado_display_contact_sidebar_order', $values['contact_sidebar_order']);
        \Drupal::state()->set('chado_display_contact_overview_order', $values['contact_overview_order']);
        \Drupal::state()->set('chado_display_feature_sidebar_order', $values['feature_sidebar_order']);
        \Drupal::state()->set('chado_display_feature_overview_order', $values['feature_overview_order']);
        \Drupal::state()->set('chado_display_image_sidebar_order', $values['image_sidebar_order']);
        \Drupal::state()->set('chado_display_image_overview_order', $values['image_overview_order']);
        \Drupal::state()->set('chado_display_library_sidebar_order', $values['library_sidebar_order']);
        \Drupal::state()->set('chado_display_library_overview_order', $values['library_overview_order']);
        \Drupal::state()->set('chado_display_map_sidebar_order', $values['map_sidebar_order']);
        \Drupal::state()->set('chado_display_map_overview_order', $values['map_overview_order']);
        \Drupal::state()->set('chado_display_nd_geolocation_sidebar_order', $values['nd_geolocation_sidebar_order']);
        \Drupal::state()->set('chado_display_nd_geolocation_overview_order', $values['nd_geolocation_overview_order']);
        \Drupal::state()->set('chado_display_organism_sidebar_order', $values['organism_sidebar_order']);
        \Drupal::state()->set('chado_display_organism_overview_order', $values['organism_overview_order']);
        \Drupal::state()->set('chado_display_project_sidebar_order', $values['project_sidebar_order']);
        \Drupal::state()->set('chado_display_project_overview_order', $values['project_overview_order']);
        \Drupal::state()->set('chado_display_pub_sidebar_order', $values['pub_sidebar_order']);
        \Drupal::state()->set('chado_display_pub_overview_order', $values['pub_overview_order']);
        \Drupal::state()->set('chado_display_stock_sidebar_order', $values['stock_sidebar_order']);
        \Drupal::state()->set('chado_display_stock_overview_order', $values['stock_overview_order']);
        \Drupal::state()->set('chado_display_stockcollection_sidebar_order', $values['stockcollection_sidebar_order']);
        \Drupal::state()->set('chado_display_stockcollection_overview_order', $values['stockcollection_overview_order']);
        
        // Overview hidden fields
        \Drupal::state()->set('chado_display_analysis_overview_hidden', $values['analysis_overview_hidden']);
        \Drupal::state()->set('chado_display_contact_overview_hidden', $values['contact_overview_hidden']);
        \Drupal::state()->set('chado_display_feature_overview_hidden', $values['feature_overview_hidden']);
        \Drupal::state()->set('chado_display_image_overview_hidden', $values['image_overview_hidden']);
        \Drupal::state()->set('chado_display_library_overview_hidden', $values['library_overview_hidden']);
        \Drupal::state()->set('chado_display_map_overview_hidden', $values['map_overview_hidden']);
        \Drupal::state()->set('chado_display_nd_geolocation_overview_hidden', $values['nd_geolocation_overview_hidden']);
        \Drupal::state()->set('chado_display_organism_overview_hidden', $values['organism_overview_hidden']);
        \Drupal::state()->set('chado_display_project_overview_hidden', $values['project_overview_hidden']);
        \Drupal::state()->set('chado_display_pub_overview_hidden', $values['pub_overview_hidden']);
        \Drupal::state()->set('chado_display_stock_overview_hidden', $values['stock_overview_hidden']);
        \Drupal::state()->set('chado_display_stockcollection_overview_hidden', $values['stockcollection_overview_hidden']);
        
        // Properties
        if (isset($values['analysisprop'])) {
          \Drupal::state()->set('chado_display_analysisprop', $values['analysisprop']);
          \Drupal::state()->set('chado_display_analysisprop_sidebar', $values['analysisprop_sidebar']);
        }
        if (isset($values['contactprop'])) {
          \Drupal::state()->set('chado_display_contactprop', $values['contactprop']);
          \Drupal::state()->set('chado_display_contactprop_sidebar', $values['contactprop_sidebar']);
        }        
        if (isset($values['eimageprop'])) {
          \Drupal::state()->set('chado_display_eimageprop', $values['eimageprop']);
          \Drupal::state()->set('chado_display_eimageprop_sidebar', $values['eimageprop_sidebar']);
        }        
        if (isset($values['featureprop'])) {
          \Drupal::state()->set('chado_display_featureprop', $values['featureprop']);
          \Drupal::state()->set('chado_display_featureprop_sidebar', $values['featureprop_sidebar']);
        }        
        if (isset($values['libraryprop'])) {
          \Drupal::state()->set('chado_display_libraryprop', $values['libraryprop']);
          \Drupal::state()->set('chado_display_libraryprop_sidebar', $values['libraryprop_sidebar']);
        }        
        if (isset($values['featuremapprop'])) {
          \Drupal::state()->set('chado_display_featuremapprop', $values['featuremapprop']);
          \Drupal::state()->set('chado_display_featuremapprop_sidebar', $values['featuremapprop_sidebar']);
        }
        if (isset($values['nd_geolocationprop'])) {
          \Drupal::state()->set('chado_display_nd_geolocationprop', $values['nd_geolocationprop']);
          \Drupal::state()->set('chado_display_nd_geolocationprop_sidebar', $values['nd_geolocationprop_sidebar']);
        }        
        if (isset($values['organismprop'])) {
          \Drupal::state()->set('chado_display_organismprop', $values['organismprop']);
          \Drupal::state()->set('chado_display_organismprop_sidebar', $values['organismprop_sidebar']);
        }        
        if (isset($values['projectprop'])) {
          \Drupal::state()->set('chado_display_projectprop', $values['projectprop']);
          \Drupal::state()->set('chado_display_projectprop_sidebar', $values['projectprop_sidebar']);
        }        
        if (isset($values['pubprop'])) {
          \Drupal::state()->set('chado_display_pubprop', $values['pubprop']);
          \Drupal::state()->set('chado_display_pubprop_sidebar', $values['pubprop_sidebar']);
        }        
        if (isset($values['stockprop'])) {
          \Drupal::state()->set('chado_display_stockprop', $values['stockprop']);
          \Drupal::state()->set('chado_display_stockprop_sidebar', $values['stockprop_sidebar']);
        }        
        if (isset($values['stockcollectionprop'])) {
          \Drupal::state()->set('chado_display_stockcollectionprop', $values['stockcollectionprop']);
          \Drupal::state()->set('chado_display_stockcollectionprop_sidebar', $values['stockcollectionprop_sidebar']);
        }
        Cache::empty();
        \Drupal::service("router.builder")->rebuild();
    }
    
    public function selectAllProps($form, $form_state) {
      $trigger = $form_state->getTriggeringElement();
      $trigger_id = $trigger['#id'];
      $trigger_value = $trigger['#value'];
      $prop_key = $trigger_id . 'prop';
      if ($prop_key == 'imageprop') {
        $prop_key = 'e' . $prop_key;
      }
      if ($prop_key == 'mapprop') {
        $prop_key = 'feature' . $prop_key;
      }
      $element = $form[$trigger_id][$prop_key];
      if ($trigger_value) {
        foreach($element AS $key => $val) {
          if (is_numeric($key) && isset($val['#checked'])) {
            $element[$key]['#checked'] = TRUE;
          }
        }
      }
      else {
        foreach($element AS $key => $val) {
          if (is_numeric($key) && isset($val['#checked'])) {
            $element[$key]['#checked'] = FALSE;
          }
        }
      }
      return $element;
    }
}