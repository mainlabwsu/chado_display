<?php
namespace Drupal\chado_display\Form\Admin;

class NdGeolocationAdminForm {

    protected $statement;

    public function __construct($statement) {
        $this->statement = $statement;
    }

    public function addForm($form = array()) {
        /**
         * NdGeolocation Settings
         */
        $statement = $this->statement;
        $form['nd_geolocation'] = [
            '#type' => 'details',
            '#title' => 'NdGeolocation',
            '#description' => 'Settings for Chado NdGeolocation Page',
        ];
        $dispaly_opts = chado_display_render_info('Drupal\chado_display\Render\Based\NdGeolocation');
        $default_display = \Drupal::state()->get('chado_display_nd_geolocation_render_info', $dispaly_opts);
        $form['nd_geolocation']['nd_geolocation_display'] = array(
          '#type' => 'checkboxes',
          '#title' => 'Display Information in Sidebar',
          '#description' => 'Display selected information about an NdGeolocation.',
          '#options' => $dispaly_opts,
          '#default_value' => $default_display
        );
        $default_sidebar_order = \Drupal::state()->get('chado_display_nd_geolocation_sidebar_order', []);
        $form['nd_geolocation']['nd_geolocation_sidebar_order'] = [
          '#type' => 'textfield',
          '#title' => 'Sidebar Priority Order',
          '#description' => 'Items on the list will show up first in the Sidebar. Separate each item with a comma (,)',
          '#default_value' => $default_sidebar_order
        ];
        $default_overview_order = \Drupal::state()->get('chado_display_nd_geolocation_overview_order', '');
        $form['nd_geolocation']['nd_geolocation_overview_order'] = [
          '#type' => 'textfield',
          '#title' => 'Overview Priority Order',
          '#description' => 'Fields on the list will show up first in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_order
        ];
        $default_overview_hidden = \Drupal::state()->get('chado_display_nd_geolocation_overview_hidden', '');
        $form['nd_geolocation']['nd_geolocation_overview_hidden'] = [
          '#type' => 'textfield',
          '#title' => 'Hide Overview Fields',
          '#description' => 'Fields on the list will not be shown in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_hidden
        ];
        $default_props = \Drupal::state()->get('chado_display_nd_geolocationprop', []);
        $default_props_sidebar = \Drupal::state()->get('chado_display_nd_geolocationprop_sidebar', []);
        $cache_props = \Drupal::state()->get('chado_display_nd_geolocationprop_cache', []);
        $props = [];
        if (count($cache_props) == 0) {
            $ptypes = $statement->base->getPropertyTypes('nd_geolocation');
            foreach ($ptypes AS $type) {
                $props[$type->type_id] = $type->cv . ': ' . $type->name;
            }
            \Drupal::state()->set('chado_display_nd_geolocationprop_cache', $props);
        }
        else {
            $props = $cache_props;
        }
        $form['nd_geolocation']['refreshprop'] = array(
            '#type' => 'button',
            '#value' => 'Refresh Property List: nd_geolocation',
          '#suffix' => '<div id="chado_display_admin_prop_counter"> ' . count($props) . ' properties found</div>'
        );
        if (count($props) > 0) {
          $form['nd_geolocation']['allnd_geolocationprops'] = array(
            '#type' => 'checkbox',
            '#title' => 'Select all properties',
            '#id' => 'nd_geolocation',
            '#ajax' => [
              'callback' => '::selectAllProps',
              'wrapper' => 'chado_display_admin_nd_geolocation_props',
              'effect' => 'fade'
            ]
          );
          $form['nd_geolocation']['nd_geolocationprop'] = array(
              '#type' => 'checkboxes',
              '#title' => 'Show NdGeolocation Properties in Overview',
              '#description' => 'Display checked nd_geolocation properties in the Overview.',
              '#options' => $props,
              '#default_value' => $default_props,
            '#prefix' => '<div id="chado_display_admin_props"><div id="chado_display_admin_nd_geolocation_props">',
            '#suffix' => '</div>'
          );
          $form['nd_geolocation']['nd_geolocationprop_sidebar'] = array(
            '#type' => 'checkboxes',
            '#title' => 'Show NdGeolocation Properties in Sidebar',
            '#description' => 'Display checked nd_geolocation properties as a sidebar item. The Chado Display sidebar has limited space, please select sparsingly.',
            '#options' => $props,
            '#default_value' => $default_props_sidebar,
            '#prefix' => '<div class="chado_display_admin_props_in_sidebar">',
            '#suffix' => '</div></div>'
          );
        }
        return $form;
    }
}