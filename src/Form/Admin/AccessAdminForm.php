<?php 
namespace Drupal\chado_display\Form\Admin;

class AccessAdminForm {
    
    public function addForm($form = array()) {
        /**
         * Access Control Settings
         */
      $data_opt = chado_display_get_extended_data_type();
        $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
        $header = array_merge(['rid' => 'Data Access'], $data_opt);
        
        $form['access'] = [
            '#type' => 'details',
            '#title' => 'Access Control',
            '#open' => TRUE
        ];
        $form['access']['perm_instruction'] = [
            '#markup' => 'The following roles will have access to the Chado data'
        ];
        $form['access']['permission'] = array(
            '#type' => 'table',
            '#header' => $header,
            '#empty' => 'There are no items yet.',
            '#prefix' => '<div id="chado_display_access_permission">',
            '#suffix' => '</div>'
        );
        $default_permissions = \Drupal::state()->get('chado_display_permission');
        foreach ($roles AS $role) {
            foreach($header AS $k => $v) {
                if ($k == 'rid') {
                    $form['access']['permission'][$role->id()][$k] = [
                        '#plain_text' => $role->label()
                    ];
                }
                else {
                    $form['access']['permission'][$role->id()][$k] = [
                        '#type' => 'checkbox',
                        '#default_value' => isset($default_permissions[$role->id()][$k]) ? $default_permissions[$role->id()][$k] : 1
                    ];
                }
            }
        }
        $form['access']['private_instruction'] = [
            '#markup' => 'The following roles will have access to the private data. To make any data private, insert a record of (base_table, record_id) into the \'chado_display_private_data\' table.'
        ];
        $header = array_merge(['rid' => 'Private Access'], chado_display_get_extended_data_type());
        $form['access']['private'] = array(
            '#type' => 'table',
            '#header' => $header,
            '#empty' => 'There are no items yet.',
            '#prefix' => '<div id="chado_display_private_permission">',
            '#suffix' => '</div>'
        );
        $default_private_permissions = \Drupal::state()->get('chado_display_private_permission');
        foreach ($roles AS $role) {
	    $admin_default = $role->id() == 'administrator' ? 1 : 0;
            foreach($header AS $k => $v) {
                if ($k == 'rid') {
                    $form['access']['private'][$role->id()][$k] = [
                        '#plain_text' => $role->label()
                    ];
                }
                else {
                    $form['access']['private'][$role->id()][$k] = [
                        '#type' => 'checkbox',
                        '#default_value' => isset($default_private_permissions[$role->id()][$k]) ? $default_private_permissions[$role->id()][$k] : $admin_default
                    ];
                }
            }
        }
        return $form;       
    }
}
