<?php
namespace Drupal\chado_display\Form\Admin;

class StockCollectionAdminForm {

    protected $statement;

    public function __construct($statement) {
        $this->statement = $statement;
    }

    public function addForm($form = array()) {
        /**
         * StockCollection Settings
         */
        $statement = $this->statement;
        $form['stockcollection'] = [
            '#type' => 'details',
            '#title' => 'StockCollection',
            '#description' => 'Settings for Chado StockCollection Page',
        ];
        $dispaly_opts = chado_display_render_info('Drupal\chado_display\Render\Based\StockCollection');
        $default_display = \Drupal::state()->get('chado_display_stockcollection_render_info', $dispaly_opts);
        $form['stockcollection']['stockcollection_display'] = array(
          '#type' => 'checkboxes',
          '#title' => 'Display Information in Sidebar',
          '#description' => 'Display selected information about a StockCollection.',
          '#options' => $dispaly_opts,
          '#default_value' => $default_display
        );
        $default_sidebar_order = \Drupal::state()->get('chado_display_stockcollection_sidebar_order', []);
        $form['stockcollection']['stockcollection_sidebar_order'] = [
          '#type' => 'textfield',
          '#title' => 'Sidebar Priority Order',
          '#description' => 'Items on the list will show up first in the Sidebar. Separate each item with a comma (,)',
          '#default_value' => $default_sidebar_order
        ];
        $default_overview_order = \Drupal::state()->get('chado_display_stockcollection_overview_order', '');
        $form['stockcollection']['stockcollection_overview_order'] = [
          '#type' => 'textfield',
          '#title' => 'Overview Priority Order',
          '#description' => 'Fields on the list will show up first in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_order
        ];
        $default_overview_hidden = \Drupal::state()->get('chado_display_stockcollection_overview_hidden', '');
        $form['stockcollection']['stockcollection_overview_hidden'] = [
          '#type' => 'textfield',
          '#title' => 'Hide Overview Fields',
          '#description' => 'Fields on the list will not be shown in the Overview table. Separate each field with a comma (,)',
          '#default_value' => $default_overview_hidden
        ];
        $default_props = \Drupal::state()->get('chado_display_stockcollectionprop', []);
        $default_props_sidebar = \Drupal::state()->get('chado_display_stockcollectionprop_sidebar', []);
        $cache_props = \Drupal::state()->get('chado_display_stockcollectionprop_cache', []);
        $props = [];
        if (count($cache_props) == 0) {
            $ptypes = $statement->base->getPropertyTypes('stockcollection');
            foreach ($ptypes AS $type) {
                $props[$type->type_id] = $type->cv . ': ' . $type->name;
            }
            \Drupal::state()->set('chado_display_stockcollectionprop_cache', $props);
        }
        else {
            $props = $cache_props;
        }
        $form['stockcollection']['refreshprop'] = array(
            '#type' => 'button',
            '#value' => 'Refresh Property List: stockcollection',
          '#suffix' => '<div id="chado_display_admin_prop_counter"> ' . count($props) . ' properties found</div>'
        );
        if (count($props) > 0) {
          $form['stockcollection']['allstockcollectionprops'] = array(
            '#type' => 'checkbox',
            '#title' => 'Select all properties',
            '#id' => 'stockcollection',
            '#ajax' => [
              'callback' => '::selectAllProps',
              'wrapper' => 'chado_display_admin_stockcollection_props',
              'effect' => 'fade'
            ]
          );
          $form['stockcollection']['stockcollectionprop'] = array(
              '#type' => 'checkboxes',
              '#title' => 'Show StockCollection Properties in Overview',
              '#description' => 'Display checked stockcollection properties in the Overview.',
              '#options' => $props,
              '#default_value' => $default_props,
            '#prefix' => '<div id="chado_display_admin_props"><div id="chado_display_admin_stockcollection_props">',
            '#suffix' => '</div>'
          );
          $form['stockcollection']['stockcollectionprop_sidebar'] = array(
            '#type' => 'checkboxes',
            '#title' => 'Show StockCollection Properties in Sidebar',
            '#description' => 'Display checked stockcollection properties as a sidebar item. The Chado Display sidebar has limited space, please select sparsingly.',
            '#options' => $props,
            '#default_value' => $default_props_sidebar,
            '#prefix' => '<div class="chado_display_admin_props_in_sidebar">',
            '#suffix' => '</div></div>'
          );
        }
        return $form;
    }
}