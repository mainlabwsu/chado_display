<?php
namespace Drupal\chado_display\Database\Prepared;

use Drupal\chado_display\Database\Chado;

class ImageStatement {

    protected $chado;

    function __construct (Chado $chado) {
        $this->chado = $chado;
    }

    /**
     * Counters
     */
    
    function countContact ($eimage_id) {
      $counter = $this->chado->count(
          'contact_image CI',
          ['C.contact_id', 'C.name', 'V.name AS type'],
          ['inner' =>
            [
              'contact C' => 'C.contact_id = CI.contact_id',
              'cvterm V' => 'V.cvterm_id = C.type_id'
            ]
          ],
          ['CI.eimage_id' => $eimage_id]
          );
      return $counter;
    }
    
    function countOrganism ($eimage_id) {
      $counter = $this->chado->count(
          'organism_image OI',
          ['O.organism_id', 'genus', 'species', 'common_name', 'genus || \' \' || species AS organism'],
          ['inner' =>
            [
              'organism O' => 'O.organism_id = OI.organism_id',
            ]
          ],
          ['OI.eimage_id' => $eimage_id]
          );
      return $counter;
    }
    
    function countPub($eimage_id) {
      $counter = $this->chado->count(
          'pub_image PI',
          ['P.pub_id', 'title', 'series_name', 'pyear', 'V.name AS type'],
          ['inner' =>
            [
              'pub P' => 'P.pub_id = PI.pub_id',
              'cvterm V' => 'V.cvterm_id = P.type_id',
            ]
          ],
          ['PI.eimage_id' => $eimage_id]
          );
      return $counter;
    }
    
    function countProject($eimage_id) {
      $counter = $this->chado->count(
          'project_image PC',
          ['P.project_id', 'P.name'],
          ['inner' =>
            [
              'project P' => 'P.project_id = PC.project_id',
            ]
          ],
          ['PC.eimage_id' => $eimage_id]
          );
      return $counter;
    }
    
    function countStock($eimage_id) {
      $counter = $this->chado->count(
          'stock_image SE',
          ['S.stock_id', 'S.name', 'S.uniquename', 'V.name AS type'],
          ['inner' =>
            [
              'stock S' => 'S.stock_id = SE.stock_id',
              'cvterm V' => 'V.cvterm_id = S.type_id',
            ]
          ],
          ['SE.eimage_id' => $eimage_id]
          );
      return $counter;
    }
    
    function countFeature($eimage_id) {
      $counter = $this->chado->count(
          'feature_image FI',
          ['F.feature_id', 'F.name', 'F.uniquename', 'V.name AS type'],
          ['inner' =>
            [
              'feature F' => 'F.feature_id = FI.feature_id',
              'cvterm V' => 'V.cvterm_id = F.type_id',
            ]
          ],
          ['FI.eimage_id' => $eimage_id]
          );
      return $counter;
    }
    
    function countTrait($eimage_id) {
      $sql = "
        SELECT 
          V.name, 
          CI.cvterm_id, 
          TYPE.name AS type 
        FROM chado.cvterm_image CI 
        INNER JOIN chado.cvterm V ON V.cvterm_id = CI.cvterm_id
        INNER JOIN chado.cvterm_relationship CR ON CR.subject_id = V.cvterm_id
        INNER JOIN chado.cvterm TYPE on TYPE.cvterm_id = CR.type_id 
        WHERE eimage_id = :eimage_id
        AND TYPE.name = 'is_a'
      ";
      $counter = $this->chado->countQuery($sql, [':eimage_id' => $eimage_id]);
      return $counter;
    }
    
    function countTraitDescriptor($eimage_id) {
      $sql = "
        SELECT
          V.name,
          CI.cvterm_id,
          TYPE.name AS type
        FROM chado.cvterm_image CI
        INNER JOIN chado.cvterm V ON V.cvterm_id = CI.cvterm_id
        INNER JOIN chado.cvterm_relationship CR ON CR.subject_id = V.cvterm_id
        INNER JOIN chado.cvterm TYPE on TYPE.cvterm_id = CR.type_id
        WHERE eimage_id = :eimage_id
        AND TYPE.name = 'belongs_to'
      ";
      $counter = $this->chado->countQuery($sql, [':eimage_id' => $eimage_id]);
      return $counter;
    }
}