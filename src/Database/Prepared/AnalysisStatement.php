<?php 
namespace Drupal\chado_display\Database\Prepared;

use Drupal\chado_display\Database\Chado;

class AnalysisStatement {
    
    protected $chado;
    
    function __construct (Chado $chado) {
        $this->chado = $chado;
    }
    /**
     * Getters
     */
    function getJBrowseURL($analysis_id) {
        $jbrowse_url = $this->chado->getFirstObject(
            'analysisprop AP',
            ['value'],
            [
                'inner' => [
                    'cvterm V' => 'V.cvterm_id = AP.type_id',
                    'cv' => 'cv.cv_id = V.cv_Id'
                ]
            ],
            ['V.name' => 'JBrowse URL', 'cv.name' => 'analysis_property', 'analysis_id' => $analysis_id]
            );
        if ($jbrowse_url) {
            return $jbrowse_url->value;
        }
        else {
            return NULL;
        }
    }
    
    /**
     * Counters
     */
    function countFeature($analysis_id) {
        $counter = $this->chado->count(
            'analysisfeature AF',
            ['F.feature_id', 'F.name', 'F.uniquename', 'V.name AS type', "genus || ' ' || species AS organism", 'F.organism_id'],
            ['inner' =>
                [
                    'feature F' => 'F.feature_id = AF.feature_id',
                    'cvterm V' => 'V.cvterm_id = F.type_id',
                    'organism O' => 'O.organism_id = F.organism_id'
                ]
            ],
            ['AF.analysis_id' => $analysis_id]
            );
        return $counter;
    }
}