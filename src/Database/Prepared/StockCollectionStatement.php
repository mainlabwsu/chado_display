<?php
namespace Drupal\chado_display\Database\Prepared;

use Drupal\chado_display\Database\Chado;

class StockCollectionStatement {

    protected $chado;

    function __construct (Chado $chado) {
        $this->chado = $chado;
    }

    /**
     * Getters
     */
    function getStockCollection($id) {
        $stockcollection = $this->chado->getFirstObject(
            'stockcollection S',
            ['stockcollection_id', 'S.name', 'uniquename', 'S.contact_id', 'V.name AS type', 'C.name AS contact'],
            [
              'inner' => ['cvterm V' => 'cvterm_id = S.type_id'],
              'left' => ['contact C' => 'C.contact_id = S.contact_id']
            ],
            ['stockcollection_id' => $id]
            );
        return $stockcollection;
    }
    
    /**
     * Counters
     */
    function countStock($stockcollection_id) {
        $counter = $this->chado->count(
            'stockcollection_stock SS',
            ['S.stock_id', 'S.name', 'S.uniquename', 'V.name AS type'],
            ['inner' =>
                [
                    'stock S' => 'S.stock_id = SS.stock_id',
                    'cvterm V' => 'S.type_id = V.cvterm_id'
                ]
            ],
            ['SS.stockcollection_id' => $stockcollection_id]
            );
        return $counter;
    }
}