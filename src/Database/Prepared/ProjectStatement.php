<?php
namespace Drupal\chado_display\Database\Prepared;

use Drupal\chado_display\Database\Chado;

class ProjectStatement {

    protected $chado;

    function __construct (Chado $chado) {
        $this->chado = $chado;
    }

    function countContact($project_id) {
        $counter = $this->chado->count(
            'project_contact PC',
            ['C.contact_id', 'C.name', 'V.name AS type'],
            ['inner' =>
                [
                    'contact C' => 'C.contact_id = PC.contact_id',
                    'cvterm V' => 'V.cvterm_id = C.type_id'
                ]
            ],
            ['PC.project_id' => $project_id]
            );
        return $counter;
    }
    
    function countPub($project_id) {
        $counter = $this->chado->count(
            'project_pub PP',
            ['P.pub_id', 'title', 'series_name', 'pyear', 'V.name AS type'],
            ['inner' =>
                [
                    'pub P' => 'P.pub_id = PP.pub_id',
                    'cvterm V' => 'V.cvterm_id = P.type_id',
                ]
            ],
            ['PP.project_id' => $project_id]
            );
        return $counter;
    }
    
    function countQTL($project_id) {
        $sql = "
            SELECT
                F.uniquename AS LG,
                (SELECT max (object_id) FROM chado.feature_relationship WHERE subject_id = F2.feature_id AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'instance_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'relationship'))) AS feature_id,
                F2.uniquename AS qtl,
                F2.feature_id AS qtl_feature_id,
                (SELECT name FROM chado.cvterm WHERE cvterm_id = F2.type_id) AS type,
                FPP.value AS start,
                FPP2.value AS stop,
                FPP3.value AS peak
              FROM chado.featurepos FP
              INNER JOIN chado.feature_project FM ON FM.feature_id = FP.feature_id
              INNER JOIN chado.feature F ON F.feature_id = FP.map_feature_id
              INNER JOIN chado.feature F2 ON F2.feature_id = FP.feature_id
              LEFT JOIN chado.featureposprop FPP ON FP.featurepos_id = FPP.featurepos_id AND FPP.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'start' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
              LEFT JOIN chado.featureposprop FPP2 ON FP.featurepos_id = FPP2.featurepos_id AND FPP2.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'stop' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
              LEFT JOIN chado.featureposprop FPP3 ON FP.featurepos_id = FPP3.featurepos_id AND FPP3.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'qtl_peak' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
              WHERE FM.project_id = :project_id
              AND (SELECT name FROM chado.cvterm WHERE cvterm_id = F2.type_id) = 'QTL'
        ";
        return $this->chado->countQuery($sql, [':project_id' => $project_id]);
    }
    
    function countStock($project_id) {
      $sql = "
          SELECT S.stock_id, max(S.name) AS name, max(S.uniquename) AS uniquename, (SELECT name FROM chado.cvterm WHERE cvterm_id = S.type_id) AS type
          FROM chado.stock S
          INNER JOIN chado.stock_relationship SR on SR.object_id = S.stock_id
          INNER JOIN chado.nd_experiment_stock NES on NES.stock_id = SR.subject_id
          INNER JOIN chado.nd_experiment_project NEP on NEP.nd_experiment_id = NES.nd_experiment_id
          WHERE NEP.project_id = :project_id GROUP BY S.stock_id
        ";
      return $this->chado->countQuery($sql, [':project_id' => $project_id]);
    }
    
    function countTraitDescriptors($project_id) {
      $sql = "
          SELECT DISTINCT 
            C1.cvterm_id,
            CASE
              WHEN (SELECT value FROM chado.cvprop WHERE cv_id = C1.cv_id AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'searchable' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))) IS NOT NULL
              THEN   (SELECT value FROM chado.cvprop WHERE cv_id = C1.cv_id AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'searchable' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')))
              ELSE     (SELECT name FROM chado.cv WHERE cv_id = C1.cv_id)
              END
            AS group,
            C1.name AS descriptor
          FROM chado.phenotype P
          INNER JOIN chado.nd_experiment_phenotype NEP ON P.phenotype_id = NEP.phenotype_id
          INNER JOIN chado.nd_experiment_project NEJ ON NEP.nd_experiment_id = NEJ.nd_experiment_id
          INNER JOIN chado.cvterm C1 ON C1.cvterm_id = P.attr_id 
          WHERE project_id = :project_id
        ";
      return $this->chado->countQuery($sql, [':project_id' => $project_id]);
    }
    
    function countMaps($project_id) {
      $sql = "
          SELECT DISTINCT
            FM.featuremap_id, FM.name 
          FROM chado.project_pub PP 
          INNER JOIN chado.featuremap_pub FMP ON PP.pub_id = FMP.pub_Id
          INNER JOIN chado.featuremap FM ON FM.featuremap_id = FMP.featuremap_id
          WHERE project_id = :project_id
        ";
      return $this->chado->countQuery($sql, [':project_id' => $project_id]);
    }
    
    function countTraits($project_id) {
      $sql = "
          SELECT DISTINCT
            V.cvterm_id, V.name AS trait
          FROM chado.cvterm V
          INNER JOIN chado.feature_cvterm FV ON FV.cvterm_id = V.cvterm_id
          INNER JOIN chado.feature_project FJ ON FJ.feature_id = FV.feature_id
          WHERE FJ.project_id = :project_id
        ";
      return $this->chado->countQuery($sql, [':project_id' => $project_id]);
    }
}