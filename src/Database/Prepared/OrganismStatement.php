<?php 
namespace Drupal\chado_display\Database\Prepared;

use Drupal\chado_display\Database\Chado;

class OrganismStatement {
    
    protected $chado;
    
    function __construct (Chado $chado) {
        $this->chado = $chado;
    }
    
    /**
     * Getters
     */
    
    
    /**
     * Counters
     */
    function countDbxref($organism_id) {
        $counter = $this->chado->count(
            'organism_dbxref OX',
            ['db.name AS db', 'accession', 'urlprefix'],
            ['inner' =>
                [
                    'dbxref X' => 'X.dbxref_id = OX.dbxref_id',
                    'db' => 'db.db_id = X.db_id'
                ]
            ],
            ['OX.organism_id' => $organism_id]
            );
        return $counter;
    }
    
    function countFeature($organism_id) {
        $counter = $this->chado->count(
            'feature F',
            ['F.feature_id', 'F.name', 'F.uniquename', 'V.name AS type'],
            ['inner' =>
                [
                    'cvterm V' => 'V.cvterm_id = F.type_id',
                ]
            ],
            ['F.organism_id' => $organism_id]
            );
        return $counter;
    }
    
    function countImage($organism_id) {
      $counter = $this->chado->count(
          'organism_image OI',
          ['I.eimage_id', 'eimage_data', 'eimage_type', 'image_uri'],
          [
            'inner' => [
              'eimage I' => 'I.eimage_id = OI.eimage_id',
            ]
          ],
          ['OI.organism_id' => $organism_id]
          );
      return $counter;
    }
    
    function countMap($organism_id) {
      $counter = $this->chado->count(
          'featuremap_organism FO',
          ['organism_id', 'FO.featuremap_id', 'FM.name AS map', 'V.name AS unit_type'],
          [
            'inner' => [
              'featuremap FM' => 'FM.featuremap_id = FO.featuremap_id',
              'cvterm V' => 'V.cvterm_id = unittype_id'
            ]
          ],
          ['FO.organism_id' => $organism_id]
       );
      return $counter;
    }
    
    function countStock($organism_id) {
      $counter = $this->chado->count(
          'stock S',
          ['S.stock_id', 'S.name', 'S.uniquename', 'V.name AS type'],
          ['inner' =>
            [
              'cvterm V' => 'V.cvterm_id = S.type_id',
            ]
          ],
          ['S.organism_id' => $organism_id]
          );
      return $counter;
    }
}