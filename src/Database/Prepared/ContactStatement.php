<?php
namespace Drupal\chado_display\Database\Prepared;

use Drupal\chado_display\Database\Chado;

class ContactStatement {

    protected $chado;

    function __construct (Chado $chado) {
        $this->chado = $chado;
    }

    function countProject($contact_id) {
        $counter = $this->chado->count(
            'project_contact PC',
            ['P.project_id', 'P.name'],
            ['inner' =>
                [
                    'project P' => 'P.project_id = PC.project_id',
                ]
            ],
            ['PC.contact_id' => $contact_id]
            );
        return $counter;
    }
}