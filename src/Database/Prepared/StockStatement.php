<?php
namespace Drupal\chado_display\Database\Prepared;

use Drupal\chado_display\Database\Chado;

class StockStatement {

    protected $chado;

    function __construct (Chado $chado) {
        $this->chado = $chado;
    }

    /**
     * Getters
     */

    function getStock($id) {
        $stock = $this->chado->getFirstObject(
            'stock S',
            ['stock_id', 'S.name', 'uniquename', 'description', 'S.type_id', 'S.organism_id', 'V.name AS type', 'genus || \' \' || species AS organism'],
            [
              'inner' => ['cvterm V' => 'cvterm_id = S.type_id'],
              'left' => ['organism O' => 'S.organism_id = O.organism_id']
            ],
            ['stock_id' => $id]
            );
        return $stock;
    }
    
    function getParent($id, $parent_type = 'maternal') {
        $stock = $this->chado->getFirstObject(
            'stock S',
            ['stock_id', 'S.name', 'uniquename', 'description', 'S.type_id', 'S.organism_id', 'V.name AS type', 'genus || \' \' || species AS organism'],
            ['inner' =>
                [
                    'organism O' => 'S.organism_id = O.organism_id',
                    'cvterm V' => 'cvterm_id = S.type_id',
                    'stock_relationship SR' => 'SR.subject_id = S.stock_id',
                    'cvterm RT' => 'RT.cvterm_id = SR.type_id',
                    'cv' => 'cv.cv_id = RT.cv_id'
                ],
            ],
            [
                'object_id' => $id,
                'RT.name' => 'is_a_' . $parent_type . '_parent_of',
                'cv.name' => 'MAIN'
            ]
            );
        return $stock;
    }

    /**
     * Counters
     */
    function countDbxref($stock_id) {
        $q1 = $this->chado->getQuery(
            'stock_dbxref SX',
            ['db.name AS db', 'accession', 'urlprefix'],
            ['inner' =>
                [
                    'dbxref X' => 'X.dbxref_id = SX.dbxref_id',
                    'db' => 'db.db_id = X.db_id'
                ]
            ],
            ['SX.stock_id' => $stock_id]
            );
        $q2 = $this->chado->getQuery(
            'stock S',
            ['db.name AS db', 'accession', 'urlprefix'],
            ['inner' =>
                [
                    'dbxref X' => 'X.dbxref_id = S.dbxref_id',
                    'db' => 'db.db_id = X.db_id'
                ]
            ],
            ['S.stock_id' => $stock_id]
            );
        $sql = $q1->sql . ' UNION ' . $q2->sql;
        $args = array_merge($q1->args, $q2->args);
        $counter = $this->chado->countQuery($sql, $args);
        return $counter;
    }
    
    function countObjectStocks($stock_id, $relcvterm, $relcv = NULL, $stocktype = NULL) {
        $conditions = [
            'object_id' => $stock_id, 
            'RT.name' => $relcvterm, 
        ];
        if ($relcv) {
            $conditions ['cv.name'] = $relcv;
        }
        if ($stocktype) {
            $conditions['V.name'] = $stocktype;
        }
        $counter = $this->chado->count(
            'stock S',
            ['S.stock_id', 'uniquename', 'S.name', 'genus || \' \' || species AS organism', 'V.name AS type', 'S.organism_id'],
            [
                'inner' => [
                    'organism O' => 'O.organism_id = S.organism_id',
                    'cvterm V' => 'V.cvterm_id = S.type_id',
                    'stock_relationship SR' => 'SR.subject_id = S.stock_id',
                    'cvterm RT' => 'RT.cvterm_id = SR.type_id',
                    'cv' => 'cv.cv_id = RT.cv_id'
                ]
            ],
            $conditions
            );
        return $counter;
    }

    function countSubjectStocks($stock_id, $relcvterm, $relcv = NULL, $stocktype = NULL) {
        $conditions = [
            'subject_id' => $stock_id,
            'RT.name' => $relcvterm,
        ];
        if ($relcv) {
            $conditions ['cv.name'] = $relcv;
        }
        if ($stocktype) {
            $conditions['V.name'] = $stocktype;
        }
        $counter = $this->chado->count(
            'stock S',
            ['S.stock_id', 'uniquename', 'S.name', 'genus || \' \' || species AS organism', 'V.name AS type', 'S.organism_id'],
            [
                'inner' => [
                    'organism O' => 'O.organism_id = S.organism_id',
                    'cvterm V' => 'V.cvterm_id = S.type_id',
                    'stock_relationship SR' => 'SR.object_id = S.stock_id',
                    'cvterm RT' => 'RT.cvterm_id = SR.type_id',
                    'cv' => 'cv.cv_id = RT.cv_id'
                ]
            ],
            $conditions
            );
        return $counter;
    }

    function countObjectRelationship($stock_id) {
        $counter = $this->chado->count(
            'stock_relationship SR',
            ['SUB.stock_id', 'SUB.name AS subject', 'O.organism_id', 'O.genus || \' \' || O.species AS organism', 'OV.name AS type', 'V.name AS relationship'],
            ['inner' =>
                [
                    'cvterm V' => 'V.cvterm_id = SR.type_id',
                    'stock SUB' => 'SUB.stock_id = SR.subject_id',
                    'organism O' => 'O.organism_id = SUB.organism_id',
                    'cvterm OV' => 'OV.cvterm_id = SUB.type_id'
                ]
            ],
            ['object_id' => $stock_id],
            );
        return $counter;
    }
    
    function countSubjectRelationship($stock_id) {
        $counter = $this->chado->count(
            'stock_relationship SR',
            ['OBJ.stock_id', 'OBJ.name AS object', 'O.organism_id', 'O.genus || \' \' || O.species AS organism', 'OV.name AS type', 'V.name AS relationship'],
            ['inner' =>
                [
                    'cvterm V' => 'V.cvterm_id = SR.type_id',
                    'stock OBJ' => 'OBJ.stock_id = SR.object_id',
                    'organism O' => 'O.organism_id = OBJ.organism_id',
                    'cvterm OV' => 'OV.cvterm_id = OBJ.type_id'
                ]
            ],
            ['subject_id' => $stock_id],
            );
        return $counter;
    }

    function countCvterm($stock_id) {
        $counter = $this->chado->count(
            'stock_cvterm SV',
            ['C.name AS cv', 'V.name AS cvterm', 'V.definition', 'D.name AS db', 'D.urlprefix', 'X.accession'],
            ['inner' =>
                [
                    'cvterm V' => 'V.cvterm_id = SV.cvterm_id',
                    'cv C' => 'C.cv_id = V.cv_id',
                    'dbxref X' => 'X.dbxref_id = V.dbxref_id',
                    'db D' => 'D.db_id = X.db_id'
                ]
            ],
            ['SV.stock_id' => $stock_id],
            ['db', 'cv', 'accession']
            );
        return $counter;
    }
    
    function countCollection($stock_id) {
        $counter = $this->chado->count(
            'stockcollection_stock SS',
            ['S.stockcollection_id', 'S.name', 'S.uniquename', 'V.name AS type', 'C.name AS contact', 'C.contact_id'],
            ['inner' =>
                [
                    'stockcollection S' => 'S.stockcollection_id = SS.stockcollection_id',
                    'cvterm V' => 'V.cvterm_id = S.type_id',
                    'contact C' => 'C.contact_id = S.contact_id'
                ]
            ],
            ['SS.stock_id' => $stock_id]
            );
        return $counter;
    }
    
    function countPub($stock_id) {
        $counter = $this->chado->count(
            'stock_pub SP',
            ['P.pub_id', 'title', 'series_name', 'pyear', 'V.name AS type'],
            ['inner' =>
                [
                    'pub P' => 'P.pub_id = SP.pub_id',
                    'cvterm V' => 'V.cvterm_id = P.type_id',
                ]
            ],
            ['SP.stock_id' => $stock_id]
            );
        return $counter;
    }

    function countImage($stock_id) {
        $counter = $this->chado->count(
            'stock_image SI',
            ['I.eimage_id', 'I.eimage_data', 'I.eimage_type'],
            ['inner' =>
                [
                    'eimage I' => 'I.eimage_id = SI.eimage_id',
                ]
            ],
            ['SI.stock_id' => $stock_id]
            );
        return $counter;
    }

    function countLibrary($stock_id) {
        $counter = $this->chado->count(
            'library_stock LS',
            ['L.library_id', 'L.name', 'L.uniquename', 'V.name AS type', 'L.organism_id', "genus || ' ' || species AS organism"],
            ['inner' =>
                [
                    'library L' => 'L.library_id = LS.library_id',
                    'organism O' => 'O.organism_id = L.organism_id',
                    'cvterm V' => 'V.cvterm_id = L.type_id'
                ]
            ],
            ['LS.stock_id' => $stock_id]
            );
        return $counter;
    }
    
    function countSynonym($stock_id) {
        $counter = $this->chado->count(
            'stock_synonym SS',
            ['S.synonym_id', 'S.name', 'V.name AS type'],
            ['inner' =>
                [
                    'synonym S' => 'S.synonym_id = SS.synonym_id',
                    'cvterm V' => 'V.cvterm_id = S.type_id',
                ]
            ],
            ['SS.stock_id' => $stock_id]
            );
        return $counter;
    }
    
    function countPopulationMap($stock_id) {
        $sql = "
            SELECT
               M.featuremap_id,
               M.name AS map,
               (SELECT value FROM chado.featuremapprop WHERE type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'population_type' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')) AND featuremap_id = M.featuremap_id) AS pop_type,
               (SELECT value FROM chado.featuremapprop WHERE type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'genome_group' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')) AND featuremap_id = M.featuremap_id) AS genome_group,
               (SELECT value FROM chado.featuremapprop WHERE type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'map_type' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')) AND featuremap_id = M.featuremap_id) AS map_type
           FROM chado.featuremap M
           INNER JOIN chado.featuremap_stock MS ON M.featuremap_id = MS.featuremap_id
           WHERE MS.stock_id = :stock_id1
           OR MS.stock_id in
           (SELECT SR.object_id FROM chado.stock S1
               INNER JOIN chado.stock_relationship SR ON S1.stock_id = SR.subject_id
               WHERE SR.type_id IN
                  (SELECT cvterm_id
                   FROM chado.cvterm
                   WHERE (name = 'is_a_maternal_parent_of'
                   OR name = 'is_a_paternal_parent_of') AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
               AND S1.stock_id = :stock_id2)
        ";
        $counter = $this->chado->countQuery($sql, [':stock_id1' => $stock_id, ':stock_id2' => $stock_id]);
        return $counter;
    }
    
    function countPhenotypicData($stock_id) {
        $sql = "
          SELECT
            P.uniquename, 
            P.value, 
            J.project_id,
            J.name AS project, 
            GEO.nd_geolocation_id, 
            GEO.environment, 
            SP.value AS replications, 
            V.name AS descriptor
          FROM chado.nd_experiment_phenotype NEP
          INNER JOIN chado.phenotype P ON NEP.phenotype_id = P.phenotype_id
          INNER JOIN chado.cvterm V ON V.cvterm_id = P.attr_id
          INNER JOIN chado.nd_experiment_stock NES ON NES.nd_experiment_id = NEP.nd_experiment_id
          LEFT JOIN chado.nd_experiment_project NEJ ON NES.nd_experiment_id = NEJ.nd_experiment_id
          LEFT JOIN chado.project J ON J.project_id = NEJ.project_id
          INNER JOIN chado.nd_experiment NE ON NE.nd_experiment_id = NEP.nd_experiment_id
          LEFT JOIN
          (SELECT
             NG.nd_geolocation_id,
                 NGP.value AS environment
           FROM chado.nd_geolocation NG
           INNER JOIN chado.nd_geolocationprop NGP ON NG.nd_geolocation_id = NGP.nd_geolocation_id
           AND NGP.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'site_code' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
          ) GEO ON GEO.nd_geolocation_id = NE.nd_geolocation_id
              LEFT JOIN chado.stockprop SP ON NES.stock_id = SP.stock_id AND SP.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'rep' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
          WHERE NES.stock_id = :stock_id OR NES.stock_id IN (SELECT subject_id FROM chado.stock_relationship WHERE object_id = :object_id
          AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'sample_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')))
              ORDER BY project, environment, replications, uniquename
          ";
        $counter = $this->chado->countQuery($sql, [':stock_id' => $stock_id, ':object_id' => $stock_id]);
        return $counter;
    }
    
    function countSSRGenotypeData($stock_id) {
        $sql = "
            SELECT
                G.uniquename AS descriptor, 
                G.description AS genotype, 
                J.project_id,
                J.name AS project, 
                F.organism_id, 
                F.feature_id,
                F.name AS marker,
                (SELECT name FROM chado.cvterm WHERE cvterm_id = F.type_id) AS marker_type
          FROM chado.nd_experiment_genotype NEG
          INNER JOIN chado.genotype G ON NEG.genotype_id = G.genotype_id
          INNER JOIN chado.feature_genotype FG ON G.genotype_id = FG.genotype_id
          INNER JOIN chado.feature F ON F.feature_id = FG.feature_id
          INNER JOIN chado.nd_experiment_stock NES ON NES.nd_experiment_id = NEG.nd_experiment_id
          LEFT JOIN chado.nd_experiment_project NEJ ON NES.nd_experiment_id = NEJ.nd_experiment_id
          LEFT JOIN chado.project J ON J.project_id = NEJ.project_id
          WHERE stock_id IN
            (SELECT stock_id
             FROM chado.stock S
             INNER JOIN chado.stock_relationship SR ON S.stock_id = SR.subject_id
             WHERE SR.type_id =
                (SELECT cvterm_id FROM chado.cvterm WHERE name = 'sample_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
             AND SR.object_id = :object_id)
        ";
        $counter = $this->chado->countQuery($sql, [':object_id' => $stock_id]);
        return $counter;
    }
    
    function countSNPGenotypeData($stock_id) {
        $sql = "
            SELECT
              P.project_id,
              P.name AS project,
              F.feature_id,
              F.name AS marker,
              F.uniquename AS marker_uniquename,
              (SELECT max(value) FROM chado.featureprop WHERE feature_id = F.feature_id AND type_id IN (SELECT cvterm_id FROM chado.cvterm WHERE name = 'SNP')) AS allele,
              (SELECT description FROM chado.genotype WHERE genotype_id = GC.genotype_id) AS genotype
            FROM chado.genotype_call GC
            INNER JOIN chado.project P ON P.project_id = GC.project_id
            INNER JOIN chado.feature F ON F.feature_id = GC.feature_id
            INNER JOIN (SELECT * FROM chado.projectprop PP WHERE type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'project_type' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))) PTYPE ON PTYPE.project_id = P.project_id
            INNER JOIN (SELECT * FROM chado.projectprop PP WHERE type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'sub_type' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))) SUBTYPE ON SUBTYPE.project_id = P.project_id
            WHERE
              PTYPE.value = 'genotype'
            AND
              SUBTYPE.value = 'SNP'
            AND stock_id = :stock_id
        ";
        $counter = $this->chado->countQuery($sql, [':stock_id' => $stock_id]);
        return $counter;
    }
    
    function countFeature($stock_id) {
        $counter = $this->chado->count(
            'feature_stock FS',
            ['F.feature_id', 'F.name', 'F.uniquename', 'V.name AS type', "genus || ' ' || species AS organism", 'F.organism_id'],
            ['inner' =>
                [
                    'feature F' => 'F.feature_id = FS.feature_id',
                    'cvterm V' => 'V.cvterm_id = F.type_id',
                    'organism O' => 'O.organism_id = F.organism_id'
                ]
            ],
            ['FS.stock_id' => $stock_id]
            );
        return $counter;
    }
    
    function countQTL($stock_id) {
      $sql = "
        SELECT
            F.feature_id,
            F.name,
            F.uniquename,
            J.project_id,
            J.name AS project
        FROM chado.stock_pub SP
        INNER JOIN chado.feature_pub FP ON SP.pub_id = FP.pub_id
        INNER JOIN chado.feature F ON F.feature_id = FP.feature_id 
        AND F.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'QTL' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'sequence'))
        LEFT JOIN chado.feature_project FJ ON FJ.feature_id = F.feature_id
        LEFT JOIN chado.project J ON J.project_id = FJ.project_id
        WHERE SP.stock_id = :stock_id
        ";
      $counter = $this->chado->countQuery($sql, [':stock_id' => $stock_id]);
      return $counter;
    }
}