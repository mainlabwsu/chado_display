<?php
namespace Drupal\chado_display\Database\Prepared;

use Drupal\chado_display\Database\Chado;

class FeatureStatement {

    protected $chado;

    function __construct (Chado $chado) {
        $this->chado = $chado;
    }

    /**
     * Getters
     */

    function getFeature($id) {
        $feature = $this->chado->getFirstObject(
            'feature F',
            ['feature_id', 'F.name', 'uniquename', 'F.type_id', 'F.organism_id', 'seqlen', 'V.name AS type', 'genus || \' \' || species AS organism'],
            ['inner' =>
                [
                    'organism O' => 'F.organism_id = O.organism_id',
                    'cvterm V' => 'cvterm_id = F.type_id'
                ],
            ],
            ['feature_id' => $id]
            );
        return $feature;
    }
    
    function getPopulation($id) {
        $sql = "
            SELECT
              POP.stock_id,
              POP.uniquename,
              POP.maternal,
              POP.mat_stock_id,
              POP.paternal,
              POP.pat_stock_id
           FROM chado.feature QTL
           LEFT JOIN
              (SELECT feature_id, FM.name, FM.featuremap_id
               FROM chado.featuremap FM
                 INNER JOIN chado.featurepos FP ON FM.featuremap_id = FP.featuremap_id
              ) MAP ON MAP.feature_id = QTL.feature_id
           LEFT JOIN
              (SELECT
                 S.stock_id,
                 featuremap_id,
                 S.uniquename,
                 MAT.uniquename as maternal,
                 MAT.stock_id AS mat_stock_id,
                 PAT.uniquename as paternal,
                 PAT.stock_id AS pat_stock_id
               FROM chado.stock S
                 INNER JOIN chado.featuremap_stock FS ON S.stock_id = FS.stock_id
                 LEFT JOIN (
                   SELECT uniquename, object_id, MP.stock_id
                   FROM chado.stock MP
                     INNER JOIN chado.stock_relationship SR ON MP.stock_id = SR.subject_id
                   WHERE SR.type_id = (
                     SELECT cvterm_id
                     FROM chado.cvterm
                     WHERE name = 'is_a_maternal_parent_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'
                   )
                 )
               ) MAT ON MAT.object_id = S.stock_id
               LEFT JOIN
                 (SELECT uniquename, object_id, PP.stock_id
                  FROM chado.stock PP
                    INNER JOIN chado.stock_relationship SR2 ON PP.stock_id = SR2.subject_id
                  WHERE SR2.type_id = (
                    SELECT cvterm_id
                    FROM chado.cvterm
                    WHERE name = 'is_a_paternal_parent_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')
                 )
               ) PAT ON PAT.object_id = S.stock_id
              ) POP ON POP.featuremap_id = MAP.featuremap_id
           WHERE QTL.feature_id = :feature_id
        ";
        $objs = $this->chado->getQueryObjects($sql, [':feature_id' => $id]);
        return is_array($objs) && count($objs) > 0 ? $objs[0] : NULL;
    }
    
    function getMapInfo($id) {
      $sql = "
            SELECT 
          FM.featuremap_id,
          FM.name AS map,
          (SELECT name FROM chado.feature WHERE feature_id = FP.map_feature_id) AS LG,
          (SELECT value FROM chado.featureposprop WHERE featurepos_id = FP.featurepos_id AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'start' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')) LIMIT 1) AS start,
          (SELECT value FROM chado.featureposprop WHERE featurepos_id = FP.featurepos_id AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'stop' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')) LIMIT 1) AS stop
        FROM chado.featurepos FP
        INNER JOIN chado.featuremap FM ON FP.featuremap_id = FM.featuremap_id
        WHERE FP.feature_id = :feature_id
        LIMIT 1
        ";
      $objs = $this->chado->getQueryObjects($sql, [':feature_id' => $id]);
      return is_array($objs) && count($objs) > 0 ? $objs[0] : NULL;
    }
    
    function getAlias($id) {
      $sql = "
           SELECT name
    FROM chado.synonym S
      INNER JOIN chado.feature_synonym FS ON S.synonym_id = FS.synonym_id
    WHERE FS.feature_id = :feature_id
        LIMIT 1
        ";
      $objs = $this->chado->getQueryObjects($sql, [':feature_id' => $id]);
      return is_array($objs) && count($objs) > 0 ? $objs[0] : NULL;
    }
    
    function getGwasMarker($id) {
      $sql = "
            SELECT
       MARKER.feature_id,
       MARKER.uniquename AS gwas_marker
     FROM chado.feature GWAS
     INNER JOIN chado.feature_relationship FR ON GWAS.feature_id = FR.object_id
     INNER JOIN chado.feature MARKER ON MARKER.feature_id = FR.subject_id
     INNER JOIN chado.cvterm V ON V.cvterm_id = FR.type_id
     WHERE V.name = 'associated_with' AND V.cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'sequence')
     AND MARKER.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'genetic_marker' AND V.cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'sequence'))
     AND GWAS.feature_id = :feature_id
        LIMIT 1
        ";
      $objs = $this->chado->getQueryObjects($sql, [':feature_id' => $id]);
      return is_array($objs) && count($objs) > 0 ? $objs[0] : NULL;
    }

    function getProject($id) {
      $sql = "
          SELECT P.project_id, P.name FROM chado.project P
          INNER JOIN chado.feature_project FP ON FP.project_id = P.project_id
          WHERE feature_id = :feature_id
        LIMIT 1
        ";
      $objs = $this->chado->getQueryObjects($sql, [':feature_id' => $id]);
      return is_array($objs) && count($objs) > 0 ? $objs[0] : NULL;
    }
    
    function getGwasGenome($id) {
      $sql = "
          SELECT A.name, A.analysis_id
    FROM chado.analysis A
    WHERE A.analysis_id = (
      SELECT NULLIF(value, '')::int
      FROM chado.projectprop
      WHERE
        type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'analysis_id' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
      AND project_id = :project_id)
        LIMIT 1
        ";
      $objs = $this->chado->getQueryObjects($sql, [':project_id' => $id]);
      return is_array($objs) && count($objs) > 0 ? $objs[0] : NULL;
    }
    
    function getGwasPanel($id) {
      $sql = "
         SELECT uniquename, S.stock_id
          FROM chado.stock S
            INNER JOIN chado.feature_stock FS ON S.stock_id = FS.stock_id
          WHERE FS.feature_id = :feature_id
          AND S.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'panel' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
          LIMIT 1
        ";
      $objs = $this->chado->getQueryObjects($sql, [':feature_id' => $id]);
      return is_array($objs) && count($objs) > 0 ? $objs[0] : NULL;
    }
    
    function getTrait($id) {
      $sql = "
        SELECT name, V.cvterm_id
        FROM chado.cvterm V
          INNER JOIN chado.feature_cvterm FV ON V.cvterm_id = FV.cvterm_id
        WHERE FV.feature_id = :feature_id
        LIMIT 1
    ";
      $objs = $this->chado->getQueryObjects($sql, [':feature_id' => $id]);
      return is_array($objs) && count($objs) > 0 ? $objs[0] : NULL;
    }
    
    /**
     * Counters
     */
    function countAnalysis($feature_id) {
        $counter = $this->chado->count(
            'analysisfeature AF',
            ['A.analysis_id', 'A.name', 'A.program', 'A.programversion', 'A.sourcename', 'A.timeexecuted'],
            [
                'inner' => ['analysis A' => 'A.analysis_id = AF.analysis_id']
            ],
            ['AF.feature_id' => $feature_id]
            );
        return $counter;
    }

    function countDbxref($feature_id) {
        $q1 = $this->chado->getQuery(
            'feature_dbxref FX',
            ['db.name AS db', 'accession', 'urlprefix'],
            ['inner' =>
                [
                    'dbxref X' => 'X.dbxref_id = FX.dbxref_id',
                    'db' => 'db.db_id = X.db_id'
                ]
            ],
            ['FX.feature_id' => $feature_id]
            );
        $q2 = $this->chado->getQuery(
            'feature F',
            ['db.name AS db', 'accession', 'urlprefix'],
            ['inner' =>
                [
                    'dbxref X' => 'X.dbxref_id = F.dbxref_id',
                    'db' => 'db.db_id = X.db_id'
                ]
            ],
            ['F.feature_id' => $feature_id]
            );
        $sql = $q1->sql . ' UNION ' . $q2->sql;
        $args = array_merge($q1->args, $q2->args);
        $counter = $this->chado->countQuery($sql, $args);
        return $counter;
    }

    function countObjectFeatures($feature_id, $relcvterm, $relcv, $seqtype) {
        $conditions = $relcv ? ['object_id' => $feature_id, 'RT.name' => $relcvterm, 'cv.name' => $relcv, 'V.name' => $seqtype ] : ['object_id' => $feature_id, 'RT.name' => $relcvterm, 'V.name' => $seqtype ];
        $counter = $this->chado->count(
            'feature F',
            ['F.feature_id', 'uniquename', 'F.name', 'residues', 'genus || \' \' || species AS organism', 'V.name AS type', 'seqlen'],
            [
                'inner' => [
                    'organism O' => 'O.organism_id = F.organism_id',
                    'cvterm V' => 'V.cvterm_id = F.type_id',
                    'feature_relationship FR' => 'FR.subject_id = F.feature_id',
                    'cvterm RT' => 'RT.cvterm_id = FR.type_id',
                    'cv' => 'cv.cv_id = RT.cv_id'
                ]
            ],
            $conditions
            );
        return $counter;
    }

    function countSubjectFeatures($feature_id, $relcvterm, $relcv, $seqtype) {
        $counter = $this->chado->count(
            'feature F',
            ['F.feature_id', 'uniquename', 'F.name', 'residues', 'genus || \' \' || species AS organism', 'V.name AS type', 'seqlen'],
            [
                'inner' => [
                    'organism O' => 'O.organism_id = F.organism_id',
                    'cvterm V' => 'V.cvterm_id = F.type_id',
                    'feature_relationship FR' => 'FR.object_id = F.feature_id',
                    'cvterm RT' => 'RT.cvterm_id = FR.type_id',
                    'cv' => 'cv.cv_id = RT.cv_id'
                ]
            ],
            [
                'subject_id' => $feature_id,
                'RT.name' => $relcvterm,
                'cv.name' => $relcv,
                'V.name' => $seqtype
            ]
            );
        return $counter;
    }

    function countObjectRelationship($feature_id) {
        $counter = $this->chado->count(
            'feature_relationship FR',
            ['SUB.feature_id', 'SUB.name AS subject', 'O.organism_id', 'O.genus || \' \' || O.species AS organism', 'OV.name AS type', 'V.name AS relationship'],
            ['inner' =>
                [
                    'cvterm V' => 'V.cvterm_id = FR.type_id',
                    'feature SUB' => 'SUB.feature_id = FR.subject_id',
                    'organism O' => 'O.organism_id = SUB.organism_id',
                    'cvterm OV' => 'OV.cvterm_id = SUB.type_id'
                ]
            ],
            ['object_id' => $feature_id],
            );
        return $counter;
    }
    
    function countSubjectRelationship($feature_id) {
        $counter = $this->chado->count(
            'feature_relationship FR',
            ['OBJ.feature_id', 'OBJ.name AS object', 'O.organism_id', 'O.genus || \' \' || O.species AS organism', 'OV.name AS type', 'V.name AS relationship'],
            ['inner' =>
                [
                    'cvterm V' => 'V.cvterm_id = FR.type_id',
                    'feature OBJ' => 'OBJ.feature_id = FR.object_id',
                    'organism O' => 'O.organism_id = OBJ.organism_id',
                    'cvterm OV' => 'OV.cvterm_id = OBJ.type_id'
                ]
            ],
            ['subject_id' => $feature_id],
            );
        return $counter;
    }

    function countCvterm($feature_id) {
        $counter = $this->chado->count(
            'feature_cvterm FV',
            ['C.name AS cv', 'V.name AS cvterm', 'V.definition', 'D.name AS db', 'D.urlprefix', 'X.accession'],
            ['inner' =>
                [
                    'cvterm V' => 'V.cvterm_id = FV.cvterm_id',
                    'cv C' => 'C.cv_id = V.cv_id',
                    'dbxref X' => 'X.dbxref_id = V.dbxref_id',
                    'db D' => 'D.db_id = X.db_id'
                ]
            ],
            ['FV.feature_id' => $feature_id],
            ['db', 'cv', 'accession']
            );
        return $counter;
    }

    function countFeatureloc($feature_id) {
        $counter = $this->chado->count(
            'featureloc FL',
            ['LM.feature_id', 'LM.name AS landmark', 'V.name AS type', 'fmin + 1 || \'..\' || fmax AS location', 'A.name AS analysis', 'A.program', 'A.analysis_id'],
            ['inner' =>
                [
                    'feature LM' => 'LM.feature_id = FL.srcfeature_id',
                    'cvterm V' => 'V.cvterm_id = LM.type_id',
                    'analysisfeature AF' => 'AF.feature_id = LM.feature_id',
                    'analysis A' => 'A.analysis_id = AF.analysis_id'
                ]
            ],
            ['FL.feature_id' => $feature_id]
            );
        return $counter;
    }
    
    function countGenotype ($feature_id) {
        $counter = $this->chado->count(
            'feature_genotype FG',
            ['G.genotype_id', 'G.name', 'G.uniquename', 'G.description', 'V.name AS type'],
            ['inner' =>
                [
                    'genotype G' => 'G.genotype_id = FG.genotype_id',
                    'cvterm V' => 'V.cvterm_id = G.type_id',
                ]
            ],
            ['FG.feature_id' => $feature_id]
            );
        return $counter;
    }
    
    function countPhenotype ($feature_id) {
        $counter = $this->chado->count(
            'feature_phenotype FP',
            ['P.phenotype_id', 'P.name', 'P.uniquename', 'P.value'],
            ['inner' =>
                [
                    'phenotype P' => 'P.phenotype_id = FP.phenotype_id',
                ]
            ],
            ['FP.feature_id' => $feature_id]
            );
        return $counter;
    }
    
    function countPub($feature_id) {
        $counter = $this->chado->count(
            'feature_pub FP',
            ['P.pub_id', 'title', 'series_name', 'pyear', 'V.name AS type'],
            ['inner' =>
                [
                    'pub P' => 'P.pub_id = FP.pub_id',
                    'cvterm V' => 'V.cvterm_id = P.type_id',
                ]
            ],
            ['FP.feature_id' => $feature_id]
            );
        return $counter;
    }

    function countContact($feature_id) {
        $counter = $this->chado->count(
            'feature_contact FC',
            ['C.contact_id', 'C.name', 'V.name AS type'],
            ['inner' =>
                [
                    'contact C' => 'C.contact_id = FC.contact_id',
                    'cvterm V' => 'V.cvterm_id = C.type_id',
                ]
            ],
            ['FC.feature_id' => $feature_id]
            );
        return $counter;
    }

    function countLibrary($feature_id) {
        $counter = $this->chado->count(
            'library_feature LF',
            ['L.library_id', 'L.name', 'V.name AS type'],
            ['inner' =>
                [
                    'library L' => 'L.library_id = LF.library_id',
                    'cvterm V' => 'V.cvterm_id = L.type_id',
                ]
            ],
            ['LF.feature_id' => $feature_id]
            );
        return $counter;
    }
    
    function countProject($feature_id) {
        $counter = $this->chado->count(
            'feature_project FP',
            ['P.project_id', 'P.name'],
            ['inner' =>
                [
                    'project P' => 'P.project_id = FP.project_id',
                ]
            ],
            ['FP.feature_id' => $feature_id]
            );
        return $counter;
    }

    function countImage($feature_id) {
        $counter = $this->chado->count(
            'feature_image FI',
            ['I.eimage_id', 'I.eimage_data', 'I.eimage_type'],
            ['inner' =>
                [
                    'eimage I' => 'I.eimage_id = FI.eimage_id',
                ]
            ],
            ['FI.feature_id' => $feature_id]
            );
        return $counter;
    }
    
    function countStock($feature_id) {
        $counter = $this->chado->count(
            'feature_stock FS',
            ['S.stock_id', 'S.name', 'S.uniquename', 'V.name AS type'],
            ['inner' =>
                [
                    'stock S' => 'S.stock_id = FS.stock_id',
                    'cvterm V' => 'V.cvterm_id = S.type_id',
                ]
            ],
            ['FS.feature_id' => $feature_id]
            );
        return $counter;
    }

    function countSynonym($feature_id) {
        $counter = $this->chado->count(
            'feature_synonym FS',
            ['S.synonym_id', 'S.name', 'V.name AS type'],
            ['inner' =>
                [
                    'synonym S' => 'S.synonym_id = FS.synonym_id',
                    'cvterm V' => 'V.cvterm_id = S.type_id',
                ]
            ],
            ['FS.feature_id' => $feature_id]
            );
        return $counter;
    }
    
    function countMapPosition($feature_id) {
        $sql = 
            "SELECT
              FM.featuremap_id,
              FM.name AS map,
               X.accession,
               DB.urlprefix,
              LG.name AS linkage_group,
              BIN.name AS bin,
              LGP.value AS chr,
              FPP.value AS start,
              LOCUS.name AS locus
            FROM chado.feature LOCUS
            INNER JOIN chado.feature_relationship FR ON FR.subject_id = LOCUS.feature_id
            INNER JOIN chado.featurepos FP ON LOCUS.feature_id = FP.feature_id
            INNER JOIN chado.featuremap FM ON FP.featuremap_id = FM.featuremap_id
             LEFT JOIN chado.featuremap_dbxref FD ON FP.featuremap_id = FD.featuremap_id
             LEFT JOIN chado.dbxref X ON FD.dbxref_id = X.dbxref_id
             LEFT JOIN chado.db ON db.db_id = X.db_id
            INNER JOIN chado.feature LG ON FP.map_feature_id = LG.feature_id
            LEFT JOIN
             (SELECT * FROM chado.featureprop WHERE type_id =
              (SELECT cvterm_id
               FROM chado.cvterm
               WHERE name = 'chr_number'
               AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')
              )
             )LGP ON LG.feature_id = LGP.feature_id
            INNER JOIN chado.featureposprop FPP ON FP.featurepos_id = FPP.featurepos_id
            LEFT JOIN
             (SELECT F2.name, FR2.subject_id FROM chado.feature F2
              INNER JOIN chado.feature_relationship FR2 ON FR2.object_id = F2.feature_id
             WHERE FR2.type_id =
               (SELECT cvterm_id FROM chado.cvterm WHERE name = 'located_in' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'relationship'))
             ) BIN ON LOCUS.feature_id = BIN.subject_id
            WHERE FR.type_id =
             (SELECT cvterm_id
              FROM chado.cvterm
              WHERE name = 'instance_of'
             AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'relationship')
             )
            AND LOCUS.type_id =
             (SELECT cvterm_id
              FROM chado.cvterm
              WHERE name = 'marker_locus'
             AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')
             )
            AND FPP.type_id =
             (SELECT cvterm_id
              FROM chado.cvterm
              WHERE name = 'start'
             AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')
             )               
            AND FR.object_id = :object_id";
        $counter = $this->chado->countQuery($sql, [':object_id' => $feature_id]);
        return $counter;     
    }
    
    function countXTLPosition($feature_id) {
        $sql =
        "SELECT
              FM.featuremap_id,
              FM.name AS map,
              X.accession,
              DB.urlprefix,
              LG.name AS linkage_group,
              BIN.name AS bin,
              LGP.value AS chr,
              START.value AS start,
              STOP.value AS stop,
              PEAK.value AS peak,
              XTL.uniquename
            FROM chado.feature XTL
            INNER JOIN chado.featurepos FP ON XTL.feature_id = FP.feature_id
            INNER JOIN chado.featuremap FM ON FP.featuremap_id = FM.featuremap_id
            LEFT JOIN chado.featuremap_dbxref FD ON FP.featuremap_id = FD.featuremap_id
            LEFT JOIN chado.dbxref X ON FD.dbxref_id = X.dbxref_id
            LEFT JOIN chado.db ON db.db_id = X.db_id
            INNER JOIN chado.feature LG ON FP.map_feature_id = LG.feature_id
            LEFT JOIN (
              SELECT FP.feature_id, FP.value
              FROM chado.featureprop FP
              WHERE FP.type_id =
              (SELECT cvterm_id
              FROM chado.cvterm
              WHERE name = 'chr_number' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
            ) LGP ON LG.feature_id = LGP.feature_id
            LEFT JOIN (
              SELECT featurepos_id, value
              FROM chado.featureposprop FPP
              WHERE FPP.type_id =
              (SELECT cvterm_id
              FROM chado.cvterm
              WHERE name = 'start' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
            ) START ON FP.featurepos_id = START.featurepos_id
            LEFT JOIN (
              SELECT featurepos_id, value
              FROM chado.featureposprop FPP
              WHERE FPP.type_id =
              (SELECT cvterm_id
              FROM chado.cvterm
              WHERE name = 'stop' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
            ) STOP ON FP.featurepos_id = STOP.featurepos_id
            LEFT JOIN (
              SELECT featurepos_id, value
              FROM chado.featureposprop FPP
              WHERE FPP.type_id =
              (SELECT cvterm_id
              FROM chado.cvterm
                       WHERE name LIKE '_tl_peak' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
              ) PEAK ON FP.featurepos_id = PEAK.featurepos_id
              LEFT JOIN
                  (SELECT F2.name, FR2.subject_id
                   FROM chado.feature F2
                     INNER JOIN chado.feature_relationship FR2 ON FR2.object_id = F2.feature_id
                   WHERE FR2.type_id =
                     (SELECT cvterm_id FROM chado.cvterm WHERE name = 'located_in')
                ) BIN ON XTL.feature_id = BIN.subject_id
                WHERE
                  XTL.type_id = (SELECT type_id FROM chado.feature WHERE feature_id = :feature_id1) AND
                  XTL.feature_id = :feature_id2";
        $counter = $this->chado->countQuery($sql, [':feature_id1' => $feature_id, ':feature_id2' => $feature_id]);
        return $counter;
    }
    
    function countRelatedMarker($feature_id, $cvterm) {
        $sql =
        "SELECT
            Marker.feature_id,
            Marker.name,
            Marker.uniquename,
            Marker.organism_id,
            Marker.organism,
            Marker.type
         FROM chado.feature F
         INNER JOIN
           (SELECT 
              MKR.name, 
              MKR.uniquename, 
              FR.object_id, MKR.feature_id,
              MKR.organism_id,
              (SELECT genus || ' ' || species FROM chado.organism WHERE organism_id = MKR.organism_id) AS organism,
              (SELECT name FROM chado.cvterm WHERE cvterm_id = MKR.type_id) AS type
            FROM chado.feature MKR
              INNER JOIN chado.feature_relationship FR ON MKR.feature_id = FR.subject_id
              INNER JOIN chado.cvterm V ON V.cvterm_id = FR.type_id
            WHERE V.name = :cvterm AND V.cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'relationship')
           ) Marker ON Marker.object_id = F.feature_id
         WHERE F.feature_id = :feature_id";
        $counter = $this->chado->countQuery($sql, [':cvterm' => $cvterm, ':feature_id' => $feature_id]);
        return $counter;
    }
    
    
    function countEnvironment($feature_id) {
        $sql =
        "SELECT 
            ENV.nd_geolocation_id, 
            description
          FROM chado.feature QTL
          INNER JOIN chado.feature_nd_geolocation FENV ON FENV.feature_id = QTL.feature_id
          INNER JOIN chado.nd_geolocation ENV ON ENV.nd_geolocation_id = FENV.nd_geolocation_id
          WHERE QTL.feature_id = :feature_id";
        $counter = $this->chado->countQuery($sql, [':feature_id' => $feature_id]);
        return $counter;
    }
    
    function countPolymorphism ($feature_id) {
        $sql ="
            SELECT 
                organism_id,
                feature_id,
                marker || '_' || allele AS marker_allele,
                allele,
                string_agg(DISTINCT project, '::') AS projects,
                string_agg(DISTINCT stock, '::') AS stocks,
                count (DISTINCT stock_id) AS num_stocks
            FROM
                (SELECT
                        NEG.nd_experiment_id,
                        FG.feature_id,
                        G.genotype_id,
                        MARKER.name AS marker,
                        MARKER.organism_id,
                        regexp_split_to_table(G.description, '\|') AS allele,
                        P.project_id,
                        P.project_id || '|' || P.name AS project,
                        S.stock_id,
                        S.stock_id || '|' || S.name AS stock
                    FROM chado.genotype G
                    INNER JOIN chado.feature_genotype FG ON G.genotype_id = FG.genotype_id
                    INNER JOIN chado.feature MARKER ON MARKER.feature_id = FG.feature_id
                    INNER JOIN chado.nd_experiment_genotype NEG ON G.genotype_id = NEG.genotype_id
                    LEFT JOIN chado.nd_experiment_stock NES ON NES.nd_experiment_id = NEG.nd_experiment_id
                    LEFT JOIN chado.stock EXP ON EXP.stock_id = NES.stock_id
                    LEFT JOIN chado.stock_relationship SR ON SR.subject_id = EXP.stock_id AND SR.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'sample_of' AND cv_id =(SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
                    INNER JOIN chado.stock S ON S.stock_id = SR.object_id
                    LEFT JOIN chado.nd_experiment_project NEP ON NEP.nd_experiment_id = NEG.nd_experiment_id
                    LEFT JOIN chado.project P ON P.project_id = NEP.project_id
                    WHERE FG.feature_id = :feature_id
                ) T
            GROUP BY allele, organism_id, feature_id, marker";
        $counter = $this->chado->countQuery($sql, [':feature_id' => $feature_id]);
        return $counter;
    }
    
    function countAllele ($organism_id, $feature_id, $allele) {
        $sql ="
            SELECT 
                feature_id,
                organism_id,
                marker,
                allele,
                stock_id,
                stock,
                string_agg(DISTINCT project, '::') AS projects
            FROM 
                (SELECT
                    NEG.nd_experiment_id,
                    FG.feature_id,
                    G.genotype_id,
                    MARKER.name AS marker,
                    MARKER.organism_id,
                    regexp_split_to_table(G.description, '\|') AS allele,
                    P.project_id || '|' || P.name AS project,
                    S.stock_id,
                    S.name AS stock
                FROM chado.genotype G
                INNER JOIN chado.feature_genotype FG ON G.genotype_id = FG.genotype_id
                INNER JOIN chado.feature MARKER ON MARKER.feature_id = FG.feature_id
                INNER JOIN chado.nd_experiment_genotype NEG ON G.genotype_id = NEG.genotype_id
                LEFT JOIN chado.nd_experiment_stock NES ON NES.nd_experiment_id = NEG.nd_experiment_id
                LEFT JOIN chado.stock EXP ON EXP.stock_id = NES.stock_id
                LEFT JOIN chado.stock_relationship SR ON SR.subject_id = EXP.stock_id AND SR.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'sample_of' AND cv_id =(SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
                INNER JOIN chado.stock S ON S.stock_id = SR.object_id
                LEFT JOIN chado.nd_experiment_project NEP ON NEP.nd_experiment_id = NEG.nd_experiment_id
                LEFT JOIN chado.project P ON P.project_id = NEP.project_id
                )T 
            WHERE feature_id = :feature_id
            AND organism_id = :organism_id
            AND allele = :allele
            GROUP BY organism_id,feature_id,marker,allele,stock_id,stock";
        $counter = $this->chado->countQuery($sql, [':feature_id' => $feature_id, ':organism_id' => $organism_id, ':allele' => $allele]);
        return $counter;
    }
    
    function countHaplotypeBlockMapPosition ($feature_id) {
        $sql = "
            SELECT
              FM.featuremap_id,
              FM.name AS map,
              LG.name AS linkage_group,
              FPP.value AS start,
              FPP2.value AS stop,
              LOCUS.name AS name
            FROM chado.feature LOCUS
            INNER JOIN chado.featurepos FP ON LOCUS.feature_id = FP.feature_id
            INNER JOIN chado.featuremap FM ON FP.featuremap_id = FM.featuremap_id
            INNER JOIN chado.feature LG ON FP.map_feature_id = LG.feature_id
            INNER JOIN chado.featureposprop FPP ON FP.featurepos_id = FPP.featurepos_id
            INNER JOIN chado.featureposprop FPP2 ON FP.featurepos_id = FPP2.featurepos_id
            WHERE
             FPP.type_id =
             (SELECT cvterm_id
              FROM chado.cvterm
              WHERE name = 'start'
             AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')
             )
            AND
              FPP2.type_id =
             (SELECT cvterm_id
              FROM chado.cvterm
              WHERE name = 'stop'
             AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')
             )
            AND LOCUS.feature_id = :feature_id
        ";
        $counter = $this->chado->countQuery($sql, [':feature_id' => $feature_id]);
        return $counter;
    }
    
    function countHaplotypeBlockProject ($feature_id) {
        $sql = "
            SELECT 
                DISTINCT P.project_id, P.name 
            FROM chado.feature_genotype FG
            INNER JOIN chado.nd_experiment_genotype NEG ON NEG.genotype_id = FG.genotype_id
            INNER JOIN chado.nd_experiment_project NEP ON NEG.nd_experiment_id = NEP.nd_experiment_id
            INNER JOIN chado.project P ON P.project_id = NEP.project_id
            WHERE feature_id =:feature_id
        ";
        $counter = $this->chado->countQuery($sql, [':feature_id' => $feature_id]);
        return $counter;
    }
    
    function countStockHaplotype($feature_id) {
        $sql = "
          SELECT
              S.stock_id,
              S.uniquename AS stock,
              string_agg (DISTINCT H.name, ' ') AS haplotype
          FROM chado.feature HB
          INNER JOIN chado.feature_relationship FR ON HB.feature_id = FR.object_id AND FR.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'variant_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'sequence'))
          INNER JOIN chado.feature H ON H.feature_id = FR.subject_id
          INNER JOIN chado.feature_genotype FG ON FG.feature_id = HB.feature_id
          INNER JOIN chado.nd_experiment_genotype NEG ON NEG.genotype_id = FG.genotype_id
          INNER JOIN chado.genotype G ON G.genotype_id = NEG.genotype_id
          INNER JOIN chado.nd_experiment_stock NES ON NEG.nd_experiment_id = NES.nd_experiment_id
          INNER JOIN chado.stock S ON S.stock_id = NES.stock_id
          WHERE lower(H.name) = ANY(string_to_array(lower(G.description), '|'))
          AND HB.feature_id = :feature_id
          GROUP BY S.stock_id, S.uniquename
        ";
        $counter = $this->chado->countQuery($sql, [':feature_id' => $feature_id]);
        return $counter;
    }
    
    
    function countHaplotype($feature_id) {
        $sql = "
            SELECT
              string_agg(haplotype, ' ') AS haplotypes,
              string_agg(value, ' ') AS values,
              feature_id,
              marker
            FROM
                (SELECT
                    (SELECT replace (uniquename, HB.uniquename || '_', '') FROM chado.feature WHERE feature_id = FR.subject_id) AS haplotype,
                    FR2P.value,
                    (SELECT feature_id FROM chado.feature WHERE feature_id = FR2.object_id) AS feature_id,
                    (SELECT uniquename FROM chado.feature WHERE feature_id = FR2.object_id) AS marker
                 FROM chado.feature HB
                 LEFT JOIN chado.feature_relationship FR ON HB.feature_id = FR.object_id AND FR.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'variant_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'sequence'))
                 LEFT JOIN chado.feature_relationship FR2 ON FR2.subject_id = FR.subject_id AND FR2.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'contains' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'sequence'))
                 LEFT JOIN chado.feature_relationshipprop FR2P ON FR2.feature_relationship_id = FR2P.feature_relationship_id
                 WHERE feature_id = :feature_id
                 AND FR2P.value IS NOT NULL
                 ORDER BY FR2.object_id,haplotype
                ) T
            GROUP BY feature_id,marker";
        $counter = $this->chado->countQuery($sql, [':feature_id' => $feature_id]);
        return $counter;
    }
    
    function countSNPArrayId($feature_id) {
      $sql = "
        SELECT 
          T.synonym_id, 
          T.snp_chip_id, 
          T.library_id, 
          L.name AS library
        FROM chado.feature F
          INNER JOIN chado.library_feature LF ON LF.feature_id = F.feature_id
          INNER JOIN chado.library L ON L.library_id = LF.library_id
          INNER JOIN chado.cvterm C_L ON C_L.cvterm_id = L.type_id
          INNER JOIN (
            SELECT S.name AS snp_chip_id, S.synonym_id, S.type_id, C_S.name AS type, FS.feature_id, LS.library_id
            FROM chado.synonym S
              INNER JOIN chado.cvterm C_S ON C_S.cvterm_id = S.type_id
              INNER JOIN chado.feature_synonym FS ON FS.synonym_id = S.synonym_id
              INNER JOIN chado.library_synonym LS ON LS.synonym_id = S.synonym_id
           WHERE C_S.name = 'SNP_chip'
          ) T ON T.library_id = LF.library_id AND T.feature_id = LF.feature_id
        WHERE C_L.name = 'SNP_chip' AND 
        F.feature_id = :feature_id    
      ";
      $counter = $this->chado->countQuery($sql, [':feature_id' => $feature_id]);
      return $counter;
    }
    
    function countAnalysisFeatureProp($feature_id, $type) {
      $counter = $this->chado->count(
          'analysisfeatureprop AFP',
          ['A.analysis_id', 'A.name AS analysis', 'timeexecuted', 'value', 'rank'],
          ['inner' =>
            [
              'analysisfeature AF' => 'AF.analysisfeature_id = AFP.analysisfeature_id',
              'analysis A' => 'A.analysis_id = AF.analysis_id',
              'cvterm V' => 'V.cvterm_id = AFP.type_id',
              'cv' => 'cv.cv_id = V.cv_id'
            ]
          ],
          ['AF.feature_id' => $feature_id,
            'V.name' => $type,
            'cv.name' => 'tripal'
          ]
          );
      return $counter;
    }
    
    function countBinMapLoci($feature_id) {
      $sql =
      "SELECT
        F.uniquename AS LG,
        FR.object_id AS feature_id,
        (SELECT name FROM chado.feature WHERE feature_id = FR.object_id) AS genetic_marker,
        F2.name AS marker,
        F2.feature_id AS marker_feature_id,
        (SELECT name FROM chado.cvterm WHERE cvterm_id = F2.type_id) AS type,
        FPP.value AS position
      FROM chado.featurepos FP
      INNER JOIN chado.feature F ON F.feature_id = FP.map_feature_id
      INNER JOIN chado.feature F2 ON F2.feature_id = FP.feature_id
      INNER JOIN chado.featureposprop FPP ON FP.featurepos_id = FPP.featurepos_id AND FPP.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'start' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
      INNER JOIN chado.feature_relationship FR ON FR.subject_id = F2.feature_id AND FR.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'instance_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'relationship'))
      WHERE FP.feature_id IN (SELECT subject_id FROM chado.feature_relationship WHERE object_id = :feature_id AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'contained_in' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'relationship')))
      AND FP.featuremap_id = (SELECT featuremap_id FROM chado.featurepos WHERE feature_id = :feature_id2)
      AND (SELECT name FROM chado.cvterm WHERE cvterm_id = F2.type_id) IN ('marker_locus', 'heritable_phenotypic_marker') ORDER BY F.uniquename, FPP.value::float";
      $counter = $this->chado->countQuery($sql, [':feature_id' => $feature_id, ':feature_id2' => $feature_id]);
      return $counter;
    }
    
    function countBinMapQTL($feature_id) {
      $sql =
      "SELECT
        F.uniquename AS LG,
        F2.uniquename AS qtl,
        F2.feature_id AS qtl_feature_id,
        (SELECT name FROM chado.cvterm WHERE cvterm_id = F2.type_id) AS type,
        FPP.value AS start,
        FPP2.value AS stop,
        FPP3.value AS peak
      FROM chado.featurepos FP
      INNER JOIN chado.feature F ON F.feature_id = FP.map_feature_id
      INNER JOIN chado.feature F2 ON F2.feature_id = FP.feature_id
      LEFT JOIN chado.featureposprop FPP ON FP.featurepos_id = FPP.featurepos_id AND FPP.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'start' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
      LEFT JOIN chado.featureposprop FPP2 ON FP.featurepos_id = FPP2.featurepos_id AND FPP2.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'stop' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
      LEFT JOIN chado.featureposprop FPP3 ON FP.featurepos_id = FPP3.featurepos_id AND FPP3.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'qtl_peak' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
      WHERE FP.feature_id IN (SELECT subject_id FROM chado.feature_relationship WHERE object_id = :feature_id AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'contained_in' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'relationship')))
      AND FP.featuremap_id = (SELECT featuremap_id FROM chado.featurepos WHERE feature_id = :feature_id2)
      AND (SELECT name FROM chado.cvterm WHERE cvterm_id = F2.type_id) = 'QTL'";
      $counter = $this->chado->countQuery($sql, [':feature_id' => $feature_id, ':feature_id2' => $feature_id]);
      return $counter;
    }
    
    function countGwasLocation($feature_id) {
      $gwas_marker = $this->getGwasMarker($feature_id);
      $id = isset($gwas_marker->feature_id) ? $gwas_marker->feature_id : 0;
        $sql =
        "SELECT
            LM.name AS landmark,
            srcfeature_id,
            (SELECT name FROM chado.cvterm WHERE cvterm_id = LM.type_id) AS type,
            fmin,
            fmax,
            (fmin + 1) || '..' || fmax AS location,
            A.analysis_id,
            A.name AS analysis,
            AP.value AS jbrowse_url
          FROM chado.featureloc LOC
          INNER JOIN chado.feature LM ON LM.feature_id = LOC.srcfeature_id
          INNER JOIN chado.analysisfeature AF ON AF.feature_id = LM.feature_id
          INNER JOIN chado.analysis A ON A.analysis_id = AF.analysis_id
          LEFT JOIN chado.analysisprop AP ON AP.analysis_id = A.analysis_id AND AP.type_id IN (SELECT cvterm_id FROM chado.cvterm WHERE name = 'JBrowse URL')
          WHERE LOC.feature_id = :feature_id";
        $counter = $this->chado->countQuery($sql, [':feature_id' => $id]);
        return $counter;
    }
}