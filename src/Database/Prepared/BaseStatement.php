<?php
namespace Drupal\chado_display\Database\Prepared;

use Drupal\chado_display\Database\Chado;

class BaseStatement {

    protected $chado;

    function __construct (Chado $chado) {
        $this->chado = $chado;
    }

    /**
     * Getters
     */
    function getProperty($base_table, $record_id, $prop_type) {
      $proptable = $base_table . 'prop';
      $pk = $this->chado->getPkey($base_table);
      $result = NULL;
      if ($this->chado->tableExists($proptable)) {
        $result = $this->chado->getObjects(
            $proptable . ' P',
            ['eimage_id', 'P.type_id', 'name', 'value'],
            ['inner' => ['cvterm' => 'cvterm_id = P.type_id']],
            ['name' => $prop_type, $pk => $record_id], 
            );
      }
      return $result;
    }
    
    function getPropertyTypes($base_table) {
        $proptable = $base_table . 'prop';
        $result = [];
        if ($this->chado->tableExists($proptable)) {
            $result = $this->chado->getObjects(
                $proptable . ' P',
                ['DISTINCT P.type_id', 'V.name', 'cv.name AS cv'],
                ['inner' => [
                  'cvterm V' => 'cvterm_id = P.type_id',
                  'cv' => 'cv.cv_id = V.cv_id'
                  ]
                ],
                [],
                ['asc' => 'cv, name']
                );
        }
        return $result;
    }

    /**
     * Counters
     */
    function countProperty($base_table, $id, $agg_by = ' | ') {
        $pk = $this->chado->getPkey($base_table);
        $proptable = $base_table . 'prop';
        $counter = NULL;
        if ($this->chado->tableExists($proptable)) {
            if ($agg_by != NULL) {
                $sql = "
                    SELECT string_agg(DISTINCT value, '$agg_by') AS value, V.name AS type, P.type_id
                    FROM chado.$proptable P
                    INNER JOIN chado.cvterm V ON V.cvterm_id = P.type_id  
                    WHERE P.$pk = $id
                    GROUP BY P.type_id, V.name" ;
                $counter = $this->chado->countQuery($sql);
            }
            else {
                $counter = $this->chado->count(
                    $proptable . ' P',
                    ['value', 'name AS type', 'P.type_id'],
                    [
                        'inner' => [
                            'cvterm' => 'P.type_id = cvterm_id'
                        ]
                    ],
                    [$pk => $id]
                    );
            }
        }
        return $counter;
    }
}