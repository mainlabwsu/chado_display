<?php
namespace Drupal\chado_display\Database\Prepared;

use Drupal\chado_display\Database\Chado;

class MapStatement {

    protected $chado;

    function __construct (Chado $chado) {
        $this->chado = $chado;
    }

    /**
     * Getters
     */
    function getOrganism($featuremap_id) {
        $orgs = array();
        if ($this->chado->tableExists('featuremap_organism')) {
            $orgs = $this->chado->getObjects('organism O', ['O.organism_id', 'genus', 'species'], ['inner' => ['featuremap_organism FO' => 'FO.organism_id = O.organism_id']], ['featuremap_id' => $featuremap_id]);
        }
        return $orgs;
    }

    function getMapPopulationSize($featuremap_id) {
        $pop_sizes = $this->chado->getObjects(
            'featuremap_stock FS',
            ['FS.stock_id', 'S.name', 'value'],
            ['inner' =>
                [
                    'stock S' => 'S.stock_id = FS.stock_id',
                    'stockprop SP' => 'SP.stock_id = S.stock_id',
                    'cvterm V' => 'V.cvterm_id = SP.type_id'
                ]
            ],
            [
                'FS.featuremap_id' => $featuremap_id,
                'V.name' => 'population_size'
            ]);
        return $pop_sizes;
    }

    function getMapLGNum($featuremap_id) {
        $sql =
        "SELECT count (distinct F.uniquename) AS count
        FROM chado.featurepos FP
        INNER JOIN chado.feature F ON F.feature_id = FP.map_feature_id
        WHERE F.type_id = (SELECT cvterm_id
                                            FROM chado.cvterm
                                            WHERE name = 'linkage_group' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'sequence'))
        AND featuremap_id = :featuremap_id";
        $objs = $this->chado->getQueryObjects($sql, [':featuremap_id' => $featuremap_id]);
        $obj = $objs[0];
        return $obj->count;
    }

    function getMapPaternalParent($featuremap_id) {
        if ($this->chado->tableExists('featuremap_stock')) {
            $sql =
            "SELECT S1.stock_id, S1.uniquename, S1.description, V.name AS type
             FROM chado.stock S1
             INNER JOIN chado.stock_relationship SR ON SR.subject_id = S1.stock_id
             INNER JOIN chado.stock S2 ON S2.stock_id = SR.object_id
             INNER JOIN chado.featuremap_stock FS ON FS.stock_id = S2.stock_id
             INNER JOIN chado.cvterm V ON V.cvterm_id = S2.type_id
             WHERE SR.type_id =
             (SELECT cvterm_id
              FROM chado.cvterm
              WHERE name = 'is_a_paternal_parent_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
              AND FS.featuremap_id = :featuremap_id";
            $objs = $this->chado->getQueryObjects($sql, [':featuremap_id' => $featuremap_id]);
            if (count($objs) > 0) {
                return $objs [0];
            }
            else {
                return NULL;
            }
        }
        else {
            return NULL;
        }
    }

    function getMapMaternalParent($featuremap_id) {
        if ($this->chado->tableExists('featuremap_stock')) {
            $sql =
            "SELECT S1.stock_id, S1.uniquename, S1.description, V.name AS type
             FROM chado.stock S1
             INNER JOIN chado.stock_relationship SR ON SR.subject_id = S1.stock_id
             INNER JOIN chado.stock S2 ON S2.stock_id = SR.object_id
             INNER JOIN chado.featuremap_stock FS ON FS.stock_id = S2.stock_id
             INNER JOIN chado.cvterm V ON V.cvterm_id = S2.type_id
             WHERE SR.type_id =
             (SELECT cvterm_id
              FROM chado.cvterm
              WHERE name = 'is_a_maternal_parent_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
              AND FS.featuremap_id = :featuremap_id";
            $objs = $this->chado->getQueryObjects($sql, [':featuremap_id' => $featuremap_id]);
            if (count($objs) > 0) {
                return $objs [0];
            }
            else {
                return NULL;
            }
        }
        else {
            return NULL;
        }
    }

    /**
     * Counters
     */
    function countContact($featuremap_id) {
        if ($this->chado->tableExists('featuremap_contact')) {
            $counter = $this->chado->count(
                'featuremap_contact FC',
                ['C.contact_id', 'C.name', 'V.name AS type'],
                ['inner' =>
                    [
                        'contact C' => 'C.contact_id = FC.contact_id',
                        'cvterm V' => 'V.cvterm_id = C.type_id',
                    ]
                ],
                ['FC.featuremap_id' => $featuremap_id]
                );
            return $counter;
        }
        else {
            return new \Drupal\chado_display\Database\Counter();
        }
    }

    function countPub($featuremap_id) {
        $counter = $this->chado->count(
            'featuremap_pub FP',
            ['P.pub_id', 'title', 'series_name', 'pyear', 'V.name AS type'],
            ['inner' =>
                [
                    'pub P' => 'P.pub_id = FP.pub_id',
                    'cvterm V' => 'V.cvterm_id = P.type_id',
                ]
            ],
            ['FP.featuremap_id' => $featuremap_id]
            );
        return $counter;
    }

    function countStock($featuremap_id) {
        if ($this->chado->tableExists('featuremap_stock')) {
            $counter = $this->chado->count(
                'featuremap_stock FS',
                ['S.stock_id', 'S.name', 'S.uniquename', 'V.name AS type'],
                ['inner' =>
                    [
                        'stock S' => 'S.stock_id = FS.stock_id',
                        'cvterm V' => 'V.cvterm_id = S.type_id',
                    ]
                ],
                ['FS.featuremap_id' => $featuremap_id]
                );
            return $counter;
        }
        else {
            return new \Drupal\chado_display\Database\Counter();
        }
    }

    function countMapLoci($featuremap_id) {
        $sql = "
          SELECT
            F.uniquename AS LG,
            FR.object_id AS feature_id,
            (SELECT name FROM chado.feature WHERE feature_id = FR.object_id) AS genetic_marker,
            F2.name AS marker,
            F2.feature_id AS marker_feature_id,
            (SELECT name FROM chado.cvterm WHERE cvterm_id = F2.type_id) AS type,
            FPP.value AS position
          FROM chado.featurepos FP
          INNER JOIN chado.featuremap FM ON FM.featuremap_id = FP.featuremap_id
          INNER JOIN chado.feature F ON F.feature_id = FP.map_feature_id
          INNER JOIN chado.feature F2 ON F2.feature_id = FP.feature_id
          INNER JOIN chado.featureposprop FPP ON FP.featurepos_id = FPP.featurepos_id AND FPP.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'start' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
          INNER JOIN chado.feature_relationship FR ON FR.subject_id = F2.feature_id AND FR.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'instance_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'relationship'))
          WHERE FM.featuremap_id = :featuremap_id
          AND (SELECT name FROM chado.cvterm WHERE cvterm_id = F2.type_id) IN ('marker_locus', 'heritable_phenotypic_marker') ORDER BY F.uniquename, FPP.value::float";
        return $this->chado->countQuery($sql, [':featuremap_id' => $featuremap_id]);
    }

    function countMapQTL($featuremap_id) {
        $sql = "
          SELECT
        F.uniquename AS LG,
        F2.uniquename AS qtl,
        F2.feature_id AS qtl_feature_id,
        (SELECT name FROM chado.cvterm WHERE cvterm_id = F2.type_id) AS type,
        FPP.value AS start,
        FPP2.value AS stop,
        FPP3.value AS peak
      FROM chado.featurepos FP
      INNER JOIN chado.featuremap FM ON FM.featuremap_id = FP.featuremap_id
      INNER JOIN chado.feature F ON F.feature_id = FP.map_feature_id
      INNER JOIN chado.feature F2 ON F2.feature_id = FP.feature_id
      LEFT JOIN chado.featureposprop FPP ON FP.featurepos_id = FPP.featurepos_id AND FPP.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'start' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
      LEFT JOIN chado.featureposprop FPP2 ON FP.featurepos_id = FPP2.featurepos_id AND FPP2.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'stop' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
      LEFT JOIN chado.featureposprop FPP3 ON FP.featurepos_id = FPP3.featurepos_id AND FPP3.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'qtl_peak' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
      WHERE FM.featuremap_id = :featuremap_id
      AND (SELECT name FROM chado.cvterm WHERE cvterm_id = F2.type_id) = 'QTL'";
        return $this->chado->countQuery($sql, [':featuremap_id' => $featuremap_id]);
    }
}