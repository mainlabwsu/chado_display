<?php
namespace Drupal\chado_display\Database\Prepared;

use Drupal\chado_display\Database\Chado;

class PubStatement {

    protected $chado;

    function __construct (Chado $chado) {
        $this->chado = $chado;
    }

    /**
     * Getters
     */
    function getPub($id) {
        $pub = $this->chado->getFirstObject(
            'pub P',
            ['*', 'V.name AS type'],
            ['inner' =>
                [
                    'cvterm V' => 'cvterm_id = P.type_id'
                ],
            ],
            ['pub_id' => $id]
            );
        return $pub;
    }

    /**
     * Counters
     */
    function countDbxref($pub_id) {
        $counter = $this->chado->count(
            'pub_dbxref PX',
            ['db.name AS db', 'accession', 'urlprefix'],
            ['inner' =>
                [
                    'dbxref X' => 'X.dbxref_id = PX.dbxref_id',
                    'db' => 'db.db_id = X.db_id'
                ]
            ],
            ['PX.pub_id' => $pub_id]
            );
        return $counter;
    }

    function countLibrary($pub_id) {
        $counter = $this->chado->count(
            'library_pub LP',
            ['L.library_id', 'L.name', 'L.uniquename', 'V.name AS type', 'L.organism_id', "genus || ' ' || species AS organism"],
            ['inner' =>
                [
                    'library L' => 'L.library_id = LP.library_id',
                    'organism O' => 'O.organism_id = L.organism_id',
                    'cvterm V' => 'V.cvterm_id = L.type_id'
                ]
            ],
            ['LP.pub_id' => $pub_id]
            );
        return $counter;
    }
    
    function countMap($pub_id) {
        $counter = $this->chado->count(
            'featuremap_pub FP',
            ['F.featuremap_id', 'F.name'],
            ['inner' =>
                [
                    'featuremap F' => 'F.featuremap_id = FP.featuremap_id',
                ]
            ],
            ['FP.pub_id' => $pub_id]
            );
        return $counter;
    }
    
    function countProject($pub_id) {
        $counter = $this->chado->count(
            'project_pub PP',
            ['P.project_id', 'P.name'],
            ['inner' =>
                [
                    'project P' => 'P.project_id = PP.project_id',
                ]
            ],
            ['PP.pub_id' => $pub_id]
            );
        return $counter;
    }
    
    function countImage($pub_id) {
        $counter = $this->chado->count(
            'pub_image PI',
            ['I.eimage_id', 'I.eimage_data', 'I.eimage_type'],
            ['inner' =>
                [
                    'eimage I' => 'I.eimage_id = PI.eimage_id',
                ]
            ],
            ['PI.pub_id' => $pub_id]
            );
        return $counter;
    }
    
    function countStock($pub_id) {
        $counter = $this->chado->count(
            'stock_pub SP',
            ['S.stock_id', 'S.name', 'S.uniquename', 'V.name AS type'],
            ['inner' =>
                [
                    'stock S' => 'S.stock_id = SP.stock_id',
                    'cvterm V' => 'V.cvterm_id = S.type_id',
                ]
            ],
            ['SP.pub_id' => $pub_id]
            );
        return $counter;
    }
    
    function countFeature($pub_id) {
        $counter = $this->chado->count(
            'feature_pub FP',
            ['F.feature_id', 'F.name', 'F.uniquename', 'V.name AS type', "genus || ' ' || species AS organism", 'F.organism_id'],
            ['inner' =>
                [
                    'feature F' => 'F.feature_id = FP.feature_id',
                    'cvterm V' => 'V.cvterm_id = F.type_id',
                    'organism O' => 'O.organism_id = F.organism_id'
                ]
            ],
            ['FP.pub_id' => $pub_id]
            );
        return $counter;
    }
}