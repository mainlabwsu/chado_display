<?php
namespace Drupal\chado_display\Database\Prepared;

use Drupal\chado_display\Database\Chado;

class LibraryStatement {

    protected $chado;

    function __construct (Chado $chado) {
        $this->chado = $chado;
    }

    /**
     * Getters
     */
    function getLibrary($id) {
        $library = $this->chado->getFirstObject(
            'library L',
            ['library_id', 'L.name', 'uniquename', 'L.type_id', 'L.organism_id', 'V.name AS type', 'genus || \' \' || species AS organism'],
            ['inner' =>
                [
                    'organism O' => 'L.organism_id = O.organism_id',
                    'cvterm V' => 'cvterm_id = L.type_id'
                ],
            ],
            ['library_id' => $id]
            );
        return $library;
    }

    /**
     * Counters
     */
    function countDbxref($library_id) {
        $counter = $this->chado->count(
            'library_dbxref LX',
            ['db.name AS db', 'accession', 'urlprefix'],
            ['inner' =>
                [
                    'dbxref X' => 'X.dbxref_id = LX.dbxref_id',
                    'db' => 'db.db_id = X.db_id'
                ]
            ],
            ['LX.library_id' => $library_id]
            );
        return $counter;
    }
    
    function countPub($library_id) {
        $counter = $this->chado->count(
            'library_pub LP',
            ['P.pub_id', 'title', 'series_name', 'pyear', 'V.name AS type'],
            ['inner' =>
                [
                    'pub P' => 'P.pub_id = LP.pub_id',
                    'cvterm V' => 'V.cvterm_id = P.type_id',
                ]
            ],
            ['LP.library_id' => $library_id]
            );
        return $counter;
    }
    
    function countSynonym($library_id) {
        $counter = $this->chado->count(
            'library_synonym LS',
            ['S.synonym_id', 'S.name', 'V.name AS type'],
            ['inner' =>
                [
                    'synonym S' => 'S.synonym_id = LS.synonym_id',
                    'cvterm V' => 'V.cvterm_id = S.type_id',
                ]
            ],
            ['LS.library_id' => $library_id]
            );
        return $counter;
    }

    function countFeature($library_id) {
        $counter = $this->chado->count(
            'library_feature LF',
            ['F.feature_id', 'F.name', 'F.uniquename', 'V.name AS type', "genus || ' ' || species AS organism", 'F.organism_id'],
            ['inner' =>
                [
                    'feature F' => 'F.feature_id = LF.feature_id',
                    'cvterm V' => 'V.cvterm_id = F.type_id',
                    'organism O' => 'O.organism_id = F.organism_id'
                ]
            ],
            ['LF.library_id' => $library_id]
            );
        return $counter;
    }
}