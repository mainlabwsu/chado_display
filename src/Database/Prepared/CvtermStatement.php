<?php
namespace Drupal\chado_display\Database\Prepared;

use Drupal\chado_display\Database\Chado;

class CvtermStatement {

    protected $chado;

    function __construct (Chado $chado) {
        $this->chado = $chado;
    }

    /**
     * Getters
     */
    function getTrait ($cvterm_id) {
        $sql = "
            SELECT
              C.name AS name,
              string_agg(DISTINCT CV.name, ' | ') AS cv,
              string_agg(DISTINCT CAT.name, ' | ') AS category,
              string_agg(DISTINCT C.name, ' | ') AS trait,
              string_agg(DISTINCT A.value, ' | ') AS abbreviation,
              string_agg(DISTINCT C.definition, ' | ') AS definition
            FROM chado.cvterm C
            INNER JOIN chado.cv ON C.cv_id = cv.cv_id
            LEFT JOIN (
              SELECT cvterm_id, value
              FROM chado.cvtermprop
              WHERE type_id = (
                SELECT cvterm_id
                FROM chado.cvterm C
                INNER JOIN chado.cv on CV.cv_id = C.cv_id
                WHERE C.name = 'abbreviation' AND cv.name = 'MAIN')
            ) A on A.cvterm_id = C.cvterm_id
            LEFT JOIN (
              SELECT C1.name, C1.cvterm_id, CR.subject_id
              FROM chado.cvterm C1
              INNER JOIN chado.cvterm_relationship CR on CR.object_id = C1.cvterm_id
              INNER JOIN chado.cvterm C2 on C2.cvterm_id = CR.type_id
              INNER JOIN chado.cv CV2 on CV2.cv_id = C2.cv_id
              WHERE CV2.name = 'relationship' AND C2.name = 'is_a'
            ) CAT on CAT.subject_id = C.cvterm_id
            WHERE C.cvterm_id = :cvterm_id
            AND (CV.name LIKE '%_trait_ontology')
            GROUP BY C.cvterm_id
        ";
        $objs = $this->chado->getQueryObjects($sql, [':cvterm_id' => $cvterm_id]);
        $results = count($objs) > 0 ? $objs[0] : NULL;
        return $results;
    }
    
    function getTraitDescriptor ($cvterm_id) {
        $sql = "
            SELECT 
              CV.name AS cv,
              CAT.name AS category,
              TRAIT.cvterm_id AS trait_id, 
              TRAIT.name AS trait, 
              TRAIT.cv AS trait_cv,
              C.name AS descriptor, 
              F.value AS format, 
              C.definition
            FROM chado.cvterm C
            INNER JOIN chado.cv CV on CV.cv_id = C.cv_id
            INNER JOIN chado.cvprop CVP on CVP.cv_id = CV.cv_id
            INNER JOIN chado.cvterm S on S.cvterm_id = CVP.type_id AND S.name = 'descriptor_search'
            INNER JOIN chado.cv S_CV on S_CV.cv_id = S.cv_id AND S_CV.name = 'MAIN'
            LEFT JOIN (
              SELECT cvterm_id, value 
              FROM chado.cvtermprop
              WHERE type_id = (
                SELECT cvterm_id FROM chado.cvterm C
                INNER JOIN chado.cv on CV.cv_id = C.cv_id
                WHERE C.name = 'format' AND cv.name = 'MAIN'
              )
            ) F on F.cvterm_id = C.cvterm_id
            LEFT JOIN (
              SELECT (SELECT name FROM chado.cv WHERE cv_id = C1.cv_id) AS cv, C1.name, C1.cvterm_id, CR.subject_id
                FROM chado.cvterm C1
                INNER JOIN chado.cvterm_relationship CR on CR.object_id = C1.cvterm_id
                INNER JOIN chado.cvterm C2 on C2.cvterm_id = CR.type_id
                INNER JOIN chado.cv CV2 on CV2.cv_id = C2.cv_id
                WHERE CV2.name = 'MAIN' AND C2.name = 'belongs_to'
            ) TRAIT on TRAIT.subject_id = C.cvterm_id
            LEFT JOIN (
              SELECT C1.name, C1.cvterm_id, CR.subject_id
              FROM chado.cvterm C1
              INNER JOIN chado.cvterm_relationship CR on CR.object_id = C1.cvterm_id
              INNER JOIN chado.cvterm C2 on C2.cvterm_id = CR.type_id
              INNER JOIN chado.cv CV2 on CV2.cv_id = C2.cv_id
              WHERE CV2.name = 'relationship' AND C2.name = 'is_a'
            ) CAT on CAT.subject_id = TRAIT.cvterm_id
            WHERE C.cvterm_id = :cvterm_id
            AND (CV.name LIKE '%_trait_descriptor' OR CV.name LIKE 'GRIN_%' OR CV.name LIKE '%_CRS' OR CV.name = 'QTL')
        ";
        $objs = $this->chado->getQueryObjects($sql, [':cvterm_id' => $cvterm_id]);
        $results = count($objs) > 0 ? $objs[0] : NULL;
        return $results;
    }
    
    /**
     * Counters
     */
    function countDescriptor ($cvterm_id) {
        $sql = "
            SELECT
                C1.cvterm_id,
                C1.name AS descriptor,
                CASE
                  WHEN (SELECT value FROM chado.cvprop WHERE cv_id = C1.cv_id AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'searchable' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))) IS NOT NULL
                  THEN   (SELECT value FROM chado.cvprop WHERE cv_id = C1.cv_id AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'searchable' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')))
                  ELSE     (SELECT name FROM chado.cv WHERE cv_id = C1.cv_id)
                  END
                AS group
              FROM chado.cvterm C1
              INNER JOIN chado.cvterm_relationship CR on CR.subject_id = C1.cvterm_id
              INNER JOIN chado.cvterm C2 on C2.cvterm_id = CR.type_id
              INNER JOIN chado.cv CV2 on CV2.cv_id = C2.cv_id
              WHERE CV2.name = 'MAIN' AND C2.name = 'belongs_to'
              AND object_id = :cvterm_id
        ";
        return $this->chado->countQuery($sql, [':cvterm_id' => $cvterm_id]);
    }
    
    function countXTL ($cvterm_id) {
        $sql = "
            SELECT
                FM.featuremap_id,
                FM.name AS map,
                LG.name AS linkage_group,
                (SELECT value FROM chado.featureprop WHERE feature_id = LG.feature_id AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'chr_name' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))) AS std_lg,
                START.value AS start,
                STOP.value AS stop,
                PEAK.value AS peak,
                QTL.feature_id,
                QTL.uniquename AS name,
                J.project_id,
                J.name AS project
          FROM chado.feature_cvterm FC
          INNER JOIN chado.feature QTL on QTL.feature_id = FC.feature_id
          LEFT JOIN chado.featurepos FP ON QTL.feature_id = FP.feature_id
          LEFT JOIN chado.featuremap FM ON FP.featuremap_id = FM.featuremap_id
          LEFT JOIN chado.feature LG ON FP.map_feature_id = LG.feature_id
          LEFT JOIN chado.feature_project FJ ON FJ.feature_id = FC.feature_id
          LEFT JOIN chado.project J ON J.project_id = FJ.project_id
          LEFT JOIN (
          SELECT featurepos_id, value
          FROM chado.featureposprop FPP
          WHERE FPP.type_id =
          (SELECT cvterm_id
          FROM chado.cvterm
          WHERE name = 'start' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
          ) START ON FP.featurepos_id = START.featurepos_id
          LEFT JOIN (
          SELECT featurepos_id, value
          FROM chado.featureposprop FPP
          WHERE FPP.type_id =
          (SELECT cvterm_id
          FROM chado.cvterm
          WHERE name = 'stop' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
          ) STOP ON FP.featurepos_id = STOP.featurepos_id
          LEFT JOIN (
          SELECT featurepos_id, value
          FROM chado.featureposprop FPP
          WHERE FPP.type_id =
          (SELECT cvterm_id
          FROM chado.cvterm
                   WHERE name = 'qtl_peak' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
            ) PEAK ON FP.featurepos_id = PEAK.featurepos_id
          WHERE cvterm_id = :cvterm_id
          AND QTL.type_id IN (SELECT cvterm_id FROM chado.cvterm WHERE name IN ('GWAS', 'QTL', 'heritable_phenotypic_marker'))
          ORDER BY QTL.type_id, QTL.uniquename
            ";
        return $this->chado->countQuery($sql, [':cvterm_id' => $cvterm_id]);
    }
    
    function countImage($cvterm_id) {
        $sql = "
           SELECT 
                I.eimage_id, 
                eimage_type, 
                eimage_data,
                image_uri, 
                value as legend
           FROM chado.cvterm_image CI
           INNER JOIN chado.eimage I ON CI.eimage_id = I.eimage_id
           INNER JOIN chado.eimageprop IP ON I.eimage_id = IP.eimage_id
           WHERE IP.type_id = (SELECT cvterm_id
                                                FROM chado.cvterm
                                                WHERE name = 'legend'
                                                AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')
                                              )
           AND cvterm_id = :cvterm_id
        ";
        return $this->chado->countQuery($sql, [':cvterm_id' => $cvterm_id]);
    }
    
    
    function countAlias ($cvterm_id) {
        return $this->chado->count('cvtermsynonym', ['synonym'], [], ['cvterm_id' => $cvterm_id]);
    }
    
    function countStock ($cvterm_id) {
        $sql = "
          SELECT 
            Sample.stock_id AS sample_id, 
            Sample.uniquename AS sample_name, 
            P.value, 
            S.stock_id, 
            S.uniquename AS stock_uniquename, 
            O.organism_id, 
            species
          FROM chado.phenotype P
          INNER JOIN chado.nd_experiment_phenotype NEP ON P.phenotype_id = NEP.phenotype_id
          INNER JOIN chado.nd_experiment_stock NES ON NEP.nd_experiment_id = NES.nd_experiment_id
          INNER JOIN chado.stock Sample ON Sample.stock_id = NES.stock_id
          INNER JOIN chado.stock_relationship SR ON SR.subject_id = Sample.stock_id AND SR.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'sample_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
          INNER JOIN chado.stock S ON S.stock_id = SR.object_id
          INNER JOIN chado.organism O ON O.organism_id = S.organism_id
          WHERE P.attr_id =  :cvterm_id
        ";
        return $this->chado->countQuery($sql, [':cvterm_id' => $cvterm_id]);
    }
    
    function countProject ($cvterm_id) {
        $sql = "
          SELECT 
            DISTINCT J.project_id, J.name, JP.value AS type
          FROM chado.phenotype P
          INNER JOIN chado.nd_experiment_phenotype NEP ON P.phenotype_id = NEP.phenotype_id
          INNER JOIN chado.nd_experiment_project NEJ ON NEP.nd_experiment_id = NEJ.nd_experiment_id
          INNER JOIN chado.project J ON J.project_id = NEJ.project_id
          INNER JOIN chado.projectprop JP ON JP.project_id = J.project_id AND JP.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'project_type' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
          WHERE P.attr_id =  :cvterm_id
        ";
        return $this->chado->countQuery($sql, [':cvterm_id' => $cvterm_id]);
    }
    
    function countPub ($cvterm_id) {
          $sql = "
          SELECT
            DISTINCT P.pub_id, title, series_name, pyear, V.name AS type
          FROM chado.pub P
          INNER JOIN chado.cvterm V ON V.cvterm_id = P.type_id
          INNER JOIN chado.feature_pub FP ON FP.pub_id = P.pub_id
          INNER JOIN chado.feature_cvterm FV ON FV.feature_id = FP.feature_id
          WHERE FV.cvterm_id =  :cvterm_id
          ORDER BY pyear DESC
        ";
          return $this->chado->countQuery($sql, [':cvterm_id' => $cvterm_id]);
    }

}