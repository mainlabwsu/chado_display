<?php

namespace Drupal\chado_display\Database;

/**
 * Counter class represents a countable object returned by running a paged SQL query
 */

class QueryCounter extends Counter {

  protected $chado;
  protected $total;
  protected $sql;
  protected $args;


  function __construct (Chado $c, $total, $sql, $args) {
    $this->chado = $c;
    $this->total = $total;
    $this->sql = $sql;
    $this->args = $args;
  }

  function getObjects() {
    return $this->chado->getQueryObjects($this->sql, $this->args);
  }

  function getPagedObjects($page) {
    $limit = $this->num_per_page;
    $offset = $page * $this->num_per_page;
    $sql = $this->sql;
    if ($limit != 0) {
        $sql .= ' LIMIT ' . $limit;
    }
    if ($offset != 0) {
        $sql .= ' OFFSET ' . $offset;
    }
    return $this->chado->getQueryObjects($sql, $this->args);
  }
  
  function getQuery() {
    $query = new \stdClass();
    $query->sql = $this->sql;
    $query->args = $this->args;
    return $query;
  }
}
