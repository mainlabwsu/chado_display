<?php

namespace Drupal\chado_display\Database;

use Drupal\chado_display\Database\Prepared\BaseStatement;
use Drupal\chado_display\Database\Prepared\AnalysisStatement;
use Drupal\chado_display\Database\Prepared\ContactStatement;
use Drupal\chado_display\Database\Prepared\CvtermStatement;
use Drupal\chado_display\Database\Prepared\FeatureStatement;
use Drupal\chado_display\Database\Prepared\ImageStatement;
use Drupal\chado_display\Database\Prepared\LibraryStatement;
use Drupal\chado_display\Database\Prepared\OrganismStatement;
use Drupal\chado_display\Database\Prepared\MapStatement;
use Drupal\chado_display\Database\Prepared\ProjectStatement;
use Drupal\chado_display\Database\Prepared\PubStatement;
use Drupal\chado_display\Database\Prepared\StockStatement;
use Drupal\chado_display\Database\Prepared\StockCollectionStatement;

/**
 * This class provide reusable SQL statements
 */
class Statement {

  protected $chado;
  public $base;
  public $analysis;
  public $contact;
  public $cvterm;
  public $eimage;
  public $feature;
  public $library;
  public $organism;
  public $featuremap;
  public $project;
  public $pub;
  public $stock;
  public $stockcollection;

  function __construct (Chado $chado) {
    $this->chado = $chado;
    $this->base = new BaseStatement($chado);
    $this->analysis = new AnalysisStatement($chado);
    $this->contact = new ContactStatement($chado);
    $this->cvterm = new CvtermStatement($chado);
    $this->eimage = new ImageStatement($chado);
    $this->feature = new FeatureStatement($chado);
    $this->library = new LibraryStatement($chado);
    $this->organism = new OrganismStatement($chado);
    $this->featuremap = new MapStatement($chado);
    $this->project = new ProjectStatement($chado);
    $this->pub = new PubStatement($chado);
    $this->stock = new StockStatement($chado);
    $this->stockcollection = new StockCollectionStatement($chado);
  }
}