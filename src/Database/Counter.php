<?php

namespace Drupal\chado_display\Database;

/**
 * Counter class represents a countable object returned by running a paged SQL query
 */

class Counter {
  protected $chado;
  protected $total;
  protected $table;
  protected $select;
  protected $joins;
  protected $conditions;
  protected $order;
  protected $limit;
  protected $offset;

  public $total_pages;
  public $num_per_page = 0;
  public $page;

  function __construct (Chado $c = NULL, $total = 0, $table = NULL, $select = [], $joins = [], $conditions = [], $order =NULL, $limit = 0, $offset = 0) {
    $this->chado = $c;
    $this->total = $total;
    $this->table = $table;
    $this->select = $select;
    $this->joins = $joins;
    $this->conditions = $conditions;
    $this->order = $order;
    $this->limit = $limit;
    $this->offset = $offset;
  }

  function getTotal () {
    return $this->total;
  }

  function getObjects() {
      if ($this->chado && $this->table) {
        return $this->chado->getObjects($this->table, $this->select, $this->joins, $this->conditions, $this->order, $this->limit, $this->offset);
      }
      else {
        return NULL;
      }
  }

  function setPager($num_per_page, $pager_id) {
    $this->num_per_page = $num_per_page;
    $total_pages = (int) ($this->total / $num_per_page);
    if ($this->total % $num_per_page != 0) {
      $total_pages ++;
    }
    $this->total_pages = $total_pages;
    $pager = \Drupal::service('pager.manager')->createPager($this->total, $num_per_page, $pager_id);
    return $pager;
  }

  function getPagedObjects($page) {
    if ($this->chado && $this->table) {
      $limit = $this->num_per_page;
      $offset = $page * $this->num_per_page;
      return $this->chado->getObjects($this->table, $this->select, $this->joins, $this->conditions, $this->order, $limit, $offset);
    }
    else {
      return [];
    }
  }
  
  function getQuery() {
    return $this->chado->getQuery($this->table, $this->select, $this->joins, $this->conditions, $this->order, $this->limit, $this->offset);
  }
}
