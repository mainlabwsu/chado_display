<?php

namespace Drupal\chado_display\Database;

class Chado {

  public $verbose;
  public $handle_log;
  private $schema;
  private $prefix;

  function __construct($verbose = TRUE, $schema = 'chado') {
    $this->verbose = $verbose;
    $this->schema = $schema;
    $this->prefix = $this->schema . '.';
  }

  function __destruct() {
    if (is_resource($this->handle_log)) {
      fclose($this->handle_log);
    }
  }
  
  function query($sql, $args, $throw_exception = FALSE) {
    try {
      return \Drupal::database()->query($sql, $args);
    } catch (\Exception $e) {
      $trace = debug_backtrace();
      $caller = $trace[1];
      \Drupal::logger('chado_display')->error('Error: Query failed. calling from ' . $caller['function'] . ' (line ' . $caller['line'] . ' of ' . $caller['file'] . '). ' . $e->getMessage());
      if ($throw_exception) {
        throw $e;
      }
    }
  }

  function setLog($file) {
    $this->handle_log = fopen($file, "w");
  }

  /**
   * Get Primary Key for a Chado table
   */
  function getPkey($table) {
    if (isset($_SESSION['chado_display-cache_pk'][$table])) {
      return $_SESSION['chado_display-cache_pk'][$table];
    }
    else {
      $cache_exists = \Drupal::database()->schema()->tableExists('chado_display_cache_chado_pk');
      
      $sql = "SELECT column_name
      FROM chado_display_cache_chado_pk 
      WHERE table_name = '$table'";

      if (!$cache_exists) {
        $sql = "
        SELECT C.column_name
        FROM information_schema.table_constraints TC
        JOIN information_schema.constraint_column_usage AS CCU USING (constraint_schema, constraint_name)
        JOIN information_schema.columns AS C ON C.table_schema = TC.constraint_schema
        AND TC.table_name = C.table_name AND CCU.column_name = C.column_name
        WHERE CCU.table_schema = C.table_schema
        AND constraint_type = 'PRIMARY KEY' AND TC.table_name = '$table' AND C.table_schema = '" . $this->schema ."'";
      }

      $pk = $this->query($sql, array())->fetchField();
      $_SESSION['chado_display-cache_pk'][$table] = $pk;
      return $pk;
    }
  }

  /**
   * Get Unique Keys for a Chado table
   */
  function getUkeys($table) {
    if (isset($_SESSION['chado_display-cache_uk'][$table])) {
      return $_SESSION['chado_display-cache_uk'][$table];
    }
    else {
      $cache_exists = \Drupal::database()->schema()->tableExists('chado_display_cache_chado_uk');
      
      $sql = "SELECT column_name
      FROM chado_display_cache_chado_uk 
      WHERE table_name = '$table'";

      if (!$cache_exists) {
      $sql = "
        SELECT C.column_name
        FROM information_schema.table_constraints TC
        JOIN information_schema.constraint_column_usage AS CCU USING (constraint_schema, constraint_name)
        JOIN information_schema.columns AS C ON C.table_schema = TC.constraint_schema
        AND TC.table_name = C.table_name AND CCU.column_name = C.column_name
        WHERE CCU.table_schema = C.table_schema
        AND constraint_type = 'UNIQUE' and TC.table_name = '$table' AND C.table_schema = '" . $this->schema ."'";
      }
 
      $results = $this->query($sql, array());
      $ukeys = array();
      while ($uk = $results->fetchField()) {
        $ukeys [] = $uk;
      }
      $_SESSION['chado_display-cache_uk'][$table] = $pk;
      return $ukeys;
    }
  }

  /*
   * Get typed columns
   */
  function getTypedColumns($table) {
    $columns = [];
    if (isset($_SESSION['chado_display-cache_typed_cols'][$table])) {
      return $_SESSION['chado_display-cache_typed_cols'][$table];
    }
    else {
      $cache_exists = \Drupal::database()->schema()->tableExists('chado_display_cache_chado_typed_cols');
      
      $sql = "SELECT column_name, data_type, character_maximum_length
      FROM chado_display_cache_chado_typed_cols 
      WHERE table_name = :table";

      if (!$cache_exists) {
      $sql = "
        SELECT C.column_name, C.data_type, C.character_maximum_length
        FROM INFORMATION_SCHEMA.TABLES T
        INNER JOIN INFORMATION_SCHEMA.COLUMNS C ON C.table_name = T.table_name AND C.table_schema = T.table_schema
        WHERE T.table_type = 'BASE TABLE' AND T.table_schema = '$this->schema' AND T.table_name = :table";
      }

      $results = $this->query($sql, array(':table' => $table));
      while ($obj = $results->fetchObject()) {
        $columns [$obj->column_name] = $obj->data_type;
      }
    }
    $_SESSION['chado_display-cache_typed_cols'][$table] = $columns;
    return $columns;
  }

  /*
   * Get Not Null Columns
   */
  function getNotNullColumns($table) {
    $columns = [];
    if (isset($_SESSION['chado_display-cache_null_cols'][$table])) {
      return $_SESSION['chado_display-cache_null_cols'][$table];
    }
    else {
      $cache_exists = \Drupal::database()->schema()->tableExists('chado_display_cache_chado_null_cols');
      
      $sql = "SELECT column_name
      FROM chado_display_cache_chado_null_cols 
      WHERE table_name = :table";

      if (!$cache_exists) {
      $sql = "
        SELECT C.column_name
        FROM INFORMATION_SCHEMA.TABLES T
        INNER JOIN INFORMATION_SCHEMA.COLUMNS C ON C.table_name = T.table_name AND C.table_schema = T.table_schema
        WHERE T.table_type = 'BASE TABLE' AND T.table_schema = '$this->schema' AND T.table_name = :table AND C.is_nullable = 'NO'";
      }

      $results = $this->query($sql, array(':table' => $table));
      while ($obj = $results->fetchObject()) {
        $columns [] = $obj->column_name;
      }
    }
    $_SESSION['chado_display-cache_null_cols'][$table] = $columns;
    return $columns;
  }
  
  /*
   * Get Column Default Value
   */
  function getColumnDefault($table) {
    $columns = [];
    if (isset($_SESSION['chado_display-cache_col_default'][$table])) {
      return $_SESSION['chado_display-cache_col_default'][$table];
    }
    else {
      $cache_exists = \Drupal::database()->schema()->tableExists('chado_display_cache_chado_col_default');
      
      $sql = "SELECT column_name, column_default
      FROM chado_display_cache_chado_col_default 
      WHERE table_name = :table";

      if (!$cache_exists) {
        $sql = "
        SELECT C.column_name, column_default
        FROM INFORMATION_SCHEMA.TABLES T
        INNER JOIN INFORMATION_SCHEMA.COLUMNS C ON C.table_name = T.table_name AND C.table_schema = T.table_schema
        WHERE T.table_type = 'BASE TABLE' AND T.table_schema = '$this->schema' AND T.table_name = :table AND C.column_default IS NOT NULL";
      }

      $results = $this->query($sql, array(':table' => $table));
      while ($obj = $results->fetchObject()) {
        $columns [$obj->column_name] = $obj->column_default;
      }
    }
    $_SESSION['chado_display-cache_col_default'][$table] = $columns;
    return $columns;
  }
  
  /*
   * Get FKey Columns
   */
  function getFKeyColumns($table) {
    $columns = [];
    if (isset($_SESSION['chado_display-cache_fk_cols'][$table])) {
      return $_SESSION['chado_display-cache_fk_cols'][$table];
    }
    else {
      $cache_exists = \Drupal::database()->schema()->tableExists('chado_display_cache_chado_fk_cols');
      
      $sql = "SELECT constraint_name, key, foreign_table, foreign_key
      FROM chado_display_cache_chado_fk_cols
      WHERE table_name = :table";

      if (!$cache_exists) {
        $sql = "
            SELECT A.constraint_name, C.column_name AS key, A.table_name AS foreign_table, A.column_name AS foreign_key
            FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE A
            INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS B ON A.constraint_name = B.constraint_name AND A.table_schema = B.table_schema
            INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE C ON A.constraint_name = C.constraint_name AND C.table_schema = A.table_schema
            WHERE constraint_type = 'FOREIGN KEY'
            AND A.table_schema = '$this->schema' 
            AND C.table_name = :table";
      }

      $results = $this->query($sql, array(':table' => $table));
      
      while ($obj = $results->fetchObject()) {
        $columns [$obj->key] = [$obj->foreign_table => $obj->foreign_key];
      }
    }
    $_SESSION['chado_display-cache_fk_cols'][$table] = $columns;
    return $columns;
  }
  
  /*
   * No fail. Pass in any function and it will not fail on Exceptions. Only error message will be shown
   */
  public function noFail ($function, $args, $rowcount = 0, $silent = FALSE) {
    try {
      return call_user_func_array(array($this, $function), $args);
    } catch (\Exception $e) {
      if ($this->verbose && !$silent) {
        if ($rowcount > 0) {
          print '[Error at Line ' . $rowcount . '] ';
        }
        print $e->getMessage() . "\n";
      }
    }
  }

  /**
   * Get ID (i.e. the primary key) from a table by passing in the unique key values
   */
  public function getId ($table, $conditions = array(), $insert_if_not_exist = FALSE) {
    $pk = $this->getPkey($table);
    $all_ukey_exists = TRUE;
    $uks = $this->getUkeys($table);
    $uniqcond = array();
    foreach ($uks AS $uk) {
      if (!key_exists($uk, $conditions)) {
        $all_ukey_exists = FALSE;
      }
      else {
        $uniqcond[$uk] = $conditions[$uk];
      }
    }

    if ($all_ukey_exists) {
      $id = $this->getFirstField($table, $pk, $uniqcond);
      if ($id) {
        if (is_resource($this->handle_log)) {
          $content = 'Found> [' . $table . ': ' . $id . '] (' . implode(', ', array_keys($uniqcond)). ') '. implode (', ', $uniqcond) . "\n";
          fwrite($this->handle_log, $content);
        }
        return $id;
      }
      else if ($insert_if_not_exist) {
        return $this->setField($table, $conditions, $pk);
      }
      else {
        return NULL;
      }
    }
    else {
      $err_uks = '(' . implode(', ', $uks) . ')';
      throw new \Exception("[Chado::getId] Unique key constraints not met. Provide all unique keys $err_uks for table '$table' to get an ID.");
    }
  }

  /**
   * Get returned value from a table using the primary key
   */
  public function getFieldById ($table, $field, $id) {
    $pk = $this->getPkey($table);
    if ($pk) {
      $sql = 'SELECT ' . $field . ' FROM ' . $this->prefix . $table . ' WHERE ' . $pk . ' = :id';
      $result = $this->query($sql, array(':id' => $id));
      return $result->fetchField();
    }
    else {
      return NULL;
    }
  }

  /**
   * Get first returned value from a table
   */
  public function getFirstField ($table, $field, $conditions = array(), $order = array(), $insert_if_not_exist = FALSE) {

    $fields = $this->getFields($table, $field, $conditions, $order, 1);
    if (count($fields) > 0) {
      return $fields [0];
    }
    else if ($insert_if_not_exist) {
        return $this->setField($table, $conditions, $field);
    }
    else {
      return NULL;
    }
  }

  /**
   * Get all returned values in an array from a table.
   */
  public function getFields ($table, $field, $conditions = array(), $order = array(), $limit = 0, $offset = 0) {
    $fields = array();
    $results = $this->getResults ($table, array($field), array(), $conditions, $order, $limit, $offset);
    while (($f = $results->fetchField()) != NULL) {
      $fields [] = $f;
    }
    return $fields;
  }

  /**
   * Get result as an object from a table using the primary key
   */
  public function getObjectById ($table, $select = array(), $id = 0) {
    $pk = $this->getPkey($table);
    if ($pk) {
      $sql = 'SELECT ' . implode(',', $select) . ' FROM ' . $this->prefix . $table . ' WHERE ' . $pk . ' = :id';
      $result = $this->query($sql, array(':id' => $id));
      return $result->fetchObject();
    }
    else {
      return NULL;
    }
  }

  /**
   * Get first result as an object from a table.
   */
  public function getFirstObject ($table, $select = array(), $joins = array(), $conditions = array(), $order = array()) {
    $objects = $this->getObjects($table, $select, $joins, $conditions, $order, 1);
    if (count($objects) == 1) {
      return $objects[0];
    }
    else {
      return NULL;
    }
  }

  /**
   * Get all results in an object array from a table.
   */
  public function getObjects ($table, $select = array(), $joins = array(), $conditions = array(), $order = array(), $limit = 0, $offset = 0) {
    $objects = array();
    $results = $this->getResults ($table, $select, $joins, $conditions, $order, $limit, $offset);
    try {
      while ($obj = $results->fetchObject()) {
        $objects [] = $obj;
      }
    } catch (\Error $e) {
      $trace = debug_backtrace();
      $caller = $trace[1];
      \Drupal::logger('chado_display')->error('Error: Failed to retrieve result. calling from ' . $caller['function'] . ' (line ' . $caller['line'] . ' of ' . $caller['file'] . '). ' . $e->getMessage());
    }
    return $objects;
  }

  /**
   * Get all results in an object array directly from an SQL statement.
   */
  public function getQueryObjects ($sql, $args) {
    $objects = array();
    $results = $this->query($sql, $args);
    try {
      while ($obj = $results->fetchObject()) {
        $objects [] = $obj;
      }
    } catch (\Error $e) {
      $trace = debug_backtrace();
      $caller = $trace[1];
      \Drupal::logger('chado_display')->error('Error: Failed to retrieve result. calling from ' . $caller['function'] . ' (line ' . $caller['line'] . ' of ' . $caller['file'] . '). ' . $e->getMessage());
    }
    return $objects;
  }

  /**
   * Get count. Return a Counter object which has both getTotal() and getObjects() methods
   */
  public function count ($table, $select = array(), $joins = array(), $conditions = array(), $order = array(), $limit = 0, $offset = 0) {
    $result = $this->getResults ($table, array('count(*)'), $joins, $conditions, array(), $limit, $offset);
    $total = $result->fetchField();
    $counter = new Counter($this, $total, $table, $select, $joins, $conditions, $order, $limit, $offset);
    return $counter;
  }

  /**
   * Get count directly from an SQL statement
   */
  public function countQuery($sql, $args = [], $limit = 0, $offset = 0) {
      if ($limit != 0) {
          $sql .= ' LIMIT ' . $limit;
      }
      if ($offset != 0) {
          $sql .= ' OFFSET' . $offset;
      }
    $result = $this->query("SELECT count(*) FROM ($sql) T", $args);
    $total = $result->fetchField();
    $counter = new QueryCounter($this, $total, $sql, $args);
    return $counter;
  }

  /**
   * Get results from a table. The SELECT SQL generator.
   */
  public function getResults ($table, $select = array(), $joins = array(), $conditions = array(), $order = array(), $limit = 0, $offset = 0) {
    $query = $this->getQuery ($table, $select, $joins, $conditions, $order, $limit, $offset);
    $results = $this->query($query->sql, $query->args);
    return $results;
  }

  /**
   * SQL generator. Returns Drupal style SQL with arguments
   */
  public function getQuery ($table, $select = array(), $joins = array(), $conditions = array(), $order = array(), $limit = 0, $offset = 0) {
    // SELECT clause
    $sql = 'SELECT ' . implode(',', $select) . ' FROM ' . $this->prefix . $table;

    // JOIN clause
    foreach ($joins AS $jtype => $jconditions) {
        if (strcasecmp($jtype, 'left') == 0 || strcasecmp($jtype, 'inner') == 0) {
            foreach ($jconditions AS $t => $on) {
                $has_schema = count(explode('.', $t)) > 1 ? TRUE : FALSE;
                if (!$has_schema) {
                    $t = $this->prefix . $t;
                }
                $sql .= ' ' . $jtype . ' JOIN ' . $t . ' ON ' . $on . ' ';
            }
        }
    }

    // WHERE clause
    $sql .= ' WHERE 1=1';
    $args = array();
    foreach ($conditions AS $column => $value) {
      if (is_array($value)) {
        $sql .= ' AND ' . $column . ' IN (' . ':' . str_replace('.', '_', $column) . '[])';
        $args [':' . str_replace('.', '_', $column) . '[]'] = $value;
      }
      else {
        $sql .= ' AND ' . $column . ' = ' . ':' . str_replace('.', '_', $column);
        $args [':' . str_replace('.', '_', $column)] = $value;
      }
    }

    // ORDER BY clause
    if (count($order) > 0) {
      $sql .= ' ORDER BY ' . implode(',', $order);
      $order_keys = array_keys($order);
      $okey = $order_keys[0];
      if ($okey == 'desc') {
        $sql .= ' DESC';
      }
      else if ($okey == 'asc') {
        $sql .= ' ASC';
      }
    }

    // LIMIT and OFFSET
    if ($limit > 0) {
      $sql .= ' LIMIT ' . $limit;
    }
    if ($offset > 0) {
      $sql .= ' OFFSET ' . $offset;
    }
    $query = new \stdClass();
    $query->sql = $sql;
    $query->args = $args;
    return $query;
  }


  /**
   * Insert one record to a table. The INSERT SQL generator.
   */
  public function setField($table, $conditions = array(), $returning = NULL, $throw_exception = FALSE) {
    $columns = array_keys($conditions);
    $args = array();
    $holders = array();
    foreach ($conditions AS $col => $val) {
      $args[':' . $col] = $val;
      $holders [] = ':' . $col;
    }
    $sql = 'INSERT INTO ' . $this->prefix . $table . '(' . implode(',', $columns) . ') VALUES (' . implode(',', $holders) . ')';
    if ($returning) {
      $sql .= ' RETURNING ' . $returning;
    }
    $result = $this->query($sql, $args, $throw_exception);
    $return_val = $result == NULL ? NULL : $result->fetchField();
    if (is_resource($this->handle_log)) {
      $content = 'Insert> ['. $table;
      if ($returning) {
        $content .= ': ' . $return_val;
      }
      $content .= '] (' . implode(', ', $columns) . ') ' . implode(', ', $conditions) . "\n";
      fwrite($this->handle_log, $content);
    }
    if ($returning) {
      return $return_val;
    }
  }

  /**
   * Update records to a table. The UPDATE SQL generator.
   */
  public function updateFields($table, $values = array(), $conditions = array(), $skip_empty = FALSE, $throw_exception = FALSE) {
    if (count($conditions) == 0 || !$conditions) {
      throw new \Exception('Updating all values in a table is not supported.');
    }
    $columns = array_keys($values);
    $args = array();
    $holders = array();
    foreach ($values AS $col => $val) {
      if ($skip_empty && trim($val) == '') {
        continue;
      }
      $args[':' . $col] = $val;
      $holders [$col] = ':' . $col;
    }
    $sql = 'UPDATE ' . $this->prefix . $table . ' SET ';
    $index = 0;
    $total = count($columns);
    foreach ($columns AS $column) {
      if (isset($holders[$column])) {
        $sql .= $column . ' = ' . $holders[$column];
        if ($index < $total -1) {
          $sql .= ', ';
        }
      }
      $index ++;
    }
    // WHERE clause
    $sql .= ' WHERE 1=1';
    foreach ($conditions AS $column => $value) {
        $sql .= ' AND ' . $column . ' = ' . ':' . str_replace('.', '_', $column);
        $args [':' . str_replace('.', '_', $column)] = $value;
    }
    //dpm($sql);
    //dpm($args);
    $result = $this->query($sql, $args, $throw_exception);
   
    if (is_resource($this->handle_log)) {
      $content = 'Update> ['. $table;
      $content .= '] (' . implode(', ', $columns) . ') ' . implode(', ', $values) . "\n";
      fwrite($this->handle_log, $content);
    }
  }
  
  /**
   * Delete records from a table. The DELETE SQL generator.
   */
  public function delete($table, $conditions = array()) {
    if (count($conditions) == 0 || !$conditions) {
      throw new \Exception('Deleting all values in a table is not supported.');
    }
    
    $args = array();
    $sql = 'DELETE FROM ' . $this->prefix . $table;
    // WHERE clause
    $sql .= ' WHERE 1=1';
    foreach ($conditions AS $column => $value) {
      $sql .= ' AND ' . $column . ' = ' . ':' . str_replace('.', '_', $column);
      $args [':' . str_replace('.', '_', $column)] = $value;
    }
    //dpm($sql);
    //dpm($args);
    $result = $this->query($sql, $args);
    
    if (is_resource($this->handle_log)) {
      $content = 'Delete> ['. $table;
      $content .= '] (' . implode(', ', array_keys($conditions)) . ') ' . implode(', ', $conditions) . "\n";
      fwrite($this->handle_log, $content);
    }
  }
  
  /**
   * Check if a table exists in the chado schema
   */
  public function tableExists($table) {
    $db = \Drupal::database();
    if($db->schema()->tableExists($this->prefix . $table)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
  
  /**
   * Check if a column exists in the chado table
   */
  public function columnExists($table, $column) {
    $db = \Drupal::database();
    if($db->schema()->fieldExists($this->prefix . $table, $column)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
}
