<?php

namespace Drupal\chado_display\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    
    $biodata_reroute = \Drupal::state()->get('chado_display_biodata_reroute', FALSE);
    
    if ($biodata_reroute) {
      // If Tripal /bio_data/{tripal_entity} route exists, remove it
      if ($route = $collection->get('entity.tripal_entity_type.canonical')) {
        $collection->remove('entity.tripal_entity_type.canonical');
      }
      // Take over the /bio_data/{tripal_entity} route
      if ($route = $collection->get('chado_display.bio_data_reroute')) {
        $route->setPath('/bio_data/{tripal_entity}');
      }
    }
    
    // Remove route for extra pages when they are not enabled
    $extra = \Drupal::state()->get('chado_display_extra_render_info');
    if ($extra) {
      if ($extra['json'] === 0) {
        $collection->remove('chado_display.json');
      }
      if ($extra['allele'] === 0) {
        $collection->remove('chado_display.allele');
      }
      if ($extra['gene_class'] === 0) {
        $collection->remove('chado_display.gene_class');
      }
      if ($extra['gene_class_listing'] === 0) {
        $collection->remove('chado_display.gene_class_listing');
      }
      if ($extra['polymorphism'] === 0) {
        $collection->remove('chado_display.polymorphism');
      }
      if ($extra['trait'] === 0) {
        $collection->remove('chado_display.trait');
      }
      if ($extra['trait_descriptor'] === 0) {
        $collection->remove('chado_display.trait_descriptor');
      }
      if ($extra['trait_listing'] === 0) {
        $collection->remove('chado_display.trait_listing');
      }
    }
  }

}