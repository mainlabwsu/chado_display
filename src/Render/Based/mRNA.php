<?php
namespace Drupal\chado_display\Render\Based;

class mRNA extends Feature {
  function __construct($id) {
    parent::__construct($id);
    $this->changeHeader('overview', 'mRNA Overview');
    $this->addProteinSequence();
  }

  function addProteinSequence() {
    $counter = $this->statement->feature->countObjectFeatures($this->feature->feature_id, 'derives_from', NULL, 'polypeptide');
    $proteins = $counter->getObjects();

    $display = '';
    foreach($proteins AS $protein) {
      $default_residues = \Drupal::state()->get('chado_display_residues', 100000);
      $seqlen = $protein->residues ? strlen($protein->residues) : 0;
      $fasta =
        '<div class="chado_page-section_download chado_display-page_control">' .
          \Drupal\Core\Link::createFromRoute('FASTA', 'chado_display.fasta', ['info' => base64_encode(serialize($protein->feature_id))])->toString() .
        '</div>';
      if ($seqlen > 0) {
        if ($seqlen <= $default_residues) {
          $display .= $this->residues($protein->uniquename, $protein->residues, $protein->name, $protein->type, $protein->organism, $seqlen);
        }
        else {
          $display .= $protein->name . '\' contains ' . $seqlen . ' amino acids.  (not shown)<br>';
        }
      }
    }
    if ($display) {
      $this->addContent('protein', 'Protein Sequence', ['#markup' => $fasta . $display], 110);
    }
  }
}