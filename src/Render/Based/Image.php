<?php
namespace Drupal\chado_display\Render\Based;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Render\Renderer;

class Image extends Renderer {

  public $eimage;

  function __construct($id) {
    parent::__construct($id);
  }

  function createPage($id) {
    $this->eimage = $this->chado->getObjectById('eimage', ['*'], $id);
    $this->setTitle($this->eimage->eimage_data);
    $info = \Drupal::state()->get('chado_display_image_render_info', chado_display_render_info($this));
    $this->addContentByInfo($info);
    $this->addSidebarPropertyItems('eimage', 'eimage_id');
  }

  function renderOverview () {
    $eimage = $this->eimage;
    $table = $this->tableBuilder(TRUE);
    $table->addRow(['Data', $eimage->eimage_data], $eimage->eimage_data);
    $table->addRow(['Type', $eimage->eimage_type], $eimage->eimage_type);
    $table->addRow(['URI', $eimage->image_uri], $eimage->image_uri);
    
    $counter = $this->statement->base->countProperty('eimage', $eimage->eimage_id);
    $props = $counter->getObjects();
    $default_props = \Drupal::state()->get('chado_display_eimageprop', []);
    foreach ($props AS $prop) {
        if (isset($default_props[$prop->type_id]) && $default_props[$prop->type_id]) {
          $table->addRow([ucwords(str_replace('_' , ' ', $prop->type)), Markup::create($prop->value)], $prop->value);
        }
    }
    $overview = [];
    $overview [] = $table->render();
    // Overview image
    $image = $this->getOverviewImageRenderArray('eimage', $this->eimage->eimage_id);
    if($image) {
      $overview [] = $image;
    }
    $this->addContent('overview', 'Image Overview', $overview, -100);
  }

  function renderContacts() {
    if ($this->chado->tableExists('contact_image')) {
      $headers = ['Name', 'Type'];
      $rows = [
        ['link:contact,contact_id' => 'name'],
        'type'
      ];
      $this->addPreset('Contacts', 'eimage', 'countContact', $headers, $rows);
    }
  }
  
  function renderProjects () {
    if ($this->chado->tableExists('project_image')) {
      $headers = ['Name'];
      $rows = [
        ['link:project,project_id' => 'name'],
      ];
      $this->addPreset('Projects', 'eimage', 'countProject', $headers, $rows);
    }
  }
  
  function renderPublications () {
    if ($this->chado->tableExists('pub_image')) {
      $headers = ['Title', 'Series', 'Year', 'Type'];
      $rows = [
        ['link:pub,pub_id' => 'title'],
        'series_name',
        'pyear',
        'type'
      ];
      $this->addPreset('Publications', 'eimage', 'countPub', $headers, $rows);
    }
  }
  
  function renderSequences() {
    if ($this->chado->tableExists('feature_image')) {
      $headers = ['Name', 'Type'];
      $rows = [
        ['link:feature,feature_id' => 'uniquename|name'],
        'type'
      ];
      $this->addPreset('Sequences', 'eimage', 'countFeature', $headers, $rows);
    }
  }
  
  function renderSpecies() {
    if ($this->chado->tableExists('organism_image')) {
      $headers = ['Genus', 'Species', 'Common Name'];
      $rows = [
        ['link:organism,organism_id' => 'genus'],
        ['link:organism,organism_id' => 'species'],
        ['empty:-' => 'common_name']
      ];
      $this->addPreset('Species', 'eimage', 'countOrganism', $headers, $rows);
    }
  }
  
  function renderStocks () {
    if ($this->chado->tableExists('stock_image')) {
      $headers = ['Name', 'Uniquename', 'Type'];
      $rows = [
        ['link:stock,stock_id' => 'name'],
        ['link:stock,stock_id' => 'uniquename'],
        'type'
      ];
      $this->addPreset('Stocks', 'eimage', 'countStock', $headers, $rows);
    }
  }
  
  function renderTraitDescriptors() {
    if ($this->chado->tableExists('cvterm_image')) {
      $headers = ['Name'];
      $rows = [
        ['urlprefix:/display/trait/,cvterm_id' => 'name'],
      ];
      $this->addPreset('Trait Descriptors', 'eimage', 'countTraitDescriptor', $headers, $rows);
    }
  }
  
  function renderTraits() {
    if ($this->chado->tableExists('cvterm_image')) {
      $headers = ['Name'];
      $rows = [
        ['urlprefix:/display/trait/,cvterm_id' => 'name'],
      ];
      $this->addPreset('Traits', 'eimage', 'countTrait', $headers, $rows);
    }
  }
}