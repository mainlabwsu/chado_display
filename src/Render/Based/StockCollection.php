<?php
namespace Drupal\chado_display\Render\Based;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Render\Renderer;

class StockCollection extends Renderer {

  public $stockcollection;

  function __construct($id) {
    parent::__construct($id);
  }

  function createPage($id) {
    $stockcollection = $this->statement->stockcollection->getStockCollection($id);
    $this->stockcollection = $stockcollection;
    $this->setTitle($stockcollection->name);
    $info = \Drupal::state()->get('chado_display_stockcollection_render_info', chado_display_render_info($this));
    $this->addContentByInfo($info);
    $this->addSidebarPropertyItems('stockcollection', 'stockcollection_id');
  }

  function renderOverview () {
    $stockcollection = $this->stockcollection;
    $table = $this->tableBuilder(TRUE);
    $table->addRow(['Name', $stockcollection->name], $stockcollection->name);
    $table->addRow(['Unique Name', $stockcollection->uniquename], $stockcollection->uniquename);
    $table->addRow(['Type', $stockcollection->type], $stockcollection->type);
    $table->addRow(['Contact', $this->link('contact', $stockcollection->contact_id, $stockcollection->contact)], $stockcollection->contact);
        
    $counter = $this->statement->base->countProperty('stockcollection', $stockcollection->stockcollection_id);
    $props = $counter->getObjects();
    $default_props = \Drupal::state()->get('chado_display_stockcollectionprop', []);
    foreach ($props AS $prop) {
      if (isset($default_props[$prop->type_id]) && $default_props[$prop->type_id]) {
        $table->addRow([ucwords(str_replace('_' , ' ', $prop->type)), Markup::create($prop->value)], $prop->value);
      }
    }

    $overview = [];
    $overview [] = $table->render();
    // Overview image
    $image = $this->getOverviewImageRenderArray('stockcollection', $this->stockcollection->stockcollection_id);
    if($image) {
      $overview [] = $image;
    }
    $this->addContent('overview', 'StockCollection Overview', $overview, -100);
  }

  function renderStocks () {
    $headers = ['Name', 'Uniquename', 'Type'];
    $rows = [
      ['link:stock,stock_id' => 'name'],
      ['link:stock,stock_id' => 'uniquename'],
      'type'
    ];
    $this->addPreset('Stocks', 'stockcollection', 'countStock', $headers, $rows);
  }
  
}