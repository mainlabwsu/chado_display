<?php
namespace Drupal\chado_display\Render\Based;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Render\Renderer;

class Library extends Renderer {

  public $library;

  function __construct($id) {
    parent::__construct($id);
  }

  function createPage($id) {
    $this->library = $this->statement->library->getLibrary($id);
    $this->setTitle($this->library->name);
    $info = \Drupal::state()->get('chado_display_library_render_info', chado_display_render_info($this));
    $this->addContentByInfo($info);
    $this->addSidebarPropertyItems('library', 'library_id');
  }

  function renderOverview () {
    $library = $this->library;
    $table = $this->tableBuilder(TRUE);
    $table->addRow(['Name', $library->name], $library->name);
    $table->addRow(['Unique Name', $library->uniquename], $library->uniquename);
    $table->addRow(['Species', $library->organism], $library->organism);
    $table->addRow(['Type', $library->type], $library->type);

    $counter = $this->statement->base->countProperty('library', $library->library_id);
    $props = $counter->getObjects();
    $default_props = \Drupal::state()->get('chado_display_libraryprop', []);
    foreach ($props AS $prop) {
        if (isset($default_props[$prop->type_id]) && $default_props[$prop->type_id]) {
          $table->addRow([ucwords(str_replace('_' , ' ', $prop->type)), Markup::create($prop->value)], $prop->value);
        }
    }
    $overview = [];
    $overview [] = $table->render();
    // Overview image
    $image = $this->getOverviewImageRenderArray('library', $this->library->library_id);
    if($image) {
      $overview [] = $image;
    }
    $this->addContent('overview', 'Library Overview', $overview, -100);
  }
  
  function renderPublications() {
      $headers = ['Title', 'Series', 'Year', 'Type'];
      $rows = [
          ['link:pub,pub_id' => 'title'],
          'series_name',
          'pyear',
          'type'
      ];
      $this->addPreset('Publications', 'library', 'countPub', $headers, $rows);
  }

  function renderReferences () {
      $headers = ['External Database', 'Accession'];
      $rows = [
          'db',
          ['dbxref'=> 'accession']
      ];
      $this->addPreset('References', 'library', 'countDbxref', $headers, $rows);
  }
  
  function renderSequences() {
    $headers = ['Name', 'Species', 'Type'];
    $rows = [
      ['link:feature,feature_id' => 'name|uniquename'],
      ['link:organism,organism_id' => 'organism'],
      'type'
    ];
    $this->addPreset('Sequences', 'library', 'countFeature', $headers, $rows);
  }
  
  function renderSynonyms () {
      $headers = ['Name', 'Type'];
      $rows = [
          ['link:synonym,synonym_id' => 'name'],
          'type'
      ];
      $this->addPreset('Synonyms', 'library', 'countSynonym', $headers, $rows);
  }
}