<?php
namespace Drupal\chado_display\Render\Based;

class GWAS extends Feature {
  function __construct($id) {
    parent::__construct($id);
    $this->changeHeader('overview', 'GWAS Overview');    
    $this->changeHeader('projects', 'Trait Study');
    $this->updateOverview();
    $this->addGenomeLocation();
  }
  
  function updateOverview() {
    
    # Trait
    $trait = $this->statement->feature->getTrait($this->feature->feature_id);
    if (isset($trait->cvterm_id)) {
      $extra_opts = [
        'json' => 'Json data page',
        'allele' => 'Allele page',
        'gene_class' => 'Gene Class page',
        'gene_class_listing' => 'Gene Class Listing page',
        'polymorphism' => 'Polymorphism page',
        'trait' => 'Trait page',
        'trait_descriptor' => 'Trait Descriptor page',
        'trait_listing' => 'Triat Listing page',
      ];
      $extra = \Drupal::state()->get('chado_display_extra_render_info', $extra_opts);
      $val = isset($extra['trait']) && $extra['trait'] ? $this->url('/display/trait', $trait->cvterm_id, $trait->name) : $trait->name;
      $this->replaceOverviewField('Name', $val);
    }
    
    # GWAS marker
    $gwas_marker = $this->statement->feature->getGwasMarker($this->feature->feature_id);
    if (isset($gwas_marker->feature_id)) {
      $this->addOverviewField('GWAS Marker', $this->link('feature', $gwas_marker->feature_id, $gwas_marker->gwas_marker));
    }
    
    # Alias
    $alias = $this->statement->feature->getAlias($this->feature->feature_id);
    if (isset($alias->name)) {
      $this->addOverviewField('Trait Alias', $alias->name);
    }
    
    # GWAS study
    $study = $this->statement->feature->getProject($this->feature->feature_id);
    if (isset($study->project_id)) {
      $this->addOverviewField('GWAS Study', $this->link('project', $study->project_id, $study->name));
      
      # Genome assembly
      $genome = $this->statement->feature->getGwasGenome($study->project_id);
      if (isset($genome->analysis_id)) {
        $this->addOverviewField('Genome Assembly', $this->link('analysis', $genome->analysis_id, $genome->name));
      }
    }
    
    # GWAS panel
    $panel = $this->statement->feature->getGwasPanel($this->feature->feature_id);
    if (isset($panel->stock_id)) {
      $this->addOverviewField('GWAS Panel', $this->link('stock', $panel->stock_id, $panel->uniquename));
    }
    

    $overview = $this->getContent('overview');
    $table = $overview[0][0];
    $table['#rows'][0]['data'][0]['data']  = 'Trait Name';
    $table['#rows'][1]['data'][0]['data']  = 'GWAS Label';
    $overview[0][0] = $table;
    $this->replaceContent('overview', NULL, $overview);
    $this->orderOverviewBy(['gwas label', 'published symbol', 'trait name', 'trait alias', 'gwas marker', 'gwas study', 'genome assembly', 'gwas panel']);
  }
  
  function addGenomeLocation() {
      $headers = ['Name', 'Type', 'Location', 'Analysis'];
      $rows = [
          ['link:feature,srcfeature_id' => 'landmark'],
          'type',
          ['jbrowse' => 'location'],
          ['link:analysis,analysis_id' => 'analysis']         
      ];
      $this->addPreset('Genome Location', 'feature', 'countGwasLocation', $headers, $rows);
  }

  function renderProjects () {
    
  }
}