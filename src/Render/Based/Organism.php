<?php
namespace Drupal\chado_display\Render\Based;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Render\Renderer;

class Organism extends Renderer {

  public $organism;

  function __construct($id) {
    parent::__construct($id);
  }

  function createPage($id) {
    $this->organism = $this->chado->getObjectById('organism', ['*'], $id);
    $this->setTitle($this->organism->genus . ' ' . $this->organism->species);
    $info = \Drupal::state()->get('chado_display_organism_render_info', chado_display_render_info($this));
    $this->addContentByInfo($info);
    $this->addSidebarPropertyItems('organism', 'organism_id');
  }

  function renderOverview () {
    $organism = $this->organism;
    $table = $this->tableBuilder(TRUE);
    $table->addRow(['Genus', $organism->genus], $organism->genus);
    $table->addRow(['Species', $organism->species], $organism->species);
    $table->addRow(['Common Name', $organism->common_name], $organism->common_name);
    $table->addRow(['Abbreviation', $organism->abbreviation], $organism->abbreviation);
    
    $counter = $this->statement->base->countProperty('organism', $organism->organism_id);
    $props = $counter->getObjects();
    $default_props = \Drupal::state()->get('chado_display_organismprop', []);
    foreach ($props AS $prop) {
        if (isset($default_props[$prop->type_id]) && $default_props[$prop->type_id]) {
          $table->addRow([ucwords(str_replace('_' , ' ', $prop->type)), Markup::create($prop->value)], $prop->value);
        }
    }
    
    // If the comment is an HTML table, append the content to the main table
    $comment = preg_replace('/\s{2,}/', ' ', $organism->comment);
    $has_table = preg_match_all ('/<tr>(.*?)<\/tr>/i', $comment, $matches);
    if ($has_table) {
      foreach ($matches[1] AS $crow) {
        $has_row = preg_match_all('/<td>(.*?)<\/td>/i', $crow, $rmatches);
        if ($has_row) {
          $rheader = $rmatches[1][0];
          $rdata = $rmatches[1][1];
          $table->addRow([$rheader, Markup::create($rdata)], $rdata);
        }
      }
    }
    else {
      $table->addRow(['Comment', Markup::create($organism->comment)], $organism->comment);
    }

    $overview = [];
    $overview [] = $table->render();
    // Overview image
    $image = $this->getOverviewImageRenderArray('organism', $this->organism->organism_id);
    if($image) {
      $overview [] = $image;
    }

    $this->addContent('overview', 'Organism Overview', $overview, -100);
  }
  
  function renderImages() {
    if ($this->chado->tableExists('organism_image')) {
      $headers = ['Name', 'Type'];
      $rows = [
        ['link:eimage,eimage_id' => 'eimage_data'],
        ['empty:-' => 'eimage_type']
      ];
      $this->addPreset('Images', 'organism', 'countImage', $headers, $rows);
    }
  }
  
  function renderMaps() {
    if ($this->chado->tableExists('featuremap_organism') && $this->chado->tableExists('featuremapprop')) {
      $headers = ['Name', 'Unit Type'];
      $rows = [
        ['link:featuremap,featuremap_id' => 'map'],
        'unit_type'
      ];
      $this->addPreset('Maps', 'organism', 'countMap', $headers, $rows);
    }
  }
  
  function renderReferences () {
      $headers = ['External Database', 'Accession'];
      $rows = [
          'db',
          ['dbxref'=> 'accession']
      ];
      $this->addPreset('References', 'organism', 'countDbxref', $headers, $rows);
  }
  
  function renderSequences() {
    $headers = ['Name', 'Type'];
    $rows = [
      ['link:feature,feature_id' => 'uniquename|name'],
      'type'
    ];
    $this->addPreset('Sequences', 'organism', 'countFeature', $headers, $rows);
  }
  
  function renderStocks() {
    $headers = ['Name', 'Uniquename', 'Type'];
    $rows = [
      ['link:stock,stock_id' => 'name'],
      ['link:stock,stock_id' => 'uniquename'],
      'type'
    ];
    $this->addPreset('Stocks', 'organism', 'countStock', $headers, $rows);
  }
}