<?php
namespace Drupal\chado_display\Render\Based;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Render\Renderer;

class Pub extends Renderer {

  public $library;
  public $pub;

  function __construct($id) {
    parent::__construct($id);
  }

  function createPage($id) {
    $this->pub = $this->statement->pub->getPub($id);
    $this->setTitle($this->pub->title);
    $info = \Drupal::state()->get('chado_display_pub_render_info', chado_display_render_info($this));
    $this->addContentByInfo($info);
    $this->addSidebarPropertyItems('pub', 'pub_id');
  }
  
  function renderOverview () {
    $pub = $this->pub;
    $counter = $this->statement->base->countProperty('pub', $pub->pub_id);
    $props = $counter->getObjects();
    $authors = NULL;
    $url = NULL;
    $citation = $pub->uniquename;
    $abstract = NULL;
    $default_props = \Drupal::state()->get('chado_display_pubprop', []);
    foreach ($props AS $key => $prop) {
      if (isset($default_props[$prop->type_id]) && $default_props[$prop->type_id]) {
        if ($prop->type == 'Authors') {
            $authors = $prop->value;
            unset($props[$key]);
        }
        else if ($prop->type == 'Abstract') {
            $abstract = $prop->value;
            unset($props[$key]);
        }
        else if ($prop->type == 'URL') {
            $url = $prop->value;
            $prop->value = '<a href="' . $url . '" target=_blank>' . $url . '</a>';
            $props[$key] = $prop;
        }
        else if ($prop->type == 'Citation') {
            $citation = $citation ? $citation : $prop->value;
            unset($props[$key]);
        }
      }
    }
    
    $table = $this->tableBuilder(TRUE);
    $table->addRow(['Title', $pub->title], $pub->title);
    $table->addRow(['Authors', Markup::create($authors)], $authors);
    $table->addRow(['Series', $pub->series_name], $pub->series_name);
    $table->addRow(['Year', $pub->pyear], $pub->pyear);
    $table->addRow(['Volume Title', $pub->volumetitle], $pub->volumetitle);
    $table->addRow(['Volume', $pub->volume], $pub->volume);
    $table->addRow(['Issue', $pub->issue], $pub->issue);
    $table->addRow(['Pages', $pub->pages], $pub->pages);
    $table->addRow(['Mini Reference', $pub->miniref], $pub->miniref);
    $table->addRow(['Publisher', $pub->publisher], $pub->publisher);
    $table->addRow(['Location' , $pub->pubplace], $pub->pubplace);
    $table->addRow(['Type', $pub->type], $pub->type);

    foreach ($props AS $prop) {
        if (isset($default_props[$prop->type_id]) && $default_props[$prop->type_id]) {
          $table->addRow([ucwords(str_replace('_' , ' ', $prop->type)), Markup::create($prop->value)], $prop->value);
        }
    }
    $table->addRow(['Citation', Markup::create($citation)], $citation);
    $table->addRow(['Abstract', Markup::create($abstract)], $abstract);
    
    $overview = [];
    $overview [] = $table->render();
    // Overview image
    $image = $this->getOverviewImageRenderArray('pub', $this->pub->pub_id);
    if($image) {
      $overview [] = $image;
    }
    $this->addContent('overview', 'Publication Overview', $overview, -100);
  }
  
  function renderImages () {
    if ($this->chado->tableExists('pub_image')) {
      $headers = ['Name', 'Type'];
      $rows = [
        ['link:eimage,eimage_id' => 'eimage_data'],
        'eimage_type'
      ];
      $this->addPreset('Images', 'pub', 'countImage', $headers, $rows);
    }
  }
  
  function renderLibraries() {
      $headers = ['Name', 'Type', 'Species'];
      $rows = [
          ['link:library,library_id' => 'name|uniquename'],
          'type',
          ['link:organism,organism_id' => 'organism'],
      ];
      $this->addPreset('Libraries', 'pub', 'countLibrary', $headers, $rows);
  }
  
  function renderMaps() {
      $headers = ['Name'];
      $rows = [
          ['link:featuremap,featuremap_id' => 'name'],
      ];
      $this->addPreset('Maps', 'pub', 'countMap', $headers, $rows);
  }
  
  function renderProjects() {
      if ($this->chado->tableExists('project_pub')) {
          $headers = ['Name'];
          $rows = [
              ['link:project,project_id' => 'name'],
          ];
          $this->addPreset('Projects', 'pub', 'countProject', $headers, $rows);
      }
  }
  
  function renderReferences () {
    $headers = ['External Database', 'Accession'];
    $rows = [
      'db',
      ['dbxref'=> 'accession']
    ];
    $this->addPreset('References', 'pub', 'countDbxref', $headers, $rows);
  }
  
  function renderSequences() {
    $headers = ['Name', 'Species', 'Type'];
    $rows = [
      ['link:feature,feature_id' => 'name|uniquename'],
      ['link:organism,organism_id' => 'organism'],
      'type'
    ];
    $this->addPreset('Sequences', 'pub', 'countFeature', $headers, $rows);
  }
  
  function renderStocks () {
    $headers = ['Name', 'Uniquename', 'Type'];
    $rows = [
      ['link:stock,stock_id' => 'name'],
      ['link:stock,stock_id' => 'uniquename'],
      'type'
    ];
    $this->addPreset('Stocks', 'pub', 'countStock', $headers, $rows);
  }

}