<?php
namespace Drupal\chado_display\Render\Based;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Render\Renderer;

class Map extends Renderer {

  public $featuremap;

  function __construct($id) {
    parent::__construct($id);
  }

  function createPage($id) {
    $this->featuremap = $this->chado->getObjectById('featuremap', ['*'], $id);
    $this->setTitle($this->featuremap->name);
    $info = \Drupal::state()->get('chado_display_map_render_info', chado_display_render_info($this));
    $this->addContentByInfo($info);
    $this->addSidebarPropertyItems('featuremap', 'featuremap_id');
  }

  function renderOverview () {
    $map = $this->featuremap;
    $table = $this->tableBuilder(TRUE);
    $table->addRow(['Name', $map->name], $map->name);
    $table->addRow(['Description', $map->description], $map->description);
    
    $orgs = $this->statement->featuremap->getOrganism($map->featuremap_id);
    $display_orgs = NULL;
    foreach ($orgs AS $org) {
        $display_orgs .= $this->link('organism', $org->organism_id, $org->genus . ' ' . $org->species . '</br>');
    }
    $table->addRow(['Species', Markup::create($display_orgs)], $display_orgs);
    $table->addRow(['Map unit', $this->chado->getFieldById('cvterm', 'name', $map->unittype_id)], $map->unittype_id);

    if ($this->chado->tableExists('featuremap_stock')) {
        // Population size
        $pop_sizes = $this->statement->featuremap->getMapPopulationSize($map->featuremap_id);
        if(count($pop_sizes) > 0) {
            $pop_size = $pop_sizes[0];
            $table->addRow(['Population size', $pop_size->value], $pop_size->value);
        }
    }

    $maternal = $this->statement->featuremap->getMapMaternalParent($map->featuremap_id);
    $added = $maternal ? $table->addRow(['Maternal parent', $this->link('stock', $maternal->stock_id, $maternal->uniquename)]) : NULL;

    $paternal = $this->statement->featuremap->getMapPaternalParent($map->featuremap_id);
    $added = $paternal ? $table->addRow(['Paternal parent', $this->link('stock', $paternal->stock_id, $paternal->uniquename)]) : NULL;

    $num_lg = $this->statement->featuremap->getMapLGNum($map->featuremap_id);
    $table->addRow(['Number of linkage groups', $num_lg], $num_lg);    

    // Map Properties
    $counter = $this->statement->base->countProperty('featuremap', $map->featuremap_id);
    $props = $counter->getObjects();
    $default_props = \Drupal::state()->get('chado_display_featuremapprop', []);
    foreach ($props AS $prop) {
        if (isset($default_props[$prop->type_id]) && $default_props[$prop->type_id]) {
          $table->addRow([ucwords(str_replace('_' , ' ', $prop->type)), Markup::create($prop->value)], $prop->value);
        }
    }
    $overview = [];
    $overview [] = $table->render();
    // Overview image
    $image = $this->getOverviewImageRenderArray('featuremap', $this->featuremap->featuremap_id);
    if($image) {
      $overview [] = $image;
    }
    $this->addContent('overview', 'Map Overview', $overview, -100);
  }

  function renderContacts () {
      $headers = ['Name', 'Type'];
      $rows = [
          ['link:contact,contact_id' => 'name'],
          'type'
      ];
      $this->addPreset('Contacts', 'featuremap', 'countContact', $headers, $rows);
  }

  function renderMapLoci() {
      $headers = ['Linkage Group', 'Marker Name', 'Locus Name', 'Type', 'Position'];
      $rows = [
          'lg',
          ['link:feature,feature_id' => 'genetic_marker'],
          'marker',
          'type',
          ['round:2' => 'position']
      ];
      $this->addPreset('Map Loci', 'featuremap', 'countMapLoci', $headers, $rows, 0, TRUE, NULL, TRUE);
  }
  
  function renderMapQTL() {
    $headers = ['Linkage Group', 'Name', 'Type', 'Start', 'Stop', 'Peak'];
    $rows = [
      'lg',
      ['link:feature,qtl_feature_id' => 'qtl'],
      'type',
      ['round:2' => 'start'],
      ['round:2' => 'stop'],
      ['round:2' => 'peak']
    ];
    $this->addPreset('Map QTL', 'featuremap', 'countMapQTL', $headers, $rows, 0, TRUE, NULL, TRUE);
  }

  function renderPublications() {
      $headers = ['Title', 'Series', 'Year', 'Type'];
      $rows = [
          ['link:pub,pub_id' => 'title'],
          'series_name',
          'pyear',
          'type'
      ];
      $this->addPreset('Publications', 'featuremap', 'countPub', $headers, $rows);
  }
  
  function renderStocks() {
    $headers = ['Name', 'Uniquename', 'Type'];
    $rows = [
      ['link:stock,stock_id' => 'name'],
      ['link:stock,stock_id' => 'uniquename'],
      'type'
    ];
    $this->addPreset('Stocks', 'featuremap', 'countStock', $headers, $rows);
  }
}