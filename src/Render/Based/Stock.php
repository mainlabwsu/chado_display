<?php
namespace Drupal\chado_display\Render\Based;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Render\Renderer;

class Stock extends Renderer {

  public $stock;

  function __construct($id) {
    parent::__construct($id);
  }

  function createPage($id) {
    $stock = $this->statement->stock->getStock($id);
    $this->stock = $stock;
    $this->setTitle($stock->name);
    $info = \Drupal::state()->get('chado_display_stock_render_info', chado_display_render_info($this));
    $this->addContentByInfo($info);
    $this->addSidebarPropertyItems('stock', 'stock_id');
  }

  function renderOverview () {
    $stock = $this->stock;
    $table = $this->tableBuilder(TRUE);
    $table->addRow(['Name', $stock->name], $stock->name);
    $table->addRow(['Unique Name', $stock->uniquename], $stock->uniquename);
    $table->addRow(['Type', $stock->type], $stock->type);
    $table->addRow(['Species', $this->link('organism', $stock->organism_id, $stock->organism)], $stock->organism);
    $table->addRow(['Description', $stock->description], $stock->description);
    
    $counter = $this->statement->base->countProperty('stock', $stock->stock_id);
    $props = $counter->getObjects();
    $default_props = \Drupal::state()->get('chado_display_stockprop', []);
    foreach ($props AS $prop) {
      if (isset($default_props[$prop->type_id]) && $default_props[$prop->type_id]) {
        $table->addRow([ucwords(str_replace('_' , ' ', $prop->type)), Markup::create($prop->value)], $prop->value);
      }
    }
    
    //parentages
    $maternal = $this->statement->stock->getParent($stock->stock_id, 'maternal');
    $added =  $maternal ? $table->addRow(['Maternal Parent', $this->link('stock', $maternal->stock_id, $maternal->uniquename)]) : NULL;

    $paternal = $this->statement->stock->getParent($stock->stock_id, 'paternal');
    $added = $paternal ? $table->addRow(['Paternal Parent', $this->link('stock', $paternal->stock_id, $paternal->uniquename)]) : NULL;

    $overview = [];
    $overview [] = $table->render();
    // Overview image
    $image = $this->getOverviewImageRenderArray('stock', $this->stock->stock_id);
    if($image) {
      $overview [] = $image;
    }
    $this->addContent('overview', 'Stock Overview', $overview, -100);
  }
  
  function renderImages () {
      if ($this->chado->tableExists('stock_image')) {
          $headers = ['Name', 'Type'];
          $rows = [
              ['link:eimage,eimage_id' => 'eimage_data'],
              'eimage_type'
          ];
          $this->addPreset('Images', 'stock', 'countImage', $headers, $rows);
      }
  }
  
  function renderInCollection () {
    $headers = ['Name', 'Type'];
    $rows = [
      ['link:stockcollection,stockcollection_id' => 'name|uniquename'],
      'type'
    ];
    $this->addPreset('In Collection', 'stock', 'countCollection', $headers, $rows);
  }
  
  function renderLibraries() {
      if ($this->chado->tableExists('library_stock')) {
          $headers = ['Name', 'Type', 'Species'];
          $rows = [
              ['link:library,library_id' => 'name|uniquename'],
              'type',
              ['link:organism,organism_id' => 'organism'],
          ];
          $this->addPreset('Libraries', 'stock', 'countLibrary', $headers, $rows);
      }
  }

  function renderMaternalParentsOf() {
      $stock = $this->stock;
      $counter = $this->statement->stock->countSubjectStocks($stock->stock_id, 'is_a_maternal_parent_of', 'MAIN');
      $headers = ['Name', 'Species', 'Type'];
      $rows = [
          ['link:stock,stock_id' => 'name|uniquename'],
          ['link:organism,organism_id' =>'organism'],
          'type'
      ];
      $this->addPreset('Maternal Parent Of', 'stock', NULL, $headers, $rows, 0, TRUE, $counter);
  }
  
  function renderPaternalParentsOf() {
      $stock = $this->stock;
      $counter = $this->statement->stock->countSubjectStocks($stock->stock_id, 'is_a_paternal_parent_of', 'MAIN');
      $headers = ['Name', 'Species', 'Type'];
      $rows = [
          ['link:stock,stock_id' => 'name|uniquename'],
          ['link:organism,organism_id' =>'organism'],
          'type'
      ];
      $this->addPreset('Paternal Parent Of', 'stock', NULL, $headers, $rows, 0, TRUE, $counter);
  }
  
  function renderPhenotypicData() {
      if ($this->chado->tableExists('nd_experiment_phenotype')) {
          $headers = ['Dataset', 'Descriptor', 'Value'];
          $rows = [
              ['link:project,project_id' => 'project'],
              'descriptor',
              'value'
          ];
          $this->addPreset('Phenotypic Data', 'stock', 'countPhenotypicData', $headers, $rows, 0, TRUE, NULL, TRUE);
      }
  }
  
  function renderPopulationMap() {
    if ($this->chado->tableExists('featuremap_stock')) {
      $headers = ['Name', 'Type', 'Population Type', 'Genome'];
      $rows = [
        ['link:featuremap,featuremap_id' => 'map'],
        'map_type',
        'pop_type',
        ['empty:-' => 'genome_group']
      ];
      $this->addPreset('Genetic Map Data', 'stock', 'countPopulationMap', $headers, $rows);
    }
  }
  
  function renderPublications () {
    $headers = ['Title', 'Series', 'Year', 'Type'];
    $rows = [
      ['link:pub,pub_id' => 'title'],
      'series_name',
      'pyear',
      'type'
    ];
    $this->addPreset('Publications', 'stock', 'countPub', $headers, $rows);
  }
  
  function renderReferences () {
    $headers = ['External Database', 'Accession'];
    $rows = [
      'db',
      ['dbxref'=> 'accession']
    ];
    $this->addPreset('References', 'stock', 'countDbxref', $headers, $rows);
  }
  
  function renderRelatedStocks () {
    $display = [];
    $stock = $this->stock;
    
    // Subject relationship
    $counter = $this->statement->stock->countSubjectRelationship($stock->stock_id);
    $num_per_page = (int) ($this->num_per_page / 2);
    $pid = $this->getNextPagerId();
    $pager = $counter->setPager($num_per_page, $pid);
    $page = $pager->getCurrentPage();
    
    $rels = $counter->getPagedObjects($page);
    $headers = ['This Stock', 'Relationship', 'Related Stock (RS)', 'RS Type', 'RS Organism'];
    $rows = [];
    foreach ($rels AS $rel) {
      $rows[] = [$stock->name, $rel->relationship, $this->link('stock', $rel->stock_id, $rel->object), $rel->type, $this->link('organism', $rel->organism_id, $rel->organism)];
    }
    $table = $this->table ($headers, $rows, $pid);
    if ($counter->getTotal() != 0) {
      $display [] = ['#markup' => '<em>Subjective relation: </em>'];
      $display [] = $table;
    }
    
    // Object relationship
    $counter = $this->statement->stock->countObjectRelationship($stock->stock_id);
    $pid = $this->getNextPagerId();
    $pager = $counter->setPager($num_per_page, $pid);
    $page = $pager->getCurrentPage();
    
    $rels = $counter->getPagedObjects($page);
    $headers = ['Related Stock (RS)', 'RS Type', 'RS Organism', 'Relationship', 'This Stock'];
    $rows = [];
    foreach ($rels AS $rel) {
      $rows[] = [$this->link('stock', $rel->stock_id, $rel->subject), $rel->type, $this->link('organism', $rel->organism_id, $rel->organism), $rel->relationship, $stock->name];
    }
    $table = $this->table ($headers, $rows, $pid);
    if ($counter->getTotal() != 0) {
      $display [] = ['#markup' => '<em>Objective relation: </em>'];
      $display [] = $table;
    }
    
    if (count($display) > 0) {
      $this->addContent('relationships', 'Related Stocks', $display);
    }
  }
  
  function renderSequences() {
    if ($this->chado->tableExists('feature_stock')) {
      $headers = ['Name', 'Species', 'Type'];
      $rows = [
        ['link:feature,feature_id' => 'name|uniquename'],
        ['link:organism,organism_id' => 'organism'],
        'type'
      ];
      $this->addPreset('Sequences', 'stock', 'countFeature', $headers, $rows);
    }
  }
  
  function renderSSRGenotyeData() {
      if ($this->chado->tableExists('nd_experiment_genotype')) {
          $headers = ['Dataset', 'Marker', 'Marker Type', 'Genotype', 'Marker Allele'];
          $rows = [
              ['link:project,project_id' => 'project'],
              ['link:feature,feature_id' => 'marker'],
              'marker_type',
              'genotype',
              ['function:getSSRMarkerAllele,feature_id,organism_id,marker,genotype' => 'genotype']
          ];
          $this->addPreset('SSR Genotype Data', 'stock', 'countSSRGenotypeData', $headers, $rows, 0, TRUE, NULL, TRUE);
      }
  }
  
  function renderSNPGenotyeData() {
    if ($this->chado->tableExists('genotype_call')) {
      $headers = ['Dataset', 'Marker', 'Allele', 'Genotype'];
      $rows = [
        ['link:project,project_id' => 'project'],
        ['link:feature,feature_id' => 'marker'],
        'allele',
        'genotype'
      ];
      $this->addPreset('SNP Genotype Data', 'stock', 'countSNPGenotypeData', $headers, $rows, 0, TRUE, NULL, TRUE);
    }
  }
  
  function getSSRMarkerAllele($args) {
    $organism_id = $args['organism_id'];
    $feature_id = $args['feature_id'];
    $marker = $args['marker'];
    $genotype = $args['genotype'];
    $alleles = explode('|', $genotype);
    $display = '';
    for ($index = 0; $index < count($alleles); $index ++) {
        $display .= '<a href=/display/allele/' . $organism_id . '/' . $feature_id . '/' . $alleles[$index] . '>' . $marker . '_' . $alleles[$index] . '</a>';
        if ($index < count($alleles) - 1) {
            $display .= '; ';
        }
    }
    return Markup::create($display);
  }
  
  function renderQTL() {
    $headers = ['Name', 'Dataset'];
    $rows = [
      ['link:feature,feature_id' => 'uniquename|name'],
      ['link:project,project_id' => 'project'],
    ];
    $this->addPreset('QTL', 'stock', 'countQTL', $headers, $rows);
  }
}