<?php
namespace Drupal\chado_display\Render\Based;

class HaplotypeBlock extends Feature {
  function __construct($id) {
    parent::__construct($id);
    $this->changeHeader('overview', 'Haplotype Block Overview');
    $this->addHaplotypes();
    $this->addMapPositions();
    $this->addProjects();
    $this->addStockHaplotypes();
  }
  
  function addHaplotypes() {
    $counter = $this->statement->feature->countHaplotype($this->feature->feature_id);
    if ($counter->getTotal() > 0) {
      $objects = $counter->getObjects();
      $first_obj = $objects[0];
      $headers = ['Marker'];
      $rows = [['link:feature,feature_id' => 'marker']];
      $index = 0;
      foreach (explode(' ', $first_obj->haplotypes) AS $htype) {
        $headers[] = $htype;
        $rows[] = ['split: ,' . $index => 'values'];
        $index ++;
      }
      $this->addPreset('Haplotypes', 'feature', NULL, $headers, $rows, 0, TRUE, $counter);
    }
  }
  
  function addMapPositions() {
      $headers = ['Map', 'Linkage Group', 'Start', 'Stop'];
      $rows = [
          ['link:featuremap,featuremap_id' => 'map'],
          'linkage_group',
          ['round:2' => 'start'],
          ['round:2' => 'stop'],
      ];
      $this->addPreset('Map Positions', 'feature', 'countHaplotypeBlockMapPosition', $headers, $rows);
  }
  
  function addProjects() {
      $headers = ['Name'];
      $rows = [
          ['link:project,project_id' => 'name'],
      ];
      $this->addPreset('Projects', 'feature', 'countHaplotypeBlockProject', $headers, $rows);
  }

  
  function addStockHaplotypes() {
      $headers = ['Name', 'Haplotype'];
      $rows = [
          ['link:stock,stock_id' => 'stock'],
          'haplotype'
      ];
      $this->addPreset('Stock Haplotypes', 'feature', 'countStockHaplotype', $headers, $rows);
  }
}