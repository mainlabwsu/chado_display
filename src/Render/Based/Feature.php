<?php
namespace Drupal\chado_display\Render\Based;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Render\Renderer;

class Feature extends Renderer {

  public $feature;

  function __construct($id) {
    parent::__construct($id);
  }

  function createPage($id) {
    $feature = $this->statement->feature->getFeature($id);
    $this->feature = $feature;
    $this->setTitle(Markup::create($feature->name . ' [<i>' . $this->link('organism', $feature->organism_id, $feature->organism) . '</i>,' . ' <em>' . $feature->type . '</em>]'));
    $info = \Drupal::state()->get('chado_display_feature_render_info', chado_display_render_info($this));
    $this->addContentByInfo($info);
    $this->addSidebarPropertyItems('feature', 'feature_id');
  }

  /**
   * Get residues only when it is needed
   */
  function getResidues () {
    $residues = NULL;
    if (isset($this->feature->residues)) {
      $residues = $this->feature->residues;
    }
    else {
      $residues = $this->chado->getFieldById('feature', 'residues', $this->feature->feature_id);
      $this->feature->residues = $residues;
    }
    return $residues;
  }
  
  function renderOverview () {
    $feature = $this->feature;
    $table = $this->tableBuilder(TRUE);
    $table->addRow(['Name', $feature->name], $feature->name);
    $table->addRow(['Unique Name', $feature->uniquename], $feature->uniquename);
    $table->addRow(['Type', $feature->type], $feature->type);
    $table->addRow(['Species', $this->link('organism', $feature->organism_id, $feature->organism)], trim($feature->organism));
    $table->addRow(['Sequence Length', $feature->seqlen], $feature->seqlen);
    
    $counter = $this->statement->base->countProperty('feature', $feature->feature_id);
    $props = $counter->getObjects();
    $default_props = \Drupal::state()->get('chado_display_featureprop', []);
    foreach ($props AS $prop) {
      if (isset($default_props[$prop->type_id]) && $default_props[$prop->type_id]) {
        $table->addRow([ucwords(str_replace('_' , ' ', $prop->type)), Markup::create($prop->value)], $prop->value);
      }
    }
    $overview = [];
    $overview [] = $table->render();
    // Overview image
    $image = $this->getOverviewImageRenderArray('feature', $this->feature->feature_id);
    if($image) {
      $overview [] = $image;
    }
    $this->addContent('overview', 'Sequence Overview', $overview, -100);
  }

  function renderAnalyses () {
    $headers = ['Analysis Name', 'Date Performed'];
    $rows = [
        ['link:analysis,analysis_id' => 'name|program'],
        ['date' => 'timeexecuted']
    ];
    $this->addPreset('Analyses', 'feature', 'countAnalysis', $headers, $rows);
  }

  function renderAnnotatedTerms () {
    $headers = ['Vocabulary', 'Accession', 'Term', 'Definition'];
    $rows = [
        'cv', 
        ['dbxref' => 'accession'], 
        'cvterm', 
        'definition'
    ];
    $this->addPreset('Annotated Terms', 'feature', 'countCvterm', $headers, $rows);
  }

  function renderContacts () {
      $headers = ['Name', 'Type'];     
      $rows = [
          ['link:contact,contact_id' => 'name'],
          'type'
      ];
      $this->addPreset('Contacts', 'feature', 'countContact', $headers, $rows);
  }
  
  function renderGenotypes() {
      $headers = ['Genotype', 'Type'];
      $rows = [
          ['link:genotype,genotype_id' => 'name|uniquename'],
          'type'
      ];
      $this->addPreset('Genotypes', 'feature', 'countGenotype', $headers, $rows);
  }
  
  function renderImages () {
    if ($this->chado->tableExists('feature_image')) {
      $headers = ['Name', 'Type'];
      $rows = [
        ['link:eimage,eimage_id' => 'eimage_data'],
        'eimage_type'
      ];
      $this->addPreset('Images', 'feature', 'countImage', $headers, $rows);
    }
  }
  
  function renderLibraries () {
      $headers = ['Name', 'Type'];
      $rows = [
          ['link:library,library_id' => 'name'],
          'type'
      ];
      $this->addPreset('Libraries', 'feature', 'countLibrary', $headers, $rows);
  }  
  
  function renderLocation () {
    $counter = $this->statement->feature->countFeatureloc($this->feature->feature_id);
    $pid = $this->getNextPagerId();
    $pager = $counter->setPager($this->num_per_page, $pid);
    $page = $pager->getCurrentPage();
    
    $alignments = $counter->getPagedObjects($page);
    $headers = ['Name', 'Type', 'Location', 'Analysis'];
    $rows = [];
    $jbrowse = '';
    foreach ($alignments AS $alignment) {
      $jbrowse_url = $this->statement->analysis->getJBrowseURL($alignment->analysis_id);
      $location = $jbrowse_url ? Markup::create("<a href=$jbrowse_url" . $alignment->landmark . ':' . $alignment->location . '>' . $alignment->location . '</a>') : $alignment->location;
      $rows [] = [
        $this->link('feature', $alignment->feature_id, $alignment->landmark),
        $alignment->type,
        $location,
        $this->link('analysis', $alignment->analysis_id, $alignment->analysis ? $alignment->analysis : $alignment->program)
      ];
      if ($alignment->type == 'chromosome' || $alignment->type == 'supercontig') {
        $jb_enabled = \Drupal::state()->get('chado_display_jbrowse_width', 800) != 0 ? TRUE : FALSE;
        if ($jb_enabled) {
          $jb_enabled = \Drupal::state()->get('chado_display_jbrowse_width', 800) == 0 ? TRUE : FALSE;
          if ($jbrowse_url) {
            $jbrowse .= $this->addJbrowse($jbrowse_url, $alignment->landmark, $alignment->location, $alignment->analysis);
          }
        }
      }
    }
    
    $table = $this->table ($headers, $rows, $pid);
    if ($counter->getTotal() != 0) {
      $this->addContent('location', 'Location', $table, 0, NULL, 'Genomic or Aligned Location');
      if ($jbrowse != '') {
        $this->addContent('jbrowse', 'Genome Viewer', ['#markup' => Markup::create($jbrowse)]);
      }
    }
    
  }
  
  function addJbrowse ($jbrowse_url, $landmark, $location, $analysis) {
    $default_jbrowse_width = \Drupal::state()->get('chado_display_jbrowse_width', 800);
    $default_jbrowse_height = \Drupal::state()->get('chado_display_jbrowse_height', 300);
    $jbrowse = '
      <div style="width:'. $default_jbrowse_width . 'px;clear:both"><div>' . $analysis . '</div>
            <iframe
                src="' . $jbrowse_url .
                $landmark . '%3A' . $location .
                '&menu=false&nav=true&overview=true&tracklist=false"
                style="border: 1px solid black"
                width="' . $default_jbrowse_width . '"
                height="' . $default_jbrowse_height . '"
            >
            </iframe>
        </div>
    ';
                return $jbrowse;
  }
  
  function renderPhenotypes() {
      $headers = ['Phenotype', 'Value'];
      $rows = [
         ['link:phenotype,phenotype_id' => 'name|uniquename'],
          'value'
      ];
      $this->addPreset('Phenotypes', 'feature', 'countPhenotype', $headers, $rows);
  }
  
  function renderProjects () {
    if ($this->chado->tableExists('feature_project')) {
      $headers = ['Name'];
      $rows = [
        ['link:project,project_id' => 'name'],
      ];
      $this->addPreset('Projects', 'feature', 'countProject', $headers, $rows);
    }
  }
  
  function renderPublications () {
      $headers = ['Title', 'Series', 'Year', 'Type'];
      $rows = [
          ['link:pub,pub_id' => 'title'],
          'series_name',
          'pyear',
          'type'
      ];
      $this->addPreset('Publications', 'feature', 'countPub', $headers, $rows);
  }
  
  function renderReferences () {
    $headers = ['External Database', 'Accession'];
    $rows = [
        'db', 
        ['dbxref'=> 'accession']
    ];
    $this->addPreset('References', 'feature', 'countDbxref', $headers, $rows);
  }

  function renderRelatedSequences () {
    $display = [];
    $feature = $this->feature;

    // Subject relationship
    $counter = $this->statement->feature->countSubjectRelationship($feature->feature_id);
    $num_per_page = (int) ($this->num_per_page / 2);
    $pid = $this->getNextPagerId();
    $pager = $counter->setPager($num_per_page, $pid);
    $page = $pager->getCurrentPage();

    $rels = $counter->getPagedObjects($page);
    $headers = ['This Sequence', 'Relationship', 'Related Sequence (RS)', 'RS Type', 'RS Organism'];
    $rows = [];
    foreach ($rels AS $rel) {
      $rows [] = [$feature->name, $rel->relationship, $this->link('feature', $rel->feature_id, $rel->object), $rel->type, $this->link('organism', $rel->organism_id, $rel->organism)];
    }
    $table = $this->table ($headers, $rows, $pid);
    if ($counter->getTotal() != 0) {
      $display [] = ['#markup' => '<em>Subjective relation: </em>'];
      $display [] = $table;
    }

    // Object relationship
    $counter = $this->statement->feature->countObjectRelationship($feature->feature_id);
    $pid = $this->getNextPagerId();
    $pager = $counter->setPager($num_per_page, $pid);
    $page = $pager->getCurrentPage();

    $rels = $counter->getPagedObjects($page);
    $headers = ['Related Sequence (RS)', 'RS Type', 'RS Organism', 'Relationship', 'This Sequence'];
    $rows = [];
    foreach ($rels AS $rel) {
      $rows [] = [$this->link('feature', $rel->feature_id, $rel->subject), $rel->type, $this->link('organism', $rel->organism_id, $rel->organism), $rel->relationship, $feature->name];
    }
    $table = $this->table ($headers, $rows, $pid);
    if ($counter->getTotal() != 0) {
      $display [] = ['#markup' => '<em>Objective relation: </em>'];
      $display [] = $table;
    }

    if (count($display) > 0) {
      $this->addContent('relationships', 'Related Sequences', $display);
    }
  }

  function renderStocks () {
    if ($this->chado->tableExists('feature_stock')) {
      $headers = ['Name', 'Uniquename', 'Type'];
      $rows = [
        ['link:stock,stock_id' => 'name'],
        ['link:stock,stock_id' => 'uniquename'],
        'type'
      ];
      $this->addPreset('Stocks', 'feature', 'countStock', $headers, $rows);
    }
  }

  function renderSynonyms () {
    $headers = ['Name', 'Type'];
    $rows = [
      ['link:synonym,synonym_id' => 'name'],
      'type'
    ];
    $this->addPreset('Synonyms', 'feature', 'countSynonym', $headers, $rows);
  }
  
  function renderHomology() {
    $counter = $this->statement->feature->countAnalysisFeatureProp($this->feature->feature_id, 'analysis_blast_output_iteration_hits');
    if($counter->getTotal() > 0 && class_exists('\XMLReader')) {
      $residues = $this->getResidues();
      $objs = $counter->getObjects();
      $blast = new \Drupal\chado_display\Render\Element\Blast();
      $render_array = $blast->render ($objs, $residues);
      if ($render_array) {
        $this->addContent('homology', 'Homology', $render_array);
        $this->addLib('chado_display/jquery310');
        $this->addLib('chado_display/d33517');
        $this->addLib('chado_display/bootstrap337');
        $this->addLib('chado_display/feature_viewer0144');
        $this->addLib('chado_display/blast');
      }
    }
  }
  
  function renderInterPro() {
    $counter = $this->statement->feature->countAnalysisFeatureProp($this->feature->feature_id, 'analysis_interpro_xmloutput_hit');
    if($counter->getTotal() > 0) {
      $residues = $this->getResidues();
      $objs = $counter->getObjects();
      $ipr = new \Drupal\chado_display\Render\Element\InterPro();
      $render_array = $ipr->render ($objs, $residues);
      if ($render_array) {
        $this->addContent('interprop', 'InterPro', $render_array);
        $this->addLib('chado_display/jquery310');
        $this->addLib('chado_display/d33517');
        $this->addLib('chado_display/bootstrap337');
        $this->addLib('chado_display/feature_viewer0144');
        $this->addLib('chado_display/interpro');
      }
    }
  }

  function renderSequence () {
    $feature = $this->feature;
    $default_residues = \Drupal::state()->get('chado_display_residues', 100000);
    $residues = $this->getResidues();
    $seqlen = $residues? strlen($residues) : 0;
    $fasta = 
      '<div class="chado_page-section_download chado_display-page_control">' .
        \Drupal\Core\Link::createFromRoute('FASTA', 'chado_display.fasta', ['info' => base64_encode(serialize($this->feature->feature_id))])->toString() .
      '</div>';
    if ($seqlen <= $default_residues) {
      if ($residues) {
        $seq = $this->residues($feature->uniquename, $residues, $feature->name, $feature->type, $feature->organism, $seqlen);
        $this->addContent('sequence', 'Sequence', Markup::create($fasta . $seq), 100);
      }
    }
    else {
      $this->addContent('sequence', 'Sequence', ['#markup' => $fasta . 'Sequence \''. $feature->name . '\' contains ' . $seqlen . ' base pairs.  (not shown)'], 100);
    }
  }

}
