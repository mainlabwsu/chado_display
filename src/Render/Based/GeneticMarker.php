<?php
namespace Drupal\chado_display\Render\Based;

class GeneticMarker extends Feature {
  function __construct($id) {
    parent::__construct($id);
    $this->changeHeader('overview', 'Marker Overview');
    $this->addPolymorphism();
    $this->addPrimers();
    $this->addProbes();
    $this->addSourceSequence();
    $this->addMapPositions();
    $this->addSNPArrayId();
  }
  
  function addPolymorphism() {
    $counter = $this->statement->feature->countPolymorphism($this->feature->feature_id);
    if($counter->getTotal() > 0) {
      $this->addOverviewField('Polymorphism', \Drupal\Core\Render\Markup::create('<a href=/display/polymorphism/' . $this->feature->feature_id . '>P_' . $this->feature->name . '</a>'));
    }
  }
  
  function addPrimers() {
      $counter = $this->statement->feature->countSubjectFeatures($this->feature->feature_id, 'adjacent_to', 'relationship', 'primer');
      if($counter->getTotal() > 0) {
          $primers = $counter->getObjects();
          $index = 1;
          foreach ($primers AS $primer) {
            $this->addOverviewField('Primer ' . $index, $primer->name . ': ' . $primer->residues);
              $index ++;
          }
      }
  }

  function addProbes() {
    $counter = $this->statement->feature->countObjectFeatures($this->feature->feature_id, 'associated_with', 'sequence', 'probe');
    if($counter->getTotal() > 0) {
      $probes = $counter->getObjects();
      $index = 1;
      foreach ($probes AS $probe) {
        $this->addOverviewField('Probe ' . $index, $probe->name . ': ' . $probe->residues);
        $index ++;
      }
    }
  }
  
  function addSourceSequence() {
    $counter = $this->statement->feature->countObjectFeatures($this->feature->feature_id, 'derives_from', 'sequence', 'sequence_feature');
    if($counter->getTotal() > 0) {
      $sourceseqs = $counter->getObjects();
      foreach ($sourceseqs AS $srcseq) {
        $this->addOverviewField('Source Sequence', $this->link('feature', $srcseq->feature_id, $srcseq->name));
      }
    }
  }
  
  function addMapPositions() {
    $headers = ['Map', 'Linkage Group', 'Bin', 'Position', 'Locus'];
    $rows = [
      ['link:featuremap,featuremap_id' => 'map'],
      'linkage_group',
      ['empty:-' => 'bin'],
      ['round:2' => 'start'],
      'locus'
    ];
    $this->addPreset('Map Positions', 'feature', 'countMapPosition', $headers, $rows, 0, TRUE, NULL, TRUE);
  }
  
  function addSNPArrayId() {
    $headers = ['SNP Array', 'ID'];
    $rows = [
      ['link:library,library_id' => 'library'],
      'snp_chip_id'
    ];
    $this->addPreset('SNP Array ID', 'feature', 'countSNPArrayId', $headers, $rows);
  }
}