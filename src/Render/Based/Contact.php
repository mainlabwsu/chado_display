<?php
namespace Drupal\chado_display\Render\Based;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Render\Renderer;

class Contact extends Renderer {

  public $contact;
  public $type;

  function __construct($id) {
    parent::__construct($id);
  }

  function createPage($id) {
    $this->contact = $this->chado->getObjectById('contact', ['*'], $id);
    $this->type = $this->chado->getObjectById('cvterm', ['*'], $this->contact->type_id);
    $this->setTitle($this->contact->name);
    $info = \Drupal::state()->get('chado_display_contact_render_info', chado_display_render_info($this));
    $this->addContentByInfo($info);
    $this->addSidebarPropertyItems('contact', 'contact_id');
  }

  function renderOverview () {
    $contact = $this->contact;
    $table = $this->tableBuilder(TRUE);
    $table->addRow(['Name', $contact->name], $contact->name);
    $table->addRow(['Description', $contact->description], $contact->description);  
    
    $counter = $this->statement->base->countProperty('contact', $contact->contact_id);
    $props = $counter->getObjects();
    $default_props = \Drupal::state()->get('chado_display_contactprop', []);
    foreach ($props AS $prop) {
        if (isset($default_props[$prop->type_id]) && $default_props[$prop->type_id]) {
          $table->addRow([ucwords(str_replace('_' , ' ', $prop->type)), Markup::create($prop->value)], $prop->value);
        }
    }
    
    $table->addRow(['Type', $this->type->name], $this->type->name);
    
    $overview = [];
    $overview [] = $table->render();
    // Overview image
    $image = $this->getOverviewImageRenderArray('contact', $this->contact->contact_id);
    if($image) {
      $overview [] = $image;
    }
    $this->addContent('overview', 'Contact Overview', $overview, -100);
  }

  function renderProjects() {
      if ($this->chado->tableExists('project_contact')) {
          $headers = ['Name'];
          $rows = [
              ['link:project,project_id' => 'name'],
          ];
          $this->addPreset('Projects', 'contact', 'countProject', $headers, $rows);
      }
  }

}