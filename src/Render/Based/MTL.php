<?php
namespace Drupal\chado_display\Render\Based;

class MTL extends QTL {
  function __construct($id) {
    parent::__construct($id);
    $this->changeHeader('overview', 'MTL Overview');
    $this->setTitle(str_replace('heritable_phenotypic_marker', 'MTL', $this->getTitle()));
    $this->updateOverview();
  }
  
  function updateOverview() {
      $overview = $this->getContent('overview');
      $table = $overview[0][0];
      $table['#rows'][1]['data'][0]['data'] = 'MTL Label';
      $overview[0][0] = $table;      
      $this->replaceContent('overview', NULL, $overview);
  }
}