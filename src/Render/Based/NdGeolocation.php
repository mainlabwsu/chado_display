<?php
namespace Drupal\chado_display\Render\Based;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Render\Renderer;

class NdGeolocation extends Renderer {

  public $nd_geolocation;

  function __construct($id) {
    parent::__construct($id);
  }

  function createPage($id) {
    $this->nd_geolocation = $this->chado->getObjectById('nd_geolocation', ['*'], $id);
    $this->setTitle($this->nd_geolocation->description);
    $info = \Drupal::state()->get('chado_display_nd_geolocation_render_info', chado_display_render_info($this));
    $this->addContentByInfo($info);
    $this->addSidebarPropertyItems('nd_geolocation', 'nd_geolocation_id');
  }

  function renderOverview () {
    $nd_geolocation = $this->nd_geolocation;
    $table = $this->tableBuilder(TRUE);
    $table->addRow(['Name', $nd_geolocation->description], $nd_geolocation->description);
    $table->addRow(['Latitude', $nd_geolocation->latitude], $nd_geolocation->latitude);
    $table->addRow(['Longitude', $nd_geolocation->longitude], $nd_geolocation->longitude);
    $table->addRow(['Altitude', $nd_geolocation->altitude], $nd_geolocation->altitude);
    $table->addRow(['Geodetic Datum', $nd_geolocation->geodetic_datum], $nd_geolocation->geodetic_datum);
     
    $counter = $this->statement->base->countProperty('nd_geolocation', $nd_geolocation->nd_geolocation_id);
    $props = $counter->getObjects();
    $default_props = \Drupal::state()->get('chado_display_nd_geolocationprop', []);
    foreach ($props AS $prop) {
        if (isset($default_props[$prop->type_id]) && $default_props[$prop->type_id]) {
          $table->addRow([ucwords(str_replace('_' , ' ', $prop->type)), Markup::create($prop->value)], $prop->value);
        }
    }
    
    if (count($table->getRows()) == 0) {
        $table->addRow(['ID', $nd_geolocation->nd_geolocation_id]); 
    }
    
    $overview = [];
    $overview [] = $table->render();
    // Overview image
    $image = $this->getOverviewImageRenderArray('nd_geolocation', $this->nd_geolocation->nd_geolocation_id);
    if($image) {
      $overview [] = $image;
    }
    $this->addContent('overview', 'Environment Overview', $overview, -100);
  }

}