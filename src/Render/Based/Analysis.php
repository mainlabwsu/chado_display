<?php
namespace Drupal\chado_display\Render\Based;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Render\Renderer;

class Analysis extends Renderer {

  public $analysis;

  function __construct($id) {
    parent::__construct($id);
  }

  function createPage($id) {
    $this->analysis = $this->chado->getObjectById('analysis', ['*'], $id);
    $this->setTitle($this->analysis->name);
    $info = \Drupal::state()->get('chado_display_analysis_render_info', chado_display_render_info($this));
    $this->addContentByInfo($info);
    $this->addSidebarPropertyItems('analysis', 'analysis_id');
  }

  function renderOverview () {
    $analysis = $this->analysis;
    $table = $this->tableBuilder(TRUE);
    $table->addRow(['Name', $analysis->name], $analysis->name);
    $table->addRow(['Description', Markup::create($analysis->description)], $analysis->description);
    $table->addRow(['Program', $analysis->program], $analysis->program);
    $table->addRow(['Program Version', $analysis->programversion], $analysis->programversion);
    $table->addRow(['Algorithm', $analysis->algorithm], $analysis->algorithm);
    $table->addRow(['Source', $analysis->sourcename], $analysis->sourcename);
    $table->addRow(['Source Version', $analysis->sourceversion], $analysis->sourceversion);
    $table->addRow(['Source URI', $analysis->sourceuri], $analysis->sourceuri);
    if ($analysis->timeexecuted) {
        $date_time = explode(' ', $analysis->timeexecuted);
        $table->addRow(['Date', $date_time[0]]);
    }
    $counter = $this->statement->base->countProperty('analysis', $analysis->analysis_id);
    $props = $counter->getObjects();
    $default_props = \Drupal::state()->get('chado_display_analysisprop', []);
    foreach ($props AS $prop) {
        if (isset($default_props[$prop->type_id]) && $default_props[$prop->type_id]) {
          $table->addRow([ucwords(str_replace('_' , ' ', $prop->type)), Markup::create($prop->value)], $prop->value);
        }
    }
    $overview = [];
    $overview [] = $table->render();
    // Overview image
    $image = $this->getOverviewImageRenderArray('analysis', $this->analysis->analysis_id);
    if($image) {
      $overview [] = $image;
    }
    $this->addContent('overview', 'Analysis Overview', $overview, -100);
  }

  function renderSequences() {
      $headers = ['Name', 'Species', 'Type'];
      $rows = [
          ['link:feature,feature_id' => 'name|uniquename'],
          ['link:organism,organism_id' => 'organism'],
          'type'
      ];
      $this->addPreset('Sequences', 'analysis', 'countFeature', $headers, $rows);
  }

}