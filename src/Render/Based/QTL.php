<?php
namespace Drupal\chado_display\Render\Based;

class QTL extends Feature {
  function __construct($id) {
    parent::__construct($id);
    $this->changeHeader('overview', 'QTL Overview');    
    $this->changeHeader('projects', 'Trait Study');
    $this->updateOverview();
    $this->addColocalizingMarkers();
    $this->addNeighboringMarkers();
    $this->addMapPositions();
    $this->addEnvironment();
  }
  
  function updateOverview() {

      if ($this->chado->tableExists('featuremap_stock')) {
          $population = $this->statement->feature->getPopulation($this->feature->feature_id);
          if ($population) {
              if ($population->stock_id) {
                $this->addOverviewField ('Population', $this->link('stock', $population->stock_id, $population->uniquename));
              }
              if ($population->mat_stock_id) {
                $this->addOverviewField ('Female Parent', $this->link('stock', $population->mat_stock_id, $population->maternal));
              }
              if ($population->pat_stock_id) {
                $this->addOverviewField ('Male Parent', $this->link('stock', $population->pat_stock_id, $population->paternal));
              }
          }
      }
      $overview = $this->getContent('overview');
      $table = $overview[0][0];
      $table['#rows'][0]['data'][0]['data']  = 'Trait Name';
      $table['#rows'][1]['data'][0]['data']  = 'QTL Label';
      $overview[0][0] = $table;
      $this->replaceContent('overview', NULL, $overview);
  }
  
  function addColocalizingMarkers() {
      $counter = $this->statement->feature->countRelatedMarker($this->feature->feature_id, 'located_in');
      $headers = ['Name', 'Species', 'Type'];
      $rows = [
          ['link:feature,feature_id' => 'name|uniquename'],
          ['link:organism,organism_id' => 'organism'],
          'type'
      ];
      $this->addPreset('Colocalizing Markers', 'feature', NULL, $headers, $rows, 0, TRUE, $counter);
  }
  
  function addNeighboringMarkers() {
      $counter = $this->statement->feature->countRelatedMarker($this->feature->feature_id, 'adjacent_to');
      $headers = ['Name', 'Species', 'Type'];
      $rows = [
          ['link:feature,feature_id' => 'name|uniquename'],
          ['link:organism,organism_id' => 'organism'],
          'type'
      ];
      $this->addPreset('Neighboring Markers', 'feature', NULL, $headers, $rows, 0, TRUE, $counter);
  }
  
  function addEnvironment() {
    $counter = $this->statement->feature->countEnvironment($this->feature->feature_id);
    $headers = ['Environment'];
    $rows = [
      ['link:nd_geolocation,nd_geolocation_id' => 'description']
    ];
    $this->addPreset('Environment', 'feature', NULL, $headers, $rows, 0, TRUE, $counter);
  }
  
  function addMapPositions() {
      $headers = ['Map', 'Linkage Group', 'Bin', 'Chromosome', 'Peak', 'Span Start', 'Span Stop'];
      $rows = [
          ['link:featuremap,featuremap_id' => 'map'],
          'linkage_group',
          ['empty:-' => 'bin'],
          ['empty:-' => 'chr'],
          ['round:2' => 'peak'],
          ['round:2' => 'start'],
          ['round:2' => 'stop']          
      ];
      $this->addPreset('Map Positions', 'feature', 'countXTLPosition', $headers, $rows);
  }

}