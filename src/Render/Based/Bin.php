<?php
namespace Drupal\chado_display\Render\Based;

class Bin extends Feature {
  function __construct($id) {
    parent::__construct($id);
    $this->changeHeader('overview', 'Bin Overview');
    $this->addMapInfo();
    $this->addMapLoci();
    $this->addMapQTL();
  }
  
  function addMapInfo() {
    $info = $this->statement->feature->getMapInfo($this->feature->feature_id);
    if($info) {
      if ($info->featuremap_id) {
        $this->addOverviewField('Map', $this->link('featuremap', $info->featuremap_id, $info->map));
      }
      if ($info->lg) {
        $this->addOverviewField('Linkage Group', $info->lg);
      }
      if ($info->start) {
        $this->addOverviewField('Start', $info->start);
      }
      if ($info->stop) {
        $this->addOverviewField('Stop', $info->stop);
      }
    }
  }
  
  function addMapLoci() {
    $headers = ['Linkage Group', 'Marker Name', 'Locus Name', 'Type', 'Position'];
    $rows = [
      'lg',
      ['link:feature,feature_id' => 'genetic_marker'],
      'marker',
      'type',
      ['round:2' => 'position']
    ];
    $this->addPreset('Map Loci', 'feature', 'countBinMapLoci', $headers, $rows, 0, TRUE, NULL, TRUE);
  }
  
  function addMapQTL() {
    $headers = ['Linkage Group', 'Name', 'Type', 'Start', 'Stop', 'Peak'];
    $rows = [
      'lg',
      ['link:feature,qtl_feature_id' => 'qtl'],
      'type',
      ['round:2' => 'start'],
      ['round:2' => 'stop'],
      ['round:2' => 'peak']
    ];
    $this->addPreset('Map QTL', 'feature', 'countBinMapQTL', $headers, $rows, 0, TRUE, NULL, TRUE);
  }
}