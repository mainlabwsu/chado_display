<?php
namespace Drupal\chado_display\Render\Based;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Render\Renderer;

class Project extends Renderer {

  public $project;

  function __construct($id) {
    parent::__construct($id);
  }

  function createPage($id) {
    $this->project = $this->chado->getObjectById('project', ['*'], $id);
    $this->setTitle($this->project->name);
    $info = \Drupal::state()->get('chado_display_project_render_info', chado_display_render_info($this));
    $this->addContentByInfo($info);
    $this->addSidebarPropertyItems('project', 'project_id');
  }

  function renderOverview () {
    $project = $this->project;
    $table = $this->tableBuilder(TRUE);
    $table->addRow(['Name', $project->name], $project->name);
    $table->addRow(['Description', $project->description], $project->description);
    
    $counter = $this->statement->base->countProperty('project', $project->project_id);
    $props = $counter->getObjects();
    $default_props = \Drupal::state()->get('chado_display_projectprop', []);
    foreach ($props AS $prop) {
        if (isset($default_props[$prop->type_id]) && $default_props[$prop->type_id]) {
          $table->addRow([ucwords(str_replace('_' , ' ', $prop->type)), Markup::create($prop->value)], $prop->value);
        }
    }
    $overview = [];
    $overview [] = $table->render();
    // Overview image
    $image = $this->getOverviewImageRenderArray('project', $this->project->project_id);
    if($image) {
      $overview [] = $image;
    }
    $this->addContent('overview', 'Project Overview', $overview, -100);
  }

  function renderContacts() {
      if ($this->chado->tableExists('project_contact')) {
          $headers = ['Name', 'Type'];
          $rows = [
              ['link:contact,contact_id' => 'name'],
              'type'
          ];
          $this->addPreset('Contacts', 'project', 'countContact', $headers, $rows);
      }
  }
  
  function renderMaps () {
    if ($this->chado->tableExists('project_pub')) {
      $headers = ['Name'];
      $rows = [
        ['link:featuremap,featuremap_id' => 'name'],
      ];
      $this->addPreset('Maps', 'project', 'countMaps', $headers, $rows);
    }
  }
  
  function renderPublications () {
      if ($this->chado->tableExists('project_pub')) {
          $headers = ['Title', 'Series', 'Year', 'Type'];
          $rows = [
              ['link:pub,pub_id' => 'title'],
              'series_name',
              'pyear',
              'type'
          ];
          $this->addPreset('Publications', 'project', 'countPub', $headers, $rows);
      }
  }
  
  function renderQTL () {
      if ($this->chado->tableExists('feature_project')) {
          $headers = ['Linkage Group', 'Name', 'Type', 'Start', 'Stop', 'Peak'];
          $rows = [
              'lg',
              ['link:feature,qtl_feature_id' => 'qtl'],
              'type',
              ['round:2' => 'start'],
              ['round:2' => 'stop'],
              ['round:2' => 'peak']
          ];
          $this->addPreset('QTL', 'project', 'countQTL', $headers, $rows);
      }
  }
  
  function renderStocks() {
    $headers = ['Name', 'Uniquename', 'Type'];
    $rows = [
      ['link:stock,stock_id' => 'name'],
      ['link:stock,stock_id' => 'uniquename'],
      'type'
    ];
    $this->addPreset('Stocks', 'project', 'countStock', $headers, $rows);
  }
  
  function renderTraits () {
    if ($this->chado->tableExists('feature_project')) {
      $headers = ['Trait'];
      $rows = [
        ['urlprefix:/display/trait/,cvterm_id' => 'trait'],
      ];
      $this->addPreset('Traits', 'project', 'countTraits', $headers, $rows);
    }
  }
  
  function renderTraitDescriptors () {
    if ($this->chado->tableExists('nd_experiment_phenotype')) {
      $headers = ['Group', 'Descriptor'];
      $rows = [
        'group',
        ['urlprefix:/display/trait_descriptor/,cvterm_id' => 'descriptor'],
      ];
      $this->addPreset('Trait Descriptors', 'project', 'countTraitDescriptors', $headers, $rows);
    }
  }
}