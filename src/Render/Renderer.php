<?php
namespace Drupal\chado_display\Render;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Database\Chado;
use Drupal\chado_display\Database\Statement;

use Drupal\chado_display\Render\Element\Link;
use Drupal\chado_display\Render\Element\Sequence;
use Drupal\chado_display\Render\Element\Table;

class Renderer {

  public $chado;
  public $statement;
  public $base_table;
  public $preset_data = [];

  public $title;
  public $num_per_page;

  protected $weight = [];
  protected $category;
  protected $toc = [];
  protected $content = [];
  protected $alt_header = [];
  protected $libraries = [];
  protected $render_array = [];
  protected $json = [];

  public $pager_id = 0;

  function __construct($id, $base_table = NULL) {
    $this->base_table = $base_table;
    $this->chado = new Chado();
    $this->statement = new Statement($this->chado);
    $this->num_per_page = \Drupal::state()->get('chado_display_table_rows', 10);
    $this->createPage($id);
  }

  /**
   * Create default page for any of the Chado table.
   * The default page basically shows the content of a Chado table
   * This function will be overridden by the one defined in the subclass
   */
  function createPage($id) {
    $base_table = $this->base_table;
    if ($base_table) {
      $pk = $this->chado->getPkey($base_table);
      $columns = $this->chado->getTypedColumns($base_table);
      $record = $this->chado->getObjectById($base_table, ['*'], $id);
      $headers = [];
      $rows = [];
      $title = NULL;
      foreach ($columns AS $col => $type) {
        // Show value only when it's not from the pkey or _id column and not empty
        //if ($col != $pk && !preg_match('/_id$/',$col) && $record->$col) {
          $rows[] = [ucfirst($col), Markup::create($record->$col)];
          if ($col == 'name') {
            $title = '<em>' . ucfirst($base_table) . '</em>: ' . $record->$col;
          }
        //}
      }
      if ($title) {
        $this->setTitle(Markup::create($title));
      }
      else {
        $this->setTitle(Markup::create('<em>' . ucfirst($base_table) . '</em>'));
      }
      $table = $this->table($headers, $rows, FALSE, TRUE);
      $this->addContent('overview', 'Overview', $table, -100);
    }
  }
  
  /**
   * Add content by using the render_info
   * A render info is an array that contains all render functions of a Renderer class or subclass
   * Any function prefixed with 'render' in the Renderer class is a render function. (e.g. renderAnalysis)
   */
  function addContentByInfo($render_info) {
    foreach ($render_info AS $i) {
      $render_method = 'render' . $i;
      if (method_exists($this, $render_method)) {
        $this->$render_method();
      }
    }
  }

  /**
   * Add link directly to the TOC without adding any content
   */
  function addTocLink($key, $toc_header, $url, $weight = 0, $toc_category = NULL, $target_blank = TRUE) {
    // Warn about duplicate keys
    if (key_exists($key, $this->toc)) {
      \Drupal::messenger()->addError('duplicated renderer key detected: ' . $key);
    }
    // Add category
    if ($toc_category) {
      if ($this->category == NULL) {
        $this->category = [];
      }
      $this->category[$toc_category][] = $key;
    }

    // Add TOC
    if ($target_blank) {
      $this->toc[$key] = Markup::create('<a href="' . $url . '" target="_blank">' . $toc_header . '</a>');
    }
    else {
      $this->toc[$key] = Markup::create('<a href="' . $url . '">' . $toc_header . '</a>');
    }

    // Add weight
    $this->weight[$key] = $weight;
  }

  function changeHeader($key, $header) {
      if (isset($this->toc[$key])) {
        $this->toc[$key] = $header;
      }
  }
  
  function setAltHeader($key, $header) {
      $this->alt_header[$key] = $header;
  }
  
  /**
   * Add content by specifying a key, TOC header, render array, weight, and optionally a category
   */
  function addContent($key, $toc_header, $render_array, $weight = 0, $toc_category = NULL, $alt_header = NULL) {
    // Warn about duplicate keys
    if (key_exists($key, $this->toc)) {
      \Drupal::messenger()->addError('duplicated renderer key detected: ' . $key);
    }
    
    // Add category
    if ($toc_category) {
      if ($this->category == NULL) {
        $this->category = [];
      }
      $this->category[$toc_category][] = $key;
    }

    // Add TOC
    $this->toc[$key] = $toc_header;

    // Add weight
    $this->weight[$key] = $weight;

    // Add content
    $this->content[$key] = $render_array;
    
    if ($alt_header) {
        $this->alt_header[$key] = $alt_header;
    }
  }

  /**
   * Replace content with a key
   */
  function replaceContent($key, $toc_header = NULL, $render_array= [], $weight = NULL, $toc_category = NULL) {
    if ($toc_header == NULL) {
        $toc_header =$this->toc[$key];
    }
    if ($weight == NULL) {
        $weight =$this->weight[$key];
    }
    if ($toc_category == NULL && $this->category) {
      foreach ($this->category AS $cat_k => $cat_v) {
        if (in_array($key, $cat_v)) {
          $toc_category = $cat_k;
        }
      }
    }
    $this->removeContent($key);
    $this->addContent($key, $toc_header, $render_array, $weight, $toc_category);
  }

  /**
   * Remove content by key
   */
  function removeContent($key) {
    $category = $this->category;
    if (is_array($category)) {
        foreach ($category AS $cat => $toc) {
          foreach ($toc AS $tock => $tocv) {
            if ($tocv == $key) {
              unset($category[$cat][$tock]);
            }
          }
          if (count($category[$cat]) == 0) {
            unset($category[$cat]);
          }
        }
        $this->category = $category;
    }
    unset($this->toc[$key]);
    unset($this->weight[$key]);
    unset($this->content[$key]);
  }

  /**
   * Set weight for content identified by key
   */
  function setWeight($key, $weight) {
    if (key_exists($key, $this->weight)) {
      $this->weight[$key] = $weight;
    }
  }

  /**
   * Set category for a TOC item or move it from one category to another
   */
  function setCategory($key, $category) {
    if (key_exists($key, $this->content)) {
      if (is_array($this->category)) {
        foreach ($this->category AS $cat => $keys) {
          foreach ($keys AS $index => $k) {
            if ($k == $key) {
              unset ($this->category[$cat][$index]);
            }
          }
          if (count($this->category[$cat]) == 0) {
            unset ($this->category[$cat]);
          }
        }
      }
      $this->category[$category][] = $key;
    }
  }

  /**
   * Get Page Title
   */
  function getTitle() {
    return $this->title;
  }
  
  /**
   * Get Alternative Headers
   */
  function getAltHeader() {
      return $this->alt_header;
  }

  /**
   * Set Page Title
   */
  function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Get all TOC categories
   */
  function getTOCCatogory() {
    // Order by weight
    $weight = $this->weight;
    asort($weight);
    $category = [];
    if (is_array($this->category)) {
      foreach ($this->category AS $cat => $toc) {
        $category[$cat] = [];
        foreach ($weight AS $k => $v) {
          foreach ($toc AS $tock => $tocv) {
            if ($k == $tocv) {
              $category[$cat][] = $tocv;
            }
          }
        }
      }
    }
    return $category;
  }

  /**
   * Get all TOC ordered by weight
   */
  function getTOC() {
    // Order by weight
    $weight = $this->weight;
    asort($weight);
    $toc = [];
    foreach ($weight AS $k => $v) {
      $toc [$k] = $this->toc[$k];
    }

    return $toc;
  }
  
  /**
   * Order sidebar items according to the keys passed in
   */
  function orderBy ($keys = []) {
    $ordered_weight = [];
    $default_weight = 0;
    foreach ($keys AS $key) {
      if (key_exists($key, $this->weight)) {
        $ordered_weight [$key] = $default_weight;
        $default_weight = $default_weight + 10;
      }
    }
    $weight = $this->weight;
    asort($weight);
    foreach ($weight AS $k => $v) {
      if (!key_exists($k, $ordered_weight)) {
        $ordered_weight[$k] = $default_weight;
        $default_weight = $default_weight + 10;
      }
    }
    $this->weight = $ordered_weight;
  }

  /**
   * Order sidebar items according to the headers passed in
   */
  function orderByHeaders($headers = [], $pattern_match = FALSE) {
    $keys = [];
    foreach ($headers AS $header) {
      foreach ($this->toc AS $toc_k => $toc_v) {
        if ($pattern_match) {
          if (preg_match('/' . strtolower($header) . '/', strtolower($toc_v))) {
            $keys [] = $toc_k;
          }
        }
        else {
          $h = preg_replace('/ \(\d+\)/', '', strtolower($toc_v)); // Remove number count suffix from the header
          if (str_replace(' ', '', strtolower($header)) == str_replace(' ', '', $h)) {
            $keys [] = $toc_k;
          }
          else if (strtolower($header) == 'overview' || preg_match('/^w+ overview$/i', $header)) { // Try matching overview
            if ($toc_k == 'overview') {
              $keys [] = $toc_k;
            }
          }
        }        
      }
    }
    if (count($keys) > 0) {
      $this->orderBy($keys);
    }
  }
  
  /**
   * Order Overview Table according to the Headers passed in
   */
  function orderOverviewBy ($headers = [], $pattern_match = FALSE) {
    $overview = $this->getContent('overview');
    if ($overview) {
      if (isset($overview[0][0]['#rows'])) {
        $rows = $overview[0][0]['#rows'];
        $ordered_rows = [];
        $specified = [];
        foreach ($headers AS $header) {
          foreach ($rows AS $row_k => $row) {
            if ($pattern_match) {
              if (preg_match('/' . strtolower($header) . '/', strtolower($row['data'][0]['data'])) && !in_array($row_k, $specified)) {
                $ordered_rows [] = $row;
                $specified [] = $row_k;
              }
            }
            else {
              if (str_replace(' ', '', strtolower($row['data'][0]['data'])) == str_replace(' ', '', strtolower($header)) && !in_array($row_k, $specified)) {
                $ordered_rows [] = $row;
                $specified [] = $row_k;
              }
            }            
          }
        }
        foreach ($rows AS $row_k => $row) {
          if (!in_array($row_k, $specified)) {
            $ordered_rows[] = $row;
          }
        }
        $overview[0][0]['#rows'] = $ordered_rows;
        $this->replaceContent('overview', NULL, $overview);
      }
    }
  }
  
  /**
   * Hide Overview fields
   */
  function hideOverviewFields ($headers = []) {
    // to lower case, strip spaces
    $compare = [];
    foreach ($headers AS $header) {
      $compare [] = str_replace(' ', '', strtolower($header));
    }
    $overview = $this->getContent('overview');
    if ($overview) {
      if (isset($overview[0][0]['#rows'])) {
        $rows = $overview[0][0]['#rows'];
        $final_rows = [];
        foreach ($rows AS $row_k => $row) {
          if (!in_array(str_replace(' ', '', strtolower($row['data'][0]['data'])), $compare)) {
            $final_rows [] = $row;
          }
        }      
        $overview[0][0]['#rows'] = $final_rows;
        $this->replaceContent('overview', NULL, $overview);
      }
    }
  }
  
  /**
   * Add a field to the Overview table
   */
  function addOverviewField ($header, $content) {
    $overview = $this->getContent('overview');
    if ($overview) {
      if (isset($overview[0][0]['#rows'])) {
        $rows = $overview[0][0]['#rows'];
        $overview[0][0]['#rows'][] = [
          'data' => [
            0 => ['data' => $header, 'header' => true],
            1 => $content
          ],
          'class' => [0 => 'chado_display_header_row-' . $header]
        ];
        $this->replaceContent('overview', NULL, $overview);
      }
    }
  }

  /**
   * Replace a field in the Overview table
   */
  function replaceOverviewField ($header, $content) {
    $overview = $this->getContent('overview');
    if ($overview) {
      if (isset($overview[0][0]['#rows'])) {
        $rows = $overview[0][0]['#rows'];
        foreach ($rows AS $key => $row) {
          if ($row['data'][0]['data'] == $header) {
            $overview[0][0]['#rows'][$key]['data'][1] = $content;
          }
        }
        $this->replaceContent('overview', NULL, $overview);
      }
    }
  }
  
  /**
   * Get all content
   */
  function getAllContent() {
    // Order by weight
    $weight = $this->weight;
    asort($weight);
    $content = [];
    foreach ($weight AS $k => $v) {
        $content [$k] = isset($this->content[$k]) ? $this->content[$k] : NULL;
    }
    return $content;
  }
  
  /**
   *  Get json object representing this renderer object
   */
  function getJson($base_table, $id) {
    $pk = $this->chado->getPkey($base_table);
    $fks = $this->chado->getFKeyColumns($base_table);
    $columns = $this->chado->getTypedColumns($base_table);
    $record = $this->chado->getObjectById($base_table, ['*'], $id);
    $this->json[$base_table] = [];
    $limit_chrs = 100000;
    // Basic table content
    foreach ($columns AS $col => $type) {
      // Show value only when it's not from the pkey or _id column and not empty
      if ($col != $pk && !preg_match('/_id$/',$col) && $record->$col) {
        if (strlen($record->$col) > $limit_chrs) {
          $this->json [$base_table][$col] = 'value too large to display';
        }
        else {
          $this->json [$base_table][$col] = $record->$col;
        }
      }
      else if ($col == 'organism_id' && key_exists($col, $fks)) {
        $organism = $this->chado->getObjectById('organism', ['genus', 'species'], $record->$col);
        $this->json [$base_table]['organism'] = $organism->genus . ' ' . $organism->species;
      }
      else if ($col == 'type_id' && key_exists($col, $fks)) {
        $type = $this->chado->getObjectById('cvterm', ['name'], $record->$col);
        $this->json [$base_table]['type'] = $type->name;
      }
    }
    if ($this->chado->tableExists($base_table . 'prop')) {
      $base = new \Drupal\chado_display\Database\Prepared\BaseStatement ($this->chado);
      $props = $base->countProperty($base_table, $id);
      foreach ($props->getObjects() AS $prop) {
        $this->json['properties'][$prop->type] = $prop->value ;
      }
    }
    return $this->json;
  }
  
  // Set all content to a render array
  function render() {
      $category = $this->getTOCCatogory();
      $toc = $this->getTOC();
      $alt_header = $this->getAltHeader();
      $content = $this->getAllContent();
      $style = \Drupal::state()->get('chado_display_page_style', 'scroll');
      $lib = ['chado_display/chado_display_style'];
      $template = 'chado_page';
      if ($style == 'scroll') {
          $lib [] = 'chado_display/chado_display_scroll';
      }
      else if ($style == 'scroll_right') {
        $template = 'chado_page_right_sidebar';
        $lib [] = 'chado_display/chado_display_scroll_right';
      }
      else if ($style == 'fadein') {
          $lib [] = 'chado_display/chado_display_fadein';
      }
      else if ($style == 'fadein_right') {
        $template = 'chado_page_right_sidebar';
        $lib [] = 'chado_display/chado_display_fadein_right';
      }
      else if ($style == 'expandable') {
        $lib [] = 'chado_display/chado_display_expandable_boxes';
        $template = 'chado_page_expandable_boxes';
      }
      else if ($style == 'nosidebar') {
        $template = 'chado_page_nosidebar';
      }
      $tstyle = \Drupal::state()->get('chado_display_table_style', 'none');
      if ($tstyle == 'grey') {
          $lib [] = 'chado_display/chado_display_grey_table';
      } else if ($tstyle == 'blue') {
          $lib [] = 'chado_display/chado_display_blue_table';
      } else if ($tstyle == 'green') {
          $lib [] = 'chado_display/chado_display_green_table';
      } else if ($tstyle == 'light_green') {
          $lib [] = 'chado_display/chado_display_light_green_table';
      } else if ($tstyle == 'dark_green') {
        $lib [] = 'chado_display/chado_display_dark_green_table';
      } else if ($tstyle == 'red') {
          $lib [] = 'chado_display/chado_display_red_table';
      }
      $lib = array_merge($lib, $this->getLibs());
      $cname =  explode('\\', get_class($this));
      $class_name = array_pop($cname);
      $render_array = [
        '#theme' => $template,
          '#attached' => [
              'library' => $lib
          ],
          '#category' => $category,
          '#table_of_content' => $toc,
          '#alt_header' => $alt_header,
          '#content' => $content,
          '#class_name' => $class_name
      ];
      $this->render_array = $render_array;
  }

  function setRenderArray($render_array) {
      $this->render_array = $render_array;
  }
  
  function getRenderArray() {
      return $this->render_array;
  }
  
  /**
   * Get content by key
   */
  function getContent($key) {
    $content = $this->getAllContent();
    if (key_exists($key, $content)) {
      return $content[$key];
    }
    else {
      return NULL;
    }
  }

  /**
   * Added library to attach to a page
   */
  public function addLib($lib) {
    $this->libraries [] = $lib;
  }

  /**
   * Get added libraries
   */
  public function getLibs() {
    return $this->libraries;
  }

  /**
   * Get next pager ID
   */
  function getNextPagerId() {
    $this->pager_id = $this->pager_id + 1;
    return $this->pager_id;
  }

  /**
   * link to other chado_display pages
   */
  function link ($base_table, $id, $display, $new_tab = FALSE, $section = NULL) {
    $l = new Link();
    return $l->getChadoDisplayLink($base_table, $id, $display, $new_tab, $section);
  }
  
  /**
   * link to specified URL prefix
   */
  function url ($urlprefix, $id, $display, $new_tab = FALSE) {
      $l = new Link();
      return $l->getURLLink($urlprefix, $id, $display, $new_tab);
  }

  /**
   * Create a Table renderer
   */
  function table($headers, $rows, $pid = NULL, $vertical = FALSE) {
    $tab = new Table();
    return $tab->getTable($headers, $rows, $pid, $vertical);
  }
  
  function tableBuilder($vertical = FALSE, $pager = FALSE) {
    $table = new Table($vertical);
    if ($pager) {
      $pid = $this->getNextPagerId();
      $table->setPager($pid);
    }
    return $table;
  }

  /**
   * Create a sequence renderer
   */
  function residues($uniquename, $residues, $name = NULL, $type = NULL, $organism = NULL, $seqlen = NULL) {
    $seq = new Sequence();
    return $seq->getSequence($uniquename, $residues, $name, $type, $organism, $seqlen);
  }
  
  function addPreset($header, $base, $func, $headers = [], $values = [], $weight = 0, $addTotal = TRUE, $use_counter = NULL, $download = TRUE) {
      $object = $base == NULL ? NULL : $this->$base;
      $id = $base . '_id';
      $counter = $use_counter != NULL ? $use_counter : $this->statement->$base->$func($object->$id);
      $pid = $this->getNextPagerId();
      $pager = $counter->setPager($this->num_per_page, $pid);
      $page = $pager->getCurrentPage();
      
      $results = $counter->getPagedObjects($page);
      $this->preset_data [$header] = $results;
      $rows = [];
      foreach ($results AS $r) {
          $display = [];
          foreach ($values AS $val) {
              if (is_string($val)){
                $sel = explode('|', $val);
                if (count($sel) == 2) {
                    $display [] = $r->{$sel[0]} ? $r->{$sel[0]} : $r->{$sel[1]};
                }
                else {
                    $display [] = $r->$val;
                }
              }
              else if (is_array($val)) {
                  $k = array_key_first ($val);
                  $v = $val[$k];
                  // Execute command only if it's in the 'cmd:para1,para2' format
                  $token = explode(':', $k);
                  
                  // Link command: link value to a Chado Display URI
                  if (count($token) == 2 && $token[0] == 'link') {
                      $link = explode(',', $token[1]);
                      $sel = explode('|', $v);
                      if (count($sel) == 2) {
                          $display [] = $this->link($link[0], $r->{$link[1]}, $r->{$sel[0]} ? $r->{$sel[0]} : $r->{$sel[1]});
                      }
                      else {
                          $display [] = $this->link($link[0], $r->{$link[1]}, $r->$v);
                      }
                  }
                  
                  // Urlprefix command: link value to any URL prefix
                  else if (count($token) == 2 && $token[0] == 'urlprefix') {
                      $link = explode(',', $token[1]);
                      $sel = explode('|', $v);
                      if (count($sel) == 2) {
                          $display [] = $this->url($link[0], $r->{$link[1]}, $r->{$sel[0]} ? $r->{$sel[0]} : $r->{$sel[1]});
                      }
                      else {
                          $display [] = $this->url($link[0], $r->{$link[1]}, $r->$v);
                      }
                  }
                  
                  // JBrowse command: link value to JBrowse URL ($r should contain 'analysis_id', 'landmark', and 'location' properties
                  else if (count($token) == 1 && $token[0] == 'jbrowse') {
                    $link = NULL;                    
                    if (isset($r->landmark) && isset($r->location) && isset($r->analysis_id)) {
                      $jbrowse_url = $this->statement->analysis->getJBrowseURL($r->analysis_id);
                      $link = $jbrowse_url . $r->landmark . ':' . $r->location;
                    }
                    $display [] = $this->url($link, NULL, $r->$v);
                  }
                  
                  // Empty command: replace empty value with specified string
                  else if (count($token) == 2 && $token[0] == 'empty') {
                      $sel = explode('|', $v);
                      if (count($sel) == 2) {
                          $test = $r->{$sel[0]} ? $r->{$sel[0]} : $r->{$sel[1]};
                          $display [] = $test ? $test : $token[1];
                      }
                      else {
                          $display [] = $r->$v ? $r->$v : $token[1];
                      }
                  }
                  
                  // Round command: round to specified digit if it's a number. if not, reutnr '-' for NaN values
                  else if (count($token) == 2 && $token[0] == 'round') {
                      $sel = explode('|', $v);
                      if (count($sel) == 2) {
                          $test = $r->{$sel[0]} ? $r->{$sel[0]} : $r->{$sel[1]};
                          $display [] = is_numeric($test) ? round($test, $token[1]) : '-';
                      }
                      else {
                          $display [] = is_numeric($r->$v) ? round($r->$v, $token[1]) : '-';
                      }
                  }
                  
                  // Date command: display date only
                  else if (count($token) == 1 && $token[0] == 'date') {
                      $sel = explode('|', $v);
                      if (count($sel) == 2) {
                          $test = $r->{$sel[0]} ? $r->{$sel[0]} : $r->{$sel[1]};
                          $datetime = explode(' ', $test);
                          $display [] = $datetime [0];
                      }
                      else {
                          $datetime = explode(' ', $r->$v);
                          $display [] = $datetime[0];
                      }
                  }
                  
                  // Dbxref command: link value to the prefix URL
                  else if (count($token) == 1 && $token[0] == 'dbxref') {
                      $sel = explode('|', $v);
                      if (count($sel) == 2) {
                        $test = $r->{$sel[0]} ? $r->{$sel[0]} : $r->{$sel[1]};
                        $accession = $r->db == 'GO' ? 'GO:' . $r->accession : $r->accession;
                        $show = $v == 'accession' ? $accession : $test;
                        $dbxref = $r->urlprefix ? Markup::create('<a href="' . $r->urlprefix . $accession . '" target=_blank>' . $show . '</a>') : $show;
                        $display [] = $dbxref;
                      }
                      else {
                          $accession = $r->db == 'GO' ? 'GO:' . $r->accession : $r->accession;
                          $show = $v == 'accession' ? $accession : $r->$v;
                          $dbxref = $r->urlprefix ? Markup::create('<a href="' . $r->urlprefix . $accession . '" target=_blank>' . $show . '</a>') : $show;
                          $display [] = $dbxref;
                      }
                  }
                  
                  // Split command: split value into columns
                  else if (count($token) == 2 && $token[0] == 'split') {
                      $parameters = explode(',', $token[1]);
                      $delimiter = $parameters[0];
                      $offset = count($parameters) > 0 ? $parameters[1]: 0;
                      $split_v = explode($delimiter, $r->$v);
                      $display [] = $split_v[$offset];
                  }
                  
                  // User function: call user defined function
                  else if (count($token) == 2 && $token[0] == 'function') {
                      $func = explode(',', $token[1]);
                      $func_name = $func[0];
                      $args = [];
                      for ($index = 1; $index < count($func); $index ++) {
                          $arg_name = $func[$index];
                          $args[$arg_name] = $r->$arg_name;
                      }
                      $display [] = $this->$func_name($args);
                      
                  }
              }
          }
          $rows[] = $display;
      }
      $table = $this->table ($headers, $rows, $pid);
      if ($download) {
        $columns = [];
        for ($i = 0; $i < count($headers); $i ++) {
          $columns[$i] = is_array($values[$i]) ? array_pop($values[$i]) : $values[$i];
        }
        $info = new \stdClass();
        $info->key = $header;
        $info->headers = $headers;
        $info->columns = $columns;
        $info->query = $counter->getQuery();
        $dlink = \Drupal\Core\Link::createFromRoute('download', 'chado_display.download', ['info' => urlencode(serialize($info))])->toString();
        $table = [0 => ['#markup' => '<div class="chado_page-section_download chado_display-page_control">' . $dlink . '</div>'], 1 => $table];
      }
      $total = $counter->getTotal();
      if ($total != 0) {
          $key = str_replace(' ', '', strtolower($header));
          if ($addTotal) {
              $record = $total == 1 ? 'record' : 'records';
              $this->addContent($key, $header . ' (' . $total . ')', [['#markup' => '<div class="chado_display-page_content-found">Found ' . $total . ' ' . $record . ':</div>'], $table], $weight, NULL);
          }
          else {
              $this->addContent($key, $header, $table, $weight);
          }
      }
  }  
  
  protected function addSidebarPropertyItems($base_table, $base_id) {
    $counter = $this->statement->base->countProperty($base_table, $this->$base_table->$base_id);
    $props = $counter->getObjects();
    $default_props = \Drupal::state()->get('chado_display_' . $base_table . 'prop_sidebar', []);
    foreach ($props AS $prop) {
      if (isset($default_props[$prop->type_id]) && $default_props[$prop->type_id]) {
        $this->addContent('prop-' . str_replace(' ', '', $prop->type), ucwords(str_replace('_' , ' ', $prop->type)), ['#markup' => Markup::create($prop->value)]);
      }
    }
  }
  
  protected function getOverviewImageRenderArray($base_table, $id) {
    $path = chado_display_get_path() . '/' . $base_table . '/' . $id;
    $image = $this->chado->getFirstObject('eimage', ['*'], [], ['image_uri' => $path]);
    if($image) {
      $img_markup = Markup::create('<div class=chado_display-overview_image><img src="data:image;base64,' . $image->eimage_data . '"></div>');
      return ['#markup' => $img_markup];
    }
    else {
      return NULL;
    }
  }
}
