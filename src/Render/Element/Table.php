<?php
namespace Drupal\chado_display\Render\Element;

class Table {

  protected $headers;
  protected $rows;
  protected $pager_id;
  protected $vertical;
  
  function __construct($vertical = FALSE) {
    $this->headers = [];
    $this->rows = [];
    $this->vertical = $vertical;
  }
  
  function setPager($pager_id) {
    $this->pager_id = $pager_id;
  }
  
  function setHeaders($headers) {
    $this->headers = $headers;
  }
  
  function getHeaders() {
    return $this->headers;
  }
  
  function setRows ($rows) {
    $this->rows = $rows;
  }
  
  function getRows() {
    return $this->rows;
  }
  
  function addRow($row, $valueToCheck = TRUE) {
  if ($valueToCheck) {
      array_push($this->rows, $row);
    }
    return $this;
  }
  
  function render() {
    return $this->getTable($this->headers, $this->rows, $this->pager_id, $this->vertical);
  }
  
  function getTable ($headers, $rows, $pager_id = NULL, $vertical = FALSE) {
    $render = [];
    if ($pager_id) {
      $id = 'chado_display_table-pager' . $pager_id;
      $render [] = ['#markup' => '<div id=' . $id . ' class=chado_display_table_wrapper>'];
    }
    $myrows = [];

    if ($vertical) {
      foreach ($rows AS $row) {
        $mycells = [];
        $myclass = '';
        foreach ($row AS $idx => $cell) {
          if ($idx == 0) {
            $myclass = 'chado_display_header_row-' . str_replace(' ', '', strtolower($cell));
            $mycells [] = ['data' => $cell, 'header' => TRUE, 'width' => '20%'];
          }
          else {
            $mycells [] = $cell;
          }
        }
        $myrows [] = ['data' => $mycells, 'class' => [$myclass]];
      }
    }
    else {
      $myrows = $rows;
    }

    $table = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $myrows,
      '#attributes' => ['class' => ['chado_display_style_table']]
    ];
    if ($pager_id) {
      $render[] = $table;
      $render[] = [
        '#type' => 'pager',
        '#element' => $pager_id
      ];
    }
    else {
      $render [] = $table;
    }
    if ($pager_id) {
      $render [] = ['#markup' => '</div>'];
    }
    return $render;
  }

}