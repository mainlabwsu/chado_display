<?php
namespace Drupal\chado_display\Render\Element;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Database\Chado;

class InterPro {
  
  function render ($analysisfeatureprops, $residues) {
    $chado = new Chado();
    $link = new Link();
    $table = new Table();
    
    $results = $this->getResults($analysisfeatureprops);
    $render_array = [];
    if(count($results) > 0){
      foreach($results as $analysis_id => $details){
        if(!isset($details['iprterms'])) continue;
        $analysis   = $details['analysis'];
        $iprterms   = $details['iprterms'];
        $format     = $details['format'];
        
        // ANALYSIS DETAILS
        $aname = $link->getChadoDisplayLink('analysis', $analysis_id, $analysis['name']);

        $date_performed = preg_replace("/^(\d+-\d+-\d+) .*/", "$1", $analysis['timeexecuted']);
        $html = "
          Analysis Name: $aname
          <br>Date Performed: $date_performed
          <div id=\"features_vis-".$analysis['analysis_id']."\"></div>
          <script lang=\"javascript\">
              (function($) {
              // hack to work with multiple jquery versions
              backup_jquery = window.jQuery;
              window.jQuery = $;
              window.$ = $;
              window.ft".$analysis['analysis_id']." = new FeatureViewer('".$residues."',
              '#features_vis-".$analysis['analysis_id']."',
              {
                  showAxis: true,
                  showSequence: true,
                  brushActive: true, //zoom
                  toolbar:true, //current zoom & mouse position
                  bubbleHelp:false,
                  zoomMax:3 //define the maximum range of the zoom
              });
              window.jQuery = backup_jquery;
              window.$ = backup_jquery;
            })(feature_viewer_jquery);
          </script>
        ";
        
        // ALIGNMENT SUMMARY
        $headers = array(
          'IPR Term',
          'IPR Description',
          'Source',
          'Source Term',
          'Source Description',
          'Alignment'
        );
        
        $rows = array();
        foreach ($iprterms as $ipr_id => $iprterm) {
          
          $matches  = $iprterm['matches'];
          $ipr_name = $iprterm['ipr_name'];
          $ipr_desc = $iprterm['ipr_desc'];
          $ipr_type = $iprterm['ipr_type'];
          
          // iterate through the evidence matches
          foreach ($matches as $match) {
            $hsp_pos = array();
            $match_id     = $match['match_id'];
            $match_name   = $match['match_name'];
            $match_dbname = $match['match_dbname'];
            
            $locations = $match['locations'];
            $loc_details = '';
            foreach($locations as $location){
              if ($format == 'XML4') {
                $loc_details .= 'coord: ' . $location['match_start'] . ".." . $location['match_end'];
                if($location['match_score']) {
                  $loc_details .= '<br>score: ' . $location['match_score'];
                }
              }
              if ($format == 'XML5') {
                $loc_details .= 'coord: ' . $location['match_start'] . ".." . $location['match_end'];
                if($location['match_evalue']) {
                  $loc_details .= '<br>e-value: ' . $location['match_evalue'];
                }
                if($location['match_score']) {
                  $loc_details .= '<br>score: ' . $location['match_score'];
                }
                $loc_details .= '<br>';
              }
              //$match_evidence =  $location['match_evidence'];
              
              $desc = '';
              if (!empty($location['match_evalue']))
                $desc = 'Expect = '.$location['match_evalue'];
                if (!empty($location['match_score']))
                  if (!empty($desc))
                    $desc .= ' / ';
                    $desc .= 'Score = '.$location['match_score'];
                    $hsp_pos[] = array('x' => intval($location['match_start']), 'y' => intval($location['match_end']), 'description' => $desc);
            }
            
            // remove the trailing <br>
            $loc_details = substr($loc_details, 0, -4);
            
            if ($ipr_id == 'noIPR') {
              $ipr_id_link = 'None';
              $ipr_desc = 'No IPR available';
            }
            else {
              // we want to use the URL for the database
              $ipr_db = $chado->getFirstObject('db', ['urlprefix'], [], ['name' => 'INTERPRO']);
              $ipr_id_link = $ipr_id;
              if ($ipr_db and $ipr_db->urlprefix) {
                $ipr_id_link = $link->getURLLink($ipr_db->urlprefix, $ipr_id, $ipr_id, TRUE);
              }
            }
            
            // the Prosite databases are split into two libraries for InterProScan. But
            // we can just use the PROSITE database for both of them, so rename it here.
            $match_dbname = preg_replace('/(PROSITE)_.*/', '\1', $match_dbname);
            
            // get links for the matching databases
            $match_db = $chado->getFirstObject('db', ['urlprefix', 'url'], [], ['name' => strtoupper($match_dbname)]);
            if ($match_db and $match_db->url) {
              // some databases need a prefix removed
              if ($match_dbname == "GENE3D") {
                $fixed_id = preg_replace('/G3DSA:/','', $match_id);
                $match_id = $link->getURLLink($match_db->urlprefix, $fixed_id, $fixed_id, TRUE);
              }
              elseif ($match_dbname == "SUPERFAMILY") {
                $fixed_id = preg_replace('/SSF/','', $match_id);
                $match_id = $link->getURLLink($match_db->urlprefix, $fixed_id, $fixed_id, TRUE);
              }
              // for all others, just link using the URL prefix
              else {
                $match_id = $link->getURLLink($match_db->urlprefix, $match_id, $match_id, TRUE);
              }
            }
            if ($match_db and $match_db->url) {
              $match_dbname = $link->getURLLink($match_db->url, '', $match_dbname, TRUE);
            }
            
            $rows[] = array(
              $ipr_id_link,
              $ipr_desc,
              $match_dbname,
              $match_id,
              $match_name,
              array(
                'data' => Markup::create($loc_details),
                'nowrap' => 'nowrap'
              ),
            );
            
            $viz_name = $ipr_id;
            if (!empty($match['match_id']))
              $viz_name = $match['match_id'];
              
              $html .= "<script lang=\"javascript\">
              (function($) {
                  // hack to work with multiple jquery versions
                  backup_jquery = window.jQuery;
                  window.jQuery = $;
                  window.$ = $;
                  window.ft".$analysis['analysis_id'].".addFeature({
                   data: ".json_encode($hsp_pos).",
                   name: \"".$viz_name."\",
                   className: \"ipr_match\", //can be used for styling
                   color: \"#0F8292\",
                   type: \"rect\"
               });
               window.jQuery = backup_jquery;
               window.$ = backup_jquery;
             })(feature_viewer_jquery);
            </script>";
          } // end foreach ($matches as $match) {
        } // end foreach ($iprterms as $ipr_id => $iprterm) {
        
        if (count($rows) == 0) {
          $rows[] = array(
            array(
              'data' => 'No results',
              'colspan' => '6',
            ),
          );
        }
        
        // once we have our table array structure defined, we call Drupal's theme_table()
        // function to generate the table.
        $render_array[] = ['#markup' => Markup::create($html)];
        $render_array[] = $table->getTable($headers, $rows);        
      }
    }
    return $render_array;
  }
  
  function getResults($analysisfeatureprops) {
    $results = [];
    foreach ($analysisfeatureprops AS $obj) {
      $analysis_id = $obj->analysis_id;
      if (!key_exists($analysis_id, $results)) {
        $results[$analysis_id] = [];
      }
      $terms = $this->parseFeatureXML($obj->value);
      foreach ($terms['iprterms'] as $ipr_id => $iprterm) {
        
        // consolidate the IPR
        $results[$analysis_id]['iprterms'][$ipr_id]['ipr_desc'] = $iprterm['ipr_desc'];
        $results[$analysis_id]['iprterms'][$ipr_id]['ipr_name'] = $iprterm['ipr_name'];
        $results[$analysis_id]['iprterms'][$ipr_id]['ipr_type'] = $iprterm['ipr_type'];
        
        
        // merge the matches
        if (!array_key_exists('matches',  $results[$analysis_id]['iprterms'][$ipr_id])) {
          $results[$analysis_id]['iprterms'][$ipr_id]['matches'] = array();
        }
        $results[$analysis_id]['iprterms'][$ipr_id]['matches'] = array_merge($results[$analysis_id]['iprterms'][$ipr_id]['matches'], $iprterm['matches']);
        
        // merge the goterms
        if (!array_key_exists('goterms',  $results[$analysis_id]['iprterms'][$ipr_id])) {
          $results[$analysis_id]['iprterms'][$ipr_id]['goterms'] = array();
        }
        $results[$analysis_id]['iprterms'][$ipr_id]['goterms'] = array_merge($results[$analysis_id]['iprterms'][$ipr_id]['goterms'], $iprterm['goterms']);
        
      }
      // merge the go terms
      foreach ($terms['goterms'] as $go_id => $goterm) {
        $results[$analysis_id]['goterms'][$go_id]['name']     = $goterm['name'];
        $results[$analysis_id]['goterms'][$go_id]['category'] = $goterm['category'];
      }
      $results[$analysis_id]['analysis'] = ['analysis_id' => $obj->analysis_id, 'name' => $obj->analysis, 'timeexecuted' => $obj->timeexecuted];
      $results[$analysis_id]['format']   = $terms['format'];
    }
    return $results;
  }

  function parseFeatureXML($interpro_xml) {
    
    // Load the XML into an object
    $xml_obj = simplexml_load_string($interpro_xml);
    
    // we can distinguish between XML4 and XML5 by looking at the name and attributes
    $name  = $xml_obj->getname();
    $attrs = $xml_obj->attributes();
    
    if ($name == 'nucleotide-sequence') {
      return $this->parseFeatureXML5Nucelotide($xml_obj);
    }
    if ($name == 'protein') {
      // XML 5 protein key has no attributes XML4 does
      if (count($attrs) == 0) {
        return $this->parseFeatureXML5Protein($xml_obj);
      }
      return $this->parseFeatureXML4($xml_obj);
    }
  }
  
  function parseFeatureXML5Protein($xml_obj) {
    
    $terms  = array();
    $terms['format'] = 'XML5';
    $terms['iprterms'] = array();
    $terms['goterms'] = array();
    
    // iterate through each element of the 'protein' children
    foreach ($xml_obj->children() as $element) {
      
      if($element->getname() == 'matches') {
        // iterate through the match elements
        foreach ($element->children() as $match_element) {
          // initalize the $match array where we store details about this match
          $match = array();
          
          // sometimes an alignment is made but there is no corresponding IPR term
          // so we default the match IPR term to 'noIPR'
          $match_ipr_id = 'noIPR';
          $match_ipr_type = '';
          $match_ipr_desc = '';
          $match_ipr_name = '';
          
          // iterate through the children of this match to collect the details
          foreach ($match_element->children() as $match_details) {
            
            // the <signature> tag contains information about the match in the
            // member database (e.g. GENE3D, PFAM, etc).
            if ($match_details->getname() == 'signature') {
              
              // get the details about this match
              $attrs = $match_details->attributes();
              $match['match_name'] = (string) $attrs['name']; // the name of the match
              $match['match_desc'] = (string) $attrs['desc']; // the match description
              $match['match_id']   = (string) $attrs['ac'];   // the library accession number
              
              // find the IPR term and GO Terms associated with this match
              foreach ($match_details->children() as $sig_element) {
                
                // the <entry> tag contains the IPR term entry that corresponds to this match
                if ($sig_element->getname() == 'entry') {
                  $attrs = $sig_element->attributes();
                  $match_ipr_id   = (string) $attrs['ac'];
                  $match_ipr_type = (string) $attrs['type']; // e.g. DOMAIN, SITE
                  $match_ipr_name = (string) $attrs['name']; // the match name
                  $match_ipr_desc = (string) $attrs['desc']; // the match description
                  
                  // initialize the term sub array and matches if they haven't already been added.
                  if (!array_key_exists($match_ipr_id, $terms['iprterms'])) {
                    $terms['iprterms'][$match_ipr_id] = array();
                    $terms['iprterms'][$match_ipr_id]['matches'] = array();
                    $terms['iprterms'][$match_ipr_id]['goterms'] = array();
                  }
                  
                  // get the GO terms which are children of the <entry> element
                  foreach ($sig_element->children() as $entry_element) {
                    if ($entry_element->getname() == 'go-xref') {
                      $attrs = $entry_element->attributes();
                      $go_id = (string) $attrs['id'];
                      $goterm = array();
                      $goterm['category'] = (string) $attrs['category'];
                      $goterm['name']     = (string) $attrs['name'];
                      
                      // GO terms are stored twice. Once with the IPR term to which they were found
                      // and second as first-level element of the $terms array where all terms are present
                      $terms['iprterms'][$match_ipr_id]['goterms'][$go_id] = $goterm;
                      $terms['goterms'][$go_id] = $goterm;
                    }
                  }
                }
                
                // add in the match library and version information
                if ($sig_element->getname() == 'signature-library-release') {
                  $attrs = $sig_element->attributes();
                  $match['match_dbname']  = (string) $attrs['library'];
                  $match['match_version'] = (string) $attrs['version'];
                }
              }
            } // end if($match_element->getname() == 'signature') {
            
            // the <locations> tag lists the alignment locations for this match
            if($match_details->getname() == 'locations') {
              $k = 0;
              foreach ($match_details->children() as $loc_element) {
                $attrs = $loc_element->attributes();
                $match['locations'][$k]['match_start']  = (string) $attrs['start'];
                $match['locations'][$k]['match_end']    = (string) $attrs['end'];
                $match['locations'][$k]['match_score']  = (string) $attrs['score'];
                $match['locations'][$k]['match_evalue'] = (string) $attrs['evalue'];
                $match['locations'][$k]['match_level']  = (string) $attrs['level'];
                $k++;
              }
            } // end if($match_details->getname() == 'locations') {
          } // end foreach ($match_element->children() as $match_details) {
          
          $attrs = $match_element->attributes();
          $match['evalue'] = (string) $attrs['evalue'];
          $match['score']  = (string) $attrs['score'];
          
          // add this match to the IPR term key to which it is associated
          $terms['iprterms'][$match_ipr_id]['matches'][] = $match;
          $terms['iprterms'][$match_ipr_id]['ipr_type'] = $match_ipr_type;
          $terms['iprterms'][$match_ipr_id]['ipr_name'] = $match_ipr_name;
          $terms['iprterms'][$match_ipr_id]['ipr_desc'] = $match_ipr_desc;
          
          // make sure we have a goterms array in the event that none were found
          if (!array_key_exists('goterms', $terms['iprterms'][$match_ipr_id])) {
            $terms['iprterms'][$match_ipr_id]['goterms'] = array();
          }
          
        } // end foreach ($element->children() as $match_element) {
      } // end if($element->getname() == 'matches') {
    } // end foreach ($xml_obj->children() as $element) {
    
    //if (count($terms['iprterms']) > 0) {
    //  dpm($terms);
    //}
    return $terms;
  }
  
  function parseFeatureXML5Nucelotide($xml_obj) {
    
    $results  = array();
    $results['format'] = 'XML5';
    $results['iprterms'] = array();
    $results['goterms'] = array();
    
    // iterate through each element of the 'nucleotide-sequence' children
    foreach ($xml_obj->children() as $element) {
      if($element->getname() == 'orf') {
        foreach ($element->children() as $sub_element) {
          
          // the <protein> tag is an ORF in the nucleotide sequence
          if ($sub_element->getname() == 'protein') {
            $terms = $this->parseFeatureXML5Protein($sub_element);
            
            // becase there are multiple ORFs per sequence we want to combine the
            // results into a single set to be returned by this function
            foreach ($terms['iprterms'] as $ipr_id => $iprterm) {
              
              // consolidate the IPR
              $results['iprterms'][$ipr_id]['ipr_desc'] = $iprterm['ipr_desc'];
              $results['iprterms'][$ipr_id]['ipr_name'] = $iprterm['ipr_name'];
              $results['iprterms'][$ipr_id]['ipr_type'] = $iprterm['ipr_type'];
              
              // merge the matches
              if (!array_key_exists('matches', $results['iprterms'][$ipr_id])) {
                $results['iprterms'][$ipr_id]['matches'] = array();
              }
              $results['iprterms'][$ipr_id]['matches'] = array_merge($results['iprterms'][$ipr_id]['matches'], $iprterm['matches']);
              
              // merge the goterms
              if (!array_key_exists('goterms', $results['iprterms'][$ipr_id])) {
                $results['iprterms'][$ipr_id]['goterms'] = array();
              }
              $results['iprterms'][$ipr_id]['goterms'] = array_merge($results['iprterms'][$ipr_id]['goterms'], $iprterm['goterms']);
              
              // merge the go terms
              foreach ($iprterm['goterms'] as $go_id => $goterm) {
                $results['goterms'][$go_id]['name']     = $goterm['name'];
                $results['goterms'][$go_id]['category'] = $goterm['category'];
              }
            }
          }
        }
      }
    }
    return $results;
  }
  
  function parseFeatureXML4($xml_obj) {
    
    $terms  = array();
    $terms['format'] = 'XML4';
    $terms['iprterms'] = array();
    $terms['goterms'] = array();
    
    // get the properties of this result
    $attr = $xml_obj->attributes();
    $protein['orf_id']     = (string) $attr["id"];
    $protein['orf_length'] = (string) $attr["length"];
    $protein['orf_crc64']  = (string) $attr["crc64"];
    
    // iterate through each interpro results for this protein
    foreach ($xml_obj->children() as $intepro) {
      
      // get the interpro term for this match
      $attr = $intepro->attributes();
      $ipr_id = (string) $attr["id"];
      
      $terms['iprterms'][$ipr_id]['ipr_name'] = (string) $attr["name"];
      $terms['iprterms'][$ipr_id]['ipr_desc'] = (string) $attr["name"];
      $terms['iprterms'][$ipr_id]['ipr_type'] = (string) $attr["type"];
      $terms['iprterms'][$ipr_id]['matches']  = array();
      $terms['iprterms'][$ipr_id]['goterms']  = array();
      
      // iterate through the elements of the interpro result
      $match_count = 0;
      foreach ($intepro->children() as $level1) {
        $element_name = $level1->getName();
        if ($element_name == 'match') {
          
          $match = array();
          
          // get the match name for this match
          $attr = $level1->attributes();
          $match['match_id']     = (string) $attr["id"];
          $match['match_name']   = (string) $attr["name"];
          $match['match_dbname'] = (string) $attr["dbname"];
          
          // get the location information for this match
          $k = 0;
          foreach ($level1->children() as $level2) {
            $element_name = $level2->getName();
            if ($element_name == 'location') {
              $attr = $level2->attributes();
              $match['locations'][$k]['match_start']    = (string) $attr["start"];
              $match['locations'][$k]['match_end']      = (string) $attr["end"];
              $match['locations'][$k]['match_score']    = (string) $attr["score"];
              $match['locations'][$k]['match_status']   = (string) $attr["status"];
              $match['locations'][$k]['match_evidence'] = (string) $attr["evidence"];
              $k++;
            }
          }
          
          // add this match to the IPR term key to which it is associated
          $terms['iprterms'][$ipr_id]['matches'][] = $match;
        }
        
        // get the GO terms for this match
        if ($element_name == 'classification') {
          $attrs = $level1->attributes();
          if ($attrs['class_type'] == 'GO') {
            $go_id = (string) $attrs['id'];
            $goterm = array();
            $goterm['category'] = (string) $level1->category;
            $goterm['name']     = (string) $level1->description;
            
            // GO terms are stored twice. Once with the IPR term to which they were found
            // and second as first-level element of the $terms array where all terms are present
            $terms['iprterms'][$ipr_id]['goterms'][$go_id] = $goterm;
            $terms['goterms'][$go_id] = $goterm;
          }
        }
      }
    }
    
    //if (count($terms['iprterms']) > 0) {
    //  dpm($terms);
    //}
    return $terms;
  }
}