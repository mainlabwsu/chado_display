<?php
namespace Drupal\chado_display\Render\Element;

use Drupal\Core\Render\Markup;
use Drupal\chado_display\Controller\AccessController;

class Link {

  function getChadoDisplayLink($base_table, $id, $display, $new_tab = FALSE, $section = NULL) {
    $access = new AccessController();
    $data = $access->getSupportedData();
    $isValid = $access->isValidRecord($base_table, $id);
    if ($isValid && ($data == NULL || count($data) == 0 || (isset($data[$base_table]) && $data[$base_table]))) {
      $link = chado_display_get_path() . '/' . $base_table . '/' . $id;
      if ($section != NULL) {
          $link .= '#' . $section;
      }
      if ($new_tab) {
          return Markup::create('<a href=' . $link . ' target=_blank>' .$display . '</a>');
      }
      else {
          return Markup::create('<a href=' . $link . '>' .$display . '</a>');
      }
    }
    else {
      return $display;
    }
  }
  
  function getURLLink($urlprefix, $id, $display, $new_tab = FALSE) {
    if (!$urlprefix) {
      return $display;
    }
    $link = $urlprefix;
    if (!preg_match('/.+\/$/', $link) && !preg_match('/.+=$/', $link) && $id) {
      $link .= '/';
    }
    $link .= $id;
    if ($new_tab) {
        return Markup::create('<a href=' . $link . ' target=_blank>' .$display . '</a>');
    }
    else {
        return Markup::create('<a href=' . $link . '>' .$display . '</a>');
    }
  }
}