<?php
namespace Drupal\chado_display\Render\Element;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Database\Chado;

class Blast {
  
  protected $chado;
  
  function __construct() {
    $this->chado = new Chado();
  }
  function render ($analysisfeatureprops, $residues) {
    $chado = $this->chado;
    $link = new Link();
    $table = new Table();
    
    $results = $this->getResults($analysisfeatureprops);
    $render_array = [];
    $num_results_per_page = 10;
    $feature_pager_id = 25;
    if(count($results) > 0){
      $i = 0;

      foreach ($results as $blast_result) {
        $hits_array    = $blast_result->hits_array;
        $analysis      = $blast_result->analysis;
        $pager_id      = $feature_pager_id + $i;
        $total_records = count($hits_array);

        $pager = \Drupal::service('pager.manager')->createPager($total_records, $num_results_per_page, $pager_id);
        $current_page_num = $pager->getCurrentPage();
        $offset = $num_results_per_page * $current_page_num;
        
        // add this blast report to the list of available reports
        $result_name = $link->getChadoDisplayLink('analysis', $analysis['analysis_id'], $analysis['name']);
        if(count($results) > 1){
          $result_name = '#' . ($i + 1). ' '. $result_name;
        }
        $list_items[] = "<a href=\"#" . $analysis['analysis_id'] . "\">$result_name</a>";
        
        // add the div box for each blast results
        $results_html = "
      <div class=\"tripal-info-box-desc tripal_analysis_blast-info-box-desc\" id=\"" . $analysis['analysis_id'] . "\">
        <strong>" . $result_name . "</strong>
        <br>Analysis Date: " .  preg_replace("/^(\d+-\d+-\d+) .*/","$1", $analysis['timeexecuted']) . " 
        <br>Total hits: " . number_format($total_records) . "
      <div id=\"features_vis-".$analysis['analysis_id']."\"></div>
      <script lang=\"javascript\">
          (function($) {
          // hack to work with multiple jquery versions
          backup_jquery = window.jQuery;
          window.jQuery = $;
          window.$ = $;
          window.ft".$analysis['analysis_id']." = new FeatureViewer('".$residues."',
          '#features_vis-".$analysis['analysis_id']."',
          {
              showAxis: true,
              showSequence: true,
              brushActive: true, //zoom
              toolbar:true, //current zoom & mouse position
              bubbleHelp:false,
              zoomMax:3 //define the maximum range of the zoom
          });
          window.jQuery = backup_jquery;
          window.$ = backup_jquery;
        })(feature_viewer_jquery);
      </script>
       ";

    // the $headers array is an array of fields to use as the colum headers.
    // additional documentation can be found here
    // https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
    $headers = array('Match Name', 'E-value', 'Identity', 'Description', '');

    // the $rows array contains an array of rows where each row is an array
    // of values for each column of the table in that row.  Additional documentation
    // can be found here:
    // https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
    $rows = array();

    // iterate through the records to display on this page. We have access to all of them
    // but we use a pager to display just a subset.
    for ($j = $offset ; $j < $offset + $num_results_per_page && $j < $total_records; $j++) {
      $hit = $hits_array[$j];
      $hit_name = $hit['hit_name'];
      $hit_name = $hit['url'] ? $link->getURLLink($hit['url'], $hit_name,$hit_name, TRUE) : $hit_name;
      
      $description = substr($hit['description'], 0, 50);
      if (strlen($hit['description']) > 50) {
        $description = $description . "... ";
      }

      $rows[] = array(
        $hit_name,
        sprintf("%.3e", $hit['best_evalue']),
        array_key_exists('percent_identity', $hit) ? $hit['percent_identity'] : '',
        // we want a tooltip if the user mouses-over a description to show the entire thing
        Markup::create('<span title="' . $hit['description'] . '">' . $description . '</span>'),
        // add the [more] link at the end of the row.  Javascript is used
        // to display this record
        Markup::create("<a href=\"javascript:void(0);\"
          id=\"hsp-link-" . $analysis['analysis_id'] . "-" .  $j ."\"
          class=\"tripal-analysis-blast-info-hsp-link\"
          \">[more]</a>"),
      );

      // add a div box for each HSP details.  This box will be invisible by default
      // and made visible when the '[more]' link is clicked
      $hsps_array = $hit['hsp']; 
      $results_html .= '
      <div class="tripal-analysis-blast-info-hsp-desc" id="hsp-desc-' .  $analysis['analysis_id'] . '-' . $j . '">
        <div class="hsp-desc-close"><a href="javascript:void(0)" onclick="this.hide()">[Close]</a></div>
        <strong>' . $analysis['name'] . '</strong>
        <br>Match: ' . $hit_name . " (" . $hit['description'] . ")<br>";
        $hsp_pos = array();
        foreach ($hsps_array AS $hsp) {
          $results_html .= '
          <br><b>HSP ' . $hsp['hsp_num'] . '</b> Score: ' . $hsp['bit_score'] . ' bits (' . $hsp['score'] . '), Expect = ' . sprintf('%.3e', floatval($hsp['evalue'])) . '<br>Identity = ' . sprintf("%d/%d (%.2f%%)", $hsp['identity'], $hsp['align_len'], $hsp['identity']/$hsp['align_len']*100) . ', Postives = ' . sprintf("%d/%d (%.2f%%)", $hsp['positive'], $hsp['align_len'], $hsp['positive']/$hsp['align_len']*100) . ', Query Frame = ' . $hsp['query_frame'] . '
          <pre class="blast_align">
Query: ' . sprintf("%4d", $hsp['query_from']) . ' ' . $hsp['qseq'] . ' ' . sprintf("%d", $hsp['query_to']) . '<br>'; 
          
          $results_html .= '            ' . $hsp['midline'] . ' ';
          $results_html .= '
Sbjct: ' . sprintf("%4d", $hsp['hit_from']) . ' ' . $hsp['hseq'].  ' ' . sprintf("%d",$hsp['hit_to']) . '
          </pre>';
          $hsp_pos[] = array('x' => intval($hsp['query_from']), 'y' => intval($hsp['query_to']), 'description' => 'Expect = '.sprintf('%.2e', floatval($hsp['evalue'])).' / Id = '.$hit['percent_identity']);
        }
        // In case the hitname is a long id, simplify it
        $clean_hit_name = preg_replace("/^([a-zA-Z0-9._-]+)\|([a-zA-Z0-9._-]+)\|([a-zA-Z0-9._-]+)\|([a-zA-Z0-9._-]+)\|$/", "$4", $hit['hit_name']);
        $results_html .= "<script lang=\"javascript\">
            (function($) {
            // hack to work with multiple jquery versions
            backup_jquery = window.jQuery;
            window.jQuery = $;
            window.$ = $;
            window.ft".$analysis['analysis_id'].".addFeature({
               data: ".json_encode($hsp_pos).",
               name: \"".$clean_hit_name."\",
               className: \"blast_match\", //can be used for styling
               color: \"#0F8292\",
               type: \"rect\"
           });
           window.jQuery = backup_jquery;
           //window.$ = backup_jquery;
         })(feature_viewer_jquery);
        </script>"; 
        $results_html .= '
      </div>';
     }

    // if there are no results then print a nice message
    if ($total_records == 0) {
      $row[] = array(
        array(
          'data' => "There are no matches.",
          'colspan' => '5'
        ),
      );
    }

    $results_html .= '</div>';
    
    $render_array[] = ['#markup' => Markup::create($results_html)];
    $render_array[] = $table->getTable($headers, $rows, $pager_id);        
    $i++;
  }

/*
  <div class="tripal_feature-data-block-desc tripal-data-block-desc">The following BLAST results are available for this feature:</div> <?php
  print '<a name="blast-results-top"></a>';
  print theme_item_list(array(
    'items' => $list_items,
    'title' => '',
    'type' => 'ul',
    'attributes' => array(),
  ));
  print $results_html;
    }*/
    }
    return $render_array;
  }
  
  function getResults($analysisfeatureprops) {
    $chado = $this->chado;
    $blast_obj_array = [];
    $blast_obj_counter = 0;
    foreach ($analysisfeatureprops AS $obj) {
      $analysis = ['analysis_id' => $obj->analysis_id, 'name' => $obj->analysis, 'timeexecuted' => $obj->timeexecuted];
      $blast_xml = $obj->value;
      $blast_obj =$this->parseBLAST($blast_xml, $analysis);
      $blast_obj->analysis = $analysis;
      if ($blast_obj->number_hits > 0) {
        $blast_obj_array [$blast_obj_counter] = $blast_obj;
        $blast_obj_counter ++;
      }      
    }
    return $blast_obj_array;
  }
  
  function parseBLAST($xml_string, $analysis) {
    $blast_object = new \stdClass();
    
    $blast_object->xml = $xml_string;
    
    // Get analysis information
    $blast_object->analysis = $analysis;
    $blast_object->title = $analysis['name'];    
    
    // Load the file.  This XML file should be an extract
    // of the original XML file with only a single iteration.
    // An iteration is essentially all the hits for a single
    // query sequence.
    $xml = new \XMLReader();
    $xml->xml($xml_string);
    $iteration = '';
    
    // iterate though the child nodes of the <Iteration> tag
    while ($xml->read()) {
      // if we've hit the closing tag (</Iteration>) then return
      if ($xml->nodeType == \XMLReader::END_ELEMENT) {
        if (strcmp($xml->name, 'Iteration') == 0) {
          // we're done
          return;
        }
      }
      
      // if this is the start of an iteration element
      if ($xml->nodeType == \XMLReader::ELEMENT) {
        $iteration_element_name =  $xml->name;
        $xml->read();
        $iteration_element_value = $xml->value;
        
        switch ($iteration_element_name) {
          case 'Iteration_query-def':
            $blast_object->xml_tag = $iteration_element_value;
            break;
          case 'Iteration_hits':
            $blast_object->xml_tag = $iteration_element_value;

            // initalize hit variables
            $hits_array = array();
            $hit_count = 0;
            $number_hits = 0;
            $hit_name = '';
            $description = '';
            
            // initalize hsp variables
            $hsp_array = array();
            $counter = 0;
            $best_evalue = 0;
            $best_score = 0;
            $best_identity = 0;
            $best_len = 0;
            
            // iterate through hits
            while ($xml->read()) {
              
              // if we're at the begining of the hit then reset variables
              if ($xml->nodeType == \XMLReader::ELEMENT) {
                if (strcmp($xml->name, 'Hit') == 0) {
                  $hsp_array = array();
                  $counter = 0;
                  $best_evalue = 0;
                  $best_score = 0;
                  $best_identity = 0;
                  $best_len = 0;
                  $hit_name = '';
                  $description = '';
                }
              }
              
              // if we're at the beginning of any other element then collect data
              if ($xml->nodeType == \XMLReader::ELEMENT) {
                $hit_element_name =  $xml->name;
                $xml->read();
                $hit_element_value = $xml->value;
                
                switch ($hit_element_name) {
                  case 'Hit_id':
                    $hit_id = $hit_element_value;
                    break;
                  case 'Hit_def':
                    $hit_def = $this->parseHitDef($hit_id, $hit_element_value);
                    $hit_name = $hit_def['name'];
                    $description = $hit_def['description'];
                    $url = $hit_def['url'];
                    break;
                  case 'Hit_hsps':
                    // iterate through the HSP children
                    while ($xml->read()) {
                      
                      // if we're at the beginning of a new element collect data
                      if ($xml->nodeType == \XMLReader::ELEMENT) {
                        $hsp_element_name = $xml->name;
                        $xml->read();
                        $hsp_info = $xml->value;
                        switch ($hsp_element_name) {
                          case 'Hsp_num':
                            $hsp_num = $hsp_info;
                            break;
                          case 'Hsp_bit-score':
                            $hsp_bit_score = $hsp_info;
                            break;
                          case 'Hsp_score':
                            $hsp_score = $hsp_info;
                            // use the first score for this set of HSPs
                            // as the best score. This get's shown as
                            // info for the overall match.
                            if (!$best_score) {
                              $best_score = $hsp_score;
                            }
                            break;
                          case 'Hsp_evalue':
                            $hsp_evalue = $hsp_info;
                            // use the first evalue for this set of HSPs
                            // as the best evalue. This get's shown as
                            // info for the overall match.
                            if (!$best_evalue) {
                              $best_evalue = $hsp_evalue;
                            }
                            break;
                          case 'Hsp_query-from':
                            $hsp_query_from = $hsp_info;
                            break;
                          case 'Hsp_query-to':
                            $hsp_query_to = $hsp_info;
                            break;
                          case 'Hsp_hit-from':
                            $hsp_hit_from = $hsp_info;
                            break;
                          case 'Hsp_hit-to':
                            $hsp_hit_to = $hsp_info;
                            break;
                          case 'Hsp_query-frame':
                            $hsp_query_frame = $hsp_info;
                            break;
                          case 'Hsp_identity':
                            $hsp_identity = $hsp_info;
                            // use the first evalue for this set of HSPs
                            // as the best evalue. This get's shown as
                            // info for the overall match.
                            if (!$best_identity) {
                              $best_identity = $hsp_identity;
                            }
                            break;
                          case 'Hsp_positive':
                            $hsp_positive = $hsp_info;
                            break;
                          case 'Hsp_align-len':
                            $hsp_align_len = $hsp_info;
                            // use the first evalue for this set of HSPs
                            // as the best evalue. This get's shown as
                            // info for the overall match.
                            if (!$best_len) {
                              $best_len = $hsp_align_len;
                            }
                            break;
                          case 'Hsp_qseq':
                            $hsp_qseq = $hsp_info;
                            break;
                          case 'Hsp_hseq':
                            $hsp_hseq = $hsp_info;
                            break;
                          case 'Hsp_midline':
                            $hsp_midline = $hsp_info;
                            break;
                        } // end switch ($hsp_element_name) { ...
                      } //  end if ($xml->nodeType == \XMLReader::ELEMENT) { ...
                      
                      // if we're at the end of the HSP then set the values
                      // that we gather through previous iterations in this while loop.
                      if ($xml->nodeType == \XMLReader::END_ELEMENT) {
                        if (strcmp($xml->name, 'Hsp') == 0) {
                          $hsp_content = array();
                          $hsp_content['hsp_num'] = $hsp_num;
                          $hsp_content['bit_score'] = $hsp_bit_score;
                          $hsp_content['score'] = $hsp_score;
                          $hsp_content['evalue'] = $hsp_evalue;
                          $hsp_content['query_frame'] = $hsp_query_frame;
                          $hsp_content['qseq'] = $hsp_qseq;
                          $hsp_content['midline'] = $hsp_midline;
                          $hsp_content['hseq'] = $hsp_hseq;
                          $hsp_content['hit_from'] = $hsp_hit_from;
                          $hsp_content['hit_to'] = $hsp_hit_to;
                          $hsp_content['identity'] = $hsp_identity;
                          $hsp_content['align_len'] = $hsp_align_len;
                          $hsp_content['positive'] = $hsp_positive;
                          $hsp_content['query_from'] = $hsp_query_from;
                          $hsp_content['query_to'] = $hsp_query_to;
                          $hsp_array[$counter] = $hsp_content;
                          $counter ++;
                        }
                        
                        // if we're at the end of the <Hit_hsps> element then
                        // break out of this while loop
                        if (strcmp($xml->name, 'Hit_hsps') == 0) {
                          break;
                        }
                      } // end if ($xml->nodeType == \XMLReader::END_ELEMENT) { ...
                    } // end while ($xml->read()) { ...
                    break;
                } // end switch ($hit_element_name) { ...
              } // end if ($xml->nodeType == \XMLReader::ELEMENT) {
              
              // if we're at the end of the <Hit> then add this hit to the
              // hits_array
              if ($xml->nodeType == \XMLReader::END_ELEMENT) {
                if (strcmp($xml->name, 'Hit') == 0) {
                  
                  $number_hits++;
                  $hits_array[$hit_count]['hit_name'] = $hit_name;
                  $hits_array[$hit_count]['url'] = $url;
                  $hits_array[$hit_count]['best_evalue'] = $best_evalue;
                  $hits_array[$hit_count]['best_score'] = $best_score;
                  
                  if (!empty($best_len)) {
                    $percent_identity = number_format($best_identity/$best_len*100, 2);
                    $hits_array[$hit_count]['percent_identity'] = $percent_identity;
                  }
                  
                  $hits_array[$hit_count]['description'] = $description;
                  
                  // if there is at least one HSP
                  if (isset($hsp_array[0]['query_frame'])) {
                    $hits_array[$hit_count]['hsp'] = $hsp_array;
                  }
                  else {
                    $hits_array[$hit_count]['hsp'] = array();
                  }
                  
                  $hit_count ++;
                } // end if (strcmp($xml->name, 'Hit') == 0) { ...
              }  // end if ($xml->nodeType == \XMLReader::END_ELEMENT) ...
            } // end while ($xml->read()) { ...
            break; // end case 'Iteration_hits': ...
        } // end switch ($tag_name) { ...
      } // end if ($xml->nodeType == \XMLReader::ELEMENT) { ...
    } // end while ($xml->read()) { ...
    
    $blast_object->number_hits = $number_hits;
    $blast_object->hits_array = $hits_array;
    return $blast_object;
  }

  function parseHitDef($hit_id, $hit_def) {
    $result = [];
    $defs = explode(' ', $hit_def, 2);
    $name = trim($defs[0], '\|');
    $description = key_exists(1, $defs) ? trim($defs[1], '\|') : $hit_def;
    $description = trim($description) ? $description : $hit_def;
    $url = '';
    
    // Genbank
    if (preg_match('/^gi\|.+\|.+/',$hit_id)) {
      $ids = explode('|', $hit_id);
      if (isset($ids[3])) {
        $url = 'https://www.ncbi.nlm.nih.gov/nuccore/';
        $name = $ids[3];
      }
      $description = $hit_def;
    }
    //UniProt
    else if (preg_match('/^sp\|.+\|.+/',$name) || preg_match('/^tr\|.+\|.+/',$name)) {
      $names = explode('|', $name);
      if (isset($names[2])) {
        $url = 'https://www.uniprot.org/uniprot/';
        $name = $names[2];
      }
    }
    // Araport 11
    else if (preg_match('/^AT\d+G\d+\.\d+/',$name)) {
      // the araport site is down
      //$url = 'https://apps.araport.org/thalemine/portal.do?externalids=';
    }
    
    $result['name'] = $name;
    $result['description'] = $description;
    $result['url'] = $url;
    return $result;
  }
}