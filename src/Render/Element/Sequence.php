<?php
namespace Drupal\chado_display\Render\Element;

use Drupal\Core\Render\Markup;

class Sequence {

  protected $num_bases;

  function __construct() {
    $this->num_bases = \Drupal::state()->get('chado_display_sequence_wrap', 80);;
  }

  function getSequence($uniquename, $residues, $name = NULL, $type = NULL, $organism = NULL, $seqlen = NULL) {
    $sequence = '<div id="residues" class="chado_display-sequence-item">';
    $sequence .= '<pre class="chado_display-sequence">';
    $sequence .= '>' . $uniquename . ' ';
    if ($name) {
      $sequence .= '|Name=' . $name;
    }
    if ($type) {
      $sequence .='|Type=' . $type ;
    }
    if ($organism) {
      $sequence .='|Organism=' . $organism;
    }
    if ($seqlen) {
      $sequence .= '|Length=' . $seqlen .'bp';
    }
    $sequence .= "<br>";
    $sequence .= wordwrap($residues, $this->num_bases, "<br>", TRUE);
    $sequence .= '</pre>';
    $sequence .= '</div>';
    return Markup::create($sequence);
  }
}