<?php
namespace Drupal\chado_display\Render\Unbased;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Database\Chado;
use Drupal\chado_display\Database\Statement;

use Drupal\chado_display\Render\Renderer;

class Allele extends Renderer {

  public $feature_id;
  public $allele;
  public $total;
  
  function __construct($organism_id, $feature_id, $allele) {
    $this->feature_id = $feature_id;
    $this->allele = $allele;
    $this->chado = new Chado();
    $this->statement = new Statement($this->chado);
    $this->num_per_page = \Drupal::state()->get('chado_display_table_rows', 10);
    
    $counter = $this->statement->feature->countAllele($organism_id, $feature_id, $allele);
    $this->total = $counter->getTotal();
    if ($this->total > 0) {
        $this->addMarker();
        $this->addStock($counter);
        $this->render();
        $this->setTitle('Allele ' . $allele); 
    }
    else {
        $this->setRenderArray(['#markup' => 'The requested page could not be found.']);
        $this->setTitle('Page not found');
    }
  }

  function addMarker () {
      $marker = $this->statement->feature->getFeature($this->feature_id);
      $this->addContent('overview', 'Overview', ['#markup' => $this->total . ' stock(s)' . ' have been detected to have allele <b>' . $this->allele . '</b> using marker <b>' . $this->link('feature', $this->feature_id, $marker->name) . '</b>'], -100);
  }

  function addStock($counter) {
     $headers = [
        'Stock',
        'Diversity Data',
        'Dataset'
     ];
     $rows = [
       ['link:stock,stock_id' => 'stock'],
       ['function:getDiversityData,stock_id,stock' => 'stock'],
       ['function:getProjects,projects' => 'projects']
     ];
     $this->addPreset('Stocks', NULL, NULL, $headers, $rows, -80, TRUE, $counter);
  }
  
  function getDiversityData($args) {
      return $this->link('stock', $args['stock_id'], $args['stock'], FALSE, 'chado_page-section-ssrgenotypedata');
  }
  
  function getProjects($args) {
      $projects = explode('::', $args['projects']);
      $display = '';
      if (count($projects) > 0) {
          for ($index = 0; $index < count($projects); $index ++) {
              $project = explode('|', $projects[$index]);
              $display .= count($project) == 2 ? $this->link('project', $project[0], $project[1]) : $project[0];
              if ($index < count($projects) - 1) {
                  $display .= '<br>';
              }
          }
          $display = Markup::create($display);
      }
      return $display;
  }

}