<?php
namespace Drupal\chado_display\Render\Unbased;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Database\Chado;
use Drupal\chado_display\Database\Statement;

use Drupal\chado_display\Render\Renderer;

class TraitDescriptor extends Renderer {

  public $cvterm_id;
  public $allele;
  public $total;
  
  function __construct($cvterm_id) {
    $this->cvterm_id = $cvterm_id;
    $this->chado = new Chado();
    $this->statement = new Statement($this->chado);
    $this->num_per_page = \Drupal::state()->get('chado_display_table_rows', 10);
    
    $descriptor = $this->statement->cvterm->getTraitDescriptor($cvterm_id);
    
    if ($descriptor) {
        $this->addOverview($descriptor);
        $this->addAlias();
        $this->addStocks($descriptor);
        $this->addProjects();
        $this->addImages();
        $this->render();
        $this->setTitle('Trait Descriptor'); 
    }
    else {
        $this->setRenderArray(['#markup' => 'The requested page could not be found.']);
        $this->setTitle('Page not found');
    }
  }
  
  function addOverview($descriptor) {
      $headers = [];
      $rows[] = ['Trait Descriptor', $descriptor->descriptor];      
      $rows[] = ['Trait', Markup::create('<a href=/display/trait/' . $descriptor->trait_id . '>' . $descriptor->trait . '</a>')];
      $rows[] = ['Definition', $descriptor->definition];
      $rows[] = ['Format', $descriptor->format];
      $rows[] = ['Trait Category', $descriptor->category];
      $table = $this->table($headers, $rows, NULL, TRUE);
      $this->addContent('overview', 'Descriptor Overview', $table, -100);
  }
  
  function addAlias() {
      $counter = $this->statement->cvterm->countAlias($this->cvterm_id);
      $headers = ['Name'];
      $rows = [
          'synonym'
      ];
      $this->addPreset('Alias', NULL, NULL, $headers, $rows, -80, TRUE, $counter);
  }
  
  function addStocks($descriptor) {
      $counter = $this->statement->cvterm->countStock($this->cvterm_id);
      $headers = ['Sample Name', 'Species', 'Stock', $descriptor->descriptor];
      $rows = [
          ['link:stock,sample_id' => 'sample_name'],
          ['link:organism,organism_id' => 'species'],
          ['link:stock,stock_id' => 'stock_uniquename'],
          'value'
      ];
      $this->addPreset('Stocks', NULL, NULL, $headers, $rows, -60, TRUE, $counter);
  }
  
  function addProjects() {
      $counter = $this->statement->cvterm->countProject($this->cvterm_id);
      $headers = ['Name', 'Type'];
      $rows = [
        ['link:project,project_id' => 'name'],
        'type'
      ];
      $this->addPreset('Datasets', NULL, NULL, $headers, $rows, -60, TRUE, $counter);
  }
  
  function addImages() {
      if ($this->chado->tableExists('cvterm_image')) {
          $counter = $this->statement->cvterm->countImage($this->cvterm_id);
          $headers = ['Name', 'Legend'];
          $rows = [
              ['link:eimage,eimage_id' => 'eimage_data'],
              'legend'
          ];
          $this->addPreset('Images', NULL, NULL, $headers, $rows, -45, TRUE, $counter);
      }      
  }
}