<?php
namespace Drupal\chado_display\Render\Unbased;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Database\Chado;
use Drupal\chado_display\Database\Statement;

use Drupal\chado_display\Render\Renderer;

class TraitPage extends Renderer {

  public $cvterm_id;
  public $allele;
  public $total;
  
  function __construct($cvterm_id) {
    $this->cvterm_id = $cvterm_id;
    $this->chado = new Chado();
    $this->statement = new Statement($this->chado);
    $this->num_per_page = \Drupal::state()->get('chado_display_table_rows', 10);
    
    $trait = $this->statement->cvterm->getTrait($cvterm_id);
    
    if ($trait) {
        $this->addOverview($trait);
        $this->addDescriptors();
        $this->addXTL();
        $this->addImages();
        $this->addProjects();
        $this->addPublications();
        $this->render();
        $this->setTitle('Trait '); 
    }
    else {
        $this->setRenderArray(['#markup' => 'The requested page could not be found.']);
        $this->setTitle('Page not found');
    }
  }
  
  function addOverview($trait) {
      $headers = [];
      $rows[] = ['Name', $trait->name];
      $rows[] = ['Trait Category', $trait->category];
      $rows[] = ['Abbreviation', $trait->abbreviation];
      $rows[] = ['Definition', $trait->definition];
      $table = $this->table($headers, $rows, NULL, TRUE);
      $this->addContent('overview', 'Trait Overview', $table, -100);
  }
  function addDescriptors () {
      $counter = $this->statement->cvterm->countDescriptor ($this->cvterm_id);
      $headers = ['Group', 'Descriptor'];
      $rows = [
         'group',
         ['urlprefix:/display/trait_descriptor,cvterm_id' => 'descriptor'],
      ];
      $this->addPreset('Descriptor', NULL, NULL, $headers, $rows, -60, TRUE, $counter);
  }
  
  function addXTL() {
      $counter = $this->statement->cvterm->countXTL($this->cvterm_id);
      $headers = ['Name', 'Map', 'Linkage Group', 'Peak', 'Start', 'Stop', 'Dataset'];
      $rows = [
          ['link:feature,feature_id' => 'name'],
          ['link:featuremap,featuremap_id' => 'map'],
          ['empty:-' => 'linkage_group'],
          ['empty:-' => 'peak'],
          ['round:2' => 'start'],
          ['round:2' => 'stop'],
          ['link:project,project_id' => 'project']
      ];
      if (\Drupal::moduleHandler()->moduleExists('tripal_map')) {
          $headers [] = 'MapViewer';
          $rows [] = ['function:getMapViewer,featuremap_id,linkage_group,feature_id' => 'name'];
      }
      $this->addPreset('QTL/MTL', NULL, NULL, $headers, $rows, -60, TRUE, $counter);
  }
  
  function getMapViewer($args) {
      $display = $args['linkage_group'] ? '<a href="/mapviewer/'. $args['featuremap_id'] . '/' . $args['linkage_group'] . '/'. $args['feature_id'] . '">View</a>' : '-';
      if (isset($args['std_lg']) && $args['std_lg']) {
        $display = '<a href="/mapviewer/'. $args['featuremap_id'] . '/' . $args['std_lg'] . '/'. $args['feature_id'] . '">View</a>';
      }
      return Markup::create($display);
  }
  
  function addImages() {
      if ($this->chado->tableExists('cvterm_image')) {
          $counter = $this->statement->cvterm->countImage($this->cvterm_id);
          $headers = ['Name', 'Legend'];
          $rows = [
              ['link:eimage,eimage_id' => 'eimage_data'],
              'legend'
          ];
          $this->addPreset('Images', NULL, NULL, $headers, $rows, -45, TRUE, $counter);
      }      
  }
  
  function addProjects() {
    $counter = $this->statement->cvterm->countProject($this->cvterm_id);
    $headers = ['Name', 'Type'];
    $rows = [
      ['link:project,project_id' => 'name'],
      'type'
    ];
    $this->addPreset('Datasets', NULL, NULL, $headers, $rows, -45, TRUE, $counter);
  }
  
  function addPublications() {
    $counter = $this->statement->cvterm->countPub($this->cvterm_id);
    $headers = ['Title', 'Series', 'Year', 'Type'];
    $rows = [
      ['link:pub,pub_id' => 'title'],
      'series_name',
      'pyear',
      'type'
    ];
    $this->addPreset('Publications', NULL, NULL, $headers, $rows, -45, TRUE, $counter);
  }
}