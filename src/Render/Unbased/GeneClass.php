<?php 
namespace Drupal\chado_display\Render\Unbased;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Drupal\Core\Render\Markup;

class GeneClass {
  
  protected $render_array;
  
  function __construct() {
    $lib = [];
    $tstyle = \Drupal::state()->get('chado_display_table_style', 'none');
    if ($tstyle == 'grey') {
      $lib [] = 'chado_display/chado_display_grey_table';
    } else if ($tstyle == 'blue') {
      $lib [] = 'chado_display/chado_display_blue_table';
    } else if ($tstyle == 'green') {
      $lib [] = 'chado_display/chado_display_green_table';
    } else if ($tstyle == 'light_green') {
      $lib [] = 'chado_display/chado_display_light_green_table';
    } else if ($tstyle == 'red') {
      $lib [] = 'chado_display/chado_display_red_table';
    }
    $this->render_array = [
      '#attached' => [
        'library' => $lib
      ]
    ];
  }
  
  public function getTitle($name) {
    return Markup::create('Genes with symbol <strong>' . $name . '</strong>');
  }
  
  public function getListTitle() {
    return 'Gene Class';
  }
  
  public function getList($letter) {
    $db = \Drupal::database();
    $qcount =
    "SELECT count (*)
      FROM
        (SELECT uniquename FROM chado.feature
        WHERE uniquename like :uniquename
        AND type_id =
        (SELECT cvterm_id FROM chado.cvterm
        WHERE name = 'gene_class'
        AND cv_id =
        (SELECT cv_id FROM chado.cv
        WHERE name = 'sequence'))
        GROUP BY name, uniquename) CNT";
    $total = $db->query($qcount, array('uniquename' => "$letter%%"))->fetchField();
    $output = "<strong>List of All Gene Symbols</strong>
<div style=\"margin:10px 0px\">The list of gene symbols have been put together from data in the NCBI nr database and user-submitted data.
      Please <a href=\"/contact\"><strong>contact us</strong></a> if you would like to submit new gene data or edit the existing gene data. The
      Gene data submission form is available from the <a href=\"/data/submission\"><strong>data submission page</strong></a>.</div>
<div style=\"text-align:center\">";
    $output .= $letter == '' ? "All&nbsp;&nbsp" : "<a href=\"/gene_class_listing\">All&nbsp;&nbsp</a>";
    foreach (range('A', 'Z') AS $page) {
      $output .= $letter == $page ? " $page" : " <a href=\"/gene_class_listing/$page\">$page</a>";
    }
    $output .= "</div>";
    $output .= "<p>Total <strong>" . $total . "</strong></p>";
    
    $sql =
    "SELECT string_agg(distinct C.name, '. ') AS contact, F.name, F.uniquename, string_agg(distinct to_char(timelastmodified, 'YYYY-MM-DD'), '. ') AS timelastmodified FROM chado.feature F
      LEFT JOIN chado.feature_contact FC ON FC.feature_Id = F.feature_id
      LEFT JOIN chado.contact C ON FC.contact_id = C.contact_id
      WHERE uniquename like :uniquename
      AND F.type_id =
      (SELECT cvterm_id FROM chado.cvterm
      WHERE name = 'gene_class'
      AND cv_id =
      (SELECT cv_id FROM chado.cv
      WHERE name = 'sequence'))
      GROUP BY F.name, F.uniquename
      ORDER BY F.uniquename";
    
    $result = $db->query($sql,  array('uniquename' => "$letter%%"));
    $rows = array();
    $row_count = 0;
    while ($row = $result->fetchObject()) {      
      $rows[] = array('data' => array(Markup::create("<a href=\"/gene_class/$row->uniquename\">" . $row->uniquename . "</a>"), $row->name, $row->contact, $row->timelastmodified));
      $row_count ++;
    }
    $header = array('Symbol', 'Symbol Full Name', 'Contact', 'Submitted');
    $table = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => ['class' => ['chado_display_style_table']]
    ];
    $this->render_array [] = [
      '#markup' => Markup:: create($output),      
    ];    
    if ($total > 0) {
      $this->render_array [] = $table;
    }
    return $this->render_array;
  }
  
  public function getGeneClass($symbol) {
    $db = \Drupal::database();
    $output = '';    
    $sql =
    "SELECT
      GENE.feature_id,
      GENE.uniquename,
      (SELECT genus || ' ' || species FROM chado.organism WHERE organism_id = F.organism_id) AS organism,
      string_agg(C.name, '. ') AS contact
      FROM chado.feature F
      INNER JOIN chado.feature_relationship FR ON F.feature_id = subject_id
      INNER JOIN chado.feature GENE ON GENE.feature_id = object_id
      LEFT JOIN chado.feature_contact FC ON F.feature_id = FC.feature_id
      LEFT JOIN chado.contact C ON C.contact_id = FC.contact_id
        WHERE F.uniquename = :symbol
        AND F.type_id =
        (SELECT cvterm_id FROM chado.cvterm
        WHERE name = 'gene_class'
        AND cv_id =
        (SELECT cv_id FROM chado.cv
        WHERE name = 'sequence'))
         GROUP BY GENE.feature_id,GENE.uniquename, organism
         ORDER BY organism, GENE.uniquename";
    
    $result = $db->query($sql, array(':symbol' => $symbol));
    $rows = array();
    $row_count = 0;
    while ($row = $result->fetchObject()) {
      $link = chado_display_get_path() . '/feature/' . $row->feature_id;
      $rows[] = $link ? 
        array('data' => array(Markup::create("<a href=\"$link\">" . $row->uniquename . "</a>"), $row->organism, $row->contact)) : 
        array('data' => array($row->uniquename, $row->organism, $row->contact, ));
      $row_count ++;
    }
    $header = array('Name', 'Organism', 'Contact');
    $table = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => ['class' => ['chado_display_style_table']]
    ];
    if ($row_count) {
      $this->render_array [] = $table;
    }
    else {
      $this->render_array [] = ['#markup' => 'No data found.'];
    }
    return $this->render_array;
  }
}