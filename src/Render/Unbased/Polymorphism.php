<?php
namespace Drupal\chado_display\Render\Unbased;

use Drupal\Core\Render\Markup;

use Drupal\chado_display\Database\Chado;
use Drupal\chado_display\Database\Statement;

use Drupal\chado_display\Render\Renderer;

class Polymorphism extends Renderer {

  public $feature_id;
  public $marker;
  public $total;
  
  function __construct($id) {
    $this->chado = new Chado();
    $this->statement = new Statement($this->chado);
    $this->num_per_page = \Drupal::state()->get('chado_display_table_rows', 10);
    $this->feature_id = $id;
    $this->marker = $this->statement->feature->getFeature($this->feature_id);
    $counter = $this->statement->feature->countPolymorphism($id);
    $this->total = $counter->getTotal();
    if ($this->total > 0) {
        $this->addMarker();
        $this->addPolymorphism($counter);
        $this->render();
        $this->setTitle('Polymorphism P_' . $this->marker->name);
    }
    else {
        $this->setTitle('Page not found');
        $this->setRenderArray(['#markup' => 'The requested page could not be found.']);
    }
  }

  function addMarker () {
      
      $this->addContent('overview', 'Overview', ['#markup' => 'Marker ' . $this->link('feature', $this->feature_id, $this->marker->name) . ' [<i>' . $this->marker->organism . '</i>] includes ' . $this->total . ' polymorphisms'], -100);
  }

  function addPolymorphism($counter) {
      $headers = [
          'Marker Allele',
          'Allele',
          'Stock',
          'Dataset'
      ];
      $rows = [
          ['function:getMarkerAllele,organism_id,feature_id,marker_allele,allele' => 'marker_allele'],
          'allele',
          ['function:getStock,stocks,num_stocks,organism_id,feature_id,allele' => 'stocks'],
          ['function:getProjects,projects' => 'projects']
      ];
      $this->addPreset('Polymorphism', NULL, NULL, $headers, $rows, -80, TRUE, $counter);
  }

  function getMarkerAllele($args) {
      return Markup::create('<a href=/display/allele/' . $args['organism_id'] . '/' . $args['feature_id'] . '/' . $args['allele'] . '>' . $args['marker_allele'] . '</a>');
  }
  
  function getStock($args) {
      $display = '';
      $stocks = explode('::', $args['stocks']);
      if (count ($stocks) > 0) {
        $stock = explode('|', $stocks[0]);
        if (count($stock) == 2) {
            $show_all = ' [<a href=/display/allele/' . $args['organism_id'] . '/' . $args['feature_id'] . '/' . $args['allele'] . '>' . 'Show all' . $args['num_stocks'] . '</a>]';
            $display = Markup::create($this->link('stock', $stock[0], $stock[1]) .$show_all);
        }
      }
      return $display;
  }
  
  function getProjects($args) {
      $projects = explode('::', $args['projects']);
      $display = '';
      if (count($projects) > 0) {
          for ($index = 0; $index < count($projects); $index ++) {
              $project = explode('|', $projects[$index]);
              $display .= count($project) == 2 ? $this->link('project', $project[0], $project[1]) : $project[0];
              if ($index < count($projects) - 1) {
                  $display .= '<br>';
              }
          }
          $display = Markup::create($display);
      }
      return $display;
  }
}