<?php 

namespace Drupal\chado_display\Render;

class Cache {
  
  static function save ($base_table, $id, $renderer) {
    Cache::delete ($base_table, $id);
    \Drupal::database()
    ->insert('cache_chado_display')
    ->fields([
      'base_table' => $base_table,
      'record_id' => $id,
      'renderer' => serialize($renderer)
    ])
    ->execute();
  }
  
  static function load ($base_table, $id) {
    $cache = 
    \Drupal::database()
    ->select('cache_chado_display', 'ccd')
    ->fields('ccd', ['renderer'])
    ->condition('base_table', $base_table)
    ->condition('record_id', $id)
    ->execute()
    ->fetchField();
    if ($cache) {
      $renderer = (unserialize($cache));
      for ($i = 0; $i <= $renderer->max_pager_id; $i ++) {
        if(isset($renderer->pagers[$i])) {
          $p = $renderer->pagers[$i];
          \Drupal::service('pager.manager')->createPager($p->getTotalItems(), $p->getLimit(), $i);
        }
      }
      return $renderer;
    }
    else {
      return NULL;
    }    
  }
  
  static function delete ($base_table, $id) {
    \Drupal::database()
    ->delete('cache_chado_display')
    ->condition('base_table', $base_table)
    ->condition('record_id', $id)
    ->execute();
  }
  
  static function empty() {
    \Drupal::database()
    ->delete('cache_chado_display')
    ->execute();
  }
}