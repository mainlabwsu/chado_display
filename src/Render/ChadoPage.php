<?php

namespace Drupal\chado_display\Render;

use Symfony\Component\HttpFoundation\JsonResponse;

use Drupal\chado_display\Database\Chado;

use Drupal\chado_display\Render\Based\Analysis;
use Drupal\chado_display\Render\Based\Contact;
use Drupal\chado_display\Render\Based\Image;
use Drupal\chado_display\Render\Based\Feature;
use Drupal\chado_display\Render\Based\Gene;
use Drupal\chado_display\Render\Based\GeneticMarker;
use Drupal\chado_display\Render\Based\HaplotypeBlock;
use Drupal\chado_display\Render\Based\Bin;
use Drupal\chado_display\Render\Based\GWAS;
use Drupal\chado_display\Render\Based\Library;
use Drupal\chado_display\Render\Based\Map;
use Drupal\chado_display\Render\Based\mRNA;
use Drupal\chado_display\Render\Based\MTL;
use Drupal\chado_display\Render\Based\NdGeolocation;
use Drupal\chado_display\Render\Based\Organism;
use Drupal\chado_display\Render\Based\Project;
use Drupal\chado_display\Render\Based\Pub;
use Drupal\chado_display\Render\Based\QTL;
use Drupal\chado_display\Render\Based\SNPChip;
use Drupal\chado_display\Render\Based\Stock;
use Drupal\chado_display\Render\Based\StockCollection;

class ChadoPage {

  protected $renderer;

  public function getPageTitleById($base_table, $id) {
    // Link for access control
    $perm_admin_content = \Drupal::currentUser()->hasPermission('admin chado display content');
    $is_private = 
    \Drupal::database()
      ->select('chado_display_private_data', 'cdpd')
      ->fields('cdpd', ['record_id'])
      ->condition('base_table', $base_table)
      ->condition('record_id', $id)
      ->execute()
      ->fetchField();
    $admin_access = '';
    if ($perm_admin_content) {
        $admin_access = '';
        if ($is_private) {
            $admin_access = \Drupal\Core\Link::createFromRoute('+public', 'chado_display.admin_access', ['action' => 'public', 'base_table' => $base_table, 'id' => $id])->toString();
        }
        else {
            $admin_access = \Drupal\Core\Link::createFromRoute('+private', 'chado_display.admin_access', ['action' => 'private', 'base_table' => $base_table, 'id' => $id])->toString();
        }
        $admin_access =  '<div id="chado_display_page-admin_access" class="chado_display-page_control">' . $admin_access . '</div>';
    }
    $private_remark = $is_private ? '<div id="chado_display_page-private_remark"><i>Private</i></div>' : '';
    
    // Link to Json
    $extra = \Drupal::state()->get('chado_display_extra_render_info');
    $json = $extra && $extra['json'] === 0 ? '' : '<div id="chado_display_page-json" class="chado_display-page_control">' . \Drupal\Core\Link::createFromRoute('json', 'chado_display.json', ['base_table' => $base_table, 'id' => $id])->toString() . '</div>';
    
    // Link for Edit
    $perm_edit = \Drupal::currentUser()->hasPermission('edit chado display content');
    $edit = $perm_edit ?  '<div id="chado_display_page-edit" class="chado_display-page_control">' . \Drupal\Core\Link::createFromRoute('edit', 'chado_display.edit', ['base_table' => $base_table, 'id' => $id])->toString() . '</div>' : '';
    
    // Link for Delete
    $perm_del = \Drupal::currentUser()->hasPermission('delete chado display content');
    $del = $perm_del ? '<div id="chado_display_page-delete" class="chado_display-page_control">' . \Drupal\Core\Link::createFromRoute('delete', 'chado_display.delete', ['base_table' => $base_table, 'id' => $id])->toString() . '</div>' : '';
    
    $title = $this->renderer? \Drupal\Core\Render\Markup::create(
        $this->renderer->getTitle() .
        '<div id="chado_display-title-page_controls">' .
        '<div id="chado_display_page-access_control">'. $admin_access . $private_remark . '</div>' .
        $del .
        $edit .
        $json . '</div>'
    ) : NULL;
    return $title;
  }

  public function getPageById($base_table, $id, $format = NULL) {
    // Get page from cache
    $cache = Cache::load($base_table, $id);
    $mpid = \Drupal::service('pager.manager')->getMaxPagerElementId();
    // If pager(s) exist, paged contents are not cached,except page1
    $allOnP1 = TRUE;
    for ($i = 0; $i <= $mpid; $i ++) {
      $pg = \Drupal::service('pager.manager')->getPager($i);
      if ($pg && $pg->getCurrentPage() != 0) {
        $allOnP1 = FALSE;
        break;
      }
    }
    if($cache != NULL && $format != 'json' && $allOnP1) {
      $this->renderer = $cache;
      return $cache->getRenderArray();
    }
    $renderer = $this->dispatch($base_table, $id);
    $this->renderer = $renderer;
    $category = NULL;
    $toc = NULL;
    $alt_header = NULL;
    $content = NULL;
    if ($renderer instanceof Renderer) {
      if ($format == 'json') {
        return new JsonResponse(['data' => $renderer->getJson($base_table, $id)]);
      }
      else {
        $renderer->render();
        $opt_cache = \Drupal::state()->get('chado_display_page_cache', FALSE);
        if ($opt_cache && $allOnP1) {
          $pagers = [];
          $max_pid = \Drupal::service('pager.manager')->getMaxPagerElementId();
          for ($i = 0; $i <= $max_pid; $i ++) {
            $pager = \Drupal::service('pager.manager')->getPager($i);
            if ($pager instanceof \Drupal\Core\Pager\Pager) {
              $pagers [$i] = $pager;
            }
          }
          $renderer->max_pager_id = $max_pid;
          $renderer->pagers = $pagers;
          Cache::save($base_table, $id, $renderer);
        }
        return $renderer->getRenderArray();
      }
    }
    else {
      return [
        '#markup' => 'The requested ' . $base_table . ' page could not be found.'
      ];
    }
  }
  
  protected function rearrange($renderer, $renderer_type) {
    if (\Drupal::state()->get('chado_display_' . $renderer_type . '_sidebar_order')) {
      $sidebar_order = array_map('trim', explode(',', \Drupal::state()->get('chado_display_' . $renderer_type . '_sidebar_order')));
      $renderer->orderByHeaders($sidebar_order);
    }
    if (\Drupal::state()->get('chado_display_' . $renderer_type . '_overview_order')) {
      $overview_order = array_map('trim', explode(',', \Drupal::state()->get('chado_display_' . $renderer_type . '_overview_order')));
      $renderer->orderOverviewBy($overview_order);
    }
    if (\Drupal::state()->get('chado_display_' . $renderer_type . '_overview_hidden')) {
      $overview_hidden = array_map('trim', explode(',', \Drupal::state()->get('chado_display_' . $renderer_type . '_overview_hidden')));
      $renderer->hideOverviewFields($overview_hidden);
    }
    return $renderer;
  }

  public function dispatch($base_table, $id) {
    $chado = new Chado();
    $renderer = NULL;
    try {
      switch ($base_table) {
        case 'analysis':
            $renderer = new Analysis($id);
            $renderer = $this->rearrange($renderer, 'analysis');
            \Drupal::moduleHandler()->invokeAll('chado_display_analysis_alter', array($renderer));
            break;
        case 'contact':
            $renderer = new Contact($id);
            $renderer = $this->rearrange($renderer, 'contact');
            \Drupal::moduleHandler()->invokeAll('chado_display_contact_alter', array($renderer));
            break;
        case 'eimage':
          $renderer = new Image($id);
          $renderer = $this->rearrange($renderer, 'image');
          \Drupal::moduleHandler()->invokeAll('chado_display_eimage_alter', array($renderer));
          break;
        case 'feature':
          $obj = $chado->getFirstObject('feature F', array('V.name AS type'), array('inner' => array('cvterm V' => 'V.cvterm_id = F.type_id')), array('feature_id' => $id));
          $type = $obj->type;
          switch ($type) {
              case 'genetic_marker':
                  $renderer = new GeneticMarker($id);
                  $renderer = $this->rearrange($renderer, 'feature');
                  \Drupal::moduleHandler()->invokeAll('chado_display_feature_alter', array($renderer));
                  \Drupal::moduleHandler()->invokeAll('chado_display_genetic_marker_alter', array($renderer));
                  break;
              case 'mRNA':
                  $renderer = new mRNA($id);
                  $renderer = $this->rearrange($renderer, 'feature');
                  \Drupal::moduleHandler()->invokeAll('chado_display_feature_alter', array($renderer));
                  \Drupal::moduleHandler()->invokeAll('chado_display_mRNA_alter', array($renderer));
                  break;
              case 'gene':
                  $renderer = new Gene($id);
                  $renderer = $this->rearrange($renderer, 'feature');
                  \Drupal::moduleHandler()->invokeAll('chado_display_feature_alter', array($renderer));
                  \Drupal::moduleHandler()->invokeAll('chado_display_gene_alter', array($renderer));
                  break;
              case 'QTL':
                  $renderer = new QTL($id);
                  $renderer = $this->rearrange($renderer, 'feature');
                  \Drupal::moduleHandler()->invokeAll('chado_display_feature_alter', array($renderer));
                  \Drupal::moduleHandler()->invokeAll('chado_display_qtl_alter', array($renderer));
                  break;
              case 'heritable_phenotypic_marker':
                  $renderer = new MTL($id);
                  $renderer = $this->rearrange($renderer, 'feature');
                  \Drupal::moduleHandler()->invokeAll('chado_display_feature_alter', array($renderer));
                  \Drupal::moduleHandler()->invokeAll('chado_display_mtl_alter', array($renderer));
                  break;
              case 'haplotype_block':
                  $renderer = new HaplotypeBlock($id);
                  $renderer = $this->rearrange($renderer, 'feature');
                  \Drupal::moduleHandler()->invokeAll('chado_display_feature_alter', array($renderer));
                  \Drupal::moduleHandler()->invokeAll('chado_display_haplotype_block_alter', array($renderer));
                  break;
              case 'bin':
                $renderer = new Bin($id);
                $renderer = $this->rearrange($renderer, 'feature');
                \Drupal::moduleHandler()->invokeAll('chado_display_feature_alter', array($renderer));
                \Drupal::moduleHandler()->invokeAll('chado_display_bin_alter', array($renderer));
                break;
              case 'GWAS':
                $renderer = new GWAS($id);
                $renderer = $this->rearrange($renderer, 'feature');
                \Drupal::moduleHandler()->invokeAll('chado_display_feature_alter', array($renderer));
                \Drupal::moduleHandler()->invokeAll('chado_display_gwas_alter', array($renderer));
                break;
            default:
              $renderer = new Feature($id);
              $renderer = $this->rearrange($renderer, 'feature');
              \Drupal::moduleHandler()->invokeAll('chado_display_feature_alter', array($renderer));
          }
          break;        
        case 'featuremap':
            $renderer = new Map($id);
            $renderer = $this->rearrange($renderer, 'map');
            \Drupal::moduleHandler()->invokeAll('chado_display_featuremap_alter', array($renderer));          
            break;        
        case 'library':
            $obj = $chado->getFirstObject('library F', array('V.name AS type'), array('inner' => array('cvterm V' => 'V.cvterm_id = F.type_id')), array('library_id' => $id));
            $type = $obj->type;
            switch ($type) {
                case 'SNP_chip':
                    $renderer = new SNPChip($id);
                    $renderer = $this->rearrange($renderer, 'library');
                    \Drupal::moduleHandler()->invokeAll('chado_display_library_alter', array($renderer));
                    \Drupal::moduleHandler()->invokeAll('chado_display_snp_chip_alter', array($renderer));
                    break;
                default:
                    $renderer = new Library($id);
                    $renderer = $this->rearrange($renderer, 'library');
                    \Drupal::moduleHandler()->invokeAll('chado_display_library_alter', array($renderer));
            }
            break;
        case 'nd_geolocation':
            $renderer = new NdGeolocation($id);
            $renderer = $this->rearrange($renderer, 'nd_geolocation');
            \Drupal::moduleHandler()->invokeAll('chado_display_nd_geolocation_alter', array($renderer));
            break;
        case 'organism':
            $renderer = new Organism($id);
            $renderer = $this->rearrange($renderer, 'organism');
            \Drupal::moduleHandler()->invokeAll('chado_display_organism_alter', array($renderer));
            break;
        case 'project':
            $renderer = new Project($id);
            $renderer = $this->rearrange($renderer, 'project');
            \Drupal::moduleHandler()->invokeAll('chado_display_project_alter', array($renderer));
            break;
        case 'pub':
            $renderer = new Pub($id);
            $renderer = $this->rearrange($renderer, 'pub');
            \Drupal::moduleHandler()->invokeAll('chado_display_pub_alter', array($renderer));
            break;
        case 'stock':
            $renderer = new Stock($id);
            $renderer = $this->rearrange($renderer, 'stock');
            \Drupal::moduleHandler()->invokeAll('chado_display_stock_alter', array($renderer));
            break;
        case 'stockcollection':
            $renderer = new StockCollection($id);
            $renderer = $this->rearrange($renderer, 'stockcollection');
            \Drupal::moduleHandler()->invokeAll('chado_display_stockcollection_alter', array($renderer));
            break;
        default:
          $renderer = new Renderer($id, $base_table);
          \Drupal::moduleHandler()->invokeAll('chado_display_' . $base_table . '_alter', array($renderer));
          break;
      }
    } catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
    } catch (\Error $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
    return $renderer;
  }

}
