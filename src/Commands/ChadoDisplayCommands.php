<?php

namespace Drupal\chado_display\Commands;

use Drush\Commands\DrushCommands;

use Drupal\chado_display\Database\Chado;

/**
 * Drush commands
 */
class ChadoDisplayCommands extends DrushCommands {

   /**
   * Migrate Tripal Organism Images
   *
   * @command chado_display:image-migrate
   * @aliases cdimgmigrate
   * @usage cdimgmigrate --host=localhost --port=5432 --dbname=sourcedb --user=myuser --password=mypass --base_url=www.example.com
   *   Migrate Tripal Organism Images from specified source database
   */
  public function imageMigrate($options = ['host' => 'localhost', 'port' => '5432', 'dbname' => NULL, 'user' => NULL, 'password' => NULL, 'base_url' => NULL]) {
    $host = $options ['host'];
    $port = $options ['port'];
    $dbname = $options ['dbname'];
    $user = $options['user'];
    $password = $options['password'];
    $base_url = $options['base_url'];
    
    if ($user && $dbname && $password && $base_url) {
      $dbconn = pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");
      if ($dbconn) {
        $sql = "
          SELECT FM.fid, uri, filename, bundle, FU.id, data_table FROM file_managed FM 
	         INNER JOIN file_usage FU ON FM.fid = FU.fid 
	         INNER JOIN tripal_entity TE ON TE.id = FU.id
	         INNER JOIN tripal_bundle TB ON TB.name = bundle
	         INNER JOIN chado_bundle CB ON CB.bundle_id = TB.id
	         WHERE FU.type = 'TripalEntity'";
        $results = pg_query($dbconn, $sql);
        try {
          $counter_i = 0;
          $counter_s = 0;
          while ($obj = pg_fetch_object($results)) {
            $uri = str_replace('public://', 'https://' . $base_url . '/sites/default/files/', $obj->uri);
            $image = file_get_contents($uri);
            $data = base64_encode($image);
            $data_table = 'chado_' . $obj->bundle;
            $record_id = pg_fetch_object(pg_query('SELECT record_id FROM ' . $data_table . ' WHERE entity_id = ' . $obj->id))->record_id;
            $path = chado_display_get_path() . '/' . $obj->data_table . '/' . $record_id;
            
            // Insert image data only if it does not already exists
            $c = new Chado();
            $exists = $c->getFirstField('eimage', 'eimage_id', ['image_uri' => $path], []);
            if (!$exists) {
              $c->getFirstField('eimage', 'eimage_id', ['image_uri' => $path, 'eimage_data' => $data, 'eimage_type' => $obj->filename], [], TRUE);
              $counter_i ++;
            }          
            else {
              $counter_s ++;
            }
          }
          print "$counter_i records inserted. $counter_s skipped.\n";
        } catch (Exception  $e) {
          //print $e;
        }
        }
      else {
        print "[Error] Connection failed. Please check connection info.\n";
      }
    }
    else {
      print "[Error] Please provide dbname, user, password, and the base_url of the source database.\n";
    }    
  }
  
  /**
   * Migrate Tripal URL Aliases to redirect to Chado Display path
   *
   * @command chado_display:url-migrate
   * @aliases cdturlmigrate
   * @usage cdturlmigrate --host=localhost --port=5432 --dbname=sourcedb --user=myuser --password=mypass
   *   Migrate Tripal URL Aliases from specified source database and redirect urls to Chado Display path (default for Organism/Publication/Projecvt/Analysis Tripal Content Types)
   */
  public function tripalURLMigrate($options = ['host' => 'localhost', 'port' => '5432', 'dbname' => NULL, 'user' => NULL, 'password' => NULL, 'type' => NULL]) {
    $host = $options ['host'];
    $port = $options ['port'];
    $dbname = $options ['dbname'];
    $user = $options['user'];
    $password = $options['password'];
    $type = $options['type'];
    
    if ($user && $dbname && $password) {
      $dbconn = pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");
      if ($dbconn) {
        $types = $type == NULL ? ['Organism', 'Publication', 'Project', 'Analysis'] : [$type];
        foreach ($types AS $t) {
          $this->migrateTripalURLByType($dbconn, $t);
        }
      }
      else {
        print "[Error] Connection failed. Please check connection info.\n";
      }
    }
    else {
      print "[Error] Please provide dbname, user, and password of the source database.\n";
    }
  }
  
  private function migrateTripalURLByType($dbconn, $tripal_content_type) {
    print 'Migrating ' . $tripal_content_type . "...\n";
    $tripal_bundle = pg_fetch_object(pg_query("SELECT (SELECT data_table FROM chado_bundle WHERE bundle_id = T.id), name FROM tripal_bundle T WHERE label = '$tripal_content_type'"));
    if (!$tripal_bundle) {
      print "$tripal_content_type not available.\n";
      return;
    }
    $bundle_table = 'chado_' . $tripal_bundle->name;
    $base_table = $tripal_bundle->data_table;
    $sql = "
      SELECT source, alias FROM url_alias WHERE source IN (SELECT 'bio_data/' || id FROM tripal_entity WHERE bundle = (SELECT name FROM tripal_bundle WHERE label = '$tripal_content_type'))
          ";
    $results = pg_query($dbconn, $sql);
    try {
      $counter_i = 0;
      $counter_s = 0;
      while ($obj = pg_fetch_object($results)) {        
        $entity_id = str_replace('bio_data/', '', $obj->source);
        $alias = $obj->alias;
        $record = pg_fetch_object(pg_query("SELECT record_id FROM $bundle_table B WHERE entity_id = $entity_id"));
        if ($record && $base_table) {
          $path = '/display/record/' .$base_table . '/' . $record->record_id;
          $new_alias = '/' .  $alias;          // Remove redundant aliases
          \Drupal::database()->query('DELETE FROM path_alias WHERE alias = :alias', [':alias' => $new_alias]);
          \Drupal::database()->query('DELETE FROM path_alias_revision WHERE alias = :alias', [':alias' => $new_alias]);          
          $path_alias = \Drupal\path_alias\Entity\PathAlias::create([
            'path' => $path,
            'alias' => $new_alias,
          ]);
          $path_alias->save();
          $counter_i ++;
        }
        else {
          $counter_s ++;
        }
      }
      print "$counter_i records inserted. $counter_s skipped.\n";
    } catch (Exception  $e) {
      //print $e;
    }
  }
  
  /**
   * Cache schema
   *
   * @command chado_display:cache_schema
   * @aliases cdcsm
   * @usage cdcsm
   *   Migrate Tripal Organism Images from source database
   */
  public function cacheSchema($options = []) {
      print "Creating chado schema cache...\n";
      
      // Cache 1: PK
      $cache_table = 'chado_display_cache_chado_pk';
      $sql = "SELECT C.column_name, TC.table_name
          INTO $cache_table
          FROM information_schema.table_constraints TC
          JOIN information_schema.constraint_column_usage AS CCU USING (constraint_schema, constraint_name)
          JOIN information_schema.columns AS C ON C.table_schema = TC.constraint_schema
          AND TC.table_name = C.table_name AND CCU.column_name = C.column_name
          WHERE CCU.table_schema = C.table_schema AND constraint_type = 'PRIMARY KEY' AND C.table_schema = 'chado'";
      $this->cacheSchemaTable($cache_table, $sql);
    
      // Cache 2: UK
      $cache_table = 'chado_display_cache_chado_uk';
      $sql = "SELECT C.column_name, TC.table_name
          INTO $cache_table
          FROM information_schema.table_constraints TC
          JOIN information_schema.constraint_column_usage AS CCU USING (constraint_schema, constraint_name)
          JOIN information_schema.columns AS C ON C.table_schema = TC.constraint_schema
          AND TC.table_name = C.table_name AND CCU.column_name = C.column_name
          WHERE CCU.table_schema = C.table_schema AND constraint_type = 'UNIQUE' AND C.table_schema = 'chado'";
      $this->cacheSchemaTable($cache_table, $sql);      

      // Cache 3: Typed columns
      $cache_table = 'chado_display_cache_chado_typed_cols';
      $sql = "SELECT C.column_name, C.data_type, C.character_maximum_length, T.table_name
          INTO $cache_table
          FROM INFORMATION_SCHEMA.TABLES T
          INNER JOIN INFORMATION_SCHEMA.COLUMNS C ON C.table_name = T.table_name AND C.table_schema = T.table_schema
          WHERE T.table_type = 'BASE TABLE' AND T.table_schema = 'chado'";
      $this->cacheSchemaTable($cache_table, $sql);

      // Cache 4: NOT NULL columns
      $cache_table = 'chado_display_cache_chado_null_cols';
      $sql = "SELECT C.column_name, T.table_name
          INTO $cache_table
          FROM INFORMATION_SCHEMA.TABLES T
          INNER JOIN INFORMATION_SCHEMA.COLUMNS C ON C.table_name = T.table_name AND C.table_schema = T.table_schema
          WHERE T.table_type = 'BASE TABLE' AND T.table_schema = 'chado' AND C.is_nullable = 'NO'";
      $this->cacheSchemaTable($cache_table, $sql);      

      // Cache 5: Column Default Value
      $cache_table = 'chado_display_cache_chado_col_default';
      $sql = "SELECT  C.column_name, column_default, T.table_name
          INTO $cache_table
          FROM INFORMATION_SCHEMA.TABLES T
          INNER JOIN INFORMATION_SCHEMA.COLUMNS C ON C.table_name = T.table_name AND C.table_schema = T.table_schema
          WHERE T.table_type = 'BASE TABLE' AND T.table_schema = 'chado' AND C.column_default IS NOT NULL";
      $this->cacheSchemaTable($cache_table, $sql);

      // Cache 6: FKey columns
      $cache_table = 'chado_display_cache_chado_fk_cols';
      $sql = "SELECT A.constraint_name, C.column_name AS key, A.table_name AS foreign_table, A.column_name AS foreign_key, C.table_name
          INTO $cache_table
          FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE A
  				INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS B ON A.constraint_name = B.constraint_name AND A.table_schema = B.table_schema
  				INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE C ON A.constraint_name = C.constraint_name AND A.table_schema = B.table_schema
  				WHERE constraint_type = 'FOREIGN KEY'
  				AND A.table_schema = 'chado'";
      $this->cacheSchemaTable($cache_table, $sql);
  }
  
  private function cacheSchemaTable($cache_table, $sql) {
    print " - Caching $cache_table...\n";
    $db = \Drupal::database();
    $exists = $db->schema()->tableExists($cache_table);
    
    if ($exists) {
      $ans = readline(" $cache_table exists. Do you want to drop and recreate it (yes/no)? \n");
      if ($ans == 'yes') {
        $db->query("DROP TABLE IF EXISTS $cache_table");
        $db->query($sql);
      }
      else {
        print "   skipped.\n";
      }
    }
    else {
      $db->query($sql);
    }
  }

}
