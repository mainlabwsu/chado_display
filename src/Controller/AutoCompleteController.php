<?php
namespace Drupal\chado_display\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Component\Utility\Xss;

use Drupal\Core\Controller\ControllerBase;

use Drupal\chado_display\Database\Chado;

class AutoCompleteController extends ControllerBase {

    public function getMatch(Request $request, $table = NULL, $ontology = NULL) {
        $matches = array();
        $input = $request->query->get('q');
        if (!$input) {
          return new JsonResponse($matches);
        }

        $string = Xss::filter($input);
        $chado = new Chado();
        if ($chado->tableExists($table) && $string) {
          $pkey = $chado->getPkey($table);
          $ukeys = $chado->getUKeys($table);
          $typedColumns = $chado->getTypedColumns($table);
          foreach ($ukeys AS $k=> $val) {
            if ($typedColumns[$val] != 'text' && $typedColumns[$val] != 'character varying') {
              unset($ukeys[$k]); // Remove unique keys that are not text or varchar type
            }
          }
          $compare = '';
          $idx = 0;
          $total = count($ukeys);
          if ($total > 0) {
            foreach ($ukeys AS $key) {
              $compare .= $key;
              if ($idx < $total -1) {
                $compare .= " || ' ' || ";
              }
              $idx ++;
            }
            $select = $pkey . ', ' . $compare . ' AS unique';
            $compare = 'lower(' . $compare . ')';
            $cv = $ontology && $table == 'cvterm' ? "AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = '$ontology')" : '';
            $sql = "SELECT $select FROM chado.$table WHERE $compare LIKE lower('$string%') $cv ORDER BY $pkey LIMIT 50";
            //dpm($sql);
            $return = $chado->getQueryObjects($sql, array());
            foreach ($return as $row) {
                $matches[] = [
                    'value' => $row->$pkey,
                    'label' => '(' . $row->$pkey . ') ' . $row->unique,
                ];
            }
          }
        }
        return new JsonResponse($matches);
    }
}