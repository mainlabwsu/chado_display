<?php

namespace Drupal\chado_display\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Drupal\Core\Controller\ControllerBase;

class LookupController extends ControllerBase {
  
    public function reroute($type, $name) {
        $type = $type == 'MTL' ? 'heritable_phenotypic_marker' : $type;
        $name = str_replace("'", "''", urldecode($name));
        $types = array(
            'feature' => 'feature',
            'contig' => 'feature',
            'EST' => 'feature',
            'mRNA' => 'feature',
            'gene' => 'feature',
            'genetic_marker' => 'feature',
            'QTL' => 'feature',
            'heritable_phenotypic_marker' => 'feature',
            'mRNA' => 'feature',
            'region' => 'feature',
            'stock' => 'stock',
            'contact' => 'contact',
            'featuremap' => 'featuremap',
            'analysis' => 'analysis',
            'project'=> 'project',
            'library' => 'library',
            'SNP_chip' => 'library'
        );
        
        $link = NULL;
        $base_table = $types[$type];
        $db = \Drupal::database();
        if ($db->schema()->tableExists('chado.' . $base_table) && $db->schema()->fieldExists('chado.' . $base_table, $base_table . '_id') && $db->schema()->fieldExists('chado.' . $base_table, 'name')) {
            $record_id = $base_table . '_id';
            $has_type = FALSE;
            $has_uniquename = FALSE;
            if ($db->schema()->fieldExists('chado.' . $base_table, 'type_id')) {
                $has_type = TRUE;
            }
            if ($db->schema()->fieldExists('chado.' . $base_table, 'uniquename')) {
                $has_uniquename = TRUE;
            }
            // find record matching the name
            $sql =
            "SELECT $record_id
               FROM chado.$base_table
               WHERE lower(name) = lower(:name)";
            $id = NULL;
            if ($base_table == 'feature' && $type != 'feature') {
                $sql .= ' AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = :type AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = \'sequence\')) LIMIT 1';
                $id = $db->query($sql, array(':name' => $name, ':type' => $type))->fetchField();
            }
            else {
                $sql .= ' ORDER BY (SELECT name FROM chado.cvterm WHERE cvterm_id = type_id)';
                $id = $db->query($sql, array(':name' => $name))->fetchField();
            }
            
            // try again using uniquename if the table has the uniquename field
            if (!$id && $has_uniquename) {
                $sql =
                  "SELECT $record_id
                      FROM chado.$base_table
                      WHERE lower(uniquename) = lower(:name)";
                if ($base_table == 'feature') {
                    $sql .= ' AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = :type AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = \'sequence\')) LIMIT 1';
                    $id = $db->query($sql, array(':name' => $name, ':type' => $type))->fetchField();
                }
                else {
                    $id = $db->query($sql, array(':name' => $name))->fetchField();
                }
            }
            
            // try one more time using the dbxref.accession if ID is still not found and the $base_table is feature
            /*
            if (!$id && $base_table == 'feature') {
                $sql =
                    "SELECT feature_id FROM chado.feature
                     WHERE feature_id = (
                        SELECT feature_id FROM chado.feature_dbxref WHERE dbxref_id = (
                        SELECT dbxref_id FROM chado.dbxref WHERE accession = :accession LIMIT 1
                     )
                     AND type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = :type AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'sequence'))
                   ) LIMIT 1";
                $id = $db->query($sql, array(':accession' => $name, ':type' => $type))->fetchField();
            }
            */
            
            if ($id) {
                return new RedirectResponse(\Drupal\Core\Url::fromRoute('chado_display.record', ['base_table' => $base_table, 'id' => $id])->toString()); 
            }
        }
       
        return ['#markup' => 'The requested page could not be found.'];
        
    }
    
    // Reroute the path 'bio_data/{id}' to chado_display page
    public function biodataReroute($tripal_entity) {
      $base_table = NULL;
      $record_id = NULL;
      
      $db = \Drupal::database();
      if ($db->schema()->tableExists('tripal_entity') && $db->schema()->fieldExists('tripal_entity', 'bundle')) {
        $sql = 'SELECT {bundle} FROM tripal_entity WHERE id = :id';
        $bundle_name = $db->query($sql, [':id' => $tripal_entity])->fetchField();
        // TODO: Can NOT implement this function until Tripal 4 is finalized
      }
      if ($base_table && $record_id) {
        return new RedirectResponse(\Drupal\Core\Url::fromRoute('chado_display.record', ['base_table' => $base_table, 'id' => $record_id])->toString());
      }
      else {
        return ['#markup' => 'The requested page could not be found.'];
      }
    }
}
