<?php
namespace Drupal\chado_display\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Drupal\Core\Controller\ControllerBase;

use Drupal\chado_display\Database\Chado;

class DownloadController extends ControllerBase {

    public function generateData(Request $request, $info) {
      
      $info = unserialize(urldecode($info));
      $filename = str_replace(' ', '_', $info->key) . '.csv';
      $headers = $info->headers;
      $columns = $info->columns;
      $sql = $info->query->sql;
      $args = $info->query->args;
      
      $chado = new Chado();
      $objs = $chado->getQueryObjects($sql, $args);
      
      $content = '';
      // Output headers
      $num_cols = count($headers);
      for ($i = 0; $i < $num_cols; $i ++) {
        $content .= '"' . $headers[$i] . '"';
        if ($i < $num_cols - 1) {
          $content .= ",";
        }
        else {
          $content .= "\n";
        }
      }
      
      // Output rows
      foreach ($objs AS $obj) {
        for ($j = 0; $j < $num_cols; $j ++) {
          $column = explode('|', $columns[$j]);
          if (count($column) > 1) {
            $value = $obj->{$column[0]} ? $obj->{$column[0]} : $obj->{$column[1]};
            $content .= '"' . $value . '"';
          }
          else {
            $content .= '"' . $obj->{$columns[$j]} . '"';
          }
          if ($j < $num_cols - 1) {
            $content .= ",";
          }
          else {
            $content .= "\n";
          }
        }
      }

      //return ['#markup' => 'test'];
      
      $response = new Response();
      $response->headers->set('Content-type', 'text/csv');
      $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');
      $response->setContent($content);
      return $response;
    }
    
    public function generateFasta(Request $request, $info) {
      
      $feature_id = unserialize(base64_decode($info));
      
      $chado = new Chado();
      $feature = $chado->getFirstObject(
          'feature F',
          ['F.name', 'uniquename', 'residues', 'seqlen', 'V.name AS type', 'genus || \' \' || species AS organism'],
          ['inner' =>
            [
              'organism O' => 'F.organism_id = O.organism_id',
              'cvterm V' => 'cvterm_id = F.type_id'
            ],
          ],
          ['feature_id' => $feature_id]
          );
      $filename = $feature->uniquename . '.fasta';
      $content = '>' . $feature->uniquename . ' ';
      if ($feature->name) {
        $content .= '|Name=' . $feature->name;
      }
      if ($feature->type) {
        $content .='|Type=' . $feature->type ;
      }
      if ($feature->organism) {
        $content .='|Organism=' . $feature->organism;
      }
      if ($feature->seqlen) {
        $content .= '|Length=' . $feature->seqlen .'bp';
      }
      $content .= "\n";
      $content .= wordwrap($feature->residues, 80, "\n", TRUE);
      
      $response = new Response();
      $response->headers->set('Content-type', 'text');
      $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');
      $response->setContent($content);
      return $response;
    }
}