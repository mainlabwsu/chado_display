<?php

namespace Drupal\chado_display\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Drupal\Core\Controller\ControllerBase;

use Drupal\chado_display\Database\Chado;
use Drupal\chado_display\Render\ChadoPage;
use Drupal\chado_display\Render\Cache;

use Symfony\Component\DependencyInjection\ContainerInterface;

class AccessController extends ControllerBase {

  protected $chado_page;

  public function __construct (ChadoPage $chado_page = NULL) {
    $this->chado_page = $chado_page;
  }

  public static function create(ContainerInterface $container) {
    return new static($container->get('chado_display.chado_page'));
  }

  public function checkPermission ($base_table, $id) {
    $current_user = \Drupal::currentUser();
    $roles = $current_user->getRoles();
    $permissions = \Drupal::state()->get('chado_display_permission');
    $access = FALSE;
    // Check data access
    foreach ($roles AS $role) {
      $perm = isset($permissions[$role][$base_table]) ? $permissions[$role][$base_table] : 1;
      if ($perm) {
        $access = TRUE;
        break;
      }
    }
    // Check private access
    $private =
    \Drupal::database()
    ->select('chado_display_private_data', 'C')
    ->fields('C', ['record_id'])
    ->condition('base_table', $base_table)
    ->condition('record_id', $id)
    ->execute()
    ->fetchField();
    $private_access = FALSE;
    if ($private) {
      $private_permissions = \Drupal::state()->get('chado_display_private_permission');
      foreach ($roles AS $role) {
	$admin_default = $role == 'administrator' ? 1 : 0;
        $private_perm = isset($private_permissions[$role][$base_table]) ? $private_permissions[$role][$base_table] : $admin_default;
        if ($private_perm) {
          $private_access = TRUE;
          break;
        }
      }
      if (!$private_access) {
        return FALSE;
      }
    }

    return $access;
  }
  
  public function getSupportedData() {
    $data = \Drupal::state()->get('chado_display_supported_data', []);
    $extended = \Drupal::state()->get('chado_display_extended_data', '');
    if ($extended) {
    $tables = explode(',', $extended);
    foreach ($tables AS $table) {
      $table = trim($table);
        if ($table) {
          $data[$table] = $table;
        }
      }
    }
    return $data;
  }
  
  public function isValidRecord ($base_table, $id) {
    $chado = new Chado();
    $table_exists = $chado->tableExists($base_table);
    $has_record = $table_exists && is_numeric($id) ? $chado->getFieldById($base_table, '1', $id) : 0;
    
    if ($has_record) {
      if (!$chado->columnExists($base_table, 'type_id')) {
        return TRUE;
      }
      else {
        $type_id = $chado->getFieldById($base_table, 'type_id', $id);
        $ignore_types = \Drupal::state()->get('chado_display_ignored_data_type', '');
        $cvterm = $chado->getFieldById('cvterm', 'name', $type_id);
  
        $table_types = array_map('trim', explode(',', $ignore_types));
        if (in_array($base_table . ':' . $cvterm, $table_types)) {
          return FALSE;
        }
        else {
          return TRUE;
        }
      }      
    } else {
      return FALSE;
    }
  }

  public function accessPage($base_table, $id) {
    $record_exists = $this->isValidRecord($base_table, $id);
    $data = $this->getSupportedData();
    if ($record_exists && ($data == NULL || (isset($data[$base_table]) && $data[$base_table]))) {
      if ($this->checkPermission($base_table, $id)) {
        return $this->chado_page->getPageById($base_table, $id);
      }
      else {
        return [
          '#markup' => 'You don\'t have permission to access the page.'
        ];
      }
    }
    else {
      return [
        '#markup' => 'The requested page could not be found.'
      ];
    }
  }
  
  public function accessJson($base_table, $id) {
    $record_exists = $this->isValidRecord($base_table, $id);
    $data = $this->getSupportedData();
    if ($record_exists && ($data == NULL || (isset($data[$base_table]) && $data[$base_table]))) {
      if ($this->checkPermission($base_table, $id)) {
        return $this->chado_page->getPageById($base_table, $id, 'json');
      }
      else {
        return [
          '#markup' => 'You don\'t have permission to access the page.'
        ];
      }
    }
    else {
      return [
        '#markup' => 'The requested page could not be found.'
      ];
    }
  }

  public function pageTitle($base_table, $id) {
    $record_exists = $this->isValidRecord($base_table, $id);
    $data = $this->getSupportedData();
    if ($record_exists && ($data == NULL || (isset($data[$base_table]) && $data[$base_table]))) {
      if ($this->checkPermission($base_table, $id)) {
        return $this->chado_page->getPageTitleById($base_table, $id);
      }
      else {
        return 'Access denied';
      }
    }
    else {
      return 'Page not found';
    }
  }
  
  public function setAccess($action, $base_table, $id) {
      if ($action == 'public') {
          $sql = 'DELETE FROM chado_display_private_data WHERE base_table = :base_table AND record_id = :record_id';
          \Drupal::database()->query($sql, [':base_table' => $base_table, ':record_id' => $id]);
      }
      else if ($action == 'private') {
          \Drupal::database()
          ->insert('chado_display_private_data')
          ->fields([
              'base_table' => $base_table, 
              'record_id' => $id
          ])
          ->execute();
      }
      Cache::delete($base_table, $id);
      return new RedirectResponse(\Drupal\Core\Url::fromRoute('chado_display.record', ['base_table' => $base_table, 'id' => $id])->toString());
  }
}
