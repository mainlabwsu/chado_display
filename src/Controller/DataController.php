<?php

namespace Drupal\chado_display\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Drupal\Core\Controller\ControllerBase;

use Drupal\chado_display\Database\Chado;

use Drupal\chado_display\Render\Unbased\Polymorphism;
use Drupal\chado_display\Render\Unbased\Allele;
use Drupal\chado_display\Render\Unbased\TraitPage;
use Drupal\chado_display\Render\Unbased\TraitDescriptor;
use Drupal\chado_display\Render\Unbased\GeneClass;

class DataController extends ControllerBase {
  
  /**
   * Controller for Unbased pages
   */
    // Polymorphism pages
    public function polymorphismPage($feature_id) {
        $renderer = new Polymorphism($feature_id);
        \Drupal::moduleHandler()->invokeAll('chado_display_polymorphism_alter', array($renderer));
        return $renderer->getRenderArray();   
    }

    public function polymorphismTitle($feature_id) {
        $renderer = new Polymorphism($feature_id);
        return $renderer->getTitle(); 
    }
  
    // Allele pages
    public function allelePage($organism_id, $feature_id, $allele) {
        $renderer = new Allele($organism_id, $feature_id, $allele);
        \Drupal::moduleHandler()->invokeAll('chado_display_allele_alter', array($renderer));
        return $renderer->getRenderArray();
    }
  
    public function alleleTitle($organism_id, $feature_id, $allele) {
        $renderer = new Allele($organism_id, $feature_id, $allele);
        return $renderer->getTitle();
    }
  
    // Trait pages
    public function traitPage($cvterm_id) {
        $renderer = new TraitPage($cvterm_id);
        \Drupal::moduleHandler()->invokeAll('chado_display_trait_alter', array($renderer));
        return $renderer->getRenderArray();
    }
  
    public function traitTitle($cvterm_id) {
        $renderer = new TraitPage($cvterm_id);
        return $renderer->getTitle();
    }
    
    // Trait Descriptor pages
    public function traitDescriptor($cvterm_id) {
        $renderer = new TraitDescriptor($cvterm_id);
        \Drupal::moduleHandler()->invokeAll('chado_display_trait_descriptor_alter', array($renderer));
        return $renderer->getRenderArray();
    }
    
    public function traitDescriptorTitle($cvterm_id) {
        $renderer = new TraitDescriptor($cvterm_id);
        return $renderer->getTitle();
    }
    
    /**
     * Controller for Lists
     */
    // Gene Class Listing
    public function geneClassListing($letter) {
      $gc = new GeneClass();
      return $gc->getList($letter);
    }
    public function geneClassListingTitle($letter) {
      $gc = new GeneClass();
      return $gc->getListTitle();
    }
    
    // Gene Class
    public function geneClass($name) {
      $gc = new GeneClass();
      return $gc->getGeneClass($name);
    }
    public function geneClassTitle($name) {
      $gc = new GeneClass();
      return $gc->getTitle($name);
    }
}