# Mainlab Chado Display

Chado Display (formerly known as Mainlab Tripal Data Display) is a module for visualization of biological data stored in a Chado database. Chado Display is able to support all tables in the Chado schema (see details in the Administration section), although only some data types are enabled and preconfigured out of box. The preconfigured data types include analysis, contact, feature (e.g. gene, mRNA, chromosome, genetic_marker, QTL, MTL, haplotype_block, etc), image, library, map (featuremap), ND geolocation, orgainsm, project, pub, stock, and stockcollection. Chado Display can also create pages for data gathered from multiple Chado tables, such as allele, gene_class, polymorphism, trait, and trait_descriptors.

Chado Display is very flexible. Preconfigured data types can be toggled on or off easily. Access control is also built-in which allows any page to be set as private and access to said page can be restricted by user roles. Chado Display has several different page styles to choose from. For example, the 'fade-in style' brings in the related content when a sidebar topic is clicked, while the 'scroll style' shows all content at once with a floating sidebar that slides up or down the page when a related topic is clicked. Content from the chado property tables can be configured to show in the sidebar or as an Overview field. Many other settings are also available to change the look and feel (See Administration section).

The module also supports overriding built-in display so site-specific customization can be done by writing a custom module (see the Overriding default displays section).

Finally, Chado Display also provides simple add/edit/delete capabilities for changing Chado data. Site administrators should give permission of such capabilities for certain roles.

To take full advantage of these preconfigured displays, data collection templates and a loader (Mainlab Chado Loader, see https://gitlab.com/mainlabwsu/mainlab_chado_loader) are also available as a separate module. The Mainlab Chado Search (https://gitlab.com/mainlabwsu/chado_search) and/or Tripal MegaSearch (https://gitlab.com/mainlabwsu/tripal_megasearch) modules also provide search interfaces that hyperlink the search results to the Chado Display contents.

The Mainlab Chado Display is created by Main Bioinformatics Lab (Main Lab) at Washington State University. Information about the Main Lab can be found at: https://www.bioinfo.wsu.edu

## Screenshot

![](assets/screenshot.png)

## Requirement

- Drupal 10.x or 11.x
- PostgreSQL (Drupal in 'public' schema whereas Chado in 'chado' schema)

## Version

4.3.0

## Download

The Chado Display module can be downloaded from GitLab:

https://gitlab.com/mainlabwsu/chado_display

## Installation

After downloading the module, extract it into your site's module directory
(e.g. modules) then follow the instructions below:

1. Enable the module either by
   using the Drupal administrative interface:
   Go to: 'Extends', check Chado Display (under the Mainlab category) and save
   or using the 'drush' command:

   ```
   drush pm-enable chado_display
   ```
2. Chado data can now be visualized by visiting

   * https://your.site/display/record/<chado_table>/<record_id>

   For example, to visualize the organism with organism_id = 1, go to:

   * https://your.site/display/record/organism/1

   For roles with 'Create/Edit/Delete Chado Display' permission(s), go to:

   * https://your.site/display/add/<chado_table>
   * https://your.site/display/edit/<chado_table>/<record_id>
   * https://your.site/display/delete/<chado_table>/<record_id>
3. (Optional) for those who don't have Chado installed, you can download it (e.g. [chado-1.31-tripal.sql.gz](https://github.com/galaxy-genome-annotation/chado-schema-builder/releases/download/1.31-jenkins61/chado-1.31-tripal.sql.gz)) from the [Chado schema dump](https://github.com/galaxy-genome-annotation/chado-schema-builder/releases) repository and restore the file to the chado schema of your Drupal database by:

   ```
   gunzip chado-1.31-tripal.sql.gz
   psql -U <db_user> -h <db_host> -d <drupal_db_name> < chado-1.31-tripal.sql
   ```

## Administration

- **Configuration page**: Administrators can go to: Configuration > Mainlab > Chado Display to change the Chado Display settings
  i.e. http://your.site/admin/config/mainlab/chado_display
- **Content types**: click on 'Data Source' for preconfigured data types you want to enable/disable. You can also enable custom data type(s) for any underlying Chado table by providing the table names (separated by a comma) and create your own module to further customize these pages (See Overriding default displays section).

<img src="assets/data_source.png" width="80%"/>
-

* **Access control**: two levels of access control are available (i.e. public and private access). By default, all public Chado Display contents are accessible to all users whereas private Chado Display contents are not accessible to any user. Roles with 'Administer Chado Display Content' permission can easily toggle the accessibility level between private and public on a given page.

<img src="assets/access_control.png" width="80%"/>

* **Page layout**:The following table lists different layout style that can be used for a Chado Display page.

  | Layout                  | TOC Sidebar  | Details                                                                                              |
  | ----------------------- | ------------ | ---------------------------------------------------------------------------------------------------- |
  | Scroll Style            | On the left  | Clicking on the sidebar item scrolls to the position of corresponding content. (See animation below) |
  | Scroll (Right sidebar)  | On the right | Clicking on the sidebar item scrolls to the position of corresponding content.                       |
  | Fade In Style           | On the left  | Clicking on the sidebar item brings in corresponding content. (See animation below)                  |
  | Fade In (Right sidebar) | On the right | Clicking on the sidebar item brings in corresponding content.                                        |
  | Expandable Boxes        | No sidebar   | Each section of content is put in an expandable box that can be opened or closed.                    |
  | One page                | No sidebar   | Everything on one page.                                                                              |


  * *Scroll style*

  <img src="assets/scroll_style.gif" width="550px"/>

  * *Fade-in style*

  <img src="assets/fade_in_style.gif" width="550px"/>

  * *Table colors* and*number of rows*: for most data output in the table format, there are preset colors to choose from. A default number of rows can be set before table is being paginated. (hints: you can still change the color using your own CSS/theme)

  <img src="assets/table_colors.png" width="80%"/>

  * *Data Control*: for the preconfigured data types, you can control what data to present and how the data are ordered.

  <img src="assets/data_control.png" width="80%"/>

  The page is organized in the layout shown below. For all preconfigured data types, you can toggle what information you want to show in the Sidebar and in you preferred order. You can also hide rows in the Overview table or change the display order for the rows. If a Chado table has a property table, you can show the properties in either the Sidebar (as a section of content), or the Overview table (as a row), or both. Lastly, roles with 'Create Chado Display Content' or 'Edit Chado Display Content' permission can upload an Overview image for any given page. The image will be shown next to the Overview table and will be stored in the Chado eimage table.

  <img src="assets/page_layout.png" width="60%"/>

  For the Chado feature pages with sequences, you can set a limit for the residues display and/or the text wrap position. If a JBrowse instance is available, it can be displayed as part of the page as well. The JBrowse image will only be shown when there is a Chado featureloc record. The featureloc record should link to a chromosome or superconig (i.e. srcfeature_id) and said chromosome/supercontig is associated with a genome (stored as a Chado analysis record) in the Chado analysisfeature table. This genome analysis record should have a 'Analysis Type' (in 'analysis_property' cv) property record with a value of 'whole_genome'. If all above conditions are met, you can create a cvterm named 'JBrowse URL' using the 'analysis_property' cv. Then, create a Chado analysisprop record for the genome using said cvterm with a value specifying the URL of the JBrowse instance.

  <img src="assets/feature_control.png" width="80%"/>

## Overriding default displays (For Developers)

Note: go to Configuration > Mainlab > Chado Display and turn off 'Cache Chado Display Page' under the Page Layout setting before you start or your changes will not show until the Chado Display cache (not Drupal cache) is cleared.

You can override a default display by implementing a Drupal hook function in your module:

* Identify the page that you want to override and create a hook function 'hook_chado_display_[data_type]_alter' in your module:
  For example, if you wish to override a 'featuremap' page,  you need to create a 'hook_chado_display_featuremap_alter(\$renderer)' function. If you defined a custom data type such as 'cvterm' in the Chado Display settings, your hook function will be 'hook_chado_display_cvterm_alter(\$renderer)'. A \$renderer object representing the current display is passed into this function for you to customize.

  ```
  // Define the Hook function. Remember the $renderer argument that we'll be manipulating
  function my_module_chado_display_featuremap_alter ($renderer) {

  }
  ```

  The following table lists the hook functions you need to implement to override the default display

  | Chado Table     | Display            | Hook Function                            |
  | --------------- | ------------------ | ---------------------------------------- |
  | analysis        | analysis           | hook_chado_display_analysis_alter        |
  | contact         | contact            | hook_chado_display_contact_alter         |
  | eimage          | eimage             | hook_chado_display_eimage_alter          |
  | feature         | feature            | hook_chado_display_feature_alter         |
  | feature         | genetic_marker     | hook_chado_display_genetic_marker_alter  |
  | feature         | mRNA               | hook_chado_display_mRNA_alter            |
  | feature         | gene               | hook_chado_display_gene_alter            |
  | feature         | QTL                | hook_chado_display_qtl_alter             |
  | feature         | MTL                | hook_chado_display_mtl_alter             |
  | feature         | haplotype_block    | hook_chado_display_haplotype_block_alter |
  | feature         | Bin                | hook_chado_display_bin_alter             |
  | feature         | GWAS              | hook_chado_display_gwas_alter             |
  | featuremap      | featuremap         | hook_chado_display_featuremap_alter      |
  | library         | library            | hook_chado_display_library_alter         |
  | library         | SNP_chip           | hook_chado_display_snp_chip_alter        |
  | nd_geolocation  | nd_geolocation     | hook_chado_display_nd_geolocation_alter  |
  | organism        | organism           | hook_chado_display_organism_alter        |
  | project         | project            | hook_chado_display_project_alter         |
  | pub             | pub                | hook_chado_display_pub_alter             |
  | stock           | stock              | hook_chado_display_stock_alter           |
  | stockcollection | stockcollection    | hook_chado_display_stockcollection_alter |
  | $chado_table    | custom chado table | hook_chado_display_{$chado_table}_alter  |
* If you wish to add a new Sidebar section to the renderer, use the \$renderer->addContent(\$key, \$toc_header, \$render_array, \$weight) function:

  - \$key:           A unique identifier. (Make sure it is unique or it may overwrite other contents)
  - \$toc_header:    A header to display in the Table of Content (TOC) block. When a header is clicked, the page will bring the focus to its corresponding content (i.e. the $render_array)
  - \$render_array:  The content to show in Drupal's render array format.
  - \$weight:        A weight to determine the display priority. A value smaller than -100 will usually show up at the top of the page.

  ```
  function my_module_chado_display_featuremap_alter ($renderer) {

      // Define your own render array content
      $my_rendering_array = ['#markup' => '<div>Hello World</div>'];

      // Add content to the page
      $renderer->addContent('hello', 'Hello', $my_rendering_array, -200);
  }
  ```
* To manipulate the existing content, you can use the \$renderer->getContent(\$key) to retrieve the existing Drupal render array. You can then manipulate the render array as desired, and use the \$renderer->replaceContent(\$key, \$toc_header, \$render_array) to replace the existing content.

  - \$key:           The identifier of content to be replaced.
  - \$toc_header:    A new TOC header. If you don't want to change the existing value, just pass in NULL.
  - \$render_array:  A new Drupal rendering array.

  ```
  function my_module_chado_display_featuremap_alter ($renderer) {

      // Get the current Overview content
      $overview = $renderer->getContent('overview');

      // Added some markup i. e. Hello World before the Overview section
      $new_overview = [
        ['#markup' => '<div>Hello World</div>'],
        $overview
      ];

      // Replace the Overview with new content
      $renderer->replaceContent('overview', NULL, $new_overview);
  }
  ```
* The following is a list of functions provided by the $renderer object that can be used for manipulating the display:

  | *Function*       | *Arguments (in passing order with default value)*                                           | *Description*                                                                                                                                                                                 |
  | ------------------ | --------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
  | addContent         | \$key, \$toc_header, \$render_array, \$weight = 0, \$toc_category = NULL, \$alt_header = NULL | Add new content in the Sidebar section                                                                                                                                                          |
  | addLib             | \$lib                                                                                         | Add defined Drupal library to be attached to a page                                                                                                                                            |
  | addOverviewField   | \$header, \$content                                                                           | Add a row/field to the Overview table                                                                                                                                                           |
  | replaceOverviewField   | \$header, \$content                                                                           | Replace a row/field to the Overview table that matches $header                                                                                                                                    |
  | addTocLink         | \$key, \$toc_header, \$url, \$weight = 0, \$toc_category = NULL, \$target_blank = TRUE        | Add link directly to the TOC block without adding any content                                                                                                                                   |
  | changeHeader       | \$key, \$header                                                                               | Change the Sidebar TOC display header for a given key                                                                                                                                           |
  | getAllContent      |                                                                                               | Return all content in Drupal's render array format                                                                                                                                              |
  | getAltHeader       |                                                                                               | Return all alternative headers                                                                                                                                                                  |
  | getContent         | $key                                                                                          | Return a specific content using the key                                                                                                                                                         |
  | getLibs            |                                                                                               | Return all Drupal libraries currently attached                                                                                                                                                  |
  | getTitle           |                                                                                               | Return the page title                                                                                                                                                                           |
  | getTOC             |                                                                                               | Return all TOC items listed in the Sidebar, ordered by weight                                                                                                                                   |
  | getTOCCatogory     |                                                                                               | Return all categories assigned to the TOC items                                                                                                                                                 |
  | hideOverviewFields | \$headers = []                                                                                | Hide Overview rows by supplying an array of headers (case insensitive, space ignored)                                                                                                           |
  | link               | \$base_table, \$id, \$display, \$new_tab = FALSE, \$section = NULL                            | Link to another Chado Display page.\$display can be any text to be hyperlinked                                                                                                                  |
  | orderBy            | \$keys = []                                                                                   | Order Sidebar items according to the keys passed in                                                                                                                                             |
  | orderByHeaders     | \$headers = [], \$pattern_match = FALSE                                                       | Order Sidebar items according to the headers passed in                                                                                                                                          |
  | orderOverviewBy    | \$headers = [], \$pattern_match = FALSE                                                       | Order Overview Table according to the Headers passed in                                                                                                                                         |
  | removeContent      | \$key                                                                                         | Remove content                                                                                                                                                                                  |
  | replaceContent     | \$key, \$toc_header = NULL, \$render_array, \$weight = NULL, \$toc_category = NULL            | Replace content with a key                                                                                                                                                                      |
  | residues           | \$uniquename, \$residues, \$name = NULL, \$type = NULL, \$organism = NULL, \$seqlen = NULL    | Create a sequence renderer                                                                                                                                                                      |
  | setAltHeader       | \$key, \$header                                                                               | Set an alternative header (located right above the content, not in the TOC sidebar) instead of using the TOC header                                                                             |
  | setCategory        | \$key, \$category                                                                             | Set a category for the headers. (If you set a category for any header, the other headers without an assigned category will not show so make sure you assign a category for all of the headers.) |
  | setTitle           | \$title                                                                                       | Set the Page Title                                                                                                                                                                              |
  | setWeight          | \$key, \$weight                                                                               | Set a different wight for the content. The weight determine the display order for the TOC items                                                                                                 |

## Problems/Suggestions

Mainlab Chado Display module is still under active development. For questions or bug report, please contact the developers at the Main Bioinformatics Lab by emailing to:
dev@bioinfo.wsu.edu

## Citation

If you use Chado Display, please cite:
Jung S, Lee T, Cheng CH, Ficklin S, Yu J, Humann J, Main D. 2017. Extension modules for storage, visualization and querying of genomic, genetic and breeding data in Tripal databases. Database. 10.1093/database/bax092
